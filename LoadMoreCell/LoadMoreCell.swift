//
//  LoadMoreCell.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class LoadMoreCell: UITableViewCell {

    
    @IBOutlet weak var loader: THIndicatorView!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var isShowLoader:Bool = false {
        didSet{
            if isShowLoader {
               loader.startAnimation()
            }else{
                loader.stopAnimation()
            }
            
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
 
    
    
    
   
}
