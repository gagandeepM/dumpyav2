//
//  LoadMoreCollectionViewCell.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class LoadMoreCollectionViewCell: UICollectionViewCell {
   @IBOutlet weak var loader: THIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        loader.tintColor = DMColor.ACRed
    }
    var isShowLoader:Bool = false {
        didSet{ 
            if isShowLoader {
                loader.startAnimation()
            }else{
                loader.stopAnimation()
            }
            
        }
    }
    
}
