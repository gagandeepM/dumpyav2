//
//  AppDelegate.swift
//  Dumpya
//
//  Created by Chander on 13/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//


import UIKit
import CoreData
import IQKeyboardManagerSwift
import AudioToolbox
import GoogleSignIn
import FBSDKCoreKit
import CoreLocation
import Firebase
import FirebaseCore
import GiphyCoreSDK
import FirebaseDynamicLinks
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var leftSideController:THSlideMenuController?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GiphyCore.configure(apiKey: "G744KsCuIEm5wtknkK1hyMbx4cJkemK6")
        
        JKGoogleManager.shared.GSSetup()
        
        FBSDKApplicationDelegate.sharedInstance().application(application,didFinishLaunchingWithOptions:launchOptions)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.setNavigationBar()
        fireBaseConfig()
        if Platform.isSimulator == true{
            UserDefaults.DMDefault(setObject:"368fee7003a3ea34c48778d8fc052ff87c004a813d5ff0adb277534167a28acf", forKey: kDeviceToken)
            
        }else{
            
            self.registerApns(application: application)
        }
        _ = self.setupAWSS3(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    //MARK:- setupAWSS3
    private func setupAWSS3(_ application:UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)->Bool{
        AWSS3Manager.shared.initialize()
        return AWSS3Manager.shared.interceptApplication(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL?,
                                                                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        
        return googleDidHandle || facebookDidHandle || (dynamicLink == nil ? false : true)
        
    }
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL
        {
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                // ...
                if let dynamicLink = dynamicLink, let _ = dynamicLink.url {
                    self.handleDynamicLink(dynamicLink)
                } else {
                    // Check for errors
                    
                }
                
            }
            
            
            if handled{
                return true
            }else{
                //                // Maybe do other things with our incoimng URL?
                //                if let tabbarController =  self.window?.rootViewController  as? CustomTabBarController ,
                //                    let navcontroller = tabbarController.selectedViewController as? UINavigationController {
                //                    let onwerId = kUserDefaults.value(forKey: kUserId) as? String
                //                    var isSameUser = false
                //                    if linkUserID == onwerId {
                //                        isSameUser = true
                //                    }
                //                    DispatchQueue.main.async {
                //                        if let postdetailController = navcontroller.visibleViewController as? PostAdVC {
                //
                //                            postdetailController.productId = linkProductID
                //                            postdetailController.isFromDashboard = true
                //                            postdetailController.isUserSame = isSameUser
                //                            if tabbarController.selectedIndex == 2
                //                            {
                //                                postdetailController.APIProductDetails()
                //
                //                            }else
                //                            {
                //                                postdetailController.customizeControl()
                //                            }
                //
                //                        }else{
                //                            if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PostAdViewController") as? PostAdVC {
                //
                //                                viewController.productId = linkProductID
                //                                viewController.isFromDashboard = true
                //                                viewController.isUserSame = isSameUser
                //                                navcontroller.pushViewController(viewController, animated: true)
                //                            }
                //                        }
                //                    }
                //
                //                }
                //                return false
            }
        }
        return false
    }
    
    
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        
        if dynamicLink.matchType == .weak{
        }else {
            
            guard let dynamickLink = dynamicLink.url else{return}
            let dumpId = self.getQueryStringParameter(url: "\(dynamickLink)", param: "dumpId") ?? ""
            
            if isProfileUpdate , isLogin , isAccountVerified{
                if let sideMenuController  = rootController as? THSlideMenuController , let navController = sideMenuController.mainViewController as? UINavigationController ,  let visibleViewController = navController.visibleViewController{
                    if let controller = visibleViewController as? DumpDetailVC{
                        controller.notificationDumpId = dumpId
                        controller.refreshDumpDetail()
                        
                    }else{
                        
                        let controller = dumpDetailStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDumpDetailVC) as! DumpDetailVC
                        controller.notificationDumpId = dumpId
                        navController.pushViewController(controller, animated: true)
                    }
                }
            }else{
                self.setUpLogin()
            }
            
            
            
            
        }
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        print(url)
        guard let url1 = URLComponents(string: url) else { return nil }
        return url1.queryItems?.first(where: { $0.name == param })?.value
        
    }
    //
    //    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink){
    //        guard let url = dynamicLink.url else {
    //            print("That's weird. My dynamic link object has no url")
    //            return
    //        }
    //        print("Your incoming link parameter is \(url.absoluteString)")
    //    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        application.applicationIconBadgeNumber = 0
        
        //SocketIOManager.sharedInstance.establishConnection()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        self.saveContext()
    }
    
    
    //MARK:setNavigationBar-
    func setNavigationBar()
    {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
        if let font : UIFont = Roboto.Medium.font(size: 20){
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor : UIColor.white]
        }
        
        
        
        UINavigationBar.appearance().barTintColor = DMColor.deepRed
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "icon_Back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "icon_Back")
        
        
        //
        //        UINavigationBar.appearance().setGradeinetBackground(colorOne: DMColor.deepRed , colorTwo: DMColor.lightRedColor)
        //  UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -220), for:.default)
        
        
        
    }
    class var sharedDelegate:AppDelegate {
        return (UIApplication.shared.delegate as? AppDelegate)!
    }
    
    func logoutUser()
    {
        DispatchQueue.main.async {
            UserDefaults.DMDefault(removeObjectForKey: kUserDataKey)
            UserDefaults.DMDefault(removeObjectForKey: kAuthTokenKey)
            UserDefaults.DMDefault(removeObjectForKey: "currentLocation")
            self.fireBasesubscribeToTopic(subscribeToTopic: false)
            self.setUpLogin()
        }
    }
    func setUpLogin()
    {
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        let loginNavigationController : UINavigationController = self.getController(name: StoryBoardIdentity.kLoginOptionNavigation) as! UINavigationController
        self.window?.rootViewController = loginNavigationController
        
        
    }
    
    
    
    func setUpSideMenu(isNotification:Bool = false)
    {
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        let leftController = leftMenuStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDMLeftMenuNavigation) as! UINavigationController
        let identifier  = isNotification ? StoryBoardIdentity.kNotificationNavigation : StoryBoardIdentity.kDumpFeedNavigation
        
        if identifier == StoryBoardIdentity.kNotificationNavigation{
            let mainController = notificationListStoryBoard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
            leftSideController = THSlideMenuController(mainViewController: mainController, leftMenuViewController: leftController,enableLeftTapGeture:true)
            self.window?.rootViewController = leftSideController
        }else{
            
            let mainController = dumpFeedStoryBoard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
            leftSideController = THSlideMenuController(mainViewController: mainController, leftMenuViewController: leftController,enableLeftTapGeture:true)
            self.window?.rootViewController = leftSideController
        }
        
    }
    func setUpProfileController(){
        let profileController : UINavigationController = self.getController(name: StoryBoardIdentity.kProfileNavigation)as! UINavigationController
        self.window?.rootViewController = profileController
    }
    func showMainController(){
        if isProfileUpdate , isLogin , isAccountVerified{
            DispatchQueue.main.async {
                self.setUpSideMenu()
                
            }
        }else if isLogin,isAccountVerified , !isProfileUpdate {
            DispatchQueue.main.async {
                self.setUpProfileController()
            }
        }else{
            DispatchQueue.main.async {
                UserDefaults.DMDefault(removeObjectForKey: kUserDataKey)
                UserDefaults.DMDefault(removeObjectForKey: kAuthTokenKey)
                self.setUpLogin()
            }
        }
    }
    
    func getController(name identifier : String)->UIViewController
    {
        
        let controller:UIViewController = mainStoryboard.instantiateViewController(withIdentifier: identifier)as UIViewController
        return controller
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Dumpya")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    //MARK: - Push Notification Setup -
    
    
    
    //MARK:-FireBasesubscribeToTopic-
    
    
    func fireBasesubscribeToTopic(subscribeToTopic subscribe:Bool = false){
        
        if  let subscribeTopic = subscribeTopic , !subscribeTopic.isEmpty {
            if subscribe {
                Messaging.messaging().subscribe(toTopic: "\(subscribeTopic)")
                
            }else{
                Messaging.messaging().unsubscribe(fromTopic: "\(subscribeTopic)")
            }
            
        }
    }
    
    
    
    //MARK:- Register APNS
    
    
    func registerApns(application:UIApplication)
    {
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        
    }
    //MARK:- FireBaseConfig- // Use Firebase library to configure APIs
    func fireBaseConfig() {
        
        
        FirebaseApp.configure()
    }
    
    
    //MARK: Notification Methods-
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = String(format: "%@", deviceToken as CVarArg).trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
        kUserDefaults.set(token, forKey: kDeviceToken)
        print("APNs token retrieved: \(token)")
        guard let refreshedToken = InstanceID.instanceID().token()else{return}
        print("FCM token retrieved: \(refreshedToken)")
        instanceToken = refreshedToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
}

@available(iOS 10, *)
extension AppDelegate:UNUserNotificationCenterDelegate{
    
    //Called when a notification is delivered to a foreground app.
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        NSLog("Dumpya User Info : %@",notification.request.content.userInfo)
        
        let userInfo = notification.request.content.userInfo
        // create a sound ID, in this case its the tweet sound.
        
        let systemSoundID: SystemSoundID = 1315//1003 // SMSReceived (see SystemSoundID below)
        // to play sound
        AudioServicesPlaySystemSound (systemSoundID)
        
        if UIApplication.shared.applicationState == .active {
            let showAction = self.didReceiveRemoteNotification(userAction: false, resRPUsernse: userInfo)
            if showAction == true {
                completionHandler([.alert, .badge, .sound])
            }else{
                //   completionHandler([.badge, .sound])
            }
        }
        
        
    }
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive resRPUsernse: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = resRPUsernse.notification.request.content.userInfo
        
        _ =  self.didReceiveRemoteNotification(userAction: true,resRPUsernse: userInfo)
        completionHandler()
    }
    
    
    //MARK: -didReceiveRemoteNotification-
    func didReceiveRemoteNotification(userAction:Bool = true,resRPUsernse userInfo: [AnyHashable : Any])->Bool    {
        print("userInfo =" , userInfo)
        guard let apns = userInfo["notificationInfo"] as? String else { return false }
        let objNotiVM = NotificationVieWModel(parse: apns)
        return manage(notiResponse: objNotiVM, userAction: userAction)
    }
    
    func manage(notiResponse objNotiVM:NotificationVieWModel,userAction:Bool)->Bool{
        
        
        
        guard let sideMenuController  = rootController as? THSlideMenuController , let navController = sideMenuController.mainViewController as? UINavigationController ,  let visibleViewController = navController.visibleViewController else { return false }
        
        switch objNotiVM.notiType {
        case .RedumpPush ,.CommentPush,.DumpsPush:
            
            guard let dumpId = objNotiVM.dumpId else { return false }
            if let controller = visibleViewController as? DumpDetailVC{
                controller.notificationDumpId = dumpId
                controller.refreshDumpDetail()
                return false
            }else{
                if userAction {
                    // leftSideController?.toggleLeft()
                    let controller = dumpDetailStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDumpDetailVC) as! DumpDetailVC
                    controller.notificationDumpId = dumpId
                    currentController?.closeLeft()
                    navController.pushViewController(controller, animated: true)
                    
                }
                return true
            }
        case .AcceptedFollowRequestPush,.follow:
            guard let userId = objNotiVM.senderId else { return false }
            if userAction {
                //                let controller =// profileStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kNotificationNavigation) as! UINavigationController
                // leftSideController?.toggleLeft()
                currentController?.closeLeft()
                let controller = UIStoryboard(name: "Profile", bundle: nil)
                let vc = controller.instantiateViewController(withIdentifier: "ProfileDetailVC") as? ProfileDetailVC
                if  let topViewController = vc {
                    topViewController.objUserDetailViewModel.set(currentUserId:userId )
                    navController.pushViewController(topViewController, animated: true)
                }
                
            }
            return true
            
            
        case .NewFollowersPush:
            
            if let controller = visibleViewController as? NotificationVC{
                controller.refreshNotificationList()
                return false
            }else{
                if userAction {
                    currentController?.closeLeft()
                    self.setUpSideMenu(isNotification: true)
                }
                return true
            }
            
        case .DirectMessagePush:
            
            guard let userId = objNotiVM.senderId else { return false }
            if let controller = visibleViewController as? MessageVC{
                let isSameWindow = objNotiVM.isSameChatRoom(chatUserVM: controller.objChatViewModel)
                if isSameWindow{
                    controller.objChatViewModel.set(friendId: userId, friendName: objNotiVM.userName ?? "")
                    return false
                }else{
                    if userAction {
                        currentController?.closeLeft()
                        controller.objChatViewModel.userOnlineObserver(isOnline: false)
                        controller.objChatViewModel.set(friendId: userId, friendName: objNotiVM.userName ?? "")
                        controller.refreshChatRoom(other: true)
                    }
                    return true
                }
                
            }else{
                if userAction {
                    currentController?.closeLeft()
                    let controller = chatStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kMessageVC) as! MessageVC
                    controller.objChatViewModel.set(friendId: userId, friendName: objNotiVM.userName ?? "")
                    navController.pushViewController(controller, animated: true)
                }
                return true
            }
        case .DirectMessageRequestPush:
            if let controller = visibleViewController as? MessageRequestVC{
                controller.refreshMessageRequest()
                return false
            }else{
                if userAction {
                    currentController?.closeLeft()
                    let controller = chatStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kMessageRequestVC) as! MessageRequestVC
                    navController.pushViewController(controller, animated: true)
                }
                return true
            }
        default:
            return false
        }
        
    }
}
extension AppDelegate : MessagingDelegate {
    @available(iOS 10.0, *)
    //    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
}


extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}

//MARK:- Dynamic Link Sharing Functionality
