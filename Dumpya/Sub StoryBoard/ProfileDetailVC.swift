//
//  ProfileDetailVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 31/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

protocol HashTagDelegateProfile : class {
    func hashTagMediaProfileCell(_ sender: FeedMediaCell,hashName:String)
    func hashTagTextProfileCell(_ sender:FeedTextCell,hashName:String)
}
import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
class ProfileDetailVC: UIViewController {
    
    @IBOutlet weak var edit_btn: UIButton!
    
    @IBOutlet var objNotificationViewModel: NotificationVieWModel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileNameLBL: UILabel!
    @IBOutlet weak var privateAccountImageView: UIImageView!
    @IBOutlet weak fileprivate var followerBtn: UIButton!
    @IBOutlet weak fileprivate var followingBtn: UIButton!
    @IBOutlet weak var NoDumpView: UIView!
    @IBOutlet weak fileprivate var noDumpFoundLBL: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var gradientUpperView: DMGradientCard!
    @IBOutlet weak var gradientLowerView: DMGradientCard!
    fileprivate var privateAcoountStatus:Int = 0
    var isFromSideMenu = false
    var isFromMutualText = false
    var itemCount:Int {return objDumpFeed.numberOfRows()}
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    var handler:(()->Void)?
    var  isfromFollowing = false
    var type:String = ""
    var followStatus:FollowState {
        return objUserDetailViewModel.followedState ?? .none
    }
    //connectionVC selection
    
    var index = 0
    
    var followerType:String = ""
    var followHeading:String = ""
    @IBOutlet weak var isPrivateView: UIView!
    @IBOutlet var profileNavigationLbl: UILabel!
    @IBOutlet fileprivate var objDumpFeed: DumpTimeLineViewModel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak fileprivate var bio: UILabel!
    @IBOutlet weak fileprivate var locationName: UILabel!
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet var objUserDetailViewModel: ProfileDetailViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var followersCountLBL: UILabel!
    @IBOutlet weak var followingCountLBL: UILabel!
    @IBOutlet weak var dumpCountLBL: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var followHeightConstraint: NSLayoutConstraint!
    fileprivate var loadmoreCell:LoadMoreCell!
    @IBOutlet weak var profileImgBTN: DMButton!
    @IBOutlet weak var mutualViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mutualText_LBL: UILabel!
    @IBOutlet weak var privateUserCheckHeightConstraint: NSLayoutConstraint!
    @IBOutlet var mutualTextLbl: UILabel!
    @IBOutlet var activityLogHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    
    var isFromNotification = false
    fileprivate var notificationObj = NotificationViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addSubview(refreshControl)
        
        refreshControl.addTarget(self, action:
            #selector(self.refreshDumpProfileDetail),
                                 for:.valueChanged)
        
        refreshControl.layoutIfNeeded()
        checkFoloowingStatus()
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
        setUI()
        
        //MK moved from viewWillAppear to here, not to reload data when coming back from dumpee detail
        pageNumber = 0
        totalRecords = 0
        objDumpFeed.serviceType = .none
        loadTimeLineData()
        self.loadprofileData()
    }
    
    func setUI()
    {
        self.privateUserCheckHeightConstraint.constant = 0.0
        
        if userModel?.userId == objUserDetailViewModel.isOwner.ownerId
        {
            self.edit_btn.isHidden = false
            self.profileNavigationLbl.text = "myprofile".localized
        }
        else{
            self.edit_btn.isHidden = true
            self.profileNavigationLbl.text = "profile".localized
        }
    }
    
    //MARK:- Mutual text action
    
    @IBAction func mutualTextRightArrow(_ sender: DMCardView) {
        
        let storyboard = UIStoryboard(name: "Following", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FollowingVC") as! FollowingVC
        vc.isFromMutualText = true
        vc.userId = self.objUserDetailViewModel.isOwner.ownerId
        self.navigationController?.pushViewController(vc, animated: true)
        //         performSegue(withIdentifier:SegueIdentity.kFollowingSegue, sender: sender)
        
    }
    
    @IBAction func mutualTextDetailBTN(_ sender: UIButton) {
    }
    
    @IBAction func editBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "EditUserProfile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func checkFoloowingStatus()
    {
        if isFromNotification
        {
            self.showAlertAction(title: "Notification", message: "followRequest".localized, cancelTitle: "reject".localized, otherTitle: "accept".localized) { (index) in
                if index == 0
                {
                    
                    self.notificationObj.acceptRejectNotificationFollowRequest(isAccepted: true, onSuccess: {
                        
                        
                    }, onFailure: {
                        
                    })
                }
                else{
                    
                    self.notificationObj.acceptRejectNotificationFollowRequest(isAccepted: false, onSuccess: {
                        self.navigationController?.popViewController(animated: true)
                    }, onFailure: {
                        
                    })
                    
                }
            }
        }
    }
    @objc fileprivate func refreshDumpProfileDetail() {
        
        if totalRecords > 0
        {
            objDumpFeed.removeObjects()
        }
        pageNumber = 0
        totalRecords = 0
        objDumpFeed.serviceType = .none
        loadprofileData()
        loadTimeLineData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        backBtn.nornamlImage = isFromSideMenu ? #imageLiteral(resourceName: "icon_Sidebar") : #imageLiteral(resourceName: "icon_Back")
        NotificationCenter.default.post(name: Notification.Name("isFromSearch"), object: nil)
        //MK refresh view if dump editted/saved
        
        if(isDumpySaved)
        {
            isDumpySaved = false
            pageNumber = 0
            totalRecords = 0
            objDumpFeed.serviceType = .none
            loadTimeLineData()
            self.loadprofileData()
        }
    }
    
    func refreshProfileDetail(){
        loadprofileData()
    }
    fileprivate func loadprofileData(){
        
        objUserDetailViewModel.getProfileDetail(isPull:self.refreshControl.isRefreshing,onSuccess: {
            
            DispatchQueue.main.async {
                
                if self.refreshControl.isRefreshing{
                    
                    
                    
                    self.refreshControl.endRefreshing()
                    
                }
                
            }
            
            
            
            switch self.followStatus {
                
            case .follower:
                
                self.privateAcoountStatus = 0
                
                self.followBtn.setTitle("follow".localized, for: .normal)
                
            case .following:
                
                self.followBtn.setTitle("unfollow".localized, for: .normal)
                
                self.privateAcoountStatus = 2
                
            case .unfollow:
                
                self.followBtn.setTitle("requested".localized, for: .normal)
                
                self.privateAcoountStatus = 1
                
            default:
                
                break
                
            }
            
            self.objUserDetailViewModel.set(profileInfo: self.userName, profileBtn: self.profileImgBTN, bioLabel: self.bio, addressLabel: self.locationName, dumpCountLabel: self.dumpCountLBL, followingLabel: self.followingCountLBL, followersLabel: self.followersCountLBL, coverImageView: self.coverImageView)
            
            
            
            
            
            if self.objUserDetailViewModel.isPrivateProfile{
                
                // self.tableView.isHidden = true
                
                
                if self.objUserDetailViewModel.isOwner.owner{
                    
                    
                    
                    self.privateUserCheckHeightConstraint.constant = 40.0
                    
                    //self.profileView.frame.size = CGSize(width: self.profileView.frame.width, height:487.0)
                    
                    self.profileView.layoutIfNeeded()
                    
                    self.loadTimeLineData()
                    
                    self.isPrivateView.isHidden = true
                    
                    self.followingBtn.isUserInteractionEnabled = true
                    
                    self.followerBtn.isUserInteractionEnabled = true
                    
                    self.mutualViewHeightConstraint.constant = 0.0
                    
                }else{
                    
                    
                    
                    self.privateUserCheckHeightConstraint.constant = 0.0
                    
                    if self.privateAcoountStatus == 2 {
                        
                        
                        
                        
                        
                        self.loadTimeLineData()
                        
                        self.isPrivateView.isHidden = true
                        
                        self.followingBtn.isUserInteractionEnabled = true
                        
                        self.followerBtn.isUserInteractionEnabled = true
                        
                        UIView.performWithoutAnimation {
                            
                            self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:0)
                            
                            
                            
                            
                            
                            if self.objUserDetailViewModel.userProfileDetail().isEmpty{
                                
                                self.mutualViewHeightConstraint.constant = 0.0
                                
                                
                                
                            }
                                
                            else{
                                let string = self.objUserDetailViewModel.userProfileDetail()
                                
                                self.mutualTextLbl.text = string
                                self.mutualViewHeightConstraint.constant = 40.0
                                
                            }
                            
                            
                            self.isPrivateView.layoutIfNeeded()
                            
                        }
                        
                    }else if self.privateAcoountStatus == 0 {
                        
                        
                        
                        
                        
                        if self.objUserDetailViewModel.userProfileDetail().isEmpty{
                            
                            self.mutualViewHeightConstraint.constant = 0.0
                            
                            
                            
                        }
                            
                        else{
                            let string = self.objUserDetailViewModel.userProfileDetail()
                            
                            self.mutualTextLbl.text = string
                            self.mutualViewHeightConstraint.constant = 40.0
                            
                        }
                        
                        
                        
                        if !self.objUserDetailViewModel.isPrivateProfile{
                            
                            self.loadTimeLineData()
                            
                            self.isPrivateView.isHidden = true
                            
                            self.activityLogHeightConstraint.constant = 40.0
                            
                            UIView.performWithoutAnimation {
                                
                                self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:0)
                                
                                self.isPrivateView.layoutIfNeeded()
                                
                            }
                            
                        }
                            
                        else{
                            
                            UIView.performWithoutAnimation {
                                
                                self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:150.0)
                                
                                self.activityLogHeightConstraint.constant = 0.0
                                
                                self.isPrivateView.layoutIfNeeded()
                                
                            }
                            
                            
                            
                            self.noDumpFoundLBL.text = "accountPrivate".localized
                            
                            self.privateAccountImageView.isHidden = false
                            
                            self.followingBtn.isUserInteractionEnabled = false
                            
                            self.followerBtn.isUserInteractionEnabled = false
                            
                            self.isPrivateView.isHidden = false
                            
                        }
                        
                    }else{
                        
                        if self.objUserDetailViewModel.userProfileDetail().isEmpty{
                            
                            self.mutualViewHeightConstraint.constant = 0.0
                            
                            
                            
                        }
                            
                        else{
                            
                            let string = self.objUserDetailViewModel.userProfileDetail()
                            
                            self.mutualTextLbl.text = string
                            self.mutualViewHeightConstraint.constant = 40.0
                            
                        }
                        
                        
                        
                        UIView.performWithoutAnimation {
                            
                            self.activityLogHeightConstraint.constant = 0.0
                            
                            self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:150.0)
                            
                            self.isPrivateView.layoutIfNeeded()
                            
                        }
                        
                        
                        
                        self.noDumpFoundLBL.text = "accountPrivate".localized
                        
                        self.privateAccountImageView.isHidden = false
                        
                        self.loadTimeLineData()
                        
                        self.followingBtn.isUserInteractionEnabled = false
                        
                        self.followerBtn.isUserInteractionEnabled = false
                        
                        self.isPrivateView.isHidden = false
                        
                    }
                    
                    
                    
                    
                    
                }
                
            }
                
            else{
                
                self.isPrivateView.isHidden = true
                
                
                
                if !self.objUserDetailViewModel.isOwner.owner{
                    
                    if self.privateAcoountStatus == 2 {
                        self.followingBtn.isUserInteractionEnabled = true
                        self.followerBtn.isUserInteractionEnabled = true
                        UIView.performWithoutAnimation {
                            if self.objUserDetailViewModel.userProfileDetail().isEmpty{
                                self.mutualViewHeightConstraint.constant = 0.0
                            }
                                
                            else{
                                let string = self.objUserDetailViewModel.userProfileDetail()
                                
                                self.mutualTextLbl.text = string
                                self.mutualViewHeightConstraint.constant = 40.0
                                
                            }
                            
                            
                            
                            self.activityLogHeightConstraint.constant = 40.0
                            
                            
                            
                            self.privateUserCheckHeightConstraint.constant = 0.0
                            
                            self.isPrivateView.layoutIfNeeded()
                            
                        }
                        
                    }else{
                        
                        self.followingBtn.isUserInteractionEnabled = true
                        
                        self.followerBtn.isUserInteractionEnabled = true
                        
                        
                        
                        
                        UIView.performWithoutAnimation {
                            
                            
                            
                            self.activityLogHeightConstraint.constant = 40.0
                            
                            if self.objUserDetailViewModel.userProfileDetail().isEmpty{
                                
                                self.mutualViewHeightConstraint.constant = 0.0
                                
                                
                                
                            }
                                
                            else{
                                let string = self.objUserDetailViewModel.userProfileDetail()
                                
                                self.mutualTextLbl.text = string
                                
                                
                                self.mutualViewHeightConstraint.constant = 40.0
                                
                            }
                            
                            self.privateUserCheckHeightConstraint.constant = 0.0
                            
                            self.isPrivateView.layoutIfNeeded()
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                }else{
                    
                    self.mutualViewHeightConstraint.constant = 0.0
                    
                    
                    
                }
                
                
                
                UIView.performWithoutAnimation {
                    
                    self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:0)
                    
                    self.isPrivateView.layoutIfNeeded()
                    
                }
                
                self.loadTimeLineData()
                
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            
            
            self.showAlert(message: message, completion: { (index) in
                
                AppDelegate.sharedDelegate.logoutUser()
                
            })
            
        })
        
        
        
        
        
        if !objUserDetailViewModel.isOwner.owner{
            
            followBtn.isHidden = false
            
            settingsBtn.isHidden = false
            
            
            
            UIView.performWithoutAnimation {
                
                self.followHeightConstraint.constant = 35.0
                
                self.profileView.frame.size = CGSize(width: self.profileView.frame.width, height:482.0)
                
                self.profileView.layoutIfNeeded()
                
            }
            
        }else{
            
            UIView.performWithoutAnimation {
                
                self.profileView.frame.size = CGSize(width: self.profileView.frame.width, height:447.0)
                
                
                self.followHeightConstraint.constant = 0.0
                
                self.profileView.layoutIfNeeded()
                
            }
            
            
            
            UIView.performWithoutAnimation {
                
                self.isPrivateView.isHidden = true
                
                self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:0)
                
                self.isPrivateView.layoutIfNeeded()
                
            }
            
        }
        
    }

    //MARK: load dumfeed data
    
    fileprivate func loadTimeLineData() {
        objDumpFeed.getTimeLineDumpForProfile(userId:objUserDetailViewModel.isOwner.ownerId,onSuccess: { (records) in
            
            DispatchQueue.main.async {
                totalRecords = records
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                if self.objDumpFeed.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                self.tableView.reloadData()
            }
        }, onFailure: {
            if self.objDumpFeed.serviceType == .pagging {
                if pageNumber>0{
                    pageNumber -= 1
                }
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
                
            }
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickMessage(_ sender: UIButton) {
        performSegue(withIdentifier:SegueIdentity.kChatSegue, sender: sender)
        
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kFollowersSegue{
            let objFollowers : FollowersVC = segue.destination as! FollowersVC
            objFollowers.userId = self.objUserDetailViewModel.isOwner.ownerId
            objFollowers.ownerType = self.objUserDetailViewModel.isOwner.owner
            
            objFollowers.followerType = followerType
            
        }else if segue.identifier == SegueIdentity.kCommentViewSegue{
            guard let commentBtn = sender as? DMCardView else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpFeed.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
        }else if segue.identifier == SegueIdentity.kFollowingSegue{
            let objFollowing : FollowingVC = segue.destination as! FollowingVC
            
            objFollowing.userId = self.objUserDetailViewModel.isOwner.ownerId
            objFollowing.followerType = followerType
            
        }else if segue.identifier == SegueIdentity.kCommentSegue{
            guard let commentBtn = sender as? UIButton else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpFeed.didSelectAtRedump(at: index)
            
            let objComment : CommentVC = segue.destination as! CommentVC
            
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
        } else if segue.identifier ==  SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let sender = sender as? UIButton else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            objDumpeeDetail.objDumpeeDetail.set(userId: dumpInfo.userId, dumpeeId: dumpInfo.dumpeeId, dumpeeName: dumpInfo.dumpeeName)
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo.dumpeeId)
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let sender = sender as? Any else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: (sender as AnyObject).tag)
            objFollowingList.userId = self.objUserDetailViewModel.isOwner.ownerId
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }else if segue.identifier == SegueIdentity.kCommentedUserListSegue{
            
            
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
            guard let sender = sender as? DMCardView else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            print(dumpInfo.dumpFeedId)
            objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }else if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let sender = sender as? UIButton else{return}
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.userId)
        }else if segue.identifier == SegueIdentity.kChatSegue{
            
            let objMessage : MessageVC = segue.destination as! MessageVC
            //guard let sender = sender as? UIButton else{return}
            objMessage.objChatViewModel.set(friendId: objUserDetailViewModel.isOwner.ownerId, friendName: objUserDetailViewModel.setUserName())
            
        } else if segue.identifier == SegueIdentity.kNewsDetailSegue{
            let objNewsDetailVC : NewsDetailVC = segue.destination as! NewsDetailVC
            guard let sender = sender as? UIButton else{return}
            objNewsDetailVC.newsUrl =  objDumpFeed.dumpTimeLineInfo(at: sender.tag).newsUrl
        }
    }
    
    private var wasPushed: Bool {
        guard let vc = navigationController?.viewControllers.first, vc == self else {
            return true
        }
        return false
    }
    
    @IBAction func onClickComment(_ sender: DMCardView) {
        performSegue(withIdentifier: SegueIdentity.kCommentViewSegue , sender: sender)
    }
    
    @IBAction func onClickCommentPromotionalText(_ sender: DMCardView) {
        performSegue(withIdentifier:SegueIdentity.kCommentedUserViewListSegue, sender: sender)
    }
    
    @IBAction func onClickFollow(_ sender: Any) {
        
        if followStatus == .follower{
            objUserDetailViewModel.sendFollowRequest(onSuceess: {
               
                DispatchQueue.main.async {
                    self.loadprofileData()
                    self.loadTimeLineData()
                    self.updateDumpsCount()
                    self.followBtn.setTitle("Unfollow", for: .normal)
                    
                }
            }, onFailure: {
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followStatus == .following{
            objUserDetailViewModel.sendUnfollowRequest( onSuceess: {
                DispatchQueue.main.async {
                    self.loadprofileData()
                    self.loadTimeLineData()
                    self.updateDumpsCount()
                    self.followBtn.setTitle("Follow", for: .normal)
                  
                }
                
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
    }
    
    
    @IBAction func onClickFollowing(_ sender: UIButton) {
        
        followerType = "follow"
        
        performSegue(withIdentifier:SegueIdentity.kFollowingSegue, sender: sender)
        
    }
    
    @IBAction func onClickFollowers(_ sender: Any) {
        followerType = "followers"
        
        performSegue(withIdentifier:SegueIdentity.kFollowersSegue, sender: sender)
        
    }
    
    
    @IBAction func onClickSettings(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "blockUser".localized, style: .default , handler:{ (UIAlertAction)in
            
            self.objUserDetailViewModel.userBlockRequest(blockType: true , onSuceess: {
                
                self.showAlert(message:"blockedSuccessfully".localized , completion: { (index) in
                    
                    
                    
                    self.navigationController?.popViewController(animated:true)
                })
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "reportUser".localized, style: .default , handler:{ (UIAlertAction)in
            self.objUserDetailViewModel.reportUser(onSuccess: {
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    
    @IBAction func onClickDumpeeName(_ sender: UIButton) {
        let index = sender.tag
        let obj = self.objDumpFeed.dumpTimeLineInfo(at: index)
        if obj.source == "news"{
            performSegue(withIdentifier: SegueIdentity.kNewsDetailSegue, sender: sender)
        }else{
            performSegue(withIdentifier: SegueIdentity.kDumpeeDetailSegue, sender: sender)
        }
    }
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        
        let index = sender.tag
        objDumpFeed.didUpdateReadMore(at: index, states:false)
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
    
        if isFromSideMenu {
            self.toggleLeft()
        }else{
            if self.presentingViewController != nil {
                dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController(animated:true)
                
            }
        }
        if let block = handler{
            block()
        }
       
    }
    
    func updateDumpsCount(){
        
        objUserDetailViewModel.getProfileDetail(onSuccess: {
            switch self.followStatus {
            case .follower:
                self.privateAcoountStatus = 0
                self.followBtn.setTitle("follow".localized, for: .normal)
            case .following:
                self.followBtn.setTitle("unfollow".localized, for: .normal)
                self.privateAcoountStatus = 2
            case .unfollow:
                self.followBtn.setTitle("requested".localized, for: .normal)
                self.privateAcoountStatus = 1
            default:
                break
            }
            self.objUserDetailViewModel.set(profileInfo: self.userName, profileBtn: self.profileImgBTN, bioLabel: self.bio, addressLabel: self.locationName, dumpCountLabel: self.dumpCountLBL, followingLabel: self.followingCountLBL, followersLabel: self.followersCountLBL, coverImageView: self.coverImageView)
        }, onFailure: {
            
        }, onUserSuspend: {_ in
            
        })
        
    }
    @IBAction func onClickMediaTextRedump(_ sender: UIButton) {
        let index = sender.tag
        
        if objUserDetailViewModel.isOwner.owner{
            self.objDumpFeed.redumpUser(dumpFeedId: self.objDumpFeed.dumpTimeLineInfo(at: index).dumpFeedId,countryName:countryNameFromLocation ,locationName:locationNameLocation , onSuccess: { (isRedumpedByMe,redumpCount) in
                
                if isRedumpedByMe{
                    self.objDumpFeed.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
                    let indexPath = IndexPath(item: index, section: 1)
                    
                    self.updateDumpsCount()
                    self.loadTimeLineData()
                    UIView.performWithoutAnimation {
                        self.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                }else{
                    
                    self.objDumpFeed.deleteParticularDumpFeed(index:index)
                    self.tableView.reloadData()
                    self.updateDumpsCount()
                    self.loadTimeLineData()
                }
            }, onFailure: {
            }, onUserSuspend: { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }else{
            self.objDumpFeed.redumpUser(dumpFeedId: self.objDumpFeed.dumpTimeLineInfo(at: index).dumpFeedId,countryName:countryNameFromLocation , locationName:locationNameLocation,onSuccess: { (isRedumpedByMe,redumpCount) in
                
                self.objDumpFeed.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
                let indexPath = IndexPath(item: index, section: 0)
                self.updateDumpsCount()
                UIView.performWithoutAnimation {
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
                self.loadTimeLineData()
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        
        
    }
    //
    
    @IBAction  func onClickCommentCount(_ sender: UIButton) {
        performSegue(withIdentifier:SegueIdentity.kCommentSegue, sender: sender)
    }
    
    @IBAction fileprivate func onClickFollowingDetail(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    
    @IBAction func onClickFeedProfileBtn(_ sender: UIButton) {
        
        let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
        if self.objUserDetailViewModel.isOwner.ownerId != dumpInfo.userId{
            performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
        }
    }
    
    @IBAction fileprivate func  onClickRedumpCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    @IBAction fileprivate func onClickDots(_ sender: UIButton) {
        
        let index = sender.tag
        let obj = self.objDumpFeed.dumpTimeLineInfo(at: index)
        dumpSetting(dumpId: obj.dumpFeedId,userID:obj.userId,source:obj.source,index: index,dumpeeName: obj.dumpeeName,media: obj.media,content: obj.content)
        
    }
    
    @IBAction fileprivate func onClickCommentUserList(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentedUserListSegue , sender: sender)
        
    }
    
    @IBAction func onClickProfileImage(_ sender: UIButton) {
        if (profileImgBTN.currentImage?.isEqual(UIImage(named: "profile_placeholder")))!
        {
            return
        }
        
        _ = sender
        profileImgBTN.image(for: .normal)
        let newImageView = UIImageView(image: profileImgBTN.imageView?.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
       
    }
    
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        sender.view?.removeFromSuperview()
    }
    @IBAction func onClickCoverImage(_ sender: UIButton) {
        if coverImageView.image == UIImage(named: "icon_BackGroundimage")
        {
            return
        }
        
        let newImageView = UIImageView(image: coverImageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissCoverFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        //        profileImgBTN.layoutIfNeeded()
        //        guard let image = profileImgBTN.image(for: .normal) else { return }
        //        JKMediaBrowser.shared.photoBrowse(image: image, orginalView: profileImgBTN)
    }
    
    
    @objc func dismissCoverFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        sender.view?.removeFromSuperview()
    }
    
    fileprivate  func dumpSetting(dumpId:String,userID:String,source:String,index:Int,dumpeeName:String = "",media:String,content:String)   {
        
        if objUserDetailViewModel.isOwner.owner{
            
            if userID == userModel?.userId{
                
                if source == "news"{
                    var actions :[AlertActionModel] = [
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                        
                    ]
                    self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                        if indexofSheet == 2{
                            self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                                self.loadprofileData()
                            }, onFailure: {
                                
                            }, onUserSuspend: { (message) in
                                
                                self.showAlert(message: message, completion: { (index) in
                                    AppDelegate.sharedDelegate.logoutUser()
                                })
                            })
                        }else if indexofSheet == 3{
                            //share
                            JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                        }else if indexofSheet == 4{
                            //copylink
                            JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                        }
                    }
                    
                    
                }else{
                    
                    var actions :[AlertActionModel] = [
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                        
                    ]
                    self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                        if indexofSheet == 2{
                            //Edit
                            
                            let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                            dumpCreateVC?.isEdit = true
                            
                            
                            guard let dumpeeInfo = self.objDumpFeed?.dumpeeInfo(at:index) else{return}
                            dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                            dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                            dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                            dumpCreateVC?.isShowLocation = dumpeeInfo.isShowLocation
                            let dumpeeType = dumpeeInfo.type
                            dumpCreateVC?.topicName = dumpeeInfo.topicName
                            let content = dumpeeInfo.content
                            let media = dumpeeInfo.media
                            let videoThumbnail = dumpeeInfo.videoThumbnail
                            let size  = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                            switch dumpeeType{
                            case .image:
                                dumpCreateVC?.params.type = .image(item: media, size: size)
                            case .textImage:
                                dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                            case .video:
                                dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                            case .textVideo:
                                dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                            case .gradient:
                                let bgCode  = dumpeeInfo.bgCode
                                var gradientColor:GradientColor = .red
                                if GradientColor.red.name == bgCode{
                                    gradientColor = .red
                                }else if GradientColor.green.name == bgCode{
                                    gradientColor = .green
                                }else if GradientColor.purple.name == bgCode{
                                    gradientColor = .purple
                                }else if GradientColor.yellow.name == bgCode{
                                    gradientColor = .yellow
                                }else if GradientColor.darkRed.name == bgCode{
                                    gradientColor = .darkRed
                                }
                                dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                            case .text:
                                dumpCreateVC?.params.type = .text(text: content)
                            }
                            
                            
                            
                            self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                            
                        }else if indexofSheet == 3{
                            //Delete
                            self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                                self.loadTimeLineData()
                            }, onFailure: {
                                
                            }, onUserSuspend: { (message) in
                                
                                self.showAlert(message: message, completion: { (index) in
                                    AppDelegate.sharedDelegate.logoutUser()
                                })
                            })
                        }else if indexofSheet == 4{
                            //Share
                            JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                        }else if indexofSheet == 5{
                            //Copy
                            JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                        }
                    }
                    
                }
                
                
            }else{
                var actions :[AlertActionModel] = [
                    
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        //Report
                        
                        self.objDumpFeed.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                        }, onFailure: {
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }else if indexofSheet == 3{
                        //Share
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                    }else if indexofSheet == 4{
                        //Copy
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
                
                
                
            }
        }
            
        else{
            if userID == userModel?.userId{
                
                if source == "news"{
                    
                    var actions :[AlertActionModel] = [
                        
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                        
                    ]
                    self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                        if indexofSheet == 2{
                            //delete
                            
                            self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                                self.loadprofileData()
                            }, onFailure: {
                                
                            }, onUserSuspend: { (message) in
                                
                                self.showAlert(message: message, completion: { (index) in
                                    AppDelegate.sharedDelegate.logoutUser()
                                })
                            })
                            
                            
                        }else if indexofSheet == 3{
                            //Share
                             JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                        }else if indexofSheet == 4{
                            //Copy
                             JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                        }
                    }
                    
                    
                }else{
                    
                    var actions :[AlertActionModel] = [
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                        AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                        
                    ]
                    self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                        if indexofSheet == 2{
                            //Edit
                            let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                            dumpCreateVC?.isEdit = true
                            
                            
                            guard let dumpeeInfo = self.objDumpFeed?.dumpeeInfo(at:index) else{return}
                            
                            dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                            dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                            dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                            dumpCreateVC?.isShowLocation = dumpeeInfo.isShowLocation
                            let dumpeeType = dumpeeInfo.type
                            dumpCreateVC?.topicName = dumpeeInfo.topicName
                            let content = dumpeeInfo.content
                            let media = dumpeeInfo.media
                            let videoThumbnail = dumpeeInfo.videoThumbnail
                            let size  = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                            switch dumpeeType{
                            case .image:
                                dumpCreateVC?.params.type = .image(item: media, size: size)
                            case .textImage:
                                dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                            case .video:
                                dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                            case .textVideo:
                                dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                            case .gradient:
                                let bgCode  = dumpeeInfo.bgCode
                                var gradientColor:GradientColor = .red
                                if GradientColor.red.name == bgCode{
                                    gradientColor = .red
                                }else if GradientColor.green.name == bgCode{
                                    gradientColor = .green
                                }else if GradientColor.purple.name == bgCode{
                                    gradientColor = .purple
                                }else if GradientColor.yellow.name == bgCode{
                                    gradientColor = .yellow
                                }else if GradientColor.darkRed.name == bgCode{
                                    gradientColor = .darkRed
                                }
                                dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                            case .text:
                                dumpCreateVC?.params.type = .text(text: content)
                            }
                            
                            self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                            
                        }else if indexofSheet == 3{
                            //Delete
                            self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                                self.loadTimeLineData()
                            }, onFailure: {
                                
                            }, onUserSuspend: { (message) in
                                
                                self.showAlert(message: message, completion: { (index) in
                                    AppDelegate.sharedDelegate.logoutUser()
                                })
                            })
                        }else if indexofSheet == 4{
                            //Share
                             JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                        }else if indexofSheet == 5{
                            //Copy
                             JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                        }
                    }
                    
                }
            }else{
                
                var actions :[AlertActionModel] = [
                    
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        //Report
                        
                        self.objDumpFeed.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                        }, onFailure: {
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }else if indexofSheet == 3{
                        //Share
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                    }else if indexofSheet == 4{
                        //Copy
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
                
                
            }
            
            
        }
        
    }
}




extension ProfileDetailVC:UITableViewDataSource,UITableViewDelegate,SwiftyTableViewCellDelegate{
    
    func clickHashTextCell(_ sender: FeedTextCell,hashName:String) {
        guard tableView.indexPath(for: sender) != nil else { return }
        
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        
        
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func swiftyTableViewCellDidTapHeart(_ sender: FeedMediaCell,hashName:String) {
        guard tableView.indexPath(for: sender) != nil else { return }
        
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if objDumpFeed.numberOfRows() > 0
            
        {
            
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
            
        }
            
        else
            
        {
            
            if !self.objUserDetailViewModel.isPrivateProfile{
                UIView.performWithoutAnimation {
                    self.isPrivateView.frame.size = CGSize(width: self.profileView.frame.width, height:150.0)
                    self.isPrivateView.layoutIfNeeded()
                    
                }
                
                noDumpFoundLBL.text = "noFeeds".localized
                
                privateAccountImageView.isHidden = true
                
                self.isPrivateView.isHidden = false
                
            }
            
            tableView.separatorStyle  = .none
            
        }
        
        return numOfSections
        //return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objDumpFeed.serviceType == .pagging ? itemCount+1 :itemCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row<itemCount {
            let obj  = objDumpFeed.cellForRowAt(at:indexPath)
            let feedType =  obj.feedType
            
            switch feedType{
            case .textImage,.textVideo:
                let feedMediaCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaCell , for: indexPath) as! FeedMediaCell
                feedMediaCell.rowIndex = indexPath.row
                feedMediaCell.delegateFeedMedia = self
                feedMediaCell.objFeedTimeLine = obj
                return feedMediaCell
            case .image,.video:
                let  feedMediaImageCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaImageCell , for: indexPath) as!  FeedMediaImageCell
                feedMediaImageCell.objImage = obj
                feedMediaImageCell.rowIndex = indexPath.row
                return feedMediaImageCell
                
            case .text:
                let feedTextCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedTextCell , for: indexPath) as! FeedTextCell
                feedTextCell.delegateFeedText = self
                feedTextCell.rowIndex = indexPath.row
                feedTextCell.objText = obj
                return feedTextCell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedGradientCell , for: indexPath) as! FeedGradientCell
                cell.rowIndex = indexPath.row
                cell.objGradient = obj
                return cell
            }
            
        }else{
            
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(){
                if self.itemCount<totalRecords {
                    objDumpFeed.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        self.loadTimeLineData()
                    }
                }else{
                    objDumpFeed.serviceType = .none
                }
            }else{
                objDumpFeed.serviceType = .none
            }
            
            return loadmoreCell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row<itemCount {
            let obj  = objDumpFeed.cellForRowAt(at:indexPath)
            let feedType =  obj.feedType
            switch feedType {
            case .gradient:
                return 322.0
            case .textImage,.textVideo:
                return 369.0
            case .image,.video:
                return 320.0
            case .text:
                return  180.0
           
            }
        }else{
            return 50
            
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        if offset < 64 {
            gradientUpperView.isHidden = true
            gradientLowerView.isHidden = true
            //MK check for profile
            //profileNameLBL.text = "profile".localized
            if userModel?.userId == objUserDetailViewModel.isOwner.ownerId
            {
                profileNameLBL.text = "myprofile".localized
            }
            else{
                profileNameLBL.text = "profile".localized
            }
        }else{
            gradientUpperView.isHidden = false
            gradientLowerView.isHidden = false
            profileNameLBL.text = objUserDetailViewModel.setUserName()
            
            
        }
        
        if scrollView.contentOffset.y < 0 {
            topConstraint.constant = -offset/2
        } else {
            topConstraint.constant = 0
        }
    }
    
}







