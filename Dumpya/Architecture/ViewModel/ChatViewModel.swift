//
//  ChatViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 13/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatViewModel: NSObject {
    var chatRefreshHandler:((_ isReresh:Bool)->Void)?
    fileprivate var chatSectionlist = [DMChatSectionModel]()
    fileprivate var conversationId:String? = ""
    var userReceiverId:String = ""
    fileprivate var receivername:String = ""
    fileprivate var senderId:String{
        return userModel?.userId ?? ""
    }
    
    
    func set(friendId id:String,friendName name:String){
        self.userReceiverId = ""
        self.receivername = ""
        self.userReceiverId = id
        self.receivername = name
    }
    
    
    var serviceType:DMServiceType{
        
        if self.chatSectionlist.count>0 {
            let currentlist =   self.chatSectionlist.compactMap({$0.messages.compactMap({$0.messageId!})})//self.chatSectionlist.flatMap({$0.messages.flatMap({$0.messageId!})})
            if  currentlist.count<totalRecords {
                return .pagging
            }else{
                return .none
            }
        }else{
            return .none
        }
        
    }
    //MARK:- getConversationId
    func getConversationId(onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let url = kGetConversationId + "\(userReceiverId)"
            ServerManager.shared.httpGet(request:url, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(ChatResponseModel.self) else{return}
                    
                    guard let status = response.statusCode else{return}
                    
                    switch status{
                    case 200:
                        guard let conversationId = response.chatModel?.conversationId else{return}
                        self.conversationId = conversationId
                        onSuccess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    func deleteMessage(indexPath:IndexPath,tableView:UITableView){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let messageId = self.cellForRowAt(at: indexPath).messageId ?? ""
            let conversationId = self.conversationId ?? ""
            ServerManager.shared.httpDelete(request:kDeleteMessage+messageId+"&conversationId="+conversationId, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DeleteMessageResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        self.deleteItemAt(at: indexPath,tableView: tableView)
                        break
                    case 503:
                        suspendMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
        }
    }
    
    
    func getChat(page:Int = 0,limit:Int = 20, isLoader:Bool = true,onSuccess:@escaping(Bool)->Void){
        if ServerManager.shared.CheckNetwork(){
            if self.serviceType == .none || isLoader == true {
               ServerManager.shared.hideHud()
            }
            let url = kGetChat + "\(conversationId ?? "")&page=\(page)&limit=\(limit)"
            
            ServerManager.shared.httpGet(request:url, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if self.serviceType == .none || isLoader == true{
                        self.chatSectionlist.removeAll()
                        ServerManager.shared.hideHud()
                        
                    }
                    guard let response = response.JKDecoder(DMChatResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        
                        guard let chatSectionlist = response.chatHistoryModel?.messages else{return}
                     
                        totalRecords = response.chatHistoryModel?.totalCount ?? 0
                         var list  = self.chatSectionlist
                        if list.count>0{
                            self.chatSectionlist.removeAll()
                            for item  in chatSectionlist{
                                if let section = list.index(where: {$0.displayDate!.isEqual(to: item.date!.sectionDateString)}){
                                    list[section].messages.append(item.messages)
                                }else{
                                   
                                    list.append([item])
                                }
                            }
                            self.chatSectionlist = list
                          
//                            list.append(contentsOf: chatSectionlist)
//                            let group = Dictionary(grouping: list, by: {$0.displayDate!})
//
//                            for item in group{
//                               self.chatSectionlist.append(item.value)
//                            }
                           
                        }else{
                            self.chatSectionlist = chatSectionlist
                        }
                        
                        
                        self.sortMessages(isLoadFirstTime: true, completion: onSuccess)
                        print("\(self.chatSectionlist.compactMap({$0.messages.compactMap({$0.messageId!})}))")
                        break
                    case 503:
                        suspendMessage = response.message ?? ""
                         ServerManager.shared.hideHud()
                        break
                    default:
                        alertMessage = response.message ?? ""
                         ServerManager.shared.hideHud()
                        
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
        }
    }
    
}

extension ChatViewModel{
    
    func set(reciverlbl namelbl:UILabel){
        namelbl.text = receivername
    }
    
    func titleForHeaderInSection(section:Int) -> String {
        return self.chatSectionlist[section].displayDate ?? ""
    }
    
    
    func numberOfSection()->Int{
        return self.chatSectionlist.count
    }
    func numberOfRows(InSection section:Int) -> Int {
        return self.chatSectionlist[section].messages.count
    }
    
    func cellForRowAt(at indexPath:IndexPath) -> DMMessageModel {
        
        return self.chatSectionlist[indexPath.section].messages[indexPath.row]
    }
    
    func didSelectForRowAt(at indexPath:IndexPath) -> DMMessageModel {
        
        return self.chatSectionlist[indexPath.section].messages[indexPath.row]
    }
    
    func deleteItemAt(at indexPath:IndexPath,tableView:UITableView) {
        if self.numberOfRows(InSection: indexPath.section)>0 {
            self.chatSectionlist[indexPath.section].messages.remove(at: indexPath.row)
            tableView.didUpdates {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            DispatchQueue.main.async {
                
                if self.numberOfRows(InSection: indexPath.section) == 0 {
                    self.chatSectionlist.remove(at: indexPath.section)
                    tableView.didUpdates {
                        let indexSet = IndexSet(integer: indexPath.section)
                        tableView.deleteSections(indexSet , with:.fade)
                    }
                }
            }
        }
    }
    
    func getSectionRowsCount(at indexPath:IndexPath)->Int{
        return self.chatSectionlist[indexPath.section].messages.count
    }
    
}
extension ChatViewModel{
    
    //MARK:- updateReadStatus
    fileprivate func updateReadStatus(isRead:Bool = false,isChatRefresh:Bool, completion: ((_ isChatRefresh:Bool) -> Swift.Void)? = nil) {
        //For Seen
        //        let endIndex =  self.chatSectionlist.endIndex
        //        if  endIndex>0{
        //            let section = endIndex-1
        //            let endIndexOfMsg =  self.chatSectionlist[section].messages.endIndex
        //            if endIndexOfMsg>0{
        //                let row = endIndexOfMsg-1
        //                self.chatSectionlist[section].messages[row].isRead = isRead
        //            }
        //        }
        if let item = self.chatSectionlist.last,let section = self.chatSectionlist.index(of: item), let message = item.messages.last, let index = item.messages.index(of: message) {
            self.chatSectionlist[section].messages[index].isRead = isRead
        }
        
        
        print("Message LastSeen = \(String(describing: self.chatSectionlist.last?.messages.last?.isRead)) \n isChatRefresh = \(isChatRefresh)")
        completion?(isChatRefresh)
    }
    
    
    //MARK:-
    
    func didUpdateSizeForItem(at indexPath:IndexPath,mediaSize size:CGSize = .zero )  {
        self.chatSectionlist[indexPath.section].messages[indexPath.row].mediaSize = size
    }
    
    
    func shouldUpdateSizeForItem(at indexPath:IndexPath ) -> Bool  {
        return self.chatSectionlist[indexPath.section].messages[indexPath.row].mediaSize == .zero ? false : true
        
        
    }
    
    //MARK:- sortMessages
    fileprivate func sortMessages(isLoadFirstTime:Bool = false,isUpdate:Bool = false, isChatRefresh:Bool = false,completion: ((_ isChatRefresh:Bool) -> Swift.Void)? = nil){
        
        self.chatSectionlist.sort(by: {$0.date!.compare($1.date!) == .orderedAscending})
        for (index,_) in self.chatSectionlist.enumerated(){
            self.chatSectionlist[index].messages.sort(by: {$0.date!.compare($1.date!) == .orderedAscending})
        }
        let isFistTimeLoadRead =  self.chatSectionlist.last?.messages.last?.isRead ?? false
        for (index,item) in self.chatSectionlist.enumerated() {
            var messages = item.messages
            for (idx,item) in messages.enumerated(){
                if  item.isRead == true{
                    messages[idx].isRead =  false
                }
            }
            self.chatSectionlist[index].messages = messages
            
        }
        
        if !isLoadFirstTime {
            
            updateReadStatus(isRead:isUpdate, isChatRefresh: isChatRefresh,completion: completion)
        }else{
            
            updateReadStatus(isRead:isFistTimeLoadRead, isChatRefresh: isChatRefresh,completion: completion)
            
        }
    }
    
    //MARK:- add MESSAGE
    fileprivate func add(mesage:DMMessageModel,createDate:Date,completion: ((_ isChatRefresh:Bool) -> Swift.Void)? = nil){
        if self.chatSectionlist.count>0 {
         
            if let section = self.chatSectionlist.index(where: {$0.displayDate!.isEqual(to: createDate.sectionDateString)}) {
                if let row = self.chatSectionlist[section].messages.index(where: {$0.messageId! == mesage.messageId!}){
                    self.chatSectionlist[section].messages[row] = mesage
                }else{
                    self.chatSectionlist[section].messages.append(mesage)
                }
                
            }else{
                let sectionModel = DMChatSectionModel(message: mesage, createDate: createDate)
                self.chatSectionlist.append(sectionModel)
            }
            
            sortMessages(isUpdate: mesage.isRead, isChatRefresh: mesage.isChatRefresh,completion: completion)
        }else{
            
            let sectionModel = DMChatSectionModel(message: mesage, createDate:createDate)
            self.chatSectionlist.append(sectionModel)
            sortMessages(isUpdate: mesage.isRead, isChatRefresh: mesage.isChatRefresh,completion: completion)
        }
    }
    
    //MARK:- userOnline
    func userOnlineObserver(isOnline:Bool = true){
        let userOnlineJson:[String:Any] = [
            "senderId":senderId,
            "type" : isOnline ? "IN":"OUT",
            "receiverId":self.userReceiverId
        ]
        SocketIOManager.shared.userOnline(message: userOnlineJson)
    }
    //MARK:- SEND NEW MESSAGE
    func send(content:Any,messageType:MessageType,completion:((_ isChatRefresh:Bool) -> Swift.Void)? = nil){
        
        if !ServerManager.shared.CheckNetwork() {
            return
        }
        var media:String = ""
        var message:String = ""
        if messageType == .photo {
            media = "\(content)"
        }else{
            message = "\(content)"
        }
        let params:[String:Any] = [
            "message": message,
            "conversationId":self.conversationId ?? "",
            "senderId":self.senderId ,
            "media":media ,
            "messageType": messageType.description,
            "receiverId":self.userReceiverId]
        SocketIOManager.shared.sendMessageNew(message: params)
        getInstanceMessageObserver(completion: completion)
        
    }
    
    func getInstanceMessageObserver(completion:((_ isChatRefresh:Bool) -> Swift.Void)? = nil){
        self.getChatMessageObserver { (response) in
            
            
            let json  = JSON.init(response)
            let model = DMMessageModel(parse: json)
            if  (model.conversationId?.isEqual(to: self.conversationId ?? ""))!, let date = model.date {
                self.add(mesage: model, createDate: date,completion: completion)

            }
            else{
                completion?(false)
            }
        
        }
    }
    //MARK:- readNotify
    func readNotify(completion: ((_ isChatRefresh:Bool) -> Swift.Void)? = nil){
        if !ServerManager.shared.CheckNetwork() {
            return
        }
        SocketIOManager.shared.readMessage { (response) in
            print("THE READ STATUS RESPONSE IS NEW--------->",response)
            let json  = JSON.init(response)
            let senderId = json["senderId"].stringValue
            let receiverId = json["receiverId"].stringValue
            let isRead = true
            if self.chatSectionlist.count>0, !self.userReceiverId.isEmpty,!self.senderId.isEmpty, receiverId.isEqual(to: self.senderId),  senderId.isEqual(to: self.userReceiverId){
                self.sortMessages(isUpdate: isRead, completion: completion)
            }
        }
    }
    //MARK:- getChatMessageObserver
    func getChatMessageObserver(onSuccess:@escaping(_ messageInfo: [String: Any])->Void){
        if !ServerManager.shared.CheckNetwork() {return}
        SocketIOManager.shared.getChatMessage(completionHandler: onSuccess)
        readNotify()
    }
    func socketToggleObserver(){
        SocketIOManager.shared.socketOn(userID:senderId)

    }
    
    var isChatRefresh:Bool{
        
        let filtered = chatSectionlist.filter { obj in
            obj.messages.contains(where: { $0.isChatRefresh == true })
        }
        if let obj = filtered.first, let isChatRefresh = obj.messages.filter({$0.isChatRefresh == true}).first?.isChatRefresh {
            return isChatRefresh
        }else{
            return false
        }
    }
}

