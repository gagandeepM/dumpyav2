//
//  DumpCreateModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 23/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

struct CreateTopicModel:Mappable {
    
    let id : String?
    
    enum CodingKeys: String, CodingKey {
        case id  = "_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
       
    }
}
struct DumpCreateModel:Mappable {
    let status : Int?
    let message : String?
    let createTopic:CreateTopicModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case createTopic = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        createTopic =  values.contains(.createTopic) == true ? try values.decodeIfPresent(CreateTopicModel.self, forKey: .createTopic) : nil
    }
}
