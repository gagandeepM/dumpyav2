//
//  LoginViewModel.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DMUserViewModel: NSObject {

    //MARK:- Login
    func login(email:String,password:String,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void) {
        
        if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kEmailValid
        }else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.length<6{
            alertMessage = FieldValidation.kPasswordLength
        }else{
            
            if  ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["email":email,"password":password]
                ServerManager.shared.httpPost(request: klogin, params: params, successHandler: { (responseData:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                  
                        guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            guard let user = response.user else{return}
                            UserDefaults.DMDefault(set: user, forKey: kUserDataKey)
                            if let accessToken = user.accessToken{
                                UserDefaults.DMDefault(setObject: accessToken, forKey: kAuthTokenKey)
                            }
                            onSuceess()
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
            
        }
   }
    //MARK:- Create Account
    func createAccount(firstName: String,lastName : String,userName : String,email : String,password : String,confirmPassword:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        if firstName.isEmpty{
            alertMessage = FieldValidation.kFirstNameEmpty
        }
        else if lastName.isEmpty{
            alertMessage = FieldValidation.kLastNameEmpty
        }
        else if userName.isEmpty{
            alertMessage = FieldValidation.kUserName
        }
        else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }
        else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPasswordEmpty
        }
        else if password != confirmPassword{
            alertMessage = FieldValidation.kMisMatchPasswordEmpty
        }
        else{
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"userName":userName,"email":email,"password":password,"deviceType":"IOS","socialType":"normal"]
                
                ServerManager.shared.httpPost(request:dogBreedURL, params: params, successHandler: { (responseData:Data) in
                    DispatchQueue.main.sync {
                        ServerManager.shared.hideHud()
                        guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 0:
                            guard let user = response.user else{return}
                            userModel = user
                            guard let token = user.accessToken else{return}
                            accessToken = token
                            
                            onSuccess()
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                            
                        }
                        
                    }
                }, failureHandler: { (error) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
}
