//
//  ProfileDetailViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class ProfileDetailViewModel: NSObject {
    var serviceType:DMServiceType = .none
    fileprivate var objFollowFollowerModel : FollowFollowersModel?
    fileprivate var currentUserId:String?
    fileprivate var profileModel:DMUserModel?
   
    func getProfileDetail(isPull:Bool = false,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            
            if !isPull || serviceType == .none{
                ServerManager.shared.showHud()
            }
           
            let userid:String = isOwner.ownerId
           
            ServerManager.shared.httpGet(request:kgetUserDetail+"\(userid)"+"&otherInfo=1", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if !isPull || self.serviceType == .none{
                        ServerManager.shared.hideHud()
                    }
                    guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        guard let user  =  response.user else{return}
                        self.profileModel = user
                        onSuccess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    
    func sendFollowRequest(isFromProfile:Bool = true,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        let userId :String?
        if !isFromProfile{
            userId = objFollowFollowerModel?.userId ?? ""
        }else{
           userId = profileModel?.userId ?? ""
        }
            if  ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":userId ?? ""]
                ServerManager.shared.httpPost(request: kSendFollowRequest, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        
                        guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                       
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                           
                            onSuceess()
                            break
                            
                        case 503:
                            onUserSuspend(response.message ?? "")
                            break
                        case 403:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
         }
    }
    
    
    //Unfollow User
    
    
    func sendUnfollowRequest(onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
             let userId:String = isOwner.ownerId
            let params:[String:Any] = ["userId":userId]
            ServerManager.shared.httpPut(request: kSendUnfollowRequest, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    
                    guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                    
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        onSuceess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    case 403:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    
    func userBlockRequest(blockType:Bool,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            let blockUserId:String = isOwner.ownerId
            let params:[String:Any] = ["blockUserId":blockUserId,"blockType":blockType]
            ServerManager.shared.httpPut(request: kBlockUnblockUser, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        onSuceess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    case 403:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    
    
    func reportUser(onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        let userId = isOwner.ownerId
        let params:[String:Any] =
            [
                "reportType":"user",
                "userId":userId
        ]
        ServerManager.shared.httpPost(request:kReportUser, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                alertMessage = response.message ?? ""
                onSuccess()
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
}
    

extension ProfileDetailViewModel{
    
    func userProfileDetail() -> String
    {
        print(profileModel?.mutualFollowingText)
        return profileModel?.mutualFollowingText ?? ""
    }
    
    func set(currentUserId:String?){
        self.currentUserId = currentUserId
    }
    //test comment			
    func set(objFollowFollowing:FollowFollowersModel)  {
        self.objFollowFollowerModel = objFollowFollowing
    }
    var isOwner:(owner:Bool,ownerId:String){
        let ownerUserId = userModel?.userId ?? ""
        let otherUserId = self.currentUserId ?? ""
        
        if !otherUserId.isEmpty , !ownerUserId.isEmpty , ownerUserId == otherUserId{
            return (true , ownerUserId)
        }else{
            return (false , otherUserId)
        }
    }
    var isPrivateProfile:Bool{
       
        return profileModel?.settingPrefrence?.isPrivateProfile ?? false
    }
    var isBlockedUser:Bool
    {
        return profileModel?.isBlocked ?? false
    }
    var followedState:FollowState?{
        let value =  profileModel?.isFollowed ?? -1
        return FollowState(rawValue: value)
    }
 
    func set(profileInfo fullnameLabel:UILabel , profileBtn:DMButton ,bioLabel:UILabel, addressLabel:UILabel,dumpCountLabel:UILabel,followingLabel:UILabel,followersLabel:UILabel,coverImageView:UIImageView){
        fullnameLabel.text = self.profileModel?.fullName ?? ""
        bioLabel.text = self.profileModel?.bio ?? ""
        addressLabel.text = self.profileModel?.locationName ?? ""
        dumpCountLabel.text = "\(self.profileModel?.otherInfo?.reDumpCount ?? 0)"
        followingLabel.text = "\(self.profileModel?.otherInfo?.following ?? 0)"
        followersLabel.text = "\(self.profileModel?.otherInfo?.followers ?? 0)"
        if let file = self.profileModel?.profilePic  {
            profileBtn.loadImage(filePath: file, for: .normal)
        }
        if let coverfile = self.profileModel?.coverPic  {
            coverImageView.loadImage(filePath: coverfile)
        }
    }
    
    func setUserName() -> String {
        return self.profileModel?.userName ?? ""
    }
}
