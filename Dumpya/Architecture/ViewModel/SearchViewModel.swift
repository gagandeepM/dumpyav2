//
//  SearchViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 01/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class SearchViewModel: NSObject {
    var users:[DMUserSearchModel] = [DMUserSearchModel]()
    var hashTags:[DMTagSearchModel] = [DMTagSearchModel]()
   
     var serviceType:DMServiceType = .none
   var screenApiType:DumpScreenFuncType = .history
    func removeObjects(){
        if self.users.count>0 {
            self.users.removeAll()
        }
        if self.hashTags.count>0 {
            self.hashTags.removeAll()
        }
    }
    func getSearchUsers(refreshControl:UIRefreshControl = UIRefreshControl(),search:String = "", page:Int = pageNumber,reqType:String = "",onSuccess:@escaping(_ totalRecords:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            if self.serviceType == .none || !refreshControl.isRefreshing
            {
                ServerManager.shared.showHud()
            }
            print(reqType)
            let url = kUserSearch + "\(search)&page=\(page)&reqType=\(reqType)"
            guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if self.serviceType == .none || !refreshControl.isRefreshing{
                        ServerManager.shared.hideHud()
                    }
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    guard let response = response.JKDecoder(DMUserSearchResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                       self.removeObjects()
                        guard let userList = response.userSearch?.userList else{return}
                        
                            if self.serviceType == .pagging
                            {
                                self.users.append(contentsOf: userList)
                            }
                            else
                            {
                                self.users.removeAll()
                                self.users =  userList
                            }
                       
                        totalRecords = response.userSearch?.totalCount ?? 0
                        self.serviceType = self.users.count<totalRecords ?.pagging : .none
                      
                        onSuccess(totalRecords)
                         break
                        
                    case 503:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    //Mark: Save history
    func saveUserHistory(searchUserId:String = "",searchDumpeeId:String = "",searchName:String = "",historyType:String = "",otherInfo:Any,progressHud:Bool = false,onSuccess:@escaping()->Void){
        let param:[String:Any] =
            [
                "searchUserId" : searchUserId,
                "searchDumpeeId" : searchDumpeeId,
                "searchName" : searchName,
                "historyType" : historyType,
                "otherInfo":otherInfo //Model
        ]
        print(param)
        if ServerManager.shared.CheckNetwork(){
            
            if progressHud || serviceType == .none{
                ServerManager.shared.showHud()
            }
            
            let url = KSaveHistory
            guard let urlString = url.urlQueryAllowed else{return}
            
            ServerManager.shared.httpPost(request:urlString, params:param,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if progressHud || self.serviceType == .none{
                        ServerManager.shared.hideHud()
                    }
                    guard let response = response.JKDecoder(DMUserSearchResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
//                        guard let suggestionData  =  response.userSearch else{return}
//                        self.users = suggestionData.userList
                        onSuccess()
                        break
                        
                    case 503:
                        suspendMessage = response.message
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
        }
    }
    func saveTagHistory(searchUserId:String = "",searchDumpeeId:String = "",searchName:String = "",historyType:String = "",otherInfo:Any,progressHud:Bool = false,onSuccess:@escaping()->Void){
        let param:[String:Any] =
            [
                "searchUserId" : searchUserId,
                "searchDumpeeId" : searchDumpeeId,
                "searchName" : searchName,
                "historyType" : historyType,
                "otherInfo":otherInfo //Model
        ]
        print(param)
        if ServerManager.shared.CheckNetwork(){
            
            if progressHud || serviceType == .none{
                ServerManager.shared.showHud()
            }
            
            let url = KSaveHistory
            guard let urlString = url.urlQueryAllowed else{return}
            
            ServerManager.shared.httpPost(request:urlString, params:param,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if progressHud || self.serviceType == .none{
                        ServerManager.shared.hideHud()
                    }
                    guard let response = response.JKDecoder(DMTagSearchResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
//                        guard let suggestionData  =  response.tagSearch else{return}
//                        self.hashTags = suggestionData.hashList
                        onSuccess()
                        break
                        
                    case 503:
                        suspendMessage = response.message
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
        }
    }
    
    func getSearchTags(refreshControl:UIRefreshControl = UIRefreshControl(),search:String = "", page:Int = pageNumber,onSuccess:@escaping(_ totalRecords:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            if self.serviceType == .none || !refreshControl.isRefreshing
            {
                ServerManager.shared.showHud()
            }
            
             let url = kTagSearch + "\(search)&page=\(page)"
            guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if self.serviceType == .none || !refreshControl.isRefreshing
                    {
                        ServerManager.shared.hideHud()
                    }
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    guard let response = response.JKDecoder(DMTagSearchResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                       
                        guard let hashList = response.tagSearch?.hashList else{return}
                        if self.serviceType == .pagging
                        {
                            self.hashTags.append(contentsOf: hashList)
                        }
                        else
                        {   self.hashTags.removeAll()
                            self.hashTags =  hashList
                        }
                        totalRecords = response.tagSearch?.totalCount ?? 0
                        self.serviceType = self.hashTags.count<totalRecords ?.pagging : .none
                        
                        self.hashTags = hashList
                        onSuccess(totalRecords)
                        break
                        
                    case 503:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
}

extension SearchViewModel{
    func numberOfRows() -> Int {
        return users.count
    }
    func cellForRowAt(at indexPath:IndexPath) -> DMUserSearchModel {
        return users[indexPath.row]
    }
    
    func numberOfRowsForTag() -> Int {
        return hashTags.count
    }
    
    func cellForRowAtTag(at indexPath:IndexPath) -> DMTagSearchModel {
        return hashTags[indexPath.row]
    }
    
    func didSelectUser(index:Int) -> String {
         let obj = users[index]
   
        if let jsonObject =  obj.jsonObject as? [String:Any]{
        saveUserHistory(searchUserId: obj.userId ?? "", searchDumpeeId: "", searchName: obj.userName ?? "", historyType: "users", otherInfo: jsonObject, progressHud: true) {
            
        }
        
        }
        return obj.userId ?? ""
    }
    func didSelectDumpee(index:Int) -> String {
        let obj = users[index]
        
        if let jsonObject =  obj.jsonObject as? [String:Any]{
            saveUserHistory(searchUserId:"", searchDumpeeId: "", searchName: obj.userName ?? "", historyType: "users", otherInfo: jsonObject, progressHud: true) {
                
            }
            
        }
        return obj.userId ?? ""
    }
   
    func didSelectTag(index:Int) -> String {
        
        let obj = hashTags[index]
       if let jsonObject =  obj.jsonObject as? [String:Any]{
       
        saveTagHistory(searchUserId: "", searchDumpeeId: "", searchName: obj.hashName ?? "", historyType: "tags", otherInfo: jsonObject, progressHud: true) {
            
        }
        }
        return obj.hashName ?? ""
    }
    func didSelect(index:Int)->(String,String)
    {
        let obj = users[index]
        return (obj.userId ?? "",obj.userName ?? "")
    }
}
