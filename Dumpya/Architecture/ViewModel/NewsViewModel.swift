//
//  NewsViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 17/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class NewsViewModel: NSObject {
    var serviceType:DMServiceType = .none
    fileprivate var objNewsModel:[NewsModel] = [NewsModel]()
    func getNews(isPull:Bool = false,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            
            if !isPull || serviceType == .none {
                ServerManager.shared.showHud()
            }
            ServerManager.shared.httpGet(request:kNews, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    
                    if !isPull || self.serviceType == .none {
                        ServerManager.shared.hideHud()
                    }
                   
                    guard let response = response.JKDecoder(NewsResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        let newsModel  =  response.newsModel
                        if self.serviceType == .pagging
                        {
                            self.objNewsModel.append(contentsOf: newsModel)
                        }
                        else
                        {
                            self.objNewsModel.removeAll()
                             self.objNewsModel = newsModel
                        }
//                        totalRecords = dumpData.totalCount ?? 0
//                        self.serviceType = self.feedTimelines.count<totalRecords ?.pagging : .none
                        onSuccess()
                        break
                    case 503:
                        
                        suspendMessage = response.message ?? ""
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    func redumpNews(newsId: String = "",countryName:String = "",locationName:String = "",onSuccess : @escaping(_ redumpCount:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        let params:[String:Any] =
            [
                "newsId":newsId,
                "locationName":locationName,
                "countryName":countryName
                
        ]
        ServerManager.shared.showHud()
        ServerManager.shared.httpPost(request:kNewsRedump, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                onSuccess(response.reDumpData?.redumpCount ?? 0)
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    
}
extension NewsViewModel{
    
    func numberOfRows() -> Int {
        return objNewsModel.count
    }
    
    func cellForRowAt(at indexPath:IndexPath) -> NewsModel {
        return objNewsModel[indexPath.row]
    }
    
    func didSelect(index:Int)->String{
        
        
        guard let imageFile  = objNewsModel[index].url  else {return ""}
        let urlString = imageFile.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        guard let url  = urlString  else {return ""}
    
      
        return url
    }
    func redumpStatus(index:Int)->(Bool,String){
        return (objNewsModel[index].isDump ?? false,objNewsModel[index].newsId ?? "")
    }
    
    
    func didUpdateRedumpNews(at indexPath:Int,isDump:Bool = false,redumpCount:Int = 0) {
        objNewsModel[indexPath].isDump = isDump
        objNewsModel[indexPath].dumpCount =  redumpCount
    }
}

