//
//  FollowingListViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class RedumpedUserViewModel: NSObject {

  
    fileprivate var dumpId:String = ""
  
    fileprivate var redumpedUserInfo:[RedumpedUserInfoModel] = [RedumpedUserInfoModel]()
    
    //MARK:- Redumped Users List
    
    func getRedumpUserList(userId:String = "",page:Int = 0,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            ServerManager.shared.httpGet(request:kRedumpUsers + "\(page)" + "&dumpId=\(self.dumpId)&userId=\(userId)", params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(RedumpedUserReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let redumpedUser  =  response.redumpedUser else{return}
                        self.redumpedUserInfo = redumpedUser.redumpedUserInfo
                        onSuccess()
                        break
                    case 503:
                       
                        suspendMessage = response.message ?? ""
                       
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
   
}
extension RedumpedUserViewModel{
    
    func numberOfRows() -> Int {
        
       return redumpedUserInfo.count
    }
    
    func cellForRowAt(indexPath:IndexPath) -> RedumpedUserInfoModel {
        
        return redumpedUserInfo[indexPath.row]
    }
    
    func didSelectRedumpUser(at:Int) -> String {
        
        let userId = redumpedUserInfo[at].userId
        return userId ?? ""
    }
    
    
    func setFollowingDumpId(dumpId:String)  {
        
        self.dumpId = dumpId
     
        
    }
    
   
    
}
