//
//  CreateAccountViewModel.swift
//  Dumpya
//
//  Created by Chander on 20/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

class DMFeedViewModel:NSObject{
    var categoryName:String?
    var searchString:String?
    var categoryId:String?
    var topicId:String?
    var topicName:String?
    fileprivate var objDumpbCategory:DumpeeCategoryIdModel?
    fileprivate var dumpeeDetailModel:DumpeeDetailModel?
     var dumpList:[DMDumpModel] = [DMDumpModel]()
     var categoryList:[DMCategoryModel] = [DMCategoryModel]()
    fileprivate var suggestionList:[HistoryDetailModel] = [HistoryDetailModel]()
     var serviceType:DMServiceType = .none
    var screenApiType:DumpScreenFuncType = .none
    func removeObjects(){
        self.removeDumpList()
        self.removeCatList()
        
    }
    func removeDumpList(){
        if dumpList.count>0 {
            self.dumpList.removeAll()
        }
    }
    func removeCatList(){
        if categoryList.count>0 {
            self.categoryList.removeAll()
        }
    }
    //MARK: Serach history
   fileprivate  func reload(){
        self.serviceType = self.dumpList.count<totalRecords ?.pagging : .none
    
    }
    func getDumpFeed(refreshControl:UIRefreshControl = UIRefreshControl(),search:String = "",page:Int = pageNumber,onSuccess:@escaping(_ totalRecords:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        print(page)
        if ServerManager.shared.CheckNetwork(){
            if self.serviceType == .none || !refreshControl.isRefreshing
            {
                ServerManager.shared.showHud()
            }
            if  self.screenApiType == .search
            { ServerManager.shared.showHud()}
            let url = kDumpSearch + "\(search)&page=\(page)"
            guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if self.serviceType == .none || !refreshControl.isRefreshing || self.screenApiType == .search
                    {
                        ServerManager.shared.hideHud()
                    }
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    guard let response = response.JKDecoder(DMDumpFeedResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        //self.removeObjects()
                        guard let dumpData  =  response.dumpData else{return}
                        let dumpList = dumpData.dumpList
                        let catList = dumpData.categoryList
                        self.categoryList = dumpData.categoryList
                        if dumpList.count>0{
                            //default histoy
                            if search.isEmpty{
                                self.screenApiType = .history
                            }else{
                                self.screenApiType = .search
                            }
                            
                            if self.serviceType == .pagging{
                                self.dumpList.append(contentsOf: dumpList)
                                
                            }else{
                                self.dumpList.removeAll()
                                self.dumpList = dumpList
                            }
                        }else{
                            self.removeDumpList()
                            if search.length == 0
                            {
                                self.screenApiType = .history
                            }
                            if self.screenApiType == .none || self.screenApiType == .history {
                                self.screenApiType = .history
                            }
                            else{
                                self.categoryList.removeAll()
                                self.categoryList = catList
                                self.screenApiType = .category
                            }
                           
                        }
                       
                        totalRecords = dumpData.totalCount ?? 0
                        self.reload()
                        onSuccess(totalRecords)
                        break
                        
                    case 503:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }
                DispatchQueue.main.async {
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    //MARK: Use s History 
    func getDumpeeSuggestions(search:String = "",progressHud:Bool = false,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            
            if progressHud || serviceType == .none{
                ServerManager.shared.showHud()
            }
            
            let url = kSearchSuggestion + "\(search)"
            guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                      if progressHud || self.serviceType == .none{
                    ServerManager.shared.hideHud()
                         }
                    guard let response = response.JKDecoder(HistoryResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let suggestionData  =  response.historyData else{return}
                        self.suggestionList =  suggestionData.historyList
                       
                          onSuccess()
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    //Mark: Save history
    func saveDumpeeHistory(searchUserId:String = "",searchDumpeeId:String = "",searchName:String = "",historyType:String = "",otherInfo:Any,progressHud:Bool = false,onSuccess:@escaping()->Void){
        let param:[String:Any] =
        [
        "searchUserId" : searchUserId,
        "searchDumpeeId" : searchDumpeeId,
        "searchName" : searchName,
        "historyType" : historyType,
        "otherInfo":otherInfo //Model
        ]
        print(param)
        if ServerManager.shared.CheckNetwork(){
            
            if progressHud || serviceType == .none{
                ServerManager.shared.showHud()
            }
            
            let url = KSaveHistory
            guard let urlString = url.urlQueryAllowed else{return}
           
            ServerManager.shared.httpPost(request:urlString, params:param,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if progressHud || self.serviceType == .none{
                        ServerManager.shared.hideHud()
                    }
                    guard let response = response.JKDecoder(DMDumpFeedResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
//                        guard let saveData  =  response.dumpData else{return}
//                        self.dumpList = saveData.dumpList
                        onSuccess()
                        break
                        
                    case 503:
                        suspendMessage = response.message
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                      
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                   
                }
            })
        }
    }
    
    
    
    func numberOfRows() -> Int {
        return dumpList.count
    }
    
    
    func cellForRowAt(at indexPath:IndexPath) -> DMDumpModel {
        return dumpList[indexPath.row]
    }
    func numberOfRowsForCatagory() -> Int {
        print("categoryList--------- \(categoryList.count)")
        return categoryList.count
    }
    func cellForRowAtCategory(at indexPath:IndexPath) -> DMCategoryModel {
       self.categoryName = categoryList[indexPath.row].categoryName
        self.categoryId = categoryList[indexPath.row].categoryId
        return categoryList[indexPath.row]
    }
    
    func numberOfRowsForSuggestion() -> Int {
        print("sugessionlist--------- \(suggestionList.count)")
         return suggestionList.count
    }
    
    func cellForRowAtSuggestion(at indexPath:IndexPath) -> HistoryDetailModel {
        return suggestionList[indexPath.row]
    }
    
    func didSelectAtSuggestion(at index:Int) -> String {
        return suggestionList[index].name ?? ""
    }
    
    //MARK:- Create Topic
    
    func createTopic(topicName:String,categoryId:String,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params:[String:Any] = ["dumpeeName":topicName,"categoryId":categoryId]
            ServerManager.shared.httpPost(request: kcreateTopic, params: params,headers: ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DumpCreateModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let topicId  = response.createTopic?.id else{return}
                        self.topicId = topicId
                        onSuceess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }

    func createDump(isDumpStatus:Bool = true,params:DumpCreateParameter,locationName:String = "",countryName:String = "",isShowLocation:Bool = true,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            var height:Int = 0
            var width:Int = 0
            let socialShare =  ["facebook", "twitter"]
            
            var dumpeeId:String?
            if !isDumpStatus{
                dumpeeId = getTopicInfo().dumpeeIdForCategory
            }else{
                dumpeeId = dumpeeDetailModel?.dumpeeId  ?? ""
            }
            var gradient:String = ""
            var url:String = ""
            var content:String = ""
            let dumpType = params.type
            let description = dumpType.description
            switch dumpType{
            case .gradient(let color, let text):
                gradient = color.name
                content = text ?? ""
            case .image(let item, let size):
                url = item
                height = Int(size.height)
                width = Int(size.width)
            case .textImage(let item, let text, let size):
                url = item
                content = text
                height = Int(size.height)
                width = Int(size.width)
            case .text(let text):
                content = text
            case .video(let item, let size):
                url = item
                height = Int(size.height)
                width = Int(size.width)
            case .textVideo(let item, let text, let size):
                url = item
                content = text
                height = Int(size.height)
                width = Int(size.width)
            default:break
            }
            let dimensions = ["height":height,"width":width]
            let params:[String:Any] = ["dumpeeId":dumpeeId ?? "","content":content,"bgCode":gradient,"media":url,"socialShare":socialShare,"dumpType":description,"dimensions":dimensions,"locationName":locationName,"countryName":countryName,"isShowLocation":isShowLocation]
            ServerManager.shared.httpPost(request: kCreateDump, params: params,headers: ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DumpCreateModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        onSuceess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
        
    }
    //MARK:- Create Dumpee
    
    func createDumpee(categoryId:String = "",name:String =
        "",description:String = "",website:String,twitterAccount:String = "",image:String = "",dumpeeCatergory:String = "",onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        /*else if !name.isOnlyAlphanumeric{
         alertMessage =  FieldValidation.kDumpeeNameValidation
         }*/
       
        if name.isEmpty{
            alertMessage =  FieldValidation.kDumpeeNameEmpty
        }else if dumpeeCatergory.isEmpty{
            alertMessage = FieldValidation.kCategory
        }else if  description.count > 200{
            alertMessage = FieldValidation.kDescriptionLength
        } else{
            if  ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                
                let webSiteUrl = !website.isEmpty ? "https://\(website)":website
                let params:[String:Any] = ["categoryId":categoryId ,"name":name,"description":description ,"website":"\(webSiteUrl)" ,"twitterAccount":"\(twitterAccount)","image":image]
                
                ServerManager.shared.httpPost(request: kCreateDumpee, params: params,headers: ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = responseData.JKDecoder(DumpeeDetailResponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            guard let dumpeeData = response.dumpeeData else{return}
                            self.dumpeeDetailModel = dumpeeData
                            onSuceess()
                            break
                        case 503:
                            onUserSuspend(response.message ?? "")
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
       }
    }
    
    //MARk: Edit Dump
 
    func editDump(dumpId:String = "",dumpeeId:String = "",params:DumpCreateParameter,locationName:String="",countryName:String = "",isShowLocation:Bool = true,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            var height:Int = 0
            var width:Int = 0
            let socialShare =  ["facebook", "twitter"]
            var content:String = ""
            var gradient:String = ""
            var url:String = ""
            
            let dumpType = params.type
            let description = dumpType.description
            switch dumpType{
            case .gradient(let color, let text):
                gradient = color.name
                content = text ?? ""
            case .image(let item, let size):
                url = item
                height = Int(size.height)
                width = Int(size.width)
            case .textImage(let item, let text, let size):
                url = item
                content = text
                height = Int(size.height)
                width = Int(size.width)
            case .text(let text):
                content = text
            case .video(let item, let size):
                url = item
                height = Int(size.height)
                width = Int(size.width)
            case .textVideo(let item, let text, let size):
                url = item
                content = text
                height = Int(size.height)
                width = Int(size.width)
            default:break
            }
            let dimensions = ["height":NSNumber(integerLiteral: height),"width":NSNumber(integerLiteral: width)]
            
            let params:[String:Any] = ["dumpeeId":dumpeeId,"dumpId":dumpId,"content":content,"bgCode":gradient,"media":url,"socialShare":socialShare,"dumpType":description,"locationName":locationName,"dimensions":dimensions,"countryName":countryName,"isShowLocation":isShowLocation]
            ServerManager.shared.httpPut(request: kEditDumps, params: params,headers: ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DumpCreateModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        onSuceess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
        
    }
    
    func uploadDumpMediaData(item:Any,folderType:AWSS3UploadFolder,completionHandler:@escaping(_ uploadUrl:String)->Void){
        if ServerManager.shared.CheckNetwork() {
            ServerManager.shared.showHud()
            AWSS3Manager.shared.upload(file: item, folderType: folderType, onProgress: {(progress) in}, onSuccess: {(url) in
                ServerManager.shared.hideHud()
                completionHandler(url)
            }, onError: { (error) in
                if error.code == -999{
                    return
                }

                ServerManager.shared.hideHud()
                alertMessage = error.localizedDescription
            })
            
        }
        
        
    }
    
}

extension DMFeedViewModel{
    func set(at indexPath:IndexPath,searchString:String = ""){
        self.categoryName = self.categoryList[indexPath.row].categoryName
        self.categoryId = self.categoryList[indexPath.row].categoryId
        self.searchString = searchString
    }
    func getDumpInfo()->(name:String,searchString:String,categoryId:String){
        return(self.categoryName ?? "",self.searchString ?? "",self.categoryId ?? "")
    }
    func setTopic(at indexPath:IndexPath,searchString:String = ""){
        self.topicName = self.dumpList[indexPath.row].topicName
        self.categoryId = self.dumpList[indexPath.row].categoryId
        self.categoryName = self.dumpList[indexPath.row].categoryName
        self.topicId = self.dumpList[indexPath.row].dumpeeId
    }
    func setHistoryTopic(at indexPath:IndexPath){
        self.topicName = self.suggestionList[indexPath.row].name
        //self.categoryId = self.suggestionList[indexPath.row].searchDumpeeId
        self.categoryName = self.suggestionList[indexPath.row].otherInfo?.catagory
        self.topicId = self.suggestionList[indexPath.row].searchUserId
    }
    
    func getTopicInfo()->(topicName:String,categoryId:String,categoryName:String,dumpeeIdForCategory:String){
        
        return(self.topicName ?? "",self.categoryId ?? "",self.categoryName ?? "",self.topicId ?? "")
    
    
    }
    
    func getDumpeeCategoryInfo()->(categoryId:String?,categoryName:String?,dumpeeName:String,dumpeeId:String) {
        return(self.dumpeeDetailModel?.dumpeeCategoryIdModel?.categoryId,self.dumpeeDetailModel?.dumpeeCategoryIdModel?.categoryName,self.dumpeeDetailModel?.dumpeeName ?? "",self.dumpeeDetailModel?.dumpeeId ?? "")
    }
    func set(objDumpbCategory:DumpeeDetailModel) {
         self.dumpeeDetailModel = objDumpbCategory
    }
    

    
    func setDumpeeDetail(objModel :DumpeeDetailModel?) {
        dumpeeDetailModel = objModel
        
        self.topicName = dumpeeDetailModel?.dumpeeName ?? ""
        self.categoryId = dumpeeDetailModel?.dumpeeCategoryIdModel?.categoryId ?? ""
        self.categoryName = dumpeeDetailModel?.dumpeeCategoryIdModel?.categoryName ?? ""
        self.topicId = dumpeeDetailModel?.dumpeeId ?? ""
        
        
      
    }
    func getDumpeeDetail()->DumpeeDetailModel?{
        return dumpeeDetailModel
    }
    
    
    func searchDumpeeInfo(at indexPath:IndexPath) -> (dumpeeId:String,dumpeeName:String) {
        if dumpList.count > 0 {
        let obj = dumpList[indexPath.row]
        return (obj.dumpeeId ?? "",obj.topicName ?? "")
        }
        return ("", "")
    }
}
