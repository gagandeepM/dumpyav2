//
//  SettingsViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

/* /* /*acceptedFollowRequestPushStatus = 0;
 commentPushStatus = 2;
 directMessagePushStatus = 2;
 directMessageRequestPushStatus = 0;
 dumpsPushStatus = 0;
 newFollowersPushStatus = 0;
 redumpPushStatus = 2;*/*/*/

class SettingsViewModel: NSObject {
 fileprivate var blockUser:[DMBlockUserInfo] = [DMBlockUserInfo]()
    func getSettingPrefrence(onSuccess:@escaping(_ profileData:Bool,_ redumpStatus:Int,_ commentStatus:Int,_ acceptedFollowRequestPushStatus:Int,_ directMessagePushStatus:Int,_ directMessageRequestPushStatus:Int,_ dumpsPushStatus:Int,_ newFollowersPushStatus:Int,_ commentControlStatus:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:kGetSettingsPrefrence, params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DMSettingModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let data = response.data else{return}
                      
        onSuccess(data.isPrivateProfile  ?? false,data.pushSetting?.redumpPushStatus ?? 1,data.pushSetting?.commentPushStatus ?? 1,data.pushSetting?.acceptedFollowRequestPushStatus ?? 0,data.pushSetting?.directMessagePushStatus ?? 0,data.pushSetting?.directMessageRequestPushStatus ?? 0,data.pushSetting?.dumpsPushStatus ?? 0,data.pushSetting?.newFollowersPushStatus ?? 0,data.commentControlStatus ?? 1)
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    //MARK:- Change Language
    
    
    
    func changeLanguage(languageKey:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        let params:[String:Any] =
            [
                "app_lang" : languageKey
                ]
        ServerManager.shared.httpPut(request:kLanguageChange, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                onSuccess()
                
                break
            case 503:
                 suspendMessage = response.message
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    
    func setSettingPrefrence(type:String,typeValue:Bool,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
         let params:[String:Any] =
                [
                    "type" : type,
                    "typeValue" : typeValue,
                    
            ]
            ServerManager.shared.httpPut(request:kSetSettingPrefrence, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                guard let status = response.status else{return}
                switch status{
                case 200:
                   
                    onSuccess()
                    break
                case 503:
                    onUserSuspend(response.message ?? "")
                    break
                default:
                    alertMessage = response.message ?? ""
                    onFailure()
                    break
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    
    func setPushNotificationSetting(type:String,typeValue:Int,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        let params:[String:Any] =
            [
                "type" : type,
                "typeValue" : typeValue,
                
                ]
        ServerManager.shared.httpPut(request:kSetSettingPrefrence, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    
    
    //MARK:- Unblock User
    
    
    func userUnblockRequest(blockUserId:String,blockType:Bool,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params:[String:Any] = ["blockUserId":blockUserId,"blockType":blockType]
            ServerManager.shared.httpPut(request: kBlockUnblockUser, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    
                    guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                    
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                       
                        onSuceess()
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    case 403:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    
    
    //MARK:- Clear Seacrh History
    
    
    
    func clearSearchHistory(onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
     
        ServerManager.shared.httpPut(request:kClearSeacrh, params: [:], headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
             onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    
    //MARK:- Linked Account
    func linkedAccount(isFacebookLinkedAccount:Bool = false,fbAccessToken:String = "",fbUserId:String = "",onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        let params:[String:Any] =
            [
                "type" : "isFacebookLinkedAccount",
                "typeValue" : isFacebookLinkedAccount ,
                "fbAccessToken" : fbAccessToken,
                "fbUserId" : fbUserId
                
                ]
        ServerManager.shared.httpPut(request:kSetSettingPrefrence, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                guard let user = response.user else{return}
                userModel = user
                onSuccess()
                break
            case 503:
                alertMessage = response.message ?? ""
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    //MARK:- Get Block Account
    func getBlockAccount(seacrh:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:kgetBlockAccount + "\(seacrh)"+"&page=0", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DMBlockAccountModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                       
                        guard let blockUser =  response.data else{return}
                        self.blockUser = blockUser.userBlockList
                        
                       
                            onSuccess()
                      
                       break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    func numberOfRows() -> Int {
        return blockUser.count
    }
    
    
    
    func cellForRowAt(at indexPath:IndexPath) -> DMBlockUserInfo {
        return blockUser[indexPath.row]
    }
    func unBlockUser(at indexPath:Int) -> String {
        return blockUser[indexPath].blockUserId ?? ""
    }
}
