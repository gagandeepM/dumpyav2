//
//  CommnetViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 26/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class CommentViewModel: NSObject {
 
 var objCommentDetail:[CommentDetailModel] = [CommentDetailModel]()
    fileprivate var objDumpfeedTimeLine:DMFeedTimeLineModel?
    fileprivate var commentId:String = ""
    func getComment(search:String = "",progressHud:Bool = false ,page:Int = 0,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        guard let model = objDumpfeedTimeLine else {
            return
        }
        if ServerManager.shared.CheckNetwork(){
            
            if progressHud{
                ServerManager.shared.showHud()
            }
            
            let url = kCommentGet + "\(search)&page=\(page)&dumpId=\(model.dumpFeedID ?? "")"
             guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(CommentResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let commentData  =  response.commentData else{return}
                        self.objCommentDetail =  commentData.commentList
                        onSuccess()
                         break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    
    func addComment(comment:String="",onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        let trimText = comment.trimWhiteSpace
        
        if trimText.isEmpty{
           alertMessage = FieldValidation.kCommentEmpty
        }else{
        guard let model = objDumpfeedTimeLine else {
            return
        }
        if  ServerManager.shared.CheckNetwork(){
           
            ServerManager.shared.showHud()
            
    let params:[String:Any] = ["commentMessage":comment ,"dumpId":"\(model.dumpFeedID ?? "")" ,"createdById":"\(model.userInfo?.userId ?? "")"]
            
            ServerManager.shared.httpPost(request: kAddComment, params: params,headers: ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DumpCreateModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        onSuceess()
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
        }
    }
    
    
    
    func reportComment(onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
         let params:[String:Any] =
            [
                "commentId":self.commentId,
                "reportType":"comment",
        ]
        ServerManager.shared.httpPost(request:kReportUser, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                alertMessage = response.message ?? ""
                onSuccess()
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    //MARK: Delete Comment
    func deleteComment(commentId: String, onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        
        ServerManager.shared.httpDelete(request:KDeleteComment+"\(commentId)", params: nil,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                //alertMessage = response.message ?? ""
                onSuccess()
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    

    func set(dumpFeedTimeLine obj:DMFeedTimeLineModel) {
        self.objDumpfeedTimeLine = obj
    }
    
    
    
    func getCommnetId(at index:IndexPath) {
        
        guard let commentId = objCommentDetail[index.row].commentId else { return}
        
       self.commentId =  commentId
    }
    func getCommId(at index:IndexPath) -> String {
        
        
        return objCommentDetail[index.row].commentId ?? ""
    }
    
}

extension CommentViewModel{
    
    func numberOfRows() -> Int {
        return objCommentDetail.count
    }
    
    func cellForRowAt(at indexPath:IndexPath) -> CommentDetailModel {
        return objCommentDetail[indexPath.row]
    }
    
    func didSelect(index:Int) -> String {
        return objCommentDetail[index].userInfo?.userId ?? ""
        
    }
    
   
}
