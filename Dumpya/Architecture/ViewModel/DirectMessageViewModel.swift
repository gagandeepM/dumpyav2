//
//  DirectMessageViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 17/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DirectMessageViewModel: NSObject {
    
    //MARK:- Chat Users List
    var userId:String = ""
    var conversationId:String = ""
    var requestConversationId:String = ""
    var totalRequestCount:Int = 0
    fileprivate var objDirectMessageModel:[DirectMessageModel] = [DirectMessageModel]()
    
    func getChatUsersList(isPull:Bool = false,isRequest:Bool = false,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            if !isPull{
                ServerManager.shared.showHud()
            }
            
            ServerManager.shared.httpGet(request:kChatUsersList + "\(isRequest)", params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DirectMessageResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        
                        guard let directMessageData = response.directMessageData else{return}
                        
                        self.totalRequestCount = directMessageData.totalRequestCount ?? 0
                        self.objDirectMessageModel  = directMessageData.directMessageModel
                        onSuccess()
                        break
                    case 503:
                        
                        suspendMessage = response.message ?? ""
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    func searchChat()
    {
        
    }
    func deleteConversationThread(onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            ServerManager.shared.httpDelete(request:kDeleteConversationThread+conversationId, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DeleteMessageResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        onSuccess()
                        break
                    case 503:
                        suspendMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    func acceptRejectMessageRequest(isAccepted:String = "",onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        let params:[String:Any] =
            [
                "conversationId" : requestConversationId,
                "requstAction":isAccepted]
        print(params)
        ServerManager.shared.httpPut(request:kAcceptRejectMessageRequest, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(messageRequestAcceptRejectResponse.self) else{return}
            guard let status = response.status else{return}
            guard let message = response.message else{return}
            switch status{
            case 200:
                onSuccess()
                break
            case 503:
                suspendMessage = message
                break
            default:
                alertMessage = message
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
}

extension DirectMessageViewModel{
    
    func numberOfRows() -> Int {
        
        return objDirectMessageModel.count
    }
    
    func cellForRowAt(indexPath:IndexPath) -> DirectMessageModel? {
        
        return objDirectMessageModel[indexPath.row]
    }
    
    func didSelect(index:Int)->(friendId:String,friendName:String){
        let obj = objDirectMessageModel[index]
        return (obj.userId ?? "" , obj.userName ?? "")
        //objChatViewModel.set(friendId: obj.userId ?? "", friendName: obj.userName ?? "")
    }
    func onDeleteConversationThread(indexPath:IndexPath)
    {
        self.conversationId =  objDirectMessageModel[indexPath.row].conversationId ?? ""
        
    }
    
    func onMessageRequest(index:Int)
    {
        self.requestConversationId =  objDirectMessageModel[index].conversationId ?? ""
        
    }
    
    func deleteParticularConversationThread(indexPath:IndexPath){
        objDirectMessageModel.remove(at: indexPath.row)
    }
    
}
