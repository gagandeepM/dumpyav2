//
//  StoriesViewModel.swift
//  Dumpya
//
//  Created by Chander on 23/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

class StoriesViewModel:NSObject{
    
   
   fileprivate var dataArray:[DMStoriesModelData]=[DMStoriesModelData]()
    
    func getList(onSuceess:@escaping()->Void,onFailure:@escaping()-> Void) {
        if ServerManager.shared.CheckNetwork(){
           ServerManager.shared.httpGet(request: "http://184.72.108.120/api/users/getMyStuds?limit=0&skip=0", params: nil, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    guard let response = responseData.JKDecoder(DMStoriesResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    print("THE STATUS IS-------->",status)
                    guard let totalCount = response.stories?.storiesData else{return}
                    self.dataArray = totalCount
                    onSuceess()
                }
            }, failureHandler: {(error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            }
        }
    func numberOfRow()->Int{
       return dataArray.count
    }
    func item(at indexPath:IndexPath)->DMStoriesModelData{
        return dataArray[indexPath.row]
    }
}
