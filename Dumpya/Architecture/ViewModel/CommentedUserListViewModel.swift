//
//  CommentedUserListViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 21/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
class CommentedUserListViewModel: NSObject {
    
    
    fileprivate var dumpId:String = ""
    
    fileprivate var commentedUserInfo:[CommentedUserInfoModel] = [CommentedUserInfoModel]()
    
    //MARK:- Redumped Users List
    
    func getCommentedUsersList(page:Int = 0,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            ServerManager.shared.httpGet(request:kCommentedUsersList + "\(page)" + "&dumpId=\(self.dumpId)", params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(CommentedUserListResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let commentedUser  =  response.commentedUserList else{return}
                        self.commentedUserInfo = commentedUser.commentedUserInfo
                        onSuccess()
                        break
                    case 503:
                        
                        suspendMessage = response.message ?? ""
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
}
extension CommentedUserListViewModel{
    
    func numberOfRows() -> Int {
        
        return commentedUserInfo.count
    }
    
    func cellForRowAt(indexPath:IndexPath) -> CommentedUserInfoModel {
        
        return commentedUserInfo[indexPath.row]
    }
    
    func didSelectRedumpUser(at:Int) -> String {
        
        
        
        let userId = commentedUserInfo[at].userId
        return userId ?? ""
    }
    
    
    func setFollowingDumpId(dumpId:String)  {
        
        self.dumpId = dumpId
        
    }
    
    
    
}
