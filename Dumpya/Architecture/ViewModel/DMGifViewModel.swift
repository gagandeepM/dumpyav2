//
//  DMGifViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 06/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
import GiphyCoreSDK
class DMGifViewModel:NSObject
{
    
    func removeAll()
    {
        self.gifUrlImages.removeAll()
    }
    
    //fileprivate var gifUrlImages:[String] = [String]()
    fileprivate var gifUrlImages:[DMGifModel] = [DMGifModel]()
    var serviceType:DMServiceType = .none
    fileprivate var dmGifModel:DMGifModel?
    func getTrendingGif(offset:Int = pageNumber,limit:Int = 25,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        
        if ServerManager.shared.CheckNetwork(){
            
//            if  serviceType == .none{
//
//            }
               ServerManager.shared.showHud()
            GiphyCore.shared.trending(offset: offset, limit: limit) { (response, error) in
                
                if let error = error as NSError? {
                    DispatchQueue.main.async {
                        print(error)
                        alertMessage = error.localizedDescription
                        onFailure()
                    }
                }else if let response = response {
                    let message = response.meta.msg
                    let status = response.meta.status
                    switch status{
                    case 200:
                        DispatchQueue.main.async {
                              ServerManager.shared.hideHud()
                            guard let giflist = response.data,let pagination = response.pagination else{
                                DispatchQueue.main.async {
                                       ServerManager.shared.hideHud()
                                    alertMessage = "No data found"
                                    onFailure()
                                }
                                return
                            }
                            
                            if self.serviceType == .none{
                                ServerManager.shared.hideHud()
                                self.gifUrlImages.removeAll()
                               
                            }
                            //let list = giflist.map({ (media) -> String in
                            for media in giflist{
                                let model = DMGifModel.init(gifUrl: media.images?.downsizedMedium?.gifUrl, gifHeight:  media.images?.downsizedMedium?.height ?? 0, gifWidth: media.images?.downsizedMedium?.width ?? 0)
                           
                                self.gifUrlImages.append(model)
                            }
                                //return (media.images?.downsizedMedium?.gifUrl)!
                            //})
                            //po giflist[13].images?.fixedHeight?.height
//                            self.gifUrlImages.append(list)
                            
                            self.gifUrlImages =  self.gifUrlImages.unique
                            totalRecords = pagination.totalCount
                        self.serviceType =  self.gifUrlImages.count<totalRecords ? .pagging : .none
                            onSuccess()
                            
                    }
                   
                    default:
                        DispatchQueue.main.async {
                            alertMessage = message
                             ServerManager.shared.hideHud()
                            onFailure()
                        }
                    break
                    }
                    
                    
                }
                
            }
        }
        
}
    func getGifBySearch(searchText:String = "",offset:Int = pageNumber,limit:Int = 25,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            
            if  serviceType == .none{
                ServerManager.shared.showHud()
            }
          
           GiphyCore.shared.search(searchText,offset: offset, limit: limit) { (response, error) in
                
                if let error = error as NSError? {
                    DispatchQueue.main.async {
                        print(error)
                        alertMessage = error.localizedDescription
                        onFailure()
                    }
                }else if let response = response {
                    let message = response.meta.msg
                    let status = response.meta.status
                    switch status{
                    case 200:
                        DispatchQueue.main.async {
                            
                            ServerManager.shared.hideHud()
                            guard let giflist = response.data,let pagination = response.pagination else{
                                DispatchQueue.main.async {
                                    ServerManager.shared.hideHud()
                                    alertMessage = "No data found"
                                    onFailure()
                                }
                                return
                            }
                            
                            if self.serviceType == .none{
                                ServerManager.shared.hideHud()
                                self.gifUrlImages.removeAll()
                                
                            }
//                         let list = giflist.map({ (media) -> String in
//                            print("Images-----\(media.images?.downsizedMedium?.gifUrl)")
//                                return (media.images?.downsizedMedium?.gifUrl)!
//                            })
                           
                           // self.gifUrlImages.append(list)
                            
                            for media in giflist{
                                let model = DMGifModel.init(gifUrl: media.images?.downsizedMedium?.gifUrl, gifHeight:  media.images?.downsizedMedium?.height ?? 0, gifWidth: media.images?.downsizedMedium?.width ?? 0)
                                self.gifUrlImages.append(model)
                            }
                            
                           self.gifUrlImages =  self.gifUrlImages.unique
                            totalRecords = pagination.totalCount
                            self.serviceType =  self.gifUrlImages.count<totalRecords ? .pagging : .none
                            onSuccess()
                            
                        }
                        
                    default:
                       

                        DispatchQueue.main.async {
                            alertMessage = message
                            ServerManager.shared.hideHud()
                            onFailure()
                        }
                        break
                    }
                    
                    
                }
                
            }
        }
        
    }
}
    extension DMGifViewModel{
        
        func numberOfRows() -> Int {
            return gifUrlImages.count
        }
        
//        func cellForRowAt(at indexPath:IndexPath) -> String {
//            let ind  =  indexPath.row
//            return gifUrlImages[ind]
//
//        }
        
//        func didSelectAtGifImage(at index:IndexPath) -> String {
//            let ind  =  index.row
//            return gifUrlImages[ind]
//
//
//        }
        
        func cellForRowAt(at indexPath:IndexPath) -> DMGifModel {
            let ind  =  indexPath.row
            return gifUrlImages[ind]
            
        }

        func didSelectAtGifImage(at index:IndexPath) -> DMGifModel {
            let ind  =  index.row
            return gifUrlImages[ind]
            
            
        }
       
}
