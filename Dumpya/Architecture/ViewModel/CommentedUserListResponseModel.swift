//
//  CommentedUserListResponseModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 21/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation

struct CommentedUserListResponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let commentedUserList:CommentedUserListModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case commentedUserList    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        commentedUserList =  values.contains(.commentedUserList) == true ? try values.decodeIfPresent(CommentedUserListModel.self, forKey: .commentedUserList) : nil
        
    }
}

struct CommentedUserListModel:Mappable {
    let totalCount : Int?
    var commentedUserInfo:[CommentedUserInfoModel] = [CommentedUserInfoModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount   = "totalCount"
        case commentedUserInfo     = "userList"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let redumpedUserInfo =  try values.decodeIfPresent([CommentedUserInfoModel].self, forKey: .commentedUserInfo) else{return}
        self.commentedUserInfo = redumpedUserInfo
        
    }
}

struct CommentedUserInfoModel:Mappable {
    
    var userId:String?
    var firstName:String?
    var lastName:String?
    var userName:String?
    var profilePic:String?
    
    
    enum CodingKeys:String,CodingKey {
        case userId = "userId"
        case firstName = "firstName"
        case lastName = "lastName"
        case userName = "userName"
        case profilePic = "profilePic"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        firstName =   try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        userName =   try values.decodeIfPresent(String.self, forKey: .userName)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        
        
    }
}
