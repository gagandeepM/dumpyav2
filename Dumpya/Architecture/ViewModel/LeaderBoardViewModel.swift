//
//  LeaderBoardViewModel.swift
//  Dumpya
//
//  Created by Chandan on 02/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class LeaderBoardViewModel: NSObject {

    fileprivate var objLeaderBoardModel:[LeaderBoardModelData] = [LeaderBoardModelData]()
     fileprivate var objDMCategoryModel:[DMCategoryModel] = [DMCategoryModel]()
    

    func getLeaderBoard(isPull:Bool = false,search:String,categoryId:String,dayType:String,progressHud:Bool = false ,countryName:String="",page:Int = 0,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            if !isPull{
                ServerManager.shared.showHud()
            }
            
            let url = kLeaderBoard + "\(categoryId)&dayType=\(dayType)&countryName=\(countryName)&searchString=\(search)"
            guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(LeaderBoardResponseModel.self) else{return}
                    
                    guard let status = response.statusCode else{return}
                    
                    switch status{
                    case 200:
                        guard let leaderBoardData  =  response.leaderModel else{return}
                self.objLeaderBoardModel =   leaderBoardData.leaderBoardData
                self.objDMCategoryModel =    leaderBoardData.categoryList
                        
                        onSuccess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
}

extension LeaderBoardViewModel{
    func numberOfRows() -> Int {
        return objLeaderBoardModel.count
    }
    
    func numberOfRowsForCategory() -> Int {
        return objDMCategoryModel.count
    }
    func cellForRowAt(at indexPath:IndexPath) -> LeaderBoardModelData {
        
        return objLeaderBoardModel[indexPath.row]
    }
    func cellForRowAtForCategory(at indexPath:IndexPath) -> DMCategoryModel {
        
        return objDMCategoryModel[indexPath.row]
    }
    
    func dumpeeLeaderBoardInfo(at index:Int) -> (dumpeeId:String,dumpeeName:String) {
        let obj = objLeaderBoardModel[index]
        return (obj.dumpeeId ?? "",obj.dumpeeName ?? "")
    }
    
}
