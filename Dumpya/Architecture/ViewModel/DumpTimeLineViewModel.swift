//
//  DumpTimeLineViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 25/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
class DumpTimeLineViewModel:NSObject{
    typealias  DumpTimeLineModelResult = (userId:String,dumpeeId:String,dumpeeName:String, dumpFeedId:String,source:String,newsUrl:String)
    fileprivate var userid:String = ""
    fileprivate var dumpeeId:String = ""
    fileprivate var dumpeeName:String = ""
    fileprivate  var feedTimelines:[DMFeedTimeLineModel] = [DMFeedTimeLineModel]()
    fileprivate var categoryTimelines:[DMCategoryTimeLineModel] = [DMCategoryTimeLineModel]()
    var objSetting = SettingsViewModel()
    var serviceType:DMServiceType = .none
    var screenApiType:DumpScreenFuncType = .history
    func removeObjects(){
        if self.feedTimelines.count>0 {
            self.feedTimelines.removeAll()
        }
        if self.categoryTimelines.count>0 {
            self.categoryTimelines.removeAll()
        }
    }
    
    
    //MK Put totalcount in onSuccess return to get the individual records on each screen
    //    func getTimeLineDump(refreshControl:UIRefreshControl = UIRefreshControl(),userId:String = "",page:Int = pageNumber,search:String = "",searchType:String = "normal",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
    func getTimeLineDump(refreshControl:UIRefreshControl = UIRefreshControl(),userId:String = "",page:Int = pageNumber,search:String = "",searchType:String = "normal",onSuccess:@escaping(_ totalRecords:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        print(pageNumber,searchType)
        if ServerManager.shared.CheckNetwork(){
            if !refreshControl.isRefreshing || serviceType == .none {
               // ServerManager.shared.showHud()
            }
            let url = "\(kTimeLineDump)" + "page=\(page)" + "&userId=\(userId)" + "&searchText=\(search)" + "&dumpeeId=\(dumpeeId)"+"&searchType=\(searchType)"
                     guard let urlString = url.urlQueryAllowed else{return}
            ServerManager.shared.httpGet(request:urlString, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if !refreshControl.isRefreshing || self.serviceType == .none{
                        ServerManager.shared.hideHud()
                    }
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    guard let response = response.JKDecoder(DMDumpTimeLineResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                         guard let dumpData  =  response.dumpData,let currentLanguage = dumpData.defaultLanguage else{return}
                         
                         if Language.language.rawValue != currentLanguage
                         {
                            guard let language  = Language(rawValue: currentLanguage) else{return}
                            ChangeLanguageAlert.showAction(language: language, onCompletion: {
                                LocalNotification.sent(message:
                                    "\(kAppTitle) " + "thisPhone".localized + " \(language.name). " + "tapToOpen".localized
                                    
                                )
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    exit(0)
                                    
                                }
                            })
                            //Language.language = language
                           
                         }
                        
                         if dumpData.feedTimeLine.count>0{
                            //default histoy
                            
                            if self.serviceType == .pagging{
                                
                                self.feedTimelines.append(contentsOf: dumpData.feedTimeLine)
                                
                            }else{
                                self.feedTimelines.removeAll()
                                self.feedTimelines = dumpData.feedTimeLine
                            }
                         }else{
                            self.serviceType = .none
                         }
                         let totalRecords = dumpData.totalCount ?? 0
                         self.serviceType = self.feedTimelines.count<totalRecords ?.pagging : .none
                         onSuccess(totalRecords)
                         
                      
                        break
                    case 503:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        if refreshControl.isRefreshing{
                            refreshControl.endRefreshing()
                        }
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    if refreshControl.isRefreshing{
                        refreshControl.endRefreshing()
                    }
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }else{
            if refreshControl.isRefreshing{
                refreshControl.endRefreshing()
            }
        }
    }
    
    
    //MARK: Profile dupfeed data
    //MK Put totalcount in onSuccess return to get the individual records on each screen
    //    func getTimeLineDumpForProfile(isPull:Bool = false,userId:String = "",page:Int = pageNumber,search:String = "",searchType:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
    func getTimeLineDumpForProfile(isPull:Bool = false,userId:String = "",page:Int = pageNumber,search:String = "",searchType:String = "",onSuccess:@escaping(_ totalRecords:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if ServerManager.shared.CheckNetwork(){
            if !isPull || serviceType == .none {
                ServerManager.shared.showHud()
            }
            
            ServerManager.shared.httpGet(request:kTimeLineDumpForProfile + "page=\(page)" + "&userId=\(userId )" + "&searchText=\(search)" + "&dumpeeId=\(dumpeeId)"+"&searchType=\(searchType)", params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if !isPull || self.serviceType == .none {
                        ServerManager.shared.hideHud()
                    }
                    guard let response = response.JKDecoder(DMDumpTimeLineResponse.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let dumpData  =  response.dumpData else{return}
                        if self.serviceType == .pagging{
                            
                            //MK added check for page = 0
                            //self.feedTimelines.append(contentsOf: dumpData.feedTimeLine)
                            if page == 0{
                                self.feedTimelines.removeAll()
                                self.feedTimelines = dumpData.feedTimeLine
                            }
                            else{
                                self.feedTimelines.append(contentsOf: dumpData.feedTimeLine)
                            }
                            
                        }else{
                            self.feedTimelines.removeAll()
                            self.feedTimelines = dumpData.feedTimeLine
                        }
                        
                        let totalRecords = dumpData.totalCount ?? 0
                        self.serviceType = self.feedTimelines.count<totalRecords ?.pagging : .none
                        
                        onSuccess(totalRecords)
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    func redumpUser(dumpFeedId: String,countryName:String = "",locationName:String = "",onSuccess : @escaping(_ redumpStatus:Bool,_ redumpCount:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
         
        let params:[String:Any] =
            [
                "dumpId":dumpFeedId,
                "countryName":countryName,
                "locationName":locationName
        ]
        print(params)
      
        ServerManager.shared.httpPut(request:kRedump, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
              ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                onSuccess(response.reDumpData?.isRedumpedByMe ?? false, response.reDumpData?.redumpCount ?? 0)
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    
    func editDump(dumpFeedId: String,onSuccess : @escaping(_ redumpStatus:Bool,_ redumpCount:Int)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        let params:[String:Any] =
            [
                "dumpId":dumpFeedId,
                //"dumpeeId":dumpeeId
        ]
        ServerManager.shared.httpPut(request:kEditDump, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                onSuccess(response.reDumpData?.isRedumpedByMe ?? false, response.reDumpData?.redumpCount ?? 0)
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    
    //MARK:- Delete Dump
    func remove(at index:Int)
    {
        self.feedTimelines.remove(at: index)
    }
    func deleteDump(dumpFeedId: String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        ServerManager.shared.httpDelete(request:kDeleteDump+"\(dumpFeedId)", params: nil, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DumpDeleteResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    //MARK:-Current Location
    
    
    func checkCurrentLocation(locationName:String,countryName:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void)
    {
        
        let params:[String:Any] =
            [
                "locationName":locationName,
                "countryName":countryName
        ]
        ServerManager.shared.httpPut(request:KUpdateLocation,params:params, headers: ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                guard let status = response.status else{return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    print(countryName)
                    UserDefaults.standard.set(countryName, forKey: "currentLocation")
                    onSuccess()
                    break
                case 503:
                    
                    suspendMessage = message
                    break
                default:
                    alertMessage = response.message ?? ""
                    onFailure()
                    break
                }
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    
    
    //MARK:- Report Dump
    
    
    func reportDump(dumpId:String,userId:String,reportType:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        
        let params:[String:Any] =
            [
                "dumpId":dumpId,
                "reportType":reportType,
                "userId":userId
        ]
        ServerManager.shared.httpPost(request:kReportUser, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                //                alertMessage = response.message ?? ""
                onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
}


extension DumpTimeLineViewModel{
    
    func numberOfRows() -> Int {
        return feedTimelines.count
    }
    
    
    func cellForRowAt(at indexPath:IndexPath) -> DMFeedTimeLineModel {
        let ind  =  indexPath.row
        return feedTimelines[ind]
        
    }
    
    
    func didSelectAtRedump(at index:Int) -> DMFeedTimeLineModel {
        let ind  =  index
        return feedTimelines[ind]
        
        
    }
    func didUpdateSize(at index:Int, imageSize size:CGSize){
        let ind  =  index
        var info  = feedTimelines[ind].imageinfo
        info?.width = Int(size.width)
        info?.height = Int(size.height)
        feedTimelines[ind].imageinfo = info
    }
    func shouldUpdateSize(at index:Int)->Bool{
        let ind  =  index
        return feedTimelines[ind].imageinfo?.imageSize == .zero ? false : true
    }
    
    func dumpTimeLineInfo(at index:Int) -> (userId:String,dumpeeId:String,dumpeeName:String, dumpFeedId:String,source:String,newsUrl:String,media:String,content:String) {
        let obj = feedTimelines[index]
        return (obj.userInfo?.userId ?? "",obj.dumpeeId ?? "",obj.topicName ?? "", obj.dumpFeedID ?? "",obj.source ?? "",obj.newsUrl ?? "",obj.media ?? "",obj.content ?? "")
    }
    
    
    func dumpeeInfo(at indexPath:Int) -> (type:DumpFeedType,topicName:String,dumpeeId:String,bgCode:String,content:String,media:String,width:Int,height:Int,dumpFeedID:String,categoryName:String,locationName:String,isShowLocation:Bool,videoThumbnail:String) {
        
        var locationName = feedTimelines[indexPath].locationName ?? ""
        let obj  = feedTimelines[indexPath]
        locationName = locationName.isEmpty ? obj.userInfo?.locationName ?? "" :obj.locationName ?? ""
        let width  = obj.imageinfo?.width ?? 0
        let height = obj.imageinfo?.height ?? 0
        return (obj.feedType ,obj.topicName ?? "",obj.dumpeeId ?? "",obj.bgCode ?? "",feedTimelines[indexPath].content ?? "",obj.media ?? "",width,height,obj.dumpFeedID ?? "",obj.categoryInfo?.categoryName ?? "",locationName,obj.isShowLocation ?? true,obj.videoThumbnail ?? "")
    }
    
    
    func didUpdateRedump(at indexPath:Int,isRedump:Bool = false,redumpCount:Int = 0) {
        feedTimelines[indexPath].isRedump = isRedump
        feedTimelines[indexPath].redumpCount = redumpCount
    }
    func redumpUpdate(indexPath:Int) -> DMFeedTimeLineModel{
        return feedTimelines[indexPath]
    }
    func deleteParticularDumpFeed(index:Int){
        feedTimelines.remove(at: index)
    }
    
    //MARK:- Read More Code
    
    func didUpdateReadMore(at index:Int,states:Bool = false) {
        feedTimelines[index].states = states
        
    }
    
    func set(userId:String = "", dumpeeId:String = "",dumpeeName:String = ""){
        self.userid = userId
        self.dumpeeId = dumpeeId
        self.dumpeeName = dumpeeName
    }
    func set(dumpeeNameLbl:UILabel){
        dumpeeNameLbl.text = self.dumpeeName
    }
}





