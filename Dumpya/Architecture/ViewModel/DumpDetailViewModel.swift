//
//  DumpDetailViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

class DumpDetailViewModel: NSObject{
    fileprivate var dumpDetail:DMFeedTimeLineModel?
    fileprivate var redumpDetail:DMDumpRedumpResponse?
     func getDumpDetail(dumpId:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:kDumpDetail+"\(dumpId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DumpDetailResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        self.dumpDetail = response.dumpData
                        onSuccess()
                        break
                        
                    case 503:
                        suspendMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }

    
    
    func reportDumpDetail(dumpId:String,userId:String,reportType:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        
        let params:[String:Any] =
            [
                "dumpId":dumpId,
                "reportType":reportType,
                "userId":userId
        ]
        ServerManager.shared.httpPost(request:kReportUser, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                alertMessage = response.message ?? ""
                onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    
    
    func deleteDumpDetail(dumpFeedId: String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        ServerManager.shared.httpDelete(request:kDeleteDump+"\(dumpFeedId)", params: nil, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DumpDeleteResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                onSuccess()
                break
                
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }
    
    
    func dumpDetailRedump(dumpFeedId: String = "",countryName:String = "",locationName:String = "",onSuccess : @escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        let params:[String:Any] =
            [
                "dumpId":dumpFeedId,
                "countryName":countryName,
                "locationName":locationName
        ]
        ServerManager.shared.httpPut(request:kRedump, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(DMRedumpResponseModel.self) else{return}
            guard let status = response.status else{return}
            switch status{
            case 200:
                
                self.redumpDetail = response.reDumpData
                onSuccess()
                break
            case 503:
                onUserSuspend(response.message ?? "")
                break
            default:
                alertMessage = response.message ?? ""
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
        
    }

}

extension DumpDetailViewModel{
    
    func dumpDetailInfo() -> DMFeedTimeLineModel? {
  
        return dumpDetail
    }
    
    func didSelect() -> DMFeedTimeLineModel? {
        return dumpDetail
    }
    
    func didSelectDumpDetailRedump() -> DMDumpRedumpResponse? {
        return redumpDetail
    }

    
    func dumpeeDetailInfo() -> DMFeedTimeLineModel? {
        return dumpDetail
    }
    
}
