//
//  FollowersViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 23/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class FollowersViewModel: NSObject {
    
    func removeAll()
    {
        if(self.followers.count>0){
            self.followers.removeAll()
        }
        if(self.requested.count>0){
            self.requested.removeAll()
        }
        if(self.facebookFollower.count>0){
            self.facebookFollower.removeAll()
        }
        
        
    }
    fileprivate var followers:[FollowFollowersModel] = [FollowFollowersModel]()
    fileprivate var requested:[FollowFollowersModel]=[FollowFollowersModel]()
    fileprivate var facebookFollower:[FollowFollowersModel]=[FollowFollowersModel]()
    fileprivate var objFollowerModel:FollowFollowersModel?
    fileprivate var followerId:String = ""
    fileprivate var requestUserId:String = ""
    
     var serviceType:DMServiceType = .none
    func getFollowFollwers(search:String = "",userId:String = "",page:Int = 0, type:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            if search.isEmpty
            {
                ServerManager.shared.showHud()
            }
            
            ServerManager.shared.httpGet(request:kFollwers+"searchString=\(search)&page=\(page)&type=\(type)&userId=\(userId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(FollowResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        guard let followers =  response.data else{return}
                        
                        if self.serviceType == .pagging{
                            
                            self.followers.append(contentsOf: followers.followFollowerList)
                            
                        }else{
                            self.removeAll()
                            self.followers = followers.followFollowerList
                        }
                        totalRecords = response.data?.totalCount ?? 0
                        self.serviceType = self.followers.count<totalRecords ?.pagging : .none
                        onSuccess()
                        
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    func getMutualFollowFollwers(search:String = "",userId:String = "",page:Int = 0, type:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:KMutualFollowing+"searchString=\(search)&page=\(page)&type=\(type)&userId=\(userId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(FollowResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        guard let followers =  response.data else{return}
                        
                        if self.serviceType == .pagging{
                            
                            self.followers.append(contentsOf: followers.followFollowerList)
                            
                        }else{
                            self.removeAll()
                             self.followers = followers.followFollowerList
                        }
                        totalRecords = response.data?.totalCount ?? 0
                        self.serviceType = self.followers.count<totalRecords ?.pagging : .none
                       
                        onSuccess()
                        
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    func sendFollowerRequest(onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params:[String:Any] = ["userId":followerId]
            ServerManager.shared.httpPost(request: kSendFollowRequest, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    
                    guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                    
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        onSuceess()
                        break
                    case 403:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    //Unfollow User
    
    
    func sendUnfollowRequest(type:String = "",onSuceess:@escaping() -> Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let userId:String = followerId
            let params:[String:Any] = ["userId":userId,"type":type]
            ServerManager.shared.httpPut(request: kSendUnfollowRequest, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    
                    guard let response = responseData.JKDecoder(DMRequestFollow.self) else{return}
                    
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        onSuceess()
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    case 403:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    //MARK:- Requested Api
    
    
    func getRequesteded(search:String = "",userId:String = "",page:Int = 0, type:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:kRequested+"\(search)&page=\(page)&userId=\(userId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(FollowResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        
                        guard let followers =  response.data else{return}
                        self.requested = followers.followFollowerList
                        
                        onSuccess()
                        
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    
    //MARK:- Facebook Friends API
    
    
    
    func getFacebookFriend(userId:String = "",page:Int = 0, type:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:"\(kFacebookFriendList)\(page)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(FaceBookFollowResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let followers =  response.data else{return}
                        self.facebookFollower = followers.followFollowerList
                        
                        onSuccess()
                        
                        break
                        
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    
    //MARK:- Accept Reject Request
    
    
    func acceptRejectFollowRequest(isAccepted:Bool = false,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        let params:[String:Any] =
            [
                "userId" : requestUserId,
                "isAccepted":isAccepted]
        ServerManager.shared.httpPut(request:kAcceptRejectFollowRequest, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(RequestAcceptRejectResponseModel.self) else{return}
            guard let status = response.status else{return}
            guard let message = response.message else{return}
            switch status{
            case 200:
                onSuccess()
                break
            case 503:
                suspendMessage = message
                break
            default:
                alertMessage = message
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
}
extension FollowersViewModel{
    
    func numberOfRows() -> Int {
        return followers.count
    }
    
    func cellForRowAt(at indexPath:IndexPath) -> FollowFollowersModel {
        
        return followers[indexPath.row]
    }
    
    
    func numberOfRowsForFacebookFollower() -> Int {
        return facebookFollower.count
    }
    
    func cellForRowAtFacebookFollower(at indexPath:IndexPath) -> FollowFollowersModel {
        
        return facebookFollower[indexPath.row]
    }
    
    func numberOfRowsForRequested() -> Int {
        return requested.count
    }
    
    func cellForRowAtRequested(at indexPath:IndexPath) -> FollowFollowersModel {
        return requested[indexPath.row]
    }
    
    
    func selectedAt(index:Int){
        requestUserId = requested[index].userId ?? ""
    }
    
    func followFollowingInfo(at index:Int) ->  (userId:String,fullName:String) {
        let obj = followers[index]
        return (obj.userId ?? "",obj.fullName ?? "")
    }
    func requestedInfo(at index:Int) ->  (userId:String,fullName:String) {
        let obj = requested[index]
        return (obj.userId ?? "",obj.fullName ?? "")
    }
    func setFollowerId(at index:Int) {
        
        let obj = followers[index]
        self.followerId = obj.userId ?? ""
    }
    func setFollowedState(at index:Int)->FollowState? {
        let obj = followers[index]
        let value = obj.isFollowed ?? 0
        return FollowState(rawValue: value)
    }
    
    func setFacebookFollowedState(at index:Int)->FollowState? {
        let obj = facebookFollower[index]
        let value = obj.isFollowed ?? 0
        return FollowState(rawValue: value)
    }
    func setFacebookFollowerId(at index:Int) {
        
        let obj = facebookFollower[index]
        self.followerId = obj.userId ?? ""
    }
}
