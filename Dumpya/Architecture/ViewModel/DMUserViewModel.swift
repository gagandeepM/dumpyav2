//
//  LoginViewModel.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DMUserViewModel: NSObject {
    
    var profileModel:DMUserModel?{return userModel}
    var username:String{ return profileModel?.userName ?? ""}
    var firstname:String{ return profileModel?.firstName ?? ""}
    var lastname:String{return  profileModel?.lastName ?? ""}
    var email:String{return  profileModel?.email ?? ""}
    var socialType:String{return  profileModel?.socialType ?? ""}
    var profilePic:String{return profileModel?.profilePic ?? ""}
    var coverPic:String{return profileModel?.coverPic ?? ""}
    var locationName:String{return profileModel?.locationName ?? ""}
    var countryCode :String{return profileModel?.phoneNumber?.countryCode ?? ""}
     var countryName :String{return profileModel?.countryName ?? ""}
    var objSetting = SettingsViewModel()
    var countryobj :String{return profileModel?.phoneNumber?.countryObj ?? ""}
    var gender :String{return profileModel?.gender ?? ""}
    var mobileNumber :String{return profileModel?.phoneNumber?.phone ?? ""}
    var birthDate :Int{return profileModel?.birthDate ?? 0}
    var currentLocation:String{return profileModel?.currentLocation?.countryName ?? ""}
    var bio:String{return profileModel?.bio ?? ""}
    func set(userId:String){
    }
    var userId:String?
    
    //MARK:- Login
    func login(email:String,password:String,onSuceess:@escaping() -> Void,onFailure:@escaping()->Void) {
        if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else{
            if  ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["email":email,"password":password]
                ServerManager.shared.httpPost(request: klogin, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        
                        guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                        print("THE RESPONSE IS THE-------------->",response)
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            
                             guard let user = response.user,let currentLanguage = response.user?.defaultLanguage else{return}
                            if Language.language.rawValue != currentLanguage
                            {
                                guard let language  = Language(rawValue: currentLanguage) else{return}
                                ChangeLanguageAlert.showAction(language: language, onCompletion: {
                                    LocalNotification.sent(message:
                                        "\(kAppTitle) " + "thisPhone".localized + " \(language.name). " + "tapToOpen".localized
                                        
                                    )
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                        exit(0)
                                        
                                    }
                                })
                                //Language.language = language
                                
                            }
                           
                        
                            userModel = user
                           UserDefaults.standard.set(user.currentLocation?.countryName, forKey: "currentLocation")
                           
                            if let accessToken = user.accessToken{
                                UserDefaults.DMDefault(setObject: accessToken, forKey: kAuthTokenKey)
                            }
                            guard let userID = response.user?.userId else{return}
                            self.userId = userID
                            subscribeTopic = "\(kAppTitle)_\(userID)"
                            onSuceess()
                           
                        
                            break
                         
                        case 503:
                            alertMessage = response.message ?? ""
                            break
                            
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    //MARK:- Create Account
    func createAccount(firstName: String,lastName : String,userName : String,email : String,password : String,confirmPassword:String,socialType:DMSocialType = .default,genderType:DMGender = .default,onSuccess : @escaping(_ message:String)->Void,onFailure:@escaping()->Void) {
        
        if firstName.isEmpty{
            alertMessage = FieldValidation.kFirstNameEmpty
        }else if !firstName.isAlphabetWithSpace{
            alertMessage = FieldValidation.kFirstNameAlphabatics
        }else if firstName.count>25{
            alertMessage = FieldValidation.kFirstNameLength
        }else if lastName.isEmpty{
            alertMessage = FieldValidation.kLastNameEmpty
        }else if !lastName.isAlphabetWithSpace{
            alertMessage = FieldValidation.klastNameAlphabatics
        }else if lastName.count>25{
            alertMessage = FieldValidation.kLastNameLength
        }else if userName.isEmpty{
            alertMessage = FieldValidation.kUserName
        }
        else if userName.count<3 || userName.count>20{
            alertMessage = FieldValidation.kUserNameLength
        }else if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kEmailValid
        }else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.count < 8 || password.count > 20{
            alertMessage = FieldValidation.kPasswordLength
        }
//        else if !password.isValidPassword{
//            alertMessage = FieldValidation.kPasswordValidation
//        }
        else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPasswordEmpty
        }else if password != confirmPassword{
            alertMessage = FieldValidation.kMisMatchPasswordEmpty
        }else{
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"userName":userName,"email":email,"password":password,"socialType":"\(socialType.loginTypeSelect)","socialId":"","profilePic":"","gender":"\(genderType.genderType)"]
                ServerManager.shared.httpPost(request:ksignup, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        userModel = user
                        guard let token = user.accessToken else{return}
                        accessToken = token
                        guard let userID = response.user?.userId else{return}
                        self.userId = userID
                        guard let message = response.message else{return}
                        onSuccess(message)
                        break
                        
                    case 503:
                        
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    //MARK:- Login With Gmail
    func loginWithGmail(firstName: String,lastName : String,userName : String,email : String,socialType:DMSocialType = .google,genderType:DMGender = .default,socialId:String,profilePic:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        if socialId.isEmpty{
            alertMessage = FieldValidation.kGmailIdEmpty
        }else{
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                
                let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"userName":userName,"email":email,"socialType":"\(socialType.loginTypeSelect)","socialId":socialId,"profilePic":profilePic,"gender":"\(genderType.genderType)"]
                ServerManager.shared.httpPost(request:ksignup, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        userModel = user
                        guard let token = user.accessToken else{return}
                        accessToken = token
                        onSuccess()
                        break
                        
                    case 503:
                         alertMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    
    //MARK:- Login With Facebook
    
    
    func loginWithFacebook(firstName: String,lastName : String,email : String,socialType:DMSocialType = .facebook,genderType:DMGender = .default,socialId:String,profilePic:String,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        if socialId.isEmpty{
            alertMessage = FieldValidation.kFaceBookIdEmpty
        }else{
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"email":email,"socialType":"\(socialType.loginTypeSelect)","socialId":socialId,"profilePic":profilePic,"gender":"","userName":""]
                ServerManager.shared.httpPost(request:ksignup, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
                    ServerManager.shared.hideHud()
                    guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        userModel = user
                        guard let token = user.accessToken else{return}
                        accessToken = token
                        onSuccess()
                        break
                        
                    case 503:
                         alertMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    
    //MARK:- Forgot Password
    
    func forgotPassword(email:String,onSuccess:@escaping(_ message:String?)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kEmailValid
        }else{
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["email":email]
                ServerManager.shared.httpPost(request:kForgotPassword, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            guard let userID = response.user?.userId else{return}
                            self.userId = userID
                            onSuccess(response.message ?? "")
                            break
                            
                        case 503:
                            onUserSuspend(response.message ?? "")
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                    
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    
    //MARK:- Resend Otp After Login and SignUp
    
    
    func resendOTP(onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        guard let userid = userModel?.userId else{return}
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params:[String:Any] = ["userId":userid]
            ServerManager.shared.httpPut(request:  kresendOtpSignup, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                     alertMessage = response.message ?? ""
                        onSuccess()
                        break
                        
                    case 503:
                         alertMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    
    //MARK:- Verify OTP
    
    func verifyOTP(otp:String,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if otp.isEmpty{
            alertMessage = FieldValidation.kVerificationCodeEmpty
        }else{
            guard let userid = self.userId else{return}
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":userid,"otp":otp]
                ServerManager.shared.httpPost(request:kVerifyOtp, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                             alertMessage = response.message ?? ""
                            onSuccess()
                            break
                        case 503:
                            
                            onUserSuspend(response.message ?? "")
                             alertMessage = response.message ?? ""
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    //Mark:- Verify OTP LOGIN AND SIGNUP
    
    func verifyOTPLogin(otp:String,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if otp.isEmpty{
            alertMessage = FieldValidation.kVerificationCodeEmpty
        }else{
            guard let userid = userModel?.userId else{return}
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":userid,"otp":otp]
                ServerManager.shared.httpPut(request:kVerifySignupOtp, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                             alertMessage = response.message ?? ""
                            
                            guard let user = response.user else{return}
                            userModel = user
                            guard let token = user.accessToken else{return}
                            accessToken = token
                            onSuccess()
                            break
                            
                        case 503:
                             onUserSuspend(response.message ?? "")
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    //MARK:- Check OTP
    
    func CheckOTP(otp:String,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if otp.isEmpty{
            alertMessage = FieldValidation.kVerificationCodeEmpty
        }else{
            guard let userid = self.userId else{return}
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":userid,"otp":otp ]
                ServerManager.shared.httpPost(request:kcheckOtp, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                             alertMessage = response.message ?? ""
                            onSuccess()
                            break
                        case 503:
                             alertMessage = response.message ?? ""
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    //MARK:- Reset Password
    func resetPassword(otp:String,confirmPassword:String,password:String,onSuccess:@escaping(_ message:String)->Void,onFailure:@escaping()->Void){
        if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.count < 8 || password.count > 20{
            alertMessage = FieldValidation.kPasswordLength
        }
//        else if !password.isValidPassword{
//            alertMessage = FieldValidation.kPasswordValidation
//        }
        else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPasswordEmpty}
        else if password != confirmPassword{
            alertMessage = FieldValidation.kMisMatchPasswordEmpty
        }else{
            guard let userid = self.userId else{return}
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":userid,"otp":otp,"password":password]
                ServerManager.shared.httpPost(request:kResetPassword, params: params,headers: ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            onSuccess(response.message ?? "")
                            break
                        case 503:
                             alertMessage = response.message ?? ""
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    
    //MARK:- Get User Profile
    func getProfileData( userId:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:kgetUserDetail + "\(userId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DMLoginReponseModel.self) else{return}
                    guard let status = response.status else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        
                        guard var user  =  response.user else{return}
                        if let  model = userModel{
                            user.userId = model.userId
                            //user.accessToken = model.accessToken
                            UserDefaults.DMDefault(set: user, forKey: kUserDataKey)
                            
                        } else{
                            UserDefaults.DMDefault(set: user, forKey: kUserDataKey)
                        }
                        onSuccess()
                        break
                    case 503:
                       suspendMessage = message
                         break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    //MARK:- Edit Profile
    
    func editProfile(firstName: String,lastName:String,userName : String = "",email : String,location:String,mobileNumber:String = "",birthDate:String,genderType:String = "",countryName:String,countryCode:String,latitude:Double,longitude:Double,coverPicUrl:String ,profilePicUrl:String,bio:String = "",timeInterval:Int64,countryNameFull:String,cityName:String = "",stateName:String = "",onSuccess : @escaping(_ message:String)->Void,onFailure:@escaping()->Void) {
        
        if firstName.isEmpty{
            alertMessage = FieldValidation.kNameEmpty
        }else if !firstName.isAlphabetWithSpace{
            alertMessage = FieldValidation.kNameAlphabatics
        }else if firstName.count>25{
            alertMessage = FieldValidation.kNameLength
        }else if userName.count<3 || userName.count>20{
            alertMessage = FieldValidation.kUserNameLength
        }
        else if location.isEmpty{
            alertMessage = FieldValidation.kLocationName
        }
        else if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kEmailValid
            
        }else if birthDate.isEmpty{
            alertMessage = FieldValidation.kBirthdate}
            
        else if  bio.count > 100{
            alertMessage = FieldValidation.kBio
        }
        else if !mobileNumber.isEmpty && mobileNumber.count<10 || mobileNumber.count>15{
            alertMessage = FieldValidation.kMobileNumberLength
        }
            
        else{
            let coordinates:[Double]  = [longitude,latitude]
            let phoneNumber:[String:Any] = ["countryCode" : countryCode,
                                            "countryObj"  : countryName,
                                            "phone"       : mobileNumber]
            
            let params:[String:Any] =
                [
                    "email" : email,
                    "firstName" : firstName,
                    "lastName" : lastName,
                    "userName" : userName,
                    "phoneNumber" : phoneNumber,
                    "locationName" : location,
                    "coordinates" : coordinates,
                    "coverPic" : coverPicUrl,
                    "profilePic" : profilePicUrl,
                    "gender" : genderType,
                    "birthDate" : timeInterval,
                    "bio" : bio,
                    "countryName":countryNameFull,
                    "cityName":cityName,
                    "stateName":stateName
            ]
            ServerManager.shared.httpPut(request:keditProfile, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                guard let status = response.status else{return}
                 guard let message = response.message else{return}
                switch status{
                case 200:
                    guard let user = response.user else{return}
                    userModel = user
                    onSuccess(message)
                    break
                case 503:
                  suspendMessage = message
                    break
                default:
                    alertMessage = message
                    onFailure()
                    break
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
    
    
    // MARK:- ImageUpload
   

    fileprivate func set(data:Any)->[MultipartData]{
        let dataFormate:DataFormate = .multipart
        let dataType:DataType!
        let uploadKey = "file"
        if let image = data as? UIImage {
            dataType = DataType.image(image: image, fileName: nil, uploadKey: uploadKey, formate: .jpeg(quality: .medium))
        }else if let url = data as? URL{
            dataType =  DataType.file(file: url, uploadKey: uploadKey)
        }else if let filePath = data as? String{
            dataType =  DataType.file(file: filePath, uploadKey: uploadKey)
        }else{
            return []
        }
        
        return [dataFormate.result(dataType: dataType) as! MultipartData]
    }
    func imageUpload(image:Any,onSuccess:@escaping(_ url:String)->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            
            ServerManager.shared.showHud()
            
            AWSS3Manager.shared.upload(file: image, folderType: .profile, onProgress: { (_) in}, onSuccess: { (uploadFileUrl) in
                 ServerManager.shared.hideHud()
                onSuccess(uploadFileUrl)
            }, onError: { (error) in
                if error.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                     onFailure()
                }
                })
            
        }
//            ServerManager.shared.showHud()
//            let array = self.set(data: image)
//            if array.isEmpty{
//                ServerManager.shared.hideHud()
//                return
//            }
//
//            ServerManager.shared.httpUpload(request: kUploadProfileImage, params:nil, multipartObject: array, successHandler: { (responseData:Data) in
//                DispatchQueue.main.async {
//                    ServerManager.shared.hideHud()
//                    guard let response = responseData.JKDecoder(DMReponseModel.self) else{return}
//                    guard let url = response.url else{return}
//                    guard let height = response.dimensions?.height else{return}
//                    guard let width = response.dimensions?.width else{return}
//                    onSuccess(url,height,width)
//                }
//            }, failureHandler: {(error) in
//                DispatchQueue.main.async {
//                    ServerManager.shared.hideHud()
//                    alertMessage = error?.localizedDescription
//                    onFailure()
//                }
//            })
//        }
    }
    
    //MARK:- Help
    
    
    func help(subject:String,description:String,onSuccess:@escaping(_ message:String)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        if description.isEmpty{
            alertMessage = FieldValidation.kHelpDescription
        }else{
            
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["subject":subject,"description":description]
                ServerManager.shared.httpPost(request:kHelpMessage, params: params,headers:ServerManager.shared.apiHeaders, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(DMHelpReponseModel.self) else{return}
                        guard let status = response.status else{return}
                        switch status{
                        case 200:
                            onSuccess(response.message ?? "")
                            break
                        case 503:
                            onUserSuspend(response.message ?? "")
                            break
                        default:
                            alertMessage = response.message ?? ""
                            onFailure()
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    if error?.code == -999{
                        return
                    }

                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        onFailure()
                    }
                })
            }
        }
    }
    
    
    //MARK:- Change Password
    
    func changePassword(oldPassword: String,password:String,confirmPassword:String,onSuccess : @escaping(_ message:String)->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void) {
        
        if oldPassword.isEmpty{
            alertMessage = FieldValidation.kOldPasswordEmpty
        }
        else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.count < 8 || password.count > 20{
            alertMessage = FieldValidation.kPasswordLength
        }
//        else if !password.isValidPassword{
//            alertMessage = FieldValidation.kPasswordValidation
//        }
        else if password == oldPassword{
            alertMessage = FieldValidation.kPasswordCompare
        }
        else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPasswordEmpty
        }else if password != confirmPassword{
            alertMessage = FieldValidation.kMisMatchPasswordEmpty
        }
        else{
            let params:[String:Any] =
                [
                    "oldPassword" : oldPassword,
                    "password" : password
            ]
            ServerManager.shared.httpPut(request:kChangePassword, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
                guard let status = response.status else{return}
                switch status{
                case 200:
                    guard let user = response.user else{return}
                    userModel = user
                    onSuccess(response.message ?? "")
                    break
                case 503:
                    onUserSuspend(response.message ?? "")
                    break
                default:
                    alertMessage = response.message ?? ""
                    onFailure()
                    break
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
    
}





