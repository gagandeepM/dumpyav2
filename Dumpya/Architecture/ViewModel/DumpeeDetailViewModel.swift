//
//  DumpeeDetailViewModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 22/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DumpeeDetailViewModel: NSObject {
    var userId:String?
    var dumpeeId:String?
    
    fileprivate var dumpeeCategoryIdModel:DumpeeCategoryIdModel?
    fileprivate var dumpeeDetailModel:DumpeeDetailModel?
    
    func getDumpeeDetail(isPull:Bool = false,onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
        guard let dumpeeId = self.dumpeeId else { return  }
        if ServerManager.shared.CheckNetwork(){
            
            if !isPull{
                ServerManager.shared.showHud()
            }
            ServerManager.shared.httpGet(request:kgetDumpeeDetail+"\(dumpeeId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DumpeeDetailResponseModel.self) else{return}
                    guard let status = response.status else{return}
                    switch status{
                    case 200:
                        guard let dumpeeData  =  response.dumpeeData else{return}
                        
                        self.dumpeeDetailModel = dumpeeData
                        self.userId = self.dumpeeDetailModel?.userId ?? ""
                        guard let dumpeeCategoryObj = dumpeeData.dumpeeCategoryIdModel else{return}
                        self.dumpeeCategoryIdModel = dumpeeCategoryObj
                        
                        onSuccess()
                        break
                    case 503:
                        onUserSuspend(response.message ?? "")
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }

func updateDumpee(updateDumpeeId:String = "",image:String = "",description:String = "",website:String = "",twitterAccount:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void,onUserSuspend:@escaping(_ message:String)->Void){
 
    if ServerManager.shared.CheckNetwork(){
 let webSiteUrl = !website.isEmpty ? "https://\(website)":website
        let params:[String:Any] = ["dumpeeId":updateDumpeeId,"image":image,"description":description ,"website":webSiteUrl,"twitterAccount":twitterAccount]
       
            ServerManager.shared.showHud()
        ServerManager.shared.httpPut(request:KUpdateDumpeeProfile, params:params,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                guard let response = response.JKDecoder(DumpeeDetailResponseModel.self) else{return}
                guard let status = response.status else{return}
                switch status{
                case 200:
                    onSuccess()
                    break
                case 503:
                    onUserSuspend(response.message ?? "")
                    break
                default:
                    alertMessage = response.message ?? ""
                    onFailure()
                    break
                }
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
}
}
extension DumpeeDetailViewModel {
    func categoryData() -> DumpeeDetailModel? {
        return dumpeeDetailModel
    }
    func set(dumpeeId:String){
        self.dumpeeId = dumpeeId
    }
    func setDumpeeDetail(dumpsCount:UILabel ,todayCount:UILabel ,allTimeCount:UILabel,coverImageView:UIImageView,dumpeeDescription:UILabel,dumpeeImage:UIImageView){
        dumpsCount.text = "\(self.dumpeeDetailModel?.dumpsInfo?.dumps ?? 0)"
        todayCount.text = "\(self.dumpeeDetailModel?.dumpsInfo?.today ?? 0)"
        allTimeCount.text = "\(self.dumpeeDetailModel?.dumpsInfo?.allTime ?? 0)"
        
        if let dumpeeName = self.dumpeeDetailModel?.dumpeeName
        {
            
            if self.dumpeeDetailModel?.description?.isEmpty ?? false
            {
                dumpeeDescription.text = "\(dumpeeName )"
            }
            else{
                dumpeeDescription.text = "\(self.dumpeeDetailModel?.description ?? "" )"
            }
            
            
            
        }
        if let coverfile = self.dumpeeDetailModel?.image  {
            var urlString = coverfile.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            dumpeeImage.loadImage(filePath: urlString ?? "")
            //dumpeeImage.loadImage(filePath: coverfile)
        }
        else
        {
            dumpeeImage.image = UIImage(named: "profile_placeholder")
        }
    }
   
}
