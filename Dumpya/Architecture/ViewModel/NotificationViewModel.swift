//
//  NotificationViewModel.swift
//  Dumpya
//
//  Created by Chandan on 23/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class NotificationViewModel: NSObject {
     var serviceType:DMServiceType = .none
    fileprivate var notificationList:[NotificationModel] = [NotificationModel]()
     fileprivate var requestUserId:String = ""
    fileprivate  func reload(){
        self.serviceType = self.notificationList.count<totalRecords ?.pagging : .none
    }
    func removeObjects(){
        self.removeNotificationList()
    
        
    }
    func removeNotificationList(){
        if notificationList.count>0 {
            self.notificationList.removeAll()
        }
    }
     func getNotification(isPull:Bool = false,page:Int=0,onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        
        if ServerManager.shared.CheckNetwork(){
           if !isPull || serviceType == .none {
                ServerManager.shared.showHud()
            }
            
            ServerManager.shared.httpGet(request:kNotificationList + "\(page)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    if !isPull || self.serviceType == .none {
                        ServerManager.shared.hideHud()
                    }
                   
                    guard let response = response.JKDecoder(NotificationResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                       
                    case 200:
                        //self.removeObjects()
                        guard let list = response.notificationData?.notifications else {return}
                         if list.count>0{
                        if self.serviceType == .pagging
                        {
                            self.notificationList.append(contentsOf: list)
                            print(self.notificationList.count)
                            
                        }
                        else
                        {
                            self.notificationList.removeAll()
                           self.notificationList =  list
                        }
                         }else{
                            self.serviceType = .none
                        }
                        totalRecords = response.notificationData?.totalCount ?? 0
                        print(totalRecords)
                        self.reload()
                        onSuccess()
                        
                        break
                        
                    case 503:
                        suspendMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    
    func acceptRejectNotificationFollowRequest(isAccepted:Bool = false,onSuccess : @escaping()->Void,onFailure:@escaping()->Void) {
        
        let params:[String:Any] =
            [
                "userId" : requestUserId,
                "isAccepted":isAccepted]
        print(params)
        ServerManager.shared.httpPut(request:kAcceptRejectFollowRequest, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(RequestAcceptRejectResponseModel.self) else{return}
            guard let status = response.status else{return}
            guard let message = response.message else{return}
            switch status{
            case 200:
                onSuccess()
                break
            case 503:
                suspendMessage = message
                break
            default:
                alertMessage = message
                onFailure()
                break
            }
        }, failureHandler: { (error) in
            if error?.code == -999{
                return
            }

            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                onFailure()
            }
        })
    }
    
    
    
    
    func deleteNotification(notificationId:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            
            
            ServerManager.shared.httpDelete(request:kDeleteNotification+notificationId, params:nil,headers:ServerManager.shared.apiHeaders,successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(DeleteMessageResponseModel.self) else{return}
                    guard let status = response.statusCode else{return}
                    switch status{
                    case 200:
                        onSuccess()
                        break
                    case 503:
                        suspendMessage = response.message ?? ""
                        break
                    default:
                        alertMessage = response.message ?? ""
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                if error?.code == -999{
                    return
                }

                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
        }
    }
   
    
    
    
}
extension NotificationViewModel{
    
    
    func numberOfRows() -> Int {
        return notificationList.count
    }
    
    func cellForRowAt(at indexPath:IndexPath) -> NotificationModel {
        return notificationList[indexPath.row]
    }
    
    func didSelect(at indexPath:IndexPath) -> NotificationModel {
        return notificationList[indexPath.row]
    }
    
    func selectedAt(index:Int){
        requestUserId = notificationList[index].senderInfoModel?.senderId ?? ""
    }
    func deleteParticularNotification(indexPath:IndexPath){
        notificationList.remove(at: indexPath.row)
    }
}
