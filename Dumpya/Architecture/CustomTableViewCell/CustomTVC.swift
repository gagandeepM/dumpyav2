
import UIKit
import ExpandableLabel

class CustomTVC: UITableViewCell {
    @IBOutlet weak   var onClickCommentPromotionalText: DMCardView!
    @IBOutlet weak  var onClickUserComment: DMCardView!
    @IBOutlet weak  var redumpClickable:DMCardView!
    @IBOutlet weak  var redumpPromotionalLbl: PaddingLabel!
    @IBOutlet weak  var userCommentLbl:PaddingLabel!
    @IBOutlet weak  var commentPromotionalLbl: PaddingLabel!
    @IBOutlet weak var labelCommentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelRedumpHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentDotsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentArrowHeightConstraint: NSLayoutConstraint!
    var didImageLoad: ((_ image: UIImage?)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
        
       
    }
}



class FeedMediaCell:CustomTVC{
    
    @IBOutlet weak  var readMoreLBL: UILabel!
    @IBOutlet weak  var readMoreBtn: UIButton!
    @IBOutlet weak var redumpCountBtnMedia: UIButton!
    weak var delegateFeedMedia: SwiftyTableViewCellDelegate?
    weak var tagDelegateSeacrhMedia:HashTagDelegateSearch?
    weak open var delegateReadMore: ExpandableLabelDelegate?
    weak var tagDelegateProfileMedia:HashTagDelegateProfile?
    weak var tagDetailFromUserProfile:SwiftyHashTagTableViewCellDelegate?
    weak var tagDelegateDumpeeDetailMedia:HashTagDelegateDumpeeDetail?
    @IBOutlet weak var commentMediaBtn: UIButton!
    @IBOutlet weak var dumpeeNameMediaBTN: UIButton!
    @IBOutlet weak var mediaRedumpBtn: UIButton!
    @IBOutlet weak var profileBtn: DMButton!
    @IBOutlet weak  var postTextMediaLBL: ActiveLabel!
    @IBOutlet weak  var imageTextSettingBtn: UIButton!
    @IBOutlet weak fileprivate var commentLBL: UILabel!
    @IBOutlet weak fileprivate var redumpLBL: UILabel!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak var dumpeeNameMediaBtn: UIButton!
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak fileprivate var feedImageView: UIImageView!
    @IBOutlet weak fileprivate var commentUserDetailShowBtn: UIButton!
    @IBOutlet weak fileprivate  var playBtn: UIButton!
    //    @IBOutlet weak fileprivate var commentArrorDetailShowBtn: UIButton!
    //    @IBOutlet weak var commentClickableHeightCons: NSLayoutConstraint!
    var didUpdateImage:(()->Void)?
    func cancelImageRequest(){
        feedImageView.cancelledTask()
    }
    
    var rowIndex:Int = 0{
        didSet{
            self.onClickCommentPromotionalText.tag = rowIndex
            self.onClickUserComment.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.readMoreBtn.tag = rowIndex
            self.dumpeeNameMediaBTN.tag = rowIndex
            self.mediaRedumpBtn.tag = rowIndex
            self.profileBtn.tag = rowIndex
            self.imageTextSettingBtn.tag = rowIndex
            self.commentMediaBtn.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.redumpCountBtnMedia.tag = rowIndex
        }
    }
    
    var objFeedTimeLine:DMFeedTimeLineModel?{
        didSet{
            
            guard let textPromotionalText = objFeedTimeLine?.feedPromotion else { return }
            redumpPromotionalLbl.attributedText = textPromotionalText.html2Attributed
            
            if !textPromotionalText.isEmpty{
                redumpPromotionalLbl.topInset = 5.0
                redumpPromotionalLbl.bottomInset = 5.0
                labelRedumpHeightConstraint.constant = 1
            }else{
                redumpPromotionalLbl.topInset = 0.0
                redumpPromotionalLbl.bottomInset = 0.0
                labelRedumpHeightConstraint.constant = 0
            }
            if let commentPromotional = objFeedTimeLine?.commentPromotionalText{
               
                
                if !commentPromotional.isEmpty{
                     commentPromotionalLbl.attributedText = commentPromotional.html2Attributed
                    userCommentLbl.text = objFeedTimeLine?.commentObj?.commentMessage
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 5.0
                    
                    
                    commentPromotionalLbl.topInset = 5.0
                    
                    commentPromotionalLbl.bottomInset = 5.0
                    
                    
                    labelCommentHeightConstraint.constant = 1
                    
                    commentDotsHeightConstraint.constant = 22
                    commentArrowHeightConstraint.constant = 0
                }else{
                     commentPromotionalLbl.text = ""
                     userCommentLbl.text = ""
                    labelCommentHeightConstraint.constant = 0
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 0.0
                    
                    
                    commentPromotionalLbl.topInset = 0.0
                    
                    commentPromotionalLbl.bottomInset = 0.0
                    commentDotsHeightConstraint.constant = 0
                    commentArrowHeightConstraint.constant = 0
                }
            }
            
            
            guard let sourceType = objFeedTimeLine?.source else {
                return
            }
            
            
            
            if sourceType == "news"{
                self.timeLBL.text = "\(self.objFeedTimeLine?.dumpTime ?? "") \u{2022} Dumped this "
                
                self.dumpeeNameMediaBtn.setTitle( "\(objFeedTimeLine?.topicName ?? "")", for: .normal)
                
            }else{
                self.timeLBL.text = "\(self.objFeedTimeLine?.dumpTime ?? "") \u{2022} Dumped "
                
                self.dumpeeNameMediaBtn.setTitle( "\(objFeedTimeLine?.topicName ?? "")", for: .normal)
                dumpeeNameMediaBtn.isUserInteractionEnabled = true
            }
            
            
            postTextMediaLBL.text = objFeedTimeLine?.content ?? ""
            
            if let states = objFeedTimeLine?.states{
                if states{
                    if postTextMediaLBL.countLines>3{
                        postTextMediaLBL.numberOfLines = 3
                        readMoreLBL.isHidden = false
                        readMoreBtn.isHidden = false
                    }else{
                        postTextMediaLBL.numberOfLines = 3
                        readMoreLBL.isHidden = true
                        readMoreBtn.isHidden = true
                    }
                }
                    
                else{
                    readMoreLBL.isHidden = true
                    readMoreBtn.isHidden = true
                    postTextMediaLBL.numberOfLines = 0
                }
                self.layoutIfNeeded()
            }
            
            
            
            userNameLBL.text = "\(objFeedTimeLine?.userInfo?.userName ?? "")"
            postTextMediaLBL.handleHashtagTap { (hashtag) in
                
                self.delegateFeedMedia?.swiftyTableViewCellDidTapHeart(self,hashName: hashtag)
                self.tagDelegateSeacrhMedia?.hashTagMediaCell(self, hashName: hashtag)
                self.tagDelegateProfileMedia?.hashTagMediaProfileCell(self, hashName: hashtag)
                self.tagDelegateDumpeeDetailMedia?.hashTagMediaDumpeeDetailCell(self, hashName: hashtag)
            }
            let location = self.objFeedTimeLine?.isShowLocation ?? true
            
            
            if location{
                if let locationName = self.objFeedTimeLine?.locationName{
                    if locationName.isEmpty{
                        locationLBL.text = objFeedTimeLine?.userInfo?.locationName ?? ""
                    }else{
                        locationLBL.text = locationName
                    }
                }
            }else{
                locationLBL.text = ""
            }
            
            
            redumpLBL.text = "\(objFeedTimeLine?.redumpCount ?? 0) Redumps"
            commentLBL.text = "\(objFeedTimeLine?.commentCount ?? 0) " + "dumpComments".localized
            let redumpCount = self.objFeedTimeLine?.redumpCount ?? 0
            let commentCount = self.objFeedTimeLine?.commentCount ?? 0
            if redumpCount < 2{
                self.redumpLBL.text = "\(self.objFeedTimeLine?.redumpCount ?? 0) Redump"
            }
            else{
                self.redumpLBL.text = "\(self.objFeedTimeLine?.redumpCount ?? 0) Redumps"
            }
            
            if  commentCount < 2{
                self.commentLBL.text = "\(self.objFeedTimeLine?.commentCount ?? 0) " + "dumpComment".localized
            }
            else{
                self.commentLBL.text = "\(self.objFeedTimeLine?.commentCount ?? 0) " + "dumpComments".localized
            }
            
            if let profilePic  = objFeedTimeLine?.userInfo?.profilePic   {
                profileBtn.loadImage(filePath: profilePic, for: .normal)
            }
            
            guard let imageFile  = objFeedTimeLine?.media  else { return}
            guard let videoThumbnail = objFeedTimeLine?.videoThumbnail else { return  }
            var height2:Int = 0
            var width2:Int = 0
            guard let feedType = objFeedTimeLine?.feedType else { return}
            switch feedType {
            case .textVideo:
                playBtn.isHidden = false
                feedImageView.loadImage(filePath: videoThumbnail)
                guard let height = self.objFeedTimeLine?.imageinfo?.height else{return}
                guard let width = self.objFeedTimeLine?.imageinfo?.width else{return}
                height2 = height
                width2 = width
                
            case .textImage:
                playBtn.isHidden = true
                guard let height = self.objFeedTimeLine?.imageinfo?.height else{return}
                guard let width = self.objFeedTimeLine?.imageinfo?.width else{return}
                height2 = height
                width2 = width
                feedImageView.loadImage(filePath: imageFile)
            default:break
                
            }
            let w1 = self.bounds.width
            if height2 > 0 && width2 > 0{
                let h1  = CGFloat(height2)/CGFloat(width2) * w1
                self.imageHeightConstraint.constant = h1
                
            }else{
                self.imageHeightConstraint.constant = 187
            }
            
            self.setNeedsLayout()
            
            
            guard let isRedump = objFeedTimeLine?.isRedump else {
                return
            }
            if userModel?.userId == objFeedTimeLine?.userInfo?.userId{
                mediaRedumpBtn.isUserInteractionEnabled = false
                self.mediaRedumpBtn.isHidden = true
                //mediaRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                
             
            }
            else{
                mediaRedumpBtn.isUserInteractionEnabled = true
                self.mediaRedumpBtn.isHidden = false
                if isRedump{
                    mediaRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                }
                else{
                    mediaRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                }
            }
            if objFeedTimeLine?.userInfo?.isFollowed == 0 || objFeedTimeLine?.userInfo?.isFollowed == 1
            {
                if  objFeedTimeLine?.userInfo?.settingPrefrence?.isPrivateProfile ?? false
                {
                    //self.mediaRedumpBtn.isHidden = true
                    self.commentMediaBtn.isUserInteractionEnabled = false
                    self.redumpCountBtnMedia.isUserInteractionEnabled = false
                }
                else
                {
                    //self.mediaRedumpBtn.isHidden = false
                    self.commentMediaBtn.isUserInteractionEnabled = true
                    self.redumpCountBtnMedia.isUserInteractionEnabled = true
                }
                
            }
        }
    }
    
    @IBAction fileprivate func onClickPlay(_ sender: UIButton) {
        guard let obj  = self.objFeedTimeLine else{return}
        let type  = obj.feedType
        if type == .textVideo , let url  = obj.media{
            _ = DMPicker.streamVideo(file: url)
        }
    }
}



class FeedMediaImageCell:CustomTVC{
    
    
    @IBOutlet weak fileprivate var commentedUserListTextBtn: UIButton!
    @IBOutlet weak fileprivate  var userCommentDetailBtn: UIButton!
    @IBOutlet weak fileprivate var commentDetailViewFeedMedia: NSLayoutConstraint!
    @IBOutlet weak var redumpCountBtnImage: UIButton!
    @IBOutlet weak var followingDetailImageBtn: UIButton!
    @IBOutlet weak var followViewMediaImage: NSLayoutConstraint!
    @IBOutlet weak var commentMediaImageBtn: UIButton!
    @IBOutlet weak var dumpeeNameImageBTN: UIButton!
    @IBOutlet weak var dumpeeNameImageBtn: UIButton!
    @IBOutlet weak  var imageSettingBtn: UIButton!
    @IBOutlet weak fileprivate  var playBtn: UIButton!
    @IBOutlet weak var mediaImageRedumpBtn: UIButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak fileprivate var commentLBL: UILabel!
    @IBOutlet weak fileprivate var redumpLBL: UILabel!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet  var userNameLBL: UILabel!
    @IBOutlet var feedImageBtn: JKPreviewButton!
    @IBOutlet var profilePicBtn: DMButton!
    
    @IBOutlet weak fileprivate var commentUserDetailShowBtn: UIButton!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var followImageLBL: UILabel!
    @IBOutlet weak fileprivate var feedPromotionBtn: UIButton!
    @IBOutlet weak fileprivate var feedImageView: UIImageView!
    func cancelImageRequest(){
        feedImageView.cancelledTask()
    }
    
    
    var didUpdateImage:(()->Void)?
    var rowIndex:Int = 0{
        didSet{
            self.dumpeeNameImageBtn.tag = rowIndex
            self.onClickCommentPromotionalText.tag = rowIndex
            self.onClickUserComment.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.profilePicBtn.tag = rowIndex
            self.imageSettingBtn.tag = rowIndex
            self.mediaImageRedumpBtn.tag = rowIndex
            self.dumpeeNameImageBtn.tag = rowIndex
            self.commentMediaImageBtn.tag = rowIndex
            self.redumpCountBtnImage.tag = rowIndex
            self.redumpClickable.tag = rowIndex
        }
    }
    
    var objImage:DMFeedTimeLineModel?{
        didSet{
            
           
            guard let textPromotionalText = objImage?.feedPromotion else { return }
            redumpPromotionalLbl.attributedText = textPromotionalText.html2Attributed
            if !textPromotionalText.isEmpty{
                redumpPromotionalLbl.topInset = 5.0
                redumpPromotionalLbl.bottomInset = 5.0
                labelRedumpHeightConstraint.constant = 1
            }else{
                redumpPromotionalLbl.topInset = 0.0
                redumpPromotionalLbl.bottomInset = 0.0
                labelRedumpHeightConstraint.constant = 0
            }
            if let commentPromotional = objImage?.commentPromotionalText{
               
                
                if !commentPromotional.isEmpty{
                     commentPromotionalLbl.attributedText = commentPromotional.html2Attributed
                    userCommentLbl.text = objImage?.commentObj?.commentMessage
                    labelCommentHeightConstraint.constant = 1
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 5.0
                    
                    
                    commentPromotionalLbl.topInset = 5.0
                    
                    commentPromotionalLbl.bottomInset = 5.0
                    commentDotsHeightConstraint.constant = 22
                    commentArrowHeightConstraint.constant = 0
                }else{
                    commentPromotionalLbl.text = ""
                    userCommentLbl.text = ""
                    labelCommentHeightConstraint.constant = 0
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 0.0
                    
                    
                    commentPromotionalLbl.topInset = 0.0
                    
                    commentPromotionalLbl.bottomInset = 0.0
                    commentDotsHeightConstraint.constant = 0
                    commentArrowHeightConstraint.constant = 0
                }
            }
            userNameLBL.text = "\(objImage?.userInfo?.userName ?? "")"
            self.timeLBL.text = "\(self.objImage?.dumpTime ?? "") \u{2022} Dumped "
            self.dumpeeNameImageBtn.setTitle( "\(objImage?.topicName ?? "")", for: .normal)
            commentLBL.text = "\(objImage?.commentCount ?? 0) " + "dumpComments".localized
            
            let location = self.objImage?.isShowLocation ?? true
            
            
            if location{
                if let locationName = self.objImage?.locationName{
                    if locationName.isEmpty{
                        locationLBL.text = objImage?.userInfo?.locationName ?? ""
                    }else{
                        locationLBL.text = locationName
                    }
                }
            }else{
                locationLBL.text = ""
            }
            
            
            
            redumpLBL.text = "\(objImage?.redumpCount ?? 0) Redumps"
            
            
            let redumpCount = self.objImage?.redumpCount ?? 0
            let commentCount = self.objImage?.commentCount ?? 0
            if redumpCount < 2{
                self.redumpLBL.text = "\(self.objImage?.redumpCount ?? 0) Redump"
            }
            else{
                self.redumpLBL.text = "\(self.objImage?.redumpCount ?? 0) Redumps"
            }
            
            if  commentCount < 2{
                self.commentLBL.text = "\(self.objImage?.commentCount ?? 0) " + "dumpComment".localized
            }
            else{
                self.commentLBL.text = "\(self.objImage?.commentCount ?? 0) " + "dumpComments".localized
            }
            
            
            if let profilePic  = objImage?.userInfo?.profilePic   {
                profilePicBtn.loadImage(filePath: profilePic, for: .normal)
            }
            guard let imageFile  = objImage?.media  else { return}
            guard let videoThumbnail = objImage?.videoThumbnail else { return  }
            var height2:Int = 0
            var width2:Int = 0
            guard  let feedType = objImage?.feedType else{return}
            switch feedType {
            case .image:
                playBtn.isHidden = true
                guard let height = self.objImage?.imageinfo?.height else{return}
                guard let width = self.objImage?.imageinfo?.width else{return}
                height2 = height
                width2 = width
                feedImageView.loadImage(filePath: imageFile)
            case .video:
                playBtn.isHidden = false
                feedImageView.loadImage(filePath: videoThumbnail)
                guard let height = self.objImage?.imageinfo?.height else{return}
                guard let width = self.objImage?.imageinfo?.width else{return}
                height2 = height
                width2 = width
            default:break
                
            }
            let w1 = self.bounds.width
            if height2 > 0 && width2 > 0{
                let h1  = CGFloat(height2)/CGFloat(width2) * w1
                self.imageHeightConstraint.constant = h1
                
            }else{
                self.imageHeightConstraint.constant = 187.0
            }
            
            self.setNeedsLayout()
            if userModel?.userId == objImage?.userInfo?.userId{
                mediaImageRedumpBtn.isUserInteractionEnabled = false
                self.mediaImageRedumpBtn.isHidden = true
                
                mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
            }
            else{
                mediaImageRedumpBtn.isUserInteractionEnabled = true
                self.mediaImageRedumpBtn.isHidden = false
                guard let isRedump = objImage?.isRedump else {
                    return
                }
                if isRedump{
                    mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                }
                else{
                    mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                }
            }
            if objImage?.userInfo?.isFollowed == 0 || objImage?.userInfo?.isFollowed == 1
            {
                if  objImage?.userInfo?.settingPrefrence?.isPrivateProfile ?? false
                {
                   // self.mediaImageRedumpBtn.isHidden = true
                    self.commentMediaImageBtn.isUserInteractionEnabled = false
                    self.redumpCountBtnImage.isUserInteractionEnabled = false
                }
                else
                {
                    //self.mediaImageRedumpBtn.isHidden = false
                    self.commentMediaImageBtn.isUserInteractionEnabled = true
                    self.redumpCountBtnImage.isUserInteractionEnabled = true
                }
                
            }
        }
    }
    
    @IBAction fileprivate func onClickPlay(_ sender: UIButton) {
        guard let obj  = self.objImage else{return}
        let type  = obj.feedType
        if type == .video , let url  = obj.media{
            _ = DMPicker.streamVideo(file: url)
        }
    }
}



class FeedGradientCell:CustomTVC{
    @IBOutlet weak fileprivate  var commentedUserListTextBtn: UIButton!
    @IBOutlet weak fileprivate  var userCommentDetailBtn: UIButton!
    @IBOutlet weak fileprivate var commentDetailViewFeedMedia: NSLayoutConstraint!
    @IBOutlet weak var redumpCountBtnGradient: UIButton!
    @IBOutlet weak var followingDetailGradientBtn: UIButton!
    
    @IBOutlet weak var feedGradientView: NSLayoutConstraint!
    
    @IBOutlet weak var dumpeeNameGradientBTN: UIButton!
    
    @IBOutlet weak  var gradientSettingBtn: UIButton!
    @IBOutlet weak var gradientRedumpBtn: UIButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak fileprivate var commentLBL: UILabel!
    @IBOutlet weak fileprivate var redumpLBL: UILabel!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var userNameLBL: UILabel!
    @IBOutlet weak var gradientLBL: UILabel!
    
    @IBOutlet weak var commentGradinetBtn: UIButton!
    @IBOutlet weak var profilePicBtn: DMButton!
    @IBOutlet weak var gradientView: DMGradientCard!
    
    @IBOutlet weak var followGradientLBL: UILabel!
    
    @IBOutlet weak fileprivate var commentUserDetailShowBtn: UIButton!
    @IBOutlet weak fileprivate var feedPromotionBtn: UIButton!
    
    var rowIndex:Int = 0{
        didSet{
            
            self.dumpeeNameGradientBTN.tag = rowIndex
            self.onClickCommentPromotionalText.tag = rowIndex
            self.onClickUserComment.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.gradientRedumpBtn.tag = rowIndex
            self.redumpCountBtnGradient.tag = rowIndex
            self.dumpeeNameGradientBTN.tag = rowIndex
            self.gradientSettingBtn.tag = rowIndex
            self.profilePicBtn.tag = rowIndex
            self.commentGradinetBtn.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.redumpCountBtnGradient.tag = rowIndex
        }
    }
    var objGradient:DMFeedTimeLineModel?{
        didSet{
            
            guard let textPromotionalText = objGradient?.feedPromotion else { return }
            redumpPromotionalLbl.attributedText = textPromotionalText.html2Attributed
            if !textPromotionalText.isEmpty{
                redumpPromotionalLbl.topInset = 5.0
                
                redumpPromotionalLbl.bottomInset = 5.0
                
                
                labelRedumpHeightConstraint.constant = 1
            }else{
                redumpPromotionalLbl.topInset = 0.0
                
                redumpPromotionalLbl.bottomInset = 0.0
                
                
                labelRedumpHeightConstraint.constant = 0
            }
            if let commentPromotional = objGradient?.commentPromotionalText{
              
                
                if !commentPromotional.isEmpty{
                      commentPromotionalLbl.attributedText = commentPromotional.html2Attributed
                    userCommentLbl.text = objGradient?.commentObj?.commentMessage
                    labelCommentHeightConstraint.constant = 1
                    userCommentLbl.topInset = 0.0
                    userCommentLbl.bottomInset = 5.0
                    commentPromotionalLbl.topInset = 5.0
                    
                    commentPromotionalLbl.bottomInset = 5.0
                    commentDotsHeightConstraint.constant = 22
                    commentArrowHeightConstraint.constant = 0
                }else{
                    commentPromotionalLbl.text = ""
                    userCommentLbl.text = ""
                    labelCommentHeightConstraint.constant = 0
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 0.0
                    
                    
                    commentPromotionalLbl.topInset = 0.0
                    
                    commentPromotionalLbl.bottomInset = 0.0
                    commentDotsHeightConstraint.constant = 0
                    commentArrowHeightConstraint.constant = 0
                }
            }
            guard let gradientColor = self.objGradient?.bgCode else{return}
            if gradientColor == "green"{
                
                self.gradientView.set(gradientColors: DMColor.deepGreenColor, secondColor: DMColor.deepGreenColor)
                
                
            }
            else if gradientColor == "purple" {
                
                self.gradientView.set(gradientColors: DMColor.deepBlueColor, secondColor:DMColor.lightBlueColor)
                
            } else if gradientColor == "yellow"{
                
                self.gradientView.set(gradientColors: DMColor.deepYellowColor, secondColor:DMColor.deepYellowColor)
                
                
            }
                
            else if gradientColor == "dark_red"{
                self.gradientView.set(gradientColors: DMColor.deepRedColor, secondColor:DMColor.lightRedColor)
            }
                
            else{
                self.gradientView.set(gradientColors: DMColor.deepRedColor, secondColor:DMColor.lightRedColor)
            }
            
            
            
            guard let profilePic  = self.objGradient?.userInfo?.profilePic  else { return}
            self.profilePicBtn.loadImage(filePath: profilePic, for: .normal)
            
            
            self.userNameLBL.text = "\(self.objGradient?.userInfo?.userName ?? "")"
            
            
            self.timeLBL.text = "\(self.objGradient?.dumpTime ?? "") \u{2022} Dumped "
            //
            
            self.dumpeeNameGradientBTN.setTitle("\(self.objGradient?.topicName ?? "")", for: .normal)
            
            
            
            let location = self.objGradient?.isShowLocation ?? true
            
            if location {
                if let locationName = self.objGradient?.locationName{
                    if locationName.isEmpty{
                        locationLBL.text = objGradient?.userInfo?.locationName ?? ""
                    }else{
                        locationLBL.text = locationName
                    }
                }
            }else{
                locationLBL.text = ""
            }
            
            
            
            
            let redumpCount = self.objGradient?.redumpCount ?? 0
            let commentCount = self.objGradient?.commentCount ?? 0
            if redumpCount < 2{
                self.redumpLBL.text = "\(self.objGradient?.redumpCount ?? 0) Redump"
            }
            else{
                self.redumpLBL.text = "\(self.objGradient?.redumpCount ?? 0) Redumps"
            }
            
            if  commentCount < 2{
                self.commentLBL.text = "\(self.objGradient?.commentCount ?? 0) " + "dumpComment".localized
            }
            else{
                self.commentLBL.text = "\(self.objGradient?.commentCount ?? 0) " + "dumpComments".localized
            }
            
            
            
            
            self.gradientLBL.text = "Dumped \(self.objGradient?.topicName ?? "")"
            
            
            if userModel?.userId == self.objGradient?.userInfo?.userId{
                self.gradientRedumpBtn.isUserInteractionEnabled = false
                self.gradientRedumpBtn.isHidden = true
                //self.gradientRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
            }
            else{
                self.gradientRedumpBtn.isUserInteractionEnabled = true
                self.gradientRedumpBtn.isHidden = false
                guard let isRedump = self.objGradient?.isRedump else {
                    return
                }
                if isRedump{
                    self.gradientRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                }
                else{
                    self.gradientRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                }
                
            }
            if objGradient?.userInfo?.isFollowed == 0 || objGradient?.userInfo?.isFollowed == 1
            {
                if  objGradient?.userInfo?.settingPrefrence?.isPrivateProfile ?? false
                {
//                    self.gradientRedumpBtn.isHidden = true
                    self.commentGradinetBtn.isUserInteractionEnabled = false
                    self.redumpCountBtnGradient.isUserInteractionEnabled = false
                }
                else
                {
//                    self.gradientRedumpBtn.isHidden = false
                    self.commentGradinetBtn.isUserInteractionEnabled = true
                    self.redumpCountBtnGradient.isUserInteractionEnabled = true
                }
                
            }
            
        }
        
        
        
    }
}



class FeedTextCell: CustomTVC {
    
    
    @IBOutlet weak var commentClickableHeightCons: NSLayoutConstraint!
    @IBOutlet weak fileprivate  var userCommentDetailBtn: UIButton!
    @IBOutlet weak fileprivate var redumpCountBtnText: UIButton!
    @IBOutlet weak fileprivate var commentedUserListTextBtn: UIButton!
    @IBOutlet weak fileprivate var followingDetailTextBtn: UIButton!
    @IBOutlet weak fileprivate var readMoreBtn: UIButton!
    weak var delegateFeedText: SwiftyTableViewCellDelegate?
    weak var tagDelegateSeacrh:HashTagDelegateSearch?
    weak var tagDelegateProfileText:HashTagDelegateProfile?
    weak var tagDelegateDumpeeDetailText:HashTagDelegateDumpeeDetail?
    weak open var delegateReadMore: ExpandableLabelDelegate?
    @IBOutlet weak var followTextLBL: UILabel!
    @IBOutlet weak fileprivate var readMoreLBL: UILabel!
    @IBOutlet weak var followViewText: NSLayoutConstraint!
    @IBOutlet weak var commentTextBtn: UIButton!
    @IBOutlet weak var dumpeeNameTextBTN: UIButton!
    @IBOutlet weak var dumpeeNameTextBtn: UIButton!
    @IBOutlet weak  var textSettingBtn: UIButton!
    @IBOutlet weak var feedRedumpBtn: UIButton!
    @IBOutlet weak var profilePicBtn: DMButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var postTextLBL: ActiveLabel!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak fileprivate var timeLBL: UILabel!
    @IBOutlet weak var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var commentLBL: UILabel!
    @IBOutlet weak fileprivate var redumpLBL: UILabel!
    @IBOutlet weak fileprivate var commentUserDetailShowBtn: UIButton!
    @IBOutlet weak fileprivate var commentArrorDetailShowBtn: UIButton!
    @IBOutlet weak fileprivate var feedPromotionBtn: UIButton!
    @IBOutlet weak var dumpeeNameLBL: UILabel!
    
    var rowIndex:Int = 0{
        didSet{
            self.dumpeeNameTextBTN.tag = rowIndex
            self.dumpeeNameTextBtn .tag = rowIndex
            self.onClickCommentPromotionalText.tag = rowIndex
            self.onClickUserComment.tag = rowIndex
            self.redumpClickable.tag = rowIndex
            self.feedRedumpBtn.tag = rowIndex
            self.profilePicBtn.tag = rowIndex
            self.textSettingBtn.tag = rowIndex
            self.commentTextBtn.tag = rowIndex
            self.dumpeeNameTextBtn.tag = rowIndex
            self.readMoreBtn.tag = rowIndex
            self.redumpCountBtnText.tag = rowIndex
        }
    }
    
    
    var objText:DMFeedTimeLineModel?{
        didSet{
            
            
            guard let textPromotionalText = objText?.feedPromotion else { return }
            redumpPromotionalLbl.attributedText = textPromotionalText.html2Attributed
            if !textPromotionalText.isEmpty{
                redumpPromotionalLbl.topInset = 5.0
                
                redumpPromotionalLbl.bottomInset = 5.0
                
                
                labelRedumpHeightConstraint.constant = 1
            }else{
                redumpPromotionalLbl.topInset = 0.0
                
                redumpPromotionalLbl.bottomInset = 0.0
                
                
                labelRedumpHeightConstraint.constant = 0
            }
            if let commentPromotional = objText?.commentPromotionalText{
               
                
                if !commentPromotional.isEmpty{
                     commentPromotionalLbl.attributedText = commentPromotional.html2Attributed
                    userCommentLbl.text = objText?.commentObj?.commentMessage
                    labelCommentHeightConstraint.constant = 1
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 5.0
                    
                    
                    commentPromotionalLbl.topInset = 5.0
                    
                    commentPromotionalLbl.bottomInset = 5.0
                    
                    
                    commentDotsHeightConstraint.constant = 22
                    commentArrowHeightConstraint.constant = 0
                }else{
                    commentPromotionalLbl.text = ""
                    userCommentLbl.text = ""
                    labelCommentHeightConstraint.constant = 0
                    
                    userCommentLbl.topInset = 0.0
                    
                    userCommentLbl.bottomInset = 0.0
                    
                    
                    commentPromotionalLbl.topInset = 0.0
                    
                    commentPromotionalLbl.bottomInset = 0.0
                    
                    
                    commentDotsHeightConstraint.constant = 0
                    commentArrowHeightConstraint.constant = 0
                }
            }
            
            guard let profilePic  = objText?.userInfo?.profilePic  else { return}
            profilePicBtn.loadImage(filePath: profilePic, for: .normal)
            userNameLBL.text = "\(objText?.userInfo?.userName ?? "")"
            
            
            if userModel?.userId == self.objText?.userInfo?.userId{
                self.feedRedumpBtn.isUserInteractionEnabled = false
                self.feedRedumpBtn.isHidden = true
                self.feedRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
            }
            else{
                self.feedRedumpBtn.isUserInteractionEnabled = true
                self.feedRedumpBtn.isHidden = false
                guard let isRedump = objText?.isRedump else {
                    return
                }
                
                if isRedump{
                    feedRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                }
                else{
                    feedRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                }
                
            }
            if objText?.userInfo?.isFollowed == 0 || objText?.userInfo?.isFollowed == 1
            {
                if  objText?.userInfo?.settingPrefrence?.isPrivateProfile ?? false
                {
                    //self.feedRedumpBtn.isHidden = true
                    self.commentTextBtn.isUserInteractionEnabled = false
                    self.redumpCountBtnText.isUserInteractionEnabled = false
                }
                else
                {
                   // self.feedRedumpBtn.isHidden = false
                    self.commentTextBtn.isUserInteractionEnabled = true
                    self.redumpCountBtnText.isUserInteractionEnabled = true
                }
                
            }
            
            self.timeLBL.text = "\(self.objText?.dumpTime ?? "") \u{2022} Dumped "
            
            
            dumpeeNameTextBtn.setTitle( "\(objText?.topicName ?? "")", for: .normal)
            
            let location = self.objText?.isShowLocation ?? true
            
            if location{
                if let locationName = objText?.locationName {
                    if locationName.isEmpty{
                        locationLBL.text = objText?.userInfo?.locationName ?? ""
                    }
                    else{
                        //MK put the location name
                        locationLBL.text = locationName //""
                    }
                }
                
            }else{
                locationLBL.text = ""
            }
            
            
            
            postTextLBL.text = objText?.content ?? ""
            
            if let states = objText?.states{
                if states{
                    if postTextLBL.countLines>3{
                        postTextLBL.numberOfLines = 3
                        readMoreLBL.isHidden = false
                        readMoreBtn.isHidden = false
                    }else{
                        postTextLBL.numberOfLines = 3
                        readMoreLBL.isHidden = true
                        readMoreBtn.isHidden = true
                    }
                }
                    
                else{
                    readMoreLBL.isHidden = true
                    readMoreBtn.isHidden = true
                    postTextLBL.numberOfLines = 0
                }
                self.layoutIfNeeded()
            }
            
            
            
            postTextLBL.handleHashtagTap { (hashtag) in
                self.delegateFeedText?.clickHashTextCell(self, hashName: hashtag)
                self.tagDelegateSeacrh?.hashTagTextCell(self,hashName: hashtag)
                self.tagDelegateProfileText?.hashTagTextProfileCell(self, hashName: hashtag)
                self.tagDelegateDumpeeDetailText?.hashTagTextDumpeeDetailCell(self, hashName: hashtag)
            }
            
            
            let redumpCount = objText?.redumpCount ?? 0
            let commentCount = objText?.commentCount ?? 0
            if redumpCount < 2{
                redumpLBL.text = "\(objText?.redumpCount ?? 0) Redump"
            }
            else{
                redumpLBL.text = "\(objText?.redumpCount ?? 0) Redumps"
            }
            
            if  commentCount < 2{
                commentLBL.text = "\(objText?.commentCount ?? 0) " + "dumpComment".localized
            }
            else{
                commentLBL.text = "\(objText?.commentCount ?? 0) " + "dumpComments".localized
            }
            
        }
    }
    
    
    
    
}

class CountryCodeCell:CustomTVC{
    
    @IBOutlet weak fileprivate var countryCodeLBL: UILabel!
    @IBOutlet weak fileprivate var countryFlagImgView: UIImageView!
    @IBOutlet weak fileprivate var countryNameLBL: UILabel!
    var objCountryModel:CountryModel?{
        didSet{
            guard let countryPhoneCode = objCountryModel?.phoneCode,let countryName = objCountryModel?.name else{return}
            countryCodeLBL.text = "(\(countryPhoneCode))"
            countryNameLBL.text =  countryName
            countryFlagImgView.image = objCountryModel?.flag
        }
    }
}
class GenderCell:CustomTVC{
    
    @IBOutlet weak fileprivate var genderLBL: UILabel!
    var objGenderModel:GenderModel?{
        didSet{
            genderLBL.text = objGenderModel?.gender
        }
    }
}

class DumpListCell: CustomTVC {
    @IBOutlet weak fileprivate var dump: UILabel!
    @IBOutlet weak fileprivate var categoryLBL: UILabel!
    
    var dumpListModel:DMDumpModel?{
        didSet{
            
            dump.text = "\(dumpListModel?.topicName ?? "")"
            
            categoryLBL.text = "\(dumpListModel?.categoryName ?? "")"
        }
    }
}
class UserSearchListCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet weak fileprivate var userProfile: DMButton!
    @IBOutlet weak fileprivate var fullName: UILabel!
    
    var objUserSearch:DMUserSearchModel?{
        didSet{
            
            DispatchQueue.main.async {
                guard let profilePic  = self.objUserSearch?.profilePic  else { return}
                self.userProfile.loadImage(filePath: profilePic, for: .normal)
                
                self.userName.text = "\(self.objUserSearch?.userName ?? "")"
                self.fullName.text = "\(self.objUserSearch?.firstName ?? "") \(self.objUserSearch?.lastName ?? "")"
                self.layoutIfNeeded()
                self.layoutSubviews()
            }
        }
        
    }
}


class DumpeeSearchListCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet weak fileprivate var userProfile: DMButton!
    @IBOutlet weak fileprivate var fullName: UILabel!
    
    var objDumpeeSearch:DMDumpModel? {
        didSet{
            DispatchQueue.main.async {
                guard let profilePic  = self.objDumpeeSearch?.image  else {
                    return
                    
                }
                let  urlString = profilePic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                print(urlString)
                self.userProfile.loadDumpeeImage(filePath: urlString ?? "", for: .normal)
                self.userName.text = "\(self.objDumpeeSearch?.topicName ?? "")"
                print(self.objDumpeeSearch?.topicName ?? "")
                print(self.objDumpeeSearch?.categoryName ?? "")
                self.fullName.text = "\(self.objDumpeeSearch?.categoryName ?? "")"
                self.layoutIfNeeded()
                self.layoutSubviews()
            }
        }
        
    }
    
}

//MARK:- Tag Search List Cell



class TagSearchListCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet weak fileprivate var userProfile: DMButton!
    @IBOutlet weak fileprivate var tagCount: UILabel!
    
    var objTagSearch:DMTagSearchModel? {
        didSet{
            DispatchQueue.main.async {
                self.userName.text = "#\(self.objTagSearch?.hashName ?? "")"
                if let dumpCount = self.objTagSearch?.dumpCount{
                    
                    if dumpCount > 1{
                        self.tagCount.text = "\(self.objTagSearch?.dumpCount ?? 0) Dumps"
                    }else{
                        self.tagCount.text = "\(self.objTagSearch?.dumpCount ?? 0) Dump"
                    }
                }
                
                
                
                self.layoutIfNeeded()
                self.layoutSubviews()
            }
        }
        
    }
    
}



class DumpListEmptyCell: CustomTVC {
    
    @IBOutlet weak fileprivate  var dumpEmpty: UILabel!
    @IBOutlet weak fileprivate var typeBtn: UIButton!
    var suggestionListModel:DMCategoryModel?{
        didSet{
            dumpEmpty.text = "addTo".localized + " \(suggestionListModel?.categoryName ?? "")"
            
            self.setNeedsLayout()
        }
    }
}

class MenuCell: CustomTVC {
    @IBOutlet weak fileprivate var mediaView: UIImageView!
    @IBOutlet weak fileprivate var titleLBL: UILabel!
    var model:DMLeftMenuModel?{
        didSet{
            titleLBL.text = model?.title ?? ""
            mediaView.image = model?.image
            
        }
    }
}

class SettingsCell: CustomTVC {
    @IBOutlet weak  var titleLBL: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var icon_Logout: UIImageView!
    @IBOutlet weak var icon: UIImageView!
}
class BlockAccountCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var userProfile: UIButton!
    @IBOutlet weak fileprivate var descriptionLBL: UILabel!
    @IBOutlet weak var unBlockBtn: UIButton!
    var objModel:DMBlockUserInfo?{
        didSet{
            guard let profilePic  = objModel?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            
            userNameLBL.text = "\(objModel?.userName ?? "")"
            descriptionLBL.text = "\(objModel?.firstName ?? "") \(objModel?.lastName ?? "")"
            //mediaView.image = model?.image
            
        }
    }
    
}



class CommentCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak  var userProfile: DMButton!
    
    @IBOutlet weak fileprivate var timeLbl: UILabel!
    @IBOutlet weak fileprivate var commentLBL: UILabel!
    var objComment:CommentDetailModel?{
        didSet{
            
            commentLBL.text = "\(objComment?.commentMessage ?? "")"
            userNameLBL.text = "\(objComment?.userInfo?.userName ?? "")"
            
            timeLbl.text = "\(objComment?.createTime ?? "")"
            guard let profilePic  = objComment?.userInfo?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            
        }
    }
}

class FollowersCell: CustomTVC {
    @IBOutlet weak var followersBtn: DMButton!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var userProfile: UIButton!
    @IBOutlet weak fileprivate var descriptionLBL: UILabel!
    @IBOutlet weak  var removeBtn: UIButton!
    @IBOutlet weak var removeFollowwidthConstraint: NSLayoutConstraint!
    var objFollowerModel:FollowFollowersModel?{
        didSet{
            
            guard let userId = objFollowerModel?.userId else {return}
            guard let state = objFollowerModel?.followState else {return}
            followersBtn.isHidden = false
            if userId == userModel?.userId ?? ""{
                followersBtn.setTitle("Follow", for: .normal)
                //removeFollowwidthConstraint.constant = 30
                removeBtn.isHidden = true
                if state == .none || state == .unfollow{
                    followersBtn.isHidden = true
                }else if state == .following || state == .follower{
                    followersBtn.isHidden = true
                    removeBtn.isHidden = true
                }
                
            } else{
                if userId == userModel?.userId ?? ""{
                    if state == .unfollow || state == .none{
                        followersBtn.setTitle("", for: .normal)
                    }else if state == .follower {
                        followersBtn.setTitle(state.description, for: .normal)
                        followersBtn.setTitleColor(.white, for: .normal)
                        followersBtn.backgroundColor = DMColor.textRedColor
                        followersBtn.borderColor = .clear
                        //  removeFollowwidthConstraint.constant = 30
                    }else{
                        followersBtn.setTitle(state.description, for: .normal)
                        followersBtn.backgroundColor = .white
                        followersBtn.setTitleColor(.black, for: .normal)
                        followersBtn.borderColor = DMColor.textRedColor
                        // removeFollowwidthConstraint.constant = 30
                    }
                }
                else{
                    if state == .follower {
                        followersBtn.setTitle(state.description, for: .normal)
                        
                        followersBtn.setTitleColor(.white, for: .normal)
                        followersBtn.backgroundColor = DMColor.textRedColor
                        followersBtn.borderColor = .clear
                        //removeFollowwidthConstraint.constant = 30
                    }else if state == .unfollow || state == .none{
                        followersBtn.setTitle("requested".localized, for: .normal)
                        followersBtn.borderColor = .clear
                    }else{
                        followersBtn.setTitle(state.description, for: .normal)
                        followersBtn.backgroundColor = .white
                        followersBtn.setTitleColor(.black, for: .normal)
                        followersBtn.borderColor = DMColor.textRedColor
                        //removeFollowwidthConstraint.constant = 30
                    }
                }
                
            }
            
            guard let profilePic  = objFollowerModel?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            userNameLBL.text = "\(objFollowerModel?.userName ?? "")"
            descriptionLBL.text = "\(objFollowerModel?.fullName ?? "")"
            self.layoutIfNeeded()
            self.layoutSubviews()
        }
    }
}



class FollowingCell: CustomTVC {
    
    @IBOutlet weak var followingBtn: DMButton!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var userProfile: UIButton!
    @IBOutlet weak fileprivate var descriptionLBL: UILabel!
    
    var objFollowingModel:FollowFollowersModel?{
        didSet{
            
            guard let userId = objFollowingModel?.userId else {return}
            guard let state = objFollowingModel?.followState else {return}
            followingBtn.isHidden = false
            if userId == userModel?.userId ?? ""{
                
                if state == .none || state == .unfollow{
                    followingBtn.isHidden = true
                }else if state == .following || state == .follower{
                    followingBtn.isHidden = true
                }else{
                    followingBtn.isHidden = true
                }
            } else{
                //               if state == .follower  {
                //                     followingBtn.setTitle(state.description, for: .normal)
                //               }else if state == .unfollow || state == .none{
                //                followingBtn.setTitle("", for: .normal)
                //               }else{
                //                     followingBtn.setTitle(state.description, for: .normal)
                //                }
                
                
                if userId == userModel?.userId ?? ""{
                    if state == .unfollow || state == .none{
                        followingBtn.setTitle("requested".localized, for: .normal)
                    }else if state == .follower {
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.setTitleColor(.white, for: .normal)
                        followingBtn.backgroundColor = DMColor.textRedColor
                        followingBtn.borderColor = .clear
                        
                    }else{
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.backgroundColor = .white
                        followingBtn.setTitleColor(.black, for: .normal)
                        followingBtn.borderColor = DMColor.textRedColor
                        
                    }
                }
                else{
                    if state == .follower {
                        followingBtn.setTitle(state.description, for: .normal)
                        
                        followingBtn.setTitleColor(.white, for: .normal)
                        followingBtn.backgroundColor = DMColor.textRedColor
                        followingBtn.borderColor = .clear
                        
                    }else if state == .unfollow || state == .none{
                        followingBtn.setTitle("requested".localized, for: .normal)
                    }else{
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.backgroundColor = .white
                        followingBtn.setTitleColor(.black, for: .normal)
                        followingBtn.borderColor = DMColor.textRedColor
                    }
                }
            }
            
            guard let profilePic  = objFollowingModel?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            userNameLBL.text = "\(objFollowingModel?.userName ?? "")"
            descriptionLBL.text = "\(objFollowingModel?.fullName ?? "")"
            self.layoutIfNeeded()
            self.layoutSubviews()
        }
    }
}


//MARK:- FaceBook Connection Cell



class FaceBookConnectionCell: CustomTVC {
    
    @IBOutlet weak var followingBtn: DMButton!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var userProfile: UIButton!
    @IBOutlet weak fileprivate var descriptionLBL: UILabel!
    
    var objFollowingModel:FollowFollowersModel?{
        didSet{
            
            guard let userId = objFollowingModel?.userId else {return}
            guard let state = objFollowingModel?.followState else {return}
            if userId == userModel?.userId ?? ""{
                
                if state == .none || state == .unfollow{
                    followingBtn.isHidden = true
                }else if state == .following || state == .follower{
                    followingBtn.isHidden = true
                }else{
                    followingBtn.isHidden = true
                }
            } else{
                //               if state == .follower  {
                //                     followingBtn.setTitle(state.description, for: .normal)
                //               }else if state == .unfollow || state == .none{
                //                followingBtn.setTitle("", for: .normal)
                //               }else{
                //                     followingBtn.setTitle(state.description, for: .normal)
                //                }
                
                
                if userId == userModel?.userId ?? ""{
                    if state == .unfollow || state == .none{
                        followingBtn.setTitle("requested".localized, for: .normal)
                    }else if state == .follower {
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.setTitleColor(.white, for: .normal)
                        followingBtn.backgroundColor = DMColor.textRedColor
                        followingBtn.borderColor = .clear
                        
                    }else{
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.backgroundColor = .white
                        followingBtn.setTitleColor(.black, for: .normal)
                        followingBtn.borderColor = DMColor.textRedColor
                        
                    }
                }
                else{
                    if state == .follower {
                        followingBtn.setTitle(state.description, for: .normal)
                        
                        followingBtn.setTitleColor(.white, for: .normal)
                        followingBtn.backgroundColor = DMColor.textRedColor
                        followingBtn.borderColor = .clear
                        
                    }else if state == .unfollow || state == .none{
                        followingBtn.setTitle("requested".localized, for: .normal)
                    }else{
                        followingBtn.setTitle(state.description, for: .normal)
                        followingBtn.backgroundColor = .white
                        followingBtn.setTitleColor(.black, for: .normal)
                        followingBtn.borderColor = DMColor.textRedColor
                    }
                }
            }
            
            guard let profilePic  = objFollowingModel?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            userNameLBL.text = "\(objFollowingModel?.userName ?? "")"
            descriptionLBL.text = "\(objFollowingModel?.fullName ?? "")"
            self.layoutIfNeeded()
            self.layoutSubviews()
        }
    }
}

class CategoryCell: CustomTVC {
    @IBOutlet weak  var categoryNameLBL: UILabel!
    var objCategory:DMCategoryModel?{
        didSet{
            categoryNameLBL.text = objCategory?.categoryName
        }
    }
}

//MARK:- RedumpUserCell


class RedumpUserCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet weak fileprivate var userProfile: DMButton!
    @IBOutlet weak fileprivate var fullName: UILabel!
    
    var objRedumpUser:RedumpedUserInfoModel?{
        didSet{
            
            DispatchQueue.main.async {
                guard let profilePic  = self.objRedumpUser?.profilePic  else { return}
                self.userProfile.loadImage(filePath: profilePic, for: .normal)
                
                self.userName.text = "\(self.objRedumpUser?.userName ?? "")"
                self.fullName.text = "\(self.objRedumpUser?.firstName ?? "") \(self.objRedumpUser?.lastName ?? "")"
                self.layoutIfNeeded()
                self.layoutSubviews()
            }
        }
        
    }
}


class CommentedUserListCell: CustomTVC {
    
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet weak fileprivate var userProfile: DMButton!
    @IBOutlet weak fileprivate var fullName: UILabel!
    
    var objCommentedUserInfo:CommentedUserInfoModel?{
        didSet{
            
            DispatchQueue.main.async {
                guard let profilePic  = self.objCommentedUserInfo?.profilePic  else { return}
                self.userProfile.loadImage(filePath: profilePic, for: .normal)
                
                self.userName.text = "\(self.objCommentedUserInfo?.userName ?? "")"
                self.fullName.text = "\(self.objCommentedUserInfo?.firstName ?? "") \(self.objCommentedUserInfo?.lastName ?? "")"
                self.layoutIfNeeded()
                self.layoutSubviews()
            }
        }
        
    }
}

//MARK:- LeaderBoaderCell
class LeaderBoardCell: CustomTVC {
    
    @IBOutlet weak var rankLBL: UILabel!
    @IBOutlet weak var dumpeeImage: DMButton!
    @IBOutlet weak var dumpeeName: UILabel!
    @IBOutlet weak var rankTop: DMButton!
    @IBOutlet weak var dumpCount: UILabel!
    
    var objLeaderModel:LeaderBoardModelData?{
        didSet{
            rankLBL.text = "\(objLeaderModel?.rank ?? 0)"
            dumpeeName.text = objLeaderModel?.dumpeeName ?? ""
            dumpCount.text = "\(objLeaderModel?.totalRedump ?? 0)"
            
            guard let isProgress = objLeaderModel?.leaderBoardStatus else {
                return
            }
            switch isProgress {
            case .up:
                rankTop.setImage(UIImage(named: "icon_LeaderBoardUp.png"),for:.normal)
            case .down:
                rankTop.setImage(UIImage(named: "icon_LeaderBoardDown.png"),for:.normal)
            case .equal:
                rankTop.setImage(UIImage(named: "ic_Equal.png"),for:.normal)
            }
            guard let profilePic  = objLeaderModel?.dumpeeImage  else { return}
            let picUrl = profilePic.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
            dumpeeImage.loadDumpeeImage(filePath: picUrl, for: .normal)
            self.setNeedsLayout()
        }
    }
}

//MARK:- MessageRequestCell
class MessageRequestCell: CustomTVC {
    @IBOutlet weak var rejectBtn: DMButton!
    @IBOutlet weak var acceptBtn: DMButton!
    
    @IBOutlet weak var messageTimelabel: UILabel!
    @IBOutlet weak var userImage: DMButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var message: UILabel!
    
    var objMessageRequest:DirectMessageModel?{
        didSet{
            if objMessageRequest?.lastMessageType == "text"{
                message.text = objMessageRequest?.lastMessage ?? ""
            }else{
                message.text = "Photo"
            }
            
            messageTimelabel.text = objMessageRequest?.messageTime ?? ""
            userName.text = objMessageRequest?.userName ?? ""
            guard let profilePic  = objMessageRequest?.profilePic  else { return}
            userImage.loadImage(filePath: profilePic, for: .normal)
        }
    }
}


//MARK:- DirectMessageCell
class DirectMessageCell: CustomTVC {
    
    @IBOutlet weak var messageUnreadLabel: UILabel!
    
    @IBOutlet weak var messageTimelabel: UILabel!
    @IBOutlet weak var userImage: DMButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var message: UILabel!
    
    var objDirectMessage:DirectMessageModel?{
        didSet{
            if objDirectMessage?.lastMessageType == "text"{
                message.text = objDirectMessage?.lastMessage ?? ""
            }else{
                message.text = "Photo"
            }
            
            
            let messageUnreadCount = objDirectMessage?.messageUnreadCount ?? 0
            if messageUnreadCount > 0{
                
                if  objDirectMessage?.receiverId == objDirectMessage?.userId {
                    messageUnreadLabel.isHidden = true
                    // messageUnreadLabel.text = "\u{2022}"
                }else{
                    messageUnreadLabel.isHidden = false
                    messageUnreadLabel.text = "\u{2022}"
                }
                
                
                
                
            }else{
                messageUnreadLabel.isHidden = true
            }
            
            messageTimelabel.text = objDirectMessage?.messageTime ?? ""
            userName.text = objDirectMessage?.userName ?? ""
            guard let profilePic  = objDirectMessage?.profilePic  else { return}
            userImage.loadImage(filePath: profilePic, for: .normal)
        }
    }
}
class NotificationListCell: CustomTVC {
    @IBOutlet weak var userImage: DMButton!
    @IBOutlet weak var acceptWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var rejectwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var rejectBtn: DMButton!
    @IBOutlet weak var acceptBtn: DMButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var notificationTime: UILabel!
    var objNotificationList:NotificationModel?{
        didSet{
            
            if objNotificationList?.notificationType == 4{
                UIView.animate(withDuration: 0.3, animations: {
                    self.acceptWidthConstraint.constant = 30.0
                    self.rejectwidthConstraint.constant = 30.0
                })
            }else{
                UIView.animate(withDuration: 0.3, animations: {
                    self.acceptWidthConstraint.constant = 0.0
                    self.rejectwidthConstraint.constant = 0.0
                })
            }
            
            message.text = objNotificationList?.notificationBody ?? ""
            userName.text = objNotificationList?.senderInfoModel?.fullName ?? ""
            guard let profilePic  = objNotificationList?.senderInfoModel?.profilePic  else { return}
            userImage.loadImage(filePath: profilePic, for: .normal)
            notificationTime.text = objNotificationList?.notificationTime ?? ""
        }
    }
}

//MARK:- Request Cell


class RequestCell: CustomTVC {
    
    @IBOutlet weak var followingBtn: DMButton!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var userProfile: UIButton!
    @IBOutlet weak fileprivate var descriptionLBL: UILabel!
    
    @IBOutlet weak var rejectBtn: DMButton!
    @IBOutlet weak var acceptBtn: DMButton!
    var objRequestedModel:FollowFollowersModel?{
        didSet{
            guard let profilePic  = objRequestedModel?.profilePic  else { return}
            userProfile.loadImage(filePath: profilePic, for: .normal)
            userNameLBL.text = "\(objRequestedModel?.userName ?? "")"
            descriptionLBL.text = "\(objRequestedModel?.fullName ?? "")"
            self.layoutIfNeeded()
            self.layoutSubviews()
        }
    }
    
}




//MARK:- Language Selection Cell


class LanguageCell:CustomTVC{
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var languageSelectionImgView: UIImageView!
    
    
}

//MARK:- NewsCell

class NewsCell:CustomTVC{
    
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dumpCountLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var mediaRedumpBtn: UIButton!
    @IBOutlet weak fileprivate var postTextMediaLBL: ActiveLabel!
    @IBOutlet weak fileprivate var feedImageView: UIImageView!
    @IBOutlet weak var readMoreBtn: UIButton!
    
    var objNewsModel:NewsModel?{
        didSet{
            postTextMediaLBL.text = objNewsModel?.title ?? ""
            timeLbl.text = objNewsModel?.publishedAt ?? ""
            dumpCountLbl.text = "\(objNewsModel?.dumpCount ?? 0) Dump"
            guard let imageFile  = objNewsModel?.urlToImage  else { return}
            let urlString = imageFile.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            guard urlString != nil  else { return}
            if  let imageFile  = objNewsModel?.urlToImage  {
                
                if imageFile.isEmpty{
                    imageHeightConstraint.constant = 0.0
                }else{
                    imageHeightConstraint.constant = 200.0
                    feedImageView.loadImage(filePath: imageFile)
                }
                
                self.layoutIfNeeded()
            }
            
            
            guard let isRedump = objNewsModel?.isDump else {
                return
            }
            if isRedump{
                mediaRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
            }
            else{
                mediaRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
            }
            self.setNeedsLayout()
        }
    }
}



//MARK:- MessageCell
class DMChatCell: UITableViewCell{
    func bubble(isIncoming:Bool = false )->UIImage{
        let image  = isIncoming ?   #imageLiteral(resourceName: "chat_bubble_white"):#imageLiteral(resourceName: "chat_bubble_red")
        return  image.resizableImage(withCapInsets:UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .stretch)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        
    }
    @IBOutlet var bubbleImageView:DMImageView!
    @IBOutlet var lastSeenlbl:UILabel!
    @IBOutlet var timeLabel:UILabel!
    @IBOutlet weak var loader: THIndicatorView!
    @IBOutlet weak var bubbleHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var bubbleWidthConstraint : NSLayoutConstraint!
    
    var rowIndex:Int = 0 {
        didSet{
            // bubbleView.tag = rowIndex
            
        }
    }
    var model:DMMessageModel?{
        didSet{
            timeLabel.text = self.model?.localTime ?? ""
            let isIncoming = model?.isIncomming ?? false
            
            
            bubbleImageView.image = bubble(isIncoming: isIncoming)
            if !isIncoming {
                if let obj = model{
                    setSeenStatus(model: obj)
                }
                
            }
        }
    }
    
    
    //MARK: - setStatusIcon -
    func setSeenStatus(model:DMMessageModel) {
        //lastSeenlbl.isHidden = self.model?.isRead == true ? false : true
        lastSeenlbl.text = self.model?.isRead == true ? "seen".localized : ""
    }
}

class MessageCell: DMChatCell {
    @IBOutlet var messagelbl:ActiveLabel!
    weak var delegateCopyLink: CopyLinkDelegate?
    @IBOutlet weak  var recieverWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak  var senderWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var receiverBubbleImage: UIImageView!
    @IBOutlet weak var senderBubbleImage: UIImageView!
    
    override var rowIndex:Int {
        didSet{
            super.rowIndex = rowIndex
        }
    }
    override var model:DMMessageModel?{
        didSet{
            messagelbl.text = model?.message ?? ""
            messagelbl.handleURLTap { (url) in
                self.delegateCopyLink?.copyLinkMessageCell(self, copyLink: url)
            }
        }
    }
    
}

protocol CellSubclassDelegate: class {
    func buttonTapped(cell: MessageImageCell)
}
class MessageImageCell: DMChatCell {
    
    weak var delegate: CellSubclassDelegate?
    @IBOutlet weak var attachmentBtn: DMButton!
    override var rowIndex:Int {
        didSet{
            self.attachmentBtn.tag = rowIndex
            super.rowIndex = rowIndex
        }
    }
    override var model:DMMessageModel?{
        didSet{
            let isIncoming = model?.isIncomming ?? false
            let type:RectCornerRadiusType = isIncoming ? .incoming : .outgoing
            attachmentBtn.layer.rectCornerRadius(rectCornerRadiusType: type, cornerRadius: 4)
            if let file = model?.media{
                attachmentBtn.imageView?.contentMode = .scaleAspectFill
                
                attachmentBtn.loadChatImage(filePath: file, for: .normal) { (image, error) in
                    
                    
                    
                    
                }
            }
        }
    }
    
    
    
    
    @IBAction func onClickImage(_ sender: DMButton) {
        self.delegate?.buttonTapped(cell: self)
    }
    
}



extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}

extension UILabel {
    var countLines:Int{
        let myText = self.text!
        let height =  myText.height(withConstrainedWidth: self.bounds.width, font: self.font)
        self.setNeedsLayout()
        return Int(ceil(height / self.font.lineHeight))
    }
    
    
    func isTruncated() -> Bool {
        
        if (self.countLines > self.numberOfLines) {
            return true
        }
        return false
    }
}

class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 5.0
    @IBInspectable var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
