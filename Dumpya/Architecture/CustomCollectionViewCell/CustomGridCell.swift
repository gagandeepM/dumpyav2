//
//  CustomGridCell.swift
//  Dumpya
//
//  Created by Chander on 14/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
class CustomGridCell: UICollectionViewCell {
    
}

class WalkThroughCell:CustomGridCell {
    
    @IBOutlet weak var walkThroughImgView: UIImageView!
    
}

class SearchCell:CustomGridCell,UITableViewDelegate,UITableViewDataSource{
    
     var objSearchModel = SearchViewModel()
     @IBOutlet weak var searchTableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        searchTableView.delegate = self
        searchTableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return objSearchModel.numberOfRows()
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let userSearchListCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kUserSearchListCell  , for: indexPath) as! UserSearchListCell
    
           userSearchListCell.objUserSearch = objSearchModel.cellForRowAt(at: indexPath)
    
    return userSearchListCell
    
    }
    
    
  
}

class GifCell:UICollectionViewCell
{
    @IBOutlet weak fileprivate var gifImage: UIImageView!
    var height:Int = 0
    var width:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func prepareForReuse() {
    }
    var image:String? {
        didSet
        {
            
            guard let image = image else{return}
            
          self.gifImage.loadImage(filePath: image, progressBlock: nil, onCompletion: nil)
//            self.gifImage.loadGifImage(filePath: image!, progressBlock: nil) { (image, error) in
//                self.height = Int(image?.size.height ?? 0)
//                 self.width = Int(image?.size.width ?? 0)
//                print(self.height,self.width)
//            }
        }
    }
    
    
}

class DumpCategoryCell:CustomGridCell{
    
    @IBOutlet weak fileprivate var defaultDumpLBL: UILabel!
    @IBOutlet weak fileprivate var dumpGradient: UIView!
    var gradientColor:GradientColor!{
        didSet{
            let colors = gradientColor.colors
            if colors.count == 2 {
                dumpGradient.setGradeinetBackground(colorOne:colors[0], colorTwo: colors[1])
            }
            
        }
    }
    var isMakeDefault:Bool = true{
        didSet{
            self.defaultDumpLBL.text = isMakeDefault ? "Default".localized : ""
        }
    }
    
}



class LeaderBoardCategoryCell:CustomGridCell{
    
    @IBOutlet weak  var categoryLBL: UILabel!
    
    @IBOutlet weak var selectedLBL: UILabel!
    
    var type:ConnectionType = .followers{
        didSet{
            categoryLBL.text  = type.description.localized
        }
    }
    var isSelectedTab:Bool = false{
        didSet{
            selectedLBL.isHidden = isSelectedTab
        }
    }
}

class SearchCategoryCell:CustomGridCell{
    
    @IBOutlet weak  var categoryLBL: UILabel!
    
    @IBOutlet weak var selectedLBL: UILabel!
    
}

class LeaderBoardTimeCell:CustomGridCell{
    @IBOutlet weak   var timeDurationLBL: UILabel!
    @IBOutlet weak  var selectedLBL: UILabel!
}

extension String{
    
    func changesNew(key:String,comment:String) -> String {
        
       return NSLocalizedString(key, tableName: "Localize",comment: comment)
    }
}
