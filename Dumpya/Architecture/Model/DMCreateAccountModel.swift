//
//  DMCreateAccountModel.swift
//  Dumpya
//
//  Created by Chander on 20/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct DMCreateAccountModel:Mappable {
    let userId : Int?
    let name : String?
    let email : String?
    let accessToken : String?
    let userName:String?
    let activityCount:Int?
    let friendsCount:Int?
    let groupsCount:Int?
    let firstName :String?
    let lastName :String?
    
    enum CodingKeys: String, CodingKey {
        case userId        = "userId"
        case name          = "name"
        case email         = "email"
        case accessToken   = "token"
        case userName      = "profile_name"
        case activityCount = "activty_count"
        case friendsCount  = "friends_count"
        case groupsCount   = "groups_count"
        case firstName     = "first_name"
        case lastName      = "last_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId         =  (values.contains(.userId)        == true) ? try values.decodeIfPresent(Int.self, forKey: .userId) : 0
        accessToken    =  (values.contains(.accessToken)   == true) ? try values.decodeIfPresent(String.self, forKey: .accessToken) : ""
        name           =  (values.contains(.name)          == true) ? try values.decodeIfPresent(String.self, forKey: .name) : ""
        email          =  (values.contains(.email)         == true) ? try values.decodeIfPresent(String.self, forKey: .email) : ""
        userName       =  (values.contains(.userName)      == true) ? try values.decodeIfPresent(String.self, forKey: .userName) : ""
        activityCount  =  (values.contains(.activityCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .activityCount) : 0
        friendsCount   =  (values.contains(.friendsCount)  == true) ? try values.decodeIfPresent(Int.self, forKey: .friendsCount) : 0
        groupsCount    =  (values.contains(.groupsCount)   == true) ? try values.decodeIfPresent(Int.self, forKey: .groupsCount) : 0
        firstName      =  (values.contains(.firstName)     == true) ? try values.decodeIfPresent(String.self , forKey: .firstName) : ""
        lastName       =  (values.contains(.lastName)      == true) ? try values.decodeIfPresent(String.self , forKey: .lastName) : ""
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userId, forKey: .userId)
        try container.encode(name, forKey: .name)
        try container.encode(email, forKey: .email)
        try container.encode(accessToken, forKey: .accessToken)
        try container.encode(userName, forKey: .userName)
        try container.encode(activityCount, forKey: .activityCount)
        try container.encode(friendsCount, forKey: .friendsCount)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
    }
}
struct DMCreateAccountReponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let user:DMCreateAccountModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case user    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        user =  values.contains(.user) == true ? try values.decodeIfPresent(DMCreateAccountModel.self, forKey: .user) : nil
        
    }
}
