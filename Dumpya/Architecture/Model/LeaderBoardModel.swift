//
//  LeaderBoardModel.swift
//  Dumpya
//
//  Created by Chandan on 02/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//


import Foundation
enum LeaderBoardStatus:Int{
    
    case equal = 0
    case up = 1
    case down = 2
    }

struct LeaderBoardModel:Mappable {
    //let totalCount:Int?
    var leaderBoardData:[LeaderBoardModelData] = [LeaderBoardModelData]()
    var categoryList:[DMCategoryModel] = [DMCategoryModel]()
    enum CodingKeys:String,CodingKey {
        case leaderBoardData = "leaderboardList"
        case categoryList = "categoryList"
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
      //  totalCount = (values.contains(.totalCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .totalCount) : 0
        
        guard let leaderBoardData =  try values.decodeIfPresent([LeaderBoardModelData].self, forKey: .leaderBoardData) else{return}
        
        
        self.leaderBoardData = leaderBoardData
        
        guard let categoryList =  try values.decodeIfPresent([DMCategoryModel].self, forKey: .categoryList) else{return}
        
        
        self.categoryList = categoryList
        
        
    }
}

struct LeaderBoardModelData:Mappable {
   
    let rank:Int?
    let dumpeeName:String?
    let dumpeeImage:String?
    let totalRedump:Int?
    let isProgress:Int?
    let dumpeeId:String?
    enum CodingKeys:String,CodingKey {
        case rank = "rank"
        case dumpeeName = "dumpeeName"
        case dumpeeImage = "dumpeeImage"
        case totalRedump = "totalRedump"
        case isProgress = "isProgress"
        case dumpeeId = "dumpeeId"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        rank = (values.contains(.rank) == true) ? try values.decodeIfPresent(Int.self, forKey: .rank) : 0
        
        dumpeeId =  (values.contains(.dumpeeId) == true) ? try values.decodeIfPresent(String.self, forKey: .dumpeeId) : ""
        dumpeeName =  (values.contains(.dumpeeName) == true) ? try values.decodeIfPresent(String.self, forKey: .dumpeeName) : ""
        
        isProgress =   (values.contains(.dumpeeName) == true) ? try values.decodeIfPresent(Int.self, forKey: .isProgress) : 0
        dumpeeImage =  (values.contains(.dumpeeImage) == true) ? try values.decodeIfPresent(String.self, forKey: .dumpeeImage) : ""
        
        totalRedump =  (values.contains(.totalRedump) == true) ? try values.decodeIfPresent(Int.self, forKey: .totalRedump) : 0
        
    }
    
    var leaderBoardStatus:LeaderBoardStatus? {
        if let leaderBoardStatus = self.isProgress {
            return LeaderBoardStatus(rawValue:leaderBoardStatus)
        }
        else{
            return .none
        }
    }
}

struct LeaderBoardResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    let leaderModel:LeaderBoardModel?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case leaderModel = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        leaderModel =  values.contains(.leaderModel) == true ? try values.decodeIfPresent(LeaderBoardModel.self, forKey: .leaderModel) : nil
    }
}

