//
//  DMGifModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 06/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
struct DMGifFile {
    var gifFiles:[URL] = [URL]()
    init(parse list:[String]) {
        self.gifFiles = list.map({ (file) -> URL in
            return URL(string: file)!
        })
    }
}


struct DMGifModel {
    var gifUrl: String?
    var gifHeight:Int = 0
    var gifWidth:Int = 0
    
}


extension DMGifModel:Equatable{
    public static func == (lhs: DMGifModel, rhs: DMGifModel) -> Bool{
        return lhs.gifHeight == rhs.gifHeight && lhs.gifWidth == rhs.gifWidth && lhs.gifUrl == rhs.gifUrl
    }
}
