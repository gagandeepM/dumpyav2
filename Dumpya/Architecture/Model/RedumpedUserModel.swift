//
//  RedumpedUserModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

struct RedumpedUserReponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let redumpedUser:RedumpedUserModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case redumpedUser    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        redumpedUser =  values.contains(.redumpedUser) == true ? try values.decodeIfPresent(RedumpedUserModel.self, forKey: .redumpedUser) : nil
        
    }
}

struct RedumpedUserModel:Mappable {
    let totalCount : Int?
    var redumpedUserInfo:[RedumpedUserInfoModel] = [RedumpedUserInfoModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount   = "totalCount"
        case redumpedUserInfo     = "feedList"
       
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let redumpedUserInfo =  try values.decodeIfPresent([RedumpedUserInfoModel].self, forKey: .redumpedUserInfo) else{return}
        self.redumpedUserInfo = redumpedUserInfo
      
    }
}

struct RedumpedUserInfoModel:Mappable {
    
    var userId:String?
    var firstName:String?
    var lastName:String?
    var userName:String?
    var profilePic:String?
   
    
    enum CodingKeys:String,CodingKey {
        case userId = "userId"
        case firstName = "firstName"
        case lastName = "lastName"
        case userName = "userName"
        case profilePic = "profilePic"
       
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        firstName =   try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        userName =   try values.decodeIfPresent(String.self, forKey: .userName)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
       
        
    }
}

