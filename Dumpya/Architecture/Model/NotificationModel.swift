//
//  NotificationModel.swift
//  Dumpya
//
//  Created by Chandan on 23/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

struct NotificationsData:Mappable {
    let totalCount:Int?
    var notifications:[NotificationModel] = [NotificationModel]()
    enum CodingKeys:String,CodingKey {
        case notifications = "notificationList"
        case totalCount = "totalCount"
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        totalCount = (values.contains(.totalCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .totalCount) : 0
        
        guard let notificationListModel =  try values.decodeIfPresent([NotificationModel].self, forKey: .notifications) else{return}
        
        self.notifications = notificationListModel
        
        
    }
}

struct NotificationModel:Mappable {
    
   
    let notificationBody:String?
    let notificationType:Int?
    let dumpId:String?
    let notificationId:String?
    let senderInfoModel:SenderInfoModel?
    let notificationTime:String?
    enum CodingKeys:String,CodingKey {
        case notificationBody = "notificationBody"
        case senderInfoModel = "senderInfo"
        case notificationType = "notificationType"
        case dumpId = "dumpId"
        case notificationId = "notificationId"
        case notificationTime = "createdAt"
       
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
       
        
        
        notificationBody =  (values.contains(.notificationBody) == true) ? try values.decodeIfPresent(String.self, forKey: .notificationBody) : ""
        dumpId =  (values.contains(.dumpId) == true) ? try values.decodeIfPresent(String.self, forKey: .dumpId) : ""
        notificationId =  (values.contains(.notificationId) == true) ? try values.decodeIfPresent(String.self, forKey: .notificationId) : ""
        notificationType =  (values.contains(.notificationType) == true) ? try values.decodeIfPresent(Int.self, forKey: .notificationType) : 0
        senderInfoModel = values.contains(.senderInfoModel) == true ? try values.decodeIfPresent(SenderInfoModel.self, forKey: .senderInfoModel) : nil
      notificationTime =  try values.decodeIfPresent(String.self, forKey: .notificationTime)
        
    }
}

struct SenderInfoModel:Mappable{
    let fullName:String?
    let profilePic:String?
    let senderId:String?
    enum CodingKeys:String,CodingKey {
        case fullName = "fullName"
        case profilePic = "profilePic"
        case senderId = "senderId"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        
        
        
        fullName =  (values.contains(.fullName) == true) ? try values.decodeIfPresent(String.self, forKey: .fullName) : ""
        profilePic = (values.contains(.profilePic) == true) ? try values.decodeIfPresent(String.self, forKey: .profilePic) : ""
        senderId = (values.contains(.senderId) == true) ? try values.decodeIfPresent(String.self, forKey: .senderId) : ""
        
    }
}

struct NotificationResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    let notificationData:NotificationsData?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case notificationData = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        notificationData =  values.contains(.notificationData) == true ? try values.decodeIfPresent(NotificationsData.self, forKey: .notificationData) : nil
    }
}
