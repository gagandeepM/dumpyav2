//
//  NewsModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 17/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct NewsModelData:Mappable {
    let totalCount:Int?
    var newsModel:[NewsModel] = [NewsModel]()
    enum CodingKeys:String,CodingKey {
        case newsModel = "newsModel"
        case totalCount = "totalCount"
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        totalCount = (values.contains(.totalCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .totalCount) : 0
        
        guard let newsModel =  try values.decodeIfPresent([NewsModel].self, forKey: .newsModel) else{return}
        
        self.newsModel = newsModel
        
        
    }
}

struct NewsModel:Mappable {
    let title:String?
    let urlToImage:String?
    let url:String?
    var isDump:Bool?
    let newsId:String?
    let publishedAt:String?
    var dumpCount:Int?
    var imageinfo:DMTimeLineImageInfo?
    enum CodingKeys:String,CodingKey {
        case title = "title"
        case urlToImage = "urlToImage"
        case url = "url"
        case imageinfo = "dimensions"
        case isDump = "isDump"
        case newsId = "newsId"
        case publishedAt = "publishedAt"
        case dumpCount = "dumpCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
         title =  (values.contains(.title) == true) ? try values.decodeIfPresent(String.self, forKey: .title) : ""
        
        imageinfo = values.contains(.imageinfo) == true ? try values.decodeIfPresent(DMTimeLineImageInfo.self, forKey: .imageinfo) : nil
        urlToImage =  (values.contains(.urlToImage) == true) ? try values.decodeIfPresent(String.self, forKey: .urlToImage) : ""
        
         dumpCount =  (values.contains(.dumpCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .dumpCount) : 0
        
        publishedAt =  (values.contains(.publishedAt) == true) ? try values.decodeIfPresent(String.self, forKey: .publishedAt) : ""
        url =  (values.contains(.url) == true) ? try values.decodeIfPresent(String.self, forKey: .url) : ""
        isDump =  (values.contains(.isDump) == true) ? try values.decodeIfPresent(Bool.self, forKey: .isDump) : false
        newsId = (values.contains(.newsId) == true) ? try values.decodeIfPresent(String.self, forKey: .newsId) : ""
    }
}

struct NewsResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    var newsModel:[NewsModel] = [NewsModel]()
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case newsModel = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        guard let newsModel =  try values.decodeIfPresent([NewsModel].self, forKey: .newsModel) else{return}
        
        self.newsModel = newsModel
    }
}
