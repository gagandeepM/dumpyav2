//
//  DMUserModel.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct DMUserModel:Mappable{
    
    var userId : String?
    var name : String?
    var email : String?
    var accessToken : String?
    var userName:String?
    var activityCount:Int?
    var isFollowed:Int?
    var firstName :String?
    var lastName :String?
    var isVerified:Bool?
    var isProfileUpdate:Bool?
    var profilePic:String?
    var coverPic:String?
    var socialType:String?
    var bio:String?
    var fullName:String?
    var locationName:String?
    var otherInfo : OtherInfo?
    var phoneNumber:PhoneNumber?
    var settingPrefrence : SettingPrefrence?
    var gender:String?
    var birthDate:Int?
    var countryName:String?
    var mutualFollowingText:String?
    var isBlocked:Bool?
    var defaultLanguage:String?
    var currentLocation:DMCurrentLocation?
    
    enum CodingKeys: String, CodingKey {
        case fullName      =     "fullName"
        case locationName  = "locationName"
        case userId        = "_id"
        case name          = "name"
        case email         = "email"
        case accessToken   = "token"
        case userName      = "userName"
        case activityCount = "activty_count"
        case firstName     = "firstName"
        case lastName      = "lastName"
        case countryName = "countryName"
        case profilePic    = "profilePic"
        case isVerified    = "isVerified"
        case socialType    = "socialType"
        case isProfileUpdate = "isProfileUpdate"
        case bio           = "bio"
        case otherInfo     = "otherInfo"
        case settingPrefrence = "settingPrefrence"
        case isFollowed = "isFollowed"
        case phoneNumber = "phoneNumber"
        case gender = "gender"
        case birthDate = "birthDate"
        case coverPic = "coverPic"
        case mutualFollowingText = "mutualFollowingText"
        case isBlocked = "isBlocked"
        case defaultLanguage = "defaultLanguage"
        case currentLocation = "currentLocation"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId         =  (values.contains(.userId)        == true) ? try values.decodeIfPresent(String.self, forKey: .userId) : ""
        
        coverPic  =  (values.contains(.coverPic)        == true) ? try values.decodeIfPresent(String.self, forKey: .coverPic) : ""
        
        countryName = (values.contains(.countryName)        == true) ? try values.decodeIfPresent(String.self, forKey: .countryName) : ""
        fullName         =  (values.contains(.fullName)        == true) ? try values.decodeIfPresent(String.self, forKey: .fullName) : ""
        
        gender  =  (values.contains(.gender)        == true) ? try values.decodeIfPresent(String.self, forKey: .gender) : ""
        
        locationName =  (values.contains(.locationName)        == true) ? try values.decodeIfPresent(String.self, forKey: .locationName) : ""
        bio =  (values.contains(.bio)        == true) ? try values.decodeIfPresent(String.self, forKey: .bio) : ""
        
        socialType  =  (values.contains(.socialType)        == true) ? try values.decodeIfPresent(String.self, forKey: .socialType) : ""
        
        accessToken    =  (values.contains(.accessToken)   == true) ? try values.decodeIfPresent(String.self, forKey: .accessToken) : ""
        name           =  (values.contains(.name)          == true) ? try values.decodeIfPresent(String.self, forKey: .name) : ""
        email          =  (values.contains(.email)         == true) ? try values.decodeIfPresent(String.self, forKey: .email) : ""
        
        profilePic  =  (values.contains(.profilePic)         == true) ? try values.decodeIfPresent(String.self, forKey: .profilePic) : ""
        
        userName       =  (values.contains(.userName)      == true) ? try values.decodeIfPresent(String.self, forKey: .userName) : ""
        activityCount  =  (values.contains(.activityCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .activityCount) : 0
        birthDate =  (values.contains(.birthDate) == true) ? try values.decodeIfPresent(Int.self, forKey: .birthDate) : 0
        defaultLanguage = (values.contains(.defaultLanguage)      == true) ? try values.decodeIfPresent(String.self, forKey: .defaultLanguage) : ""
        
        
        isFollowed =  (values.contains(.isFollowed) == true) ? try values.decodeIfPresent(Int.self, forKey: .isFollowed) : 0
        firstName      =  (values.contains(.firstName)     == true) ? try values.decodeIfPresent(String.self , forKey: .firstName) : ""
        lastName       =  (values.contains(.lastName)      == true) ? try values.decodeIfPresent(String.self , forKey: .lastName) : ""
        isVerified       =  (values.contains(.isVerified)      == true) ? try values.decodeIfPresent(Bool.self , forKey: .isVerified) : false
        isProfileUpdate  =  (values.contains(.isProfileUpdate)      == true) ? try values.decodeIfPresent(Bool.self , forKey: .isProfileUpdate) : false
        
        otherInfo =  values.contains(.otherInfo) == true ? try values.decodeIfPresent(OtherInfo.self, forKey: .otherInfo) : nil
        
        phoneNumber =  values.contains(.phoneNumber) == true ? try values.decodeIfPresent(PhoneNumber.self, forKey: .phoneNumber) : nil
        
        settingPrefrence =  values.contains(.settingPrefrence) == true ? try values.decodeIfPresent(SettingPrefrence.self, forKey: .settingPrefrence) : nil
        
        mutualFollowingText = values.contains(.mutualFollowingText) == true ? try values.decodeIfPresent(String.self, forKey: .mutualFollowingText) : nil
        
        isBlocked = try values.decodeIfPresent(Bool.self, forKey: .isBlocked)
        currentLocation = values.contains(.currentLocation) == true ? try values.decodeIfPresent(DMCurrentLocation.self, forKey: .currentLocation) : nil
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userId, forKey: .userId)
        try container.encode(locationName, forKey: .locationName)
        try container.encode(gender, forKey: .gender)
        try container.encode(birthDate, forKey: .birthDate)
        try container.encode(coverPic, forKey: .coverPic)
        try container.encode(mutualFollowingText,forKey:.mutualFollowingText)
        try container.encode(name, forKey: .name)
        try container.encode(bio, forKey: .bio)
        try container.encode(socialType, forKey: .socialType)
        try container.encode(profilePic, forKey: .profilePic)
        try container.encode(email, forKey: .email)
        try container.encode(accessToken, forKey: .accessToken)
        try container.encode(userName, forKey: .userName)
        try container.encode(activityCount, forKey: .activityCount)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(isVerified, forKey: .isVerified)
        try container.encode(isProfileUpdate, forKey: .isProfileUpdate)
        try container.encode(phoneNumber, forKey: .phoneNumber)
        try container.encode(settingPrefrence, forKey: .settingPrefrence)
        try container.encode(isBlocked, forKey: .isBlocked)
        try container.encode(countryName, forKey:.countryName)
        try container.encode(defaultLanguage, forKey:.defaultLanguage)
        try container.encode(currentLocation, forKey: .currentLocation)
    }
}


struct SettingPrefrence:Mappable {
    var isPrivateProfile:Bool?
    var isFacebookLinkedAccount:Bool?
    enum CodingKeys: String, CodingKey {
        case isPrivateProfile = "isPrivateProfile"
        case isFacebookLinkedAccount = "isFacebookLinkedAccount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        isPrivateProfile         =   try values.decodeIfPresent(Bool.self, forKey: .isPrivateProfile)
        isFacebookLinkedAccount = try values.decodeIfPresent(Bool.self, forKey: .isFacebookLinkedAccount)
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isPrivateProfile, forKey: .isPrivateProfile)
        try container.encode(isFacebookLinkedAccount, forKey: .isFacebookLinkedAccount)
    }
}




struct PhoneNumber:Mappable {
    var countryCode:String?
    var countryObj:String?
    var phone:String?
    enum CodingKeys: String, CodingKey {
        case countryCode = "countryCode"
        case countryObj = "countryObj"
        case phone = "phone"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        countryCode         =  (values.contains(.countryCode)        == true) ? try values.decodeIfPresent(String.self, forKey: .countryCode) : ""
        countryObj         =  (values.contains(.countryObj)        == true) ? try values.decodeIfPresent(String.self, forKey: .countryObj) : ""
        phone         =  (values.contains(.phone)        == true) ? try values.decodeIfPresent(String.self, forKey: .phone) : ""
        
    }
    
}


struct OtherInfo:Mappable {
    var dumpCount:Int?
    var reDumpCount:Int?
    var followers:Int?
    var following:Int?
    enum CodingKeys: String, CodingKey {
        case dumpCount = "dumpCount"
        case followers = "followers"
        case following = "following"
        case reDumpCount = "reDumpCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dumpCount         =  (values.contains(.dumpCount)        == true) ? try values.decodeIfPresent(Int.self, forKey: .dumpCount) : 0
        followers         =  (values.contains(.followers)        == true) ? try values.decodeIfPresent(Int.self, forKey: .followers) : 0
        following         =  (values.contains(.following)        == true) ? try values.decodeIfPresent(Int.self, forKey: .following) : 0
        
        reDumpCount =  (values.contains(.reDumpCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .reDumpCount) : 0
        
    }
    
}

struct DMLoginReponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let user:DMUserModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case user    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        user =  values.contains(.user) == true ? try values.decodeIfPresent(DMUserModel.self, forKey: .user) : nil
        
    }
}
struct DMCurrentLocation:Mappable
{
    let countryName:String?
    let locationName:String?
    enum CodingKeys: String, CodingKey {
        case countryName  = "countryName"
        case locationName = "locationName"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        countryName = try values.decodeIfPresent(String.self, forKey: .countryName)
        locationName =   try values.decodeIfPresent(String.self, forKey: .locationName)
        
    }
}

struct DMHelpReponseModel:Mappable {
    
    let status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        
        
    }
}

struct DMReponseModel:Mappable {
    
    let url : String?
    let dimensions:Dimensions?
    enum CodingKeys: String, CodingKey {
        case url = "url"
        case dimensions = "dimensions"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        dimensions =  values.contains(.dimensions) == true ? try values.decodeIfPresent(Dimensions.self, forKey: .dimensions) : nil
    }
}



struct Dimensions:Mappable {
    
    let height : Int?
    let width:Int?
    enum CodingKeys: String, CodingKey {
        case height = "height"
        case width = "width"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        height = try values.decodeIfPresent(Int.self, forKey: .height)
        width = try values.decodeIfPresent(Int.self, forKey: .width)
        
    }
}



struct DMRequestFollow:Mappable {
    
    let status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        
        
    }
}




struct RequestAcceptRejectResponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let acceptRejectModel:RequestAcceptRejectModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case acceptRejectModel    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        acceptRejectModel =  values.contains(.acceptRejectModel) == true ? try values.decodeIfPresent(RequestAcceptRejectModel.self, forKey: .acceptRejectModel) : nil
        
    }
}

struct RequestAcceptRejectModel:Mappable {
    let receiverId : String?
    let senderId:String?
    enum CodingKeys: String, CodingKey {
        case receiverId  = "receiverId"
        case senderId = "senderId"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        receiverId = try values.decodeIfPresent(String.self, forKey: .receiverId)
        senderId =   try values.decodeIfPresent(String.self, forKey: .senderId)
    }
}


struct messageRequestAcceptRejectResponse:Mappable {
    
    let status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        
        
    }
}
