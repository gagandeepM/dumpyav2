//
//  NotificationDataModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 19/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import SwiftyJSON
enum NotificationType:Int {
    
    /*0-None, 1-redumpPush, 2- commentPush, 3-dumpsPush, 4- newFollowersPush, 5- acceptedFollowRequestPush, 6- directMessageRequestPush, 7- directMessagePush*/
    case None = 0
    case RedumpPush = 1
    case CommentPush = 2
    case DumpsPush = 3
    case NewFollowersPush = 4
    case AcceptedFollowRequestPush = 5
    case DirectMessageRequestPush = 6
    case DirectMessagePush = 7
    case follow = 8
    
}
struct NotificationModelData {
    var notiType:NotificationType = .None
    var notiId:String?
    var userName:String?
    var dumpId:String?
    var senderId:String?
    init(parse decoder:JSON) {
        let notiType = decoder["notificationType"].intValue
        if decoder["dumpId"].exists() {
            self.dumpId = decoder["dumpId"].stringValue
        }else{
            self.dumpId = ""
        }
        if decoder["senderId"].exists() {
            self.senderId = decoder["senderId"].stringValue
        }else{
            self.senderId = ""
        }
        if decoder["senderInfo"]["userName"].exists() {
            self.userName = decoder["senderInfo"]["userName"].stringValue
        }else{
            self.userName = ""
        }
        
        
        self.notiType = NotificationType(rawValue: notiType) ?? .None
        
    }
}
class NotificationVieWModel: NSObject {
    var notiModel:NotificationModelData?
    override init() {
        super.init()
    }
    init(parse apns:String) {
        super.init()
        let json = JSON.init(parseJSON: apns)
        notiModel = NotificationModelData(parse: json)
    }
    var notiId:String?{
        return notiModel?.notiId
    }
    var dumpId:String?{
        return notiModel?.dumpId
    }
    var notiType:NotificationType{
        return notiModel?.notiType ?? .None
    }
    var senderId:String?{
        return notiModel?.senderId
    }
    var userName:String?{
        
        return notiModel?.userName
    }
    func isSameChatRoom(chatUserVM:ChatViewModel)->Bool{
        if let senderid = self.senderId {
            return senderid == chatUserVM.userReceiverId ? true : false
        }else{
            
            return false
        }
    }
    var isOwner:Bool{
        guard let currentUserId = userModel?.userId , !currentUserId.isEmpty, let otherUserId = self.senderId , !otherUserId.isEmpty else{return false}
        
        return currentUserId == otherUserId ? true :false
    }
}
