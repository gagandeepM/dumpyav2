//
//  DumpeeDetailModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 22/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

struct DumpeeDetailResponseModel:Mappable {
    let status : Int?
    let message : String?
    let dumpeeData:DumpeeDetailModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case dumpeeData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        dumpeeData =  values.contains(.dumpeeData) == true ? try values.decodeIfPresent(DumpeeDetailModel.self, forKey: .dumpeeData) : nil
    }
}

struct DumpeeDetailModel:Mappable {
    let dumpeeCategoryIdModel:DumpeeCategoryIdModel?
    let dumpeeName:String?
    let dumpeeId:String?
    let dumpsInfo:DumpsInfoModel?
    let image:String?
    let description:String?
    let userId:String?
    let website:String?
    let twitterAccount:String?
    let updateCount:Int?
    enum CodingKeys: String, CodingKey {
        case dumpeeCategoryIdModel = "categoryInfo"
        case dumpeeName = "name"
        case dumpeeId = "_id"
        case dumpsInfo = "dumpsInfo"
        case image = "image"
        case description = "description"
        case userId = "userId"
        case website = "website"
        case twitterAccount = "twitterAccount"
        case updateCount = "updateCount"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        dumpeeName =
            try values.decodeIfPresent(String.self, forKey: .dumpeeName)
        dumpeeId =  try values.decodeIfPresent(String.self, forKey: .dumpeeId)
        
        description =  try values.decodeIfPresent(String.self, forKey: .description)
        image =  try values.decodeIfPresent(String.self, forKey: .image)
        dumpeeCategoryIdModel =  values.contains(.dumpeeCategoryIdModel) == true ? try values.decodeIfPresent(DumpeeCategoryIdModel.self, forKey: .dumpeeCategoryIdModel) : nil
        
        dumpsInfo =  values.contains(.dumpsInfo) == true ? try values.decodeIfPresent(DumpsInfoModel.self, forKey: .dumpsInfo) : nil
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        twitterAccount = try values.decodeIfPresent(String.self, forKey: .twitterAccount)
        updateCount = try values.decodeIfPresent(Int.self, forKey: .updateCount)
    }
}

struct DumpeeCategoryIdModel:Mappable {
    let categoryName : String?
    let userId:String?
    let categoryId:String?
    enum CodingKeys: String, CodingKey {
        case categoryName   = "categoryName"
        case userId = "userId"
        case categoryId = "_id"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
    }
}

struct DumpsInfoModel:Mappable {
    let allTime : Int?
    let dumps:Int?
    let today:Int?
    enum CodingKeys: String, CodingKey {
        case allTime   = "allTime"
        case dumps = "redumpcount"
        case today = "today"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        allTime = try values.decodeIfPresent(Int.self, forKey: .allTime)
        dumps = try values.decodeIfPresent(Int.self, forKey: .dumps)
        today = try values.decodeIfPresent(Int.self, forKey: .today)
    }
}




