//
//  FollowFollowersResponseModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 23/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct FollowResponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let data:FollowFollowersResponseModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case data    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        data =  values.contains(.data) == true ? try values.decodeIfPresent(FollowFollowersResponseModel.self, forKey: .data) : nil
        
    }
}




struct FaceBookFollowResponseModel:Mappable {
    
    let status : Int?
    let message : String?
    let data:FacebookFollowModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case data    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        data =  values.contains(.data) == true ? try values.decodeIfPresent(FacebookFollowModel.self, forKey: .data) : nil
        
    }
}

struct FollowFollowersResponseModel:Mappable {
    let totalCount : Int?
    var followFollowerList:[FollowFollowersModel] = [FollowFollowersModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount  = "totalCount"
        case followFollowerList    = "requestList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let followFollowerList = try values.decodeIfPresent([FollowFollowersModel].self, forKey: .followFollowerList) else{return}
        self.followFollowerList = followFollowerList
        
    }
}


struct FacebookFollowModel:Mappable {
    let totalCount : Int?
    var followFollowerList:[FollowFollowersModel] = [FollowFollowersModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount  = "totalCount"
        case followFollowerList    = "userList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let followFollowerList = try values.decodeIfPresent([FollowFollowersModel].self, forKey: .followFollowerList) else{return}
        self.followFollowerList = followFollowerList
        
    }
}
enum FollowState:Int,CustomStringConvertible {
    case follower = 0
    case unfollow = 1
    case following = 2
    case none = -1
    var description: String{
        switch self {
        case .follower:
            return "follow".localized
        case .following:
            return "followingBtn".localized
        case .unfollow:
            return "unfollow".localized
        default:
            return ""
        }
    }
  
}
struct FollowFollowersModel:Mappable {
    let fullName : String?
    let userName : String?
    let profilePic : String?
    let followStatus:Int?
    let isFollower:Int?
    let isFollowed:Int?
    let userId:String?
    
    enum CodingKeys: String, CodingKey {
        case fullName  = "fullName"
        case userName = "userName"
        case profilePic = "profilePic"
        case followStatus = "followStatus"
        case isFollower = "isFollower"
        case isFollowed = "isFollowed"
        case userId = "userId"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        followStatus = try values.decodeIfPresent(Int.self, forKey: .followStatus)
        isFollower = try values.decodeIfPresent(Int.self, forKey: .isFollower)
        isFollowed = try values.decodeIfPresent(Int.self, forKey: .isFollowed)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        
        
    }
    var followState:FollowState? {
        if let follow = self.isFollowed {
            return FollowState(rawValue: follow)
        }
        else{
            return .none
        }
    }
}
