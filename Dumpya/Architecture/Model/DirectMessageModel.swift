//
//  DirectMessageModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 17/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation


struct DirectMessageModel:Mappable {
   
    let lastMessage:String?
    let userName:String?
    let profilePic:String?
    let userId:String?
    let messageTime:String?
    let receiverId:String?
    let conversationId:String?
    let messageUnreadCount:Int?
    let lastMessageType:String?
    enum CodingKeys:String,CodingKey {
        case lastMessage = "lastMessage"
        case userName = "userName"
        case profilePic = "profilePic"
        case receiverId = "receiverId"
        case userId = "userId"
        case messageTime = "messageTime"
        case messageUnreadCount = "messageUnreadCount"
        case conversationId = "conversationId"
        case lastMessageType = "lastMessageType"
       
     }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        lastMessage =  (values.contains(.lastMessage) == true) ? try values.decodeIfPresent(String.self, forKey: .lastMessage) : ""
       userName =  (values.contains(.userName) == true) ? try values.decodeIfPresent(String.self, forKey: .userName) : ""
        receiverId =  (values.contains(.receiverId) == true) ? try values.decodeIfPresent(String.self, forKey: .receiverId) : ""
        lastMessageType =  (values.contains(.lastMessageType) == true) ? try values.decodeIfPresent(String.self, forKey: .lastMessageType) : ""
        
        conversationId =  (values.contains(.conversationId) == true) ? try values.decodeIfPresent(String.self, forKey: .conversationId) : ""
        messageUnreadCount =  (values.contains(.messageUnreadCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .messageUnreadCount) : 0
        
        userId = (values.contains(.userId) == true) ? try values.decodeIfPresent(String.self, forKey: .userId) : ""
        
        messageTime = (values.contains(.messageTime) == true) ? try values.decodeIfPresent(String.self, forKey: .messageTime) : ""
      profilePic =   (values.contains(.profilePic) == true) ? try values.decodeIfPresent(String.self, forKey: .profilePic) : ""
        
    }
}


struct DirectMessageData:Mappable {
   
    let totalRequestCount:Int?
    var directMessageModel:[DirectMessageModel] = [DirectMessageModel]()
    enum CodingKeys:String,CodingKey {
        case totalRequestCount = "totalRequestCount"
        case directMessageModel = "messageData"
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        totalRequestCount = try values.decodeIfPresent(Int.self, forKey: .totalRequestCount)
        
        guard let directMessageModel =  try values.decodeIfPresent([DirectMessageModel].self, forKey: .directMessageModel) else{return}
        
        self.directMessageModel = directMessageModel
    }
}

struct DirectMessageResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    var directMessageData:DirectMessageData?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case directMessageData = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
       directMessageData =  values.contains(.directMessageData) == true ? try values.decodeIfPresent(DirectMessageData.self, forKey: .directMessageData) : nil
    }
}

/*
 
 data = 1;
 message = "Message deleted successfully";
 statusCode = 200;
 */

struct DeleteMessageResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
   
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}
