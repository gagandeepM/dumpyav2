//
//  ChatModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 14/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ChatModel:Mappable {
    let conversationId:String?
    
    enum CodingKeys:String,CodingKey {
        case conversationId = "_id"
        
        
        
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        conversationId = (values.contains(.conversationId) == true) ? try values.decodeIfPresent(String.self, forKey: .conversationId) : ""
    }
}



struct ChatResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    let chatModel:ChatModel?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case chatModel = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        chatModel =  values.contains(.chatModel) == true ? try values.decodeIfPresent(ChatModel.self, forKey: .chatModel) : nil
    }
}



/*CHAT SCREEN RESPONSE MODEL*/


struct DMChatResponseModel:Mappable {
    
    let statusCode:Int?
    let message:String?
    let chatHistoryModel:ChatHistoryModel?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case chatHistoryModel = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        chatHistoryModel =  values.contains(.chatHistoryModel) == true ? try values.decodeIfPresent(ChatHistoryModel.self, forKey: .chatHistoryModel) : nil
    }
}


struct ChatHistoryModel:Mappable {
    

    var totalCount : Int = 0
    var isChatEnable : Bool = false
    var messages:[DMChatSectionModel] = [DMChatSectionModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount      = "totalMessages"
        case messages = "chatHistory"
        case isChatEnable   = "isChatEnabled"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount) ?? 0
        isChatEnable = try values.decode(Bool.self, forKey: .isChatEnable)
        guard let chatHistorylist =  try values.decodeIfPresent([DMMessageModel].self, forKey: .messages) else{return}
        
        var chatSectionlist:[DMChatSectionModel] = []
        let groups = Dictionary(grouping: chatHistorylist, by: {$0.date?.sectionDateString})
   
        for item in groups{
            if let key  = item.key{
                var section  = DMChatSectionModel(item.value)
                section.date = groups[key]?.first(where: {$0.date?.sectionDateString == key})?.date
                chatSectionlist.append(section)
            }
        }
        self.messages = chatSectionlist
        
    }
 
}
extension ChatHistoryModel:Equatable{
    static func == (lhs: ChatHistoryModel, rhs: ChatHistoryModel) -> Bool {
        return lhs.totalCount == rhs.totalCount && lhs.isChatEnable == rhs.isChatEnable
    }


}

extension DMChatSectionModel:Equatable{
    static func == (lhs: DMChatSectionModel, rhs: DMChatSectionModel) -> Bool {
        return lhs.displayDate == rhs.displayDate && lhs.date == rhs.date
    
    }
}
struct DMChatSectionModel:Mappable {


    var date:Date?
    var messages:[DMMessageModel] = [DMMessageModel]()
   
  
    init(message:DMMessageModel, createDate:Date?) {
        self.date = createDate
        if let messageId = message.messageId,!messageId.isEmpty, let idx = self.messages.index(where: {$0.messageId! == messageId}){
            self.messages[idx] = message
        }else{
            self.messages.append(message)
        }

    }
    init(_ messages:[DMMessageModel]) {
        self.messages = messages
      
        
    }
    var displayDate : String?{
        return date?.sectionDateString
    }
}

struct DMMessageModel:Mappable{
    var messageId:String!
    var message:String?
    var media:String?
    var messageType:String?
    var senderId:String?
    var receiverId:String?
    var createdTime:String?
    var conversationId:String?
    var isRead:Bool = false
    var mediaSize:CGSize = .zero
    var isChatRefresh:Bool = false
 
    //local conversions

    var date:Date?

    enum CodingKeys:String,CodingKey {
        case messageId      = "_id"
        case message        = "message"
        case media          = "media"
        case messageType    = "messageType"
        case senderId       = "senderId"
        case receiverId     = "receiverId"
        case createdTime    = "createdTime"
        case isRead         =  "isRead"
        case conversationId = "conversationId"
        case isChatRefresh  = "chatRefresh"
    }
    
    init(from decoder:Decoder) throws {
        
        let values = try decoder.container(keyedBy:CodingKeys.self)
        messageId = try values.decodeIfPresent(String.self, forKey: .messageId)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        messageType = try values.decodeIfPresent(String.self, forKey: .messageType)
        senderId = try values.decodeIfPresent(String.self, forKey: .senderId)
        receiverId = try values.decodeIfPresent(String.self, forKey: .receiverId)
        createdTime = try values.decodeIfPresent(String.self, forKey: .createdTime)
        conversationId = try values.decodeIfPresent(String.self, forKey: .conversationId)
        isRead = try values.decodeIfPresent(Bool.self, forKey: .isRead) ?? false
        isChatRefresh = try values.decodeIfPresent(Bool.self, forKey: .isChatRefresh) ?? false
        self.convertDate()
    }
    var isIncomming:Bool{
        if let ownerId = userModel?.userId, !ownerId.isEmpty, let senderId = self.senderId,!senderId.isEmpty,ownerId != senderId {
            return true
        }else{
            return false
        }
    }
    var type:MessageType{
        guard let messageType = self.messageType else { return .text}
        return MessageType(rawValue: messageType) ?? .text
    }
   
    init (content:Any, messageType:MessageType, senderId:String?, receiverId:String) {
        self.messageType = messageType.description
        self.senderId = senderId
        self.receiverId = receiverId
        
        if messageType == .photo {
            self.media = "\(content)"
        }else{
            self.message = "\(content)"
        }
    }
    init(parse json:JSON) {
         messageId   = json["messageId"].stringValue
         createdTime = json["createdTime"].stringValue
         media       = json["media"].stringValue
         message     = json["message"].stringValue
         isRead      = json["isRead"].boolValue
         receiverId  = json["receiverId"].stringValue
         senderId    = json["senderId"].stringValue
         messageType = json["messageType"].stringValue
         conversationId = json["conversationId"].stringValue
         isChatRefresh = json["chatRefresh"].boolValue
         self.convertDate()
    }
   
    
   fileprivate mutating func convertDate(){
         if let createdTime = createdTime, !createdTime.isEmpty{
    
            let result = TimeZoneDateFormatter.dateFormatter(serverFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", serverTimeZoneType: .utc, localFormat: "MMM dd, yyyy HH:mm:ss", localTimeZoneType: .current).result(dateString: createdTime)//createdTime.timeZoneDateFormatter (localFormat: "MMM dd, yyyy", serverFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").date
            date = result.date
            
        }
    }
    var localTime:String!{
        return date?.messageTimeString ?? ""
    }
  
}

extension DMMessageModel:Equatable{
    static func == (lhs: DMMessageModel, rhs: DMMessageModel) -> Bool {
        return lhs.messageId == rhs.messageId && lhs.message == rhs.message && lhs.media == rhs.media && lhs.messageType == rhs.messageType && lhs.senderId == rhs.senderId && lhs.receiverId == rhs.receiverId && lhs.createdTime == rhs.createdTime && lhs.localTime == rhs.localTime   && lhs.conversationId == rhs.conversationId && lhs.isRead == rhs.isRead && lhs.mediaSize == rhs.mediaSize
    }
}

extension Date{
    var sectionDateString:String{
       return DateFormatterStyle.dateDisplay(dateStyle: .medium).result(date: self)
    }
    var messageTimeString:String{
        return DateFormatterStyle.timeDisplay(timeStyle: .short).result(date: self)
    }
}
