//
//  DMDumpFeedModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 18/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation

struct DMDumpFeedModel:Mappable {
    let totalCount : Int?
    var dumpList:[DMDumpModel] = [DMDumpModel]()
    var categoryList:[DMCategoryModel] = [DMCategoryModel]()
    
    enum CodingKeys: String, CodingKey {
        case totalCount   = "totalCount"
        case dumpList     = "dumpList"
        case categoryList = "categoryList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        
        guard let dumpList =  try values.decodeIfPresent([DMDumpModel].self, forKey: .dumpList) else{return}
        
        
        self.dumpList = dumpList
        
        guard let categoryList = try values.decodeIfPresent([DMCategoryModel].self, forKey: .categoryList) else{return}
        self.categoryList = categoryList
    }
}
struct DMDumpModel:Mappable {
    
    var dumpeeId:String?
    var topicName:String?
    var categoryId:String?
    var categoryName:String?
    var image:String?
    
    enum CodingKeys:String,CodingKey {
        case dumpeeId = "dumpeeId"
        case topicName = "name"
        case categoryId = "categoryId"
        case categoryName = "categoryName"
        case image = "image"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dumpeeId = try values.decodeIfPresent(String.self, forKey: .dumpeeId)
        topicName =   try values.decodeIfPresent(String.self, forKey: .topicName)
        
        image  =   try values.decodeIfPresent(String.self, forKey: .image)
        categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
        categoryName =   try values.decodeIfPresent(String.self, forKey: .categoryName)
        
    }
}

struct DMCategoryModel:Mappable {
    var categoryId:String?
    var categoryName:String?
    enum CodingKeys:String,CodingKey {
        case categoryId = "categoryId"
        case categoryName = "categoryName"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
        categoryName =   try values.decodeIfPresent(String.self, forKey: .categoryName)
    }
}

struct DMDumpFeedResponse:Mappable {
    let status : Int?
    let message : String?
    let dumpData:DMDumpFeedModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case dumpData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        dumpData =  values.contains(.dumpData) == true ? try values.decodeIfPresent(DMDumpFeedModel.self, forKey: .dumpData) : nil
    }
}



struct CommentResponseModel:Mappable {
    let status : Int?
    let message : String?
    let commentData:CommentModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case commentData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        commentData =  values.contains(.commentData) == true ? try values.decodeIfPresent(CommentModel.self, forKey: .commentData) : nil
    }
}

struct CommentModel:Mappable {
    let totalCount : Int?
    var commentList:[CommentDetailModel] = [CommentDetailModel]()
   
    enum CodingKeys: String, CodingKey {
        case totalCount   = "totalCount"
        case commentList = "commentList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
       
        guard let commentList = try values.decodeIfPresent([CommentDetailModel].self, forKey: .commentList) else{return}
        self.commentList = commentList
    }
}
struct CommentDetailModel:Mappable {
    
    var commentId:String?
    var commentMessage:String?
    var createTime:String?
    var userInfo:DMTimeLineUserInfoModel?
    
    enum CodingKeys:String,CodingKey {
        case commentMessage = "commentMessage"
        case userInfo = "userInfo"
        case commentId = "commentId"
        case createTime = "creatTime"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        commentMessage = try values.decodeIfPresent(String.self, forKey: .commentMessage)
        
        commentId = try values.decodeIfPresent(String.self, forKey: .commentId)
        
        createTime = try values.decodeIfPresent(String.self, forKey: .createTime)
        userInfo =  values.contains(.userInfo) == true ? try values.decodeIfPresent(DMTimeLineUserInfoModel.self, forKey: .userInfo) : nil
        
    }
}

//MARK:- History Response Model

struct HistoryResponseModel:Mappable {
    let status : Int?
    let message : String?
    let historyData:HistoryModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case historyData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        historyData =  values.contains(.historyData) == true ? try values.decodeIfPresent(HistoryModel.self, forKey: .historyData) : nil
    }
}

struct HistoryModel:Mappable {
   
    var historyList:[HistoryDetailModel] = [HistoryDetailModel]()
    
    enum CodingKeys: String, CodingKey {
       
        case historyList = "historyList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
      
        guard let suggestionList = try values.decodeIfPresent([HistoryDetailModel].self, forKey: .historyList) else{return}
        self.historyList = suggestionList
    }
}
struct HistoryDetailModel:Mappable {
    
    var name:String?
    var historyType:String?
    var searchUserId:String?
    var searchDumpeeId:String?
    var otherInfo:HistoryOtherInfo?
    
    enum CodingKeys:String,CodingKey {
        case name = "searchName"
        case historyType = "historyType"
        case searchUserId = "searchUserId"
        case searchDumpeeId = "searchDumpeeId"
        case otherInfo = "otherInfo"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
       name = try values.decodeIfPresent(String.self, forKey: .name)
        historyType =  try values.decodeIfPresent(String.self, forKey: .historyType)
        searchUserId = try values.decodeIfPresent(String.self, forKey: .searchUserId)
        searchDumpeeId = try values.decodeIfPresent(String.self, forKey: .searchDumpeeId)
        otherInfo = values.contains(.otherInfo) == true ? try values.decodeIfPresent(HistoryOtherInfo.self, forKey: .otherInfo) : nil
        
    }
}
struct HistoryOtherInfo:Mappable {
    var catagory:String?
    enum CodingKeys:String,CodingKey
    {
        case catagory = "catagory"
    }
    init(from decorder:Decoder) throws {
        let values = try decorder.container(keyedBy: CodingKeys.self)
        catagory = try values.decodeIfPresent(String.self, forKey: .catagory)
    }
}
