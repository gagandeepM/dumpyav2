//
//  DumpDetailModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct DumpDetailResponseModel:Mappable {
    let status : Int?
    let message : String?
    let dumpData:DMFeedTimeLineModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case dumpData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        dumpData =  values.contains(.dumpData) == true ? try values.decodeIfPresent(DMFeedTimeLineModel.self, forKey: .dumpData) : nil
    }
}





