//
//  SettingModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct DMSettingModel:Mappable {
    
    let status : Int?
    let message : String?
    let data:DMSettingPrefrenceModel?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case data    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        data =  values.contains(.data) == true ? try values.decodeIfPresent(DMSettingPrefrenceModel.self, forKey: .data) : nil
        
    }
}



struct DMSettingPrefrenceModel:Mappable {
    
    let isPrivateProfile : Bool?
    let isFacebookLinkedAccount:Bool?
    let commentControlStatus : Int?
    let pushSetting:PushSettingModel?
    enum CodingKeys: String, CodingKey {
        case isPrivateProfile     =  "isPrivateProfile"
        case commentControlStatus =  "commentControlStatus"
        case pushSetting          =  "pushSetting"
        case isFacebookLinkedAccount = "isFacebookLinkedAccount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        isPrivateProfile = try values.decodeIfPresent(Bool.self, forKey: .isPrivateProfile)
        commentControlStatus =   try values.decodeIfPresent(Int.self, forKey: .commentControlStatus)
        pushSetting =  values.contains(.pushSetting) == true ? try values.decodeIfPresent(PushSettingModel.self, forKey: .pushSetting) : nil
        isFacebookLinkedAccount = try values.decodeIfPresent(Bool.self, forKey: .isFacebookLinkedAccount)
    }
}

struct PushSettingModel:Mappable {
    let redumpPushStatus : Int?
    let commentPushStatus:Int?
    let acceptedFollowRequestPushStatus:Int?
    let directMessagePushStatus:Int?
    let dumpsPushStatus:Int?
    let newFollowersPushStatus:Int?
    let directMessageRequestPushStatus:Int?
    
   
    enum CodingKeys: String, CodingKey {
        case redumpPushStatus  = "redumpPushStatus"
        case commentPushStatus = "commentPushStatus"
        case acceptedFollowRequestPushStatus = "acceptedFollowRequestPushStatus"
        case directMessagePushStatus = "directMessagePushStatus"
        case dumpsPushStatus = "dumpsPushStatus"
        case newFollowersPushStatus = "newFollowersPushStatus"
        case directMessageRequestPushStatus = "directMessageRequestPushStatus"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        redumpPushStatus = try values.decodeIfPresent(Int.self, forKey: .redumpPushStatus)
        commentPushStatus =   try values.decodeIfPresent(Int.self, forKey: .commentPushStatus)
        acceptedFollowRequestPushStatus = try values.decodeIfPresent(Int.self, forKey: .acceptedFollowRequestPushStatus)
        
        directMessagePushStatus = try values.decodeIfPresent(Int.self, forKey: .directMessagePushStatus)
        dumpsPushStatus = try values.decodeIfPresent(Int.self, forKey: .dumpsPushStatus)
        newFollowersPushStatus = try values.decodeIfPresent(Int.self, forKey: .newFollowersPushStatus)
        directMessageRequestPushStatus = try values.decodeIfPresent(Int.self, forKey: .directMessageRequestPushStatus)
        
        
    }
}

struct DMBlockAccountModel:Mappable {
    
    let status : Int?
    let message : String?
    let data:DMBlockUser?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case data    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        data =  values.contains(.data) == true ? try values.decodeIfPresent(DMBlockUser.self, forKey: .data) : nil
        
    }
}


struct DMBlockUser:Mappable {
    let totalCount : Int?
    var userBlockList : [DMBlockUserInfo] = [DMBlockUserInfo]()
   
    enum CodingKeys: String, CodingKey {
        case totalCount  = "totalCount"
        case userBlockList    = "userList"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
       
        guard let userBlockList = try values.decodeIfPresent([DMBlockUserInfo].self, forKey: .userBlockList) else{return}
        self.userBlockList = userBlockList
        
    }
}

struct DMBlockUserInfo:Mappable {
    let firstName : String?
    let lastName : String?
    let profilePic : String?
    let blockUserId:String?
    let userName :String?
     enum CodingKeys: String, CodingKey {
        case firstName  = "firstName"
        case lastName = "lastName"
        case profilePic = "profilePic"
        case blockUserId = "userId"
        case userName = "userName"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
       lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        blockUserId = try values.decodeIfPresent(String.self, forKey: .blockUserId)
        
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
}
}
