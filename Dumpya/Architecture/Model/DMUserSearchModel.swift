//
//  DMUserSearchModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 01/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
struct DMUserSearchResponse:Mappable {
    let status : Int?
    let message : String?
    let userSearch:DMUserSearchData?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case userSearch = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        userSearch =  values.contains(.userSearch) == true ? try values.decodeIfPresent(DMUserSearchData.self, forKey: .userSearch) : nil
    }
}



struct DMUserSearchData:Mappable {
    
    var totalCount:Int?
    var userList:[DMUserSearchModel] = [DMUserSearchModel]()
    enum CodingKeys: String, CodingKey {
       
        case userList = "userList"
        case totalCount = "totalCount"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let userList = try values.decodeIfPresent([DMUserSearchModel].self, forKey: .userList) else{return}
        self.userList = userList
      
    }
}
struct DMUserSearchModel:Mappable {
   /*
     firstName = Manav;
     isFollowed = 0;
     isFollower = 0;
     lastName = Chopra;
     */
    var userName:String?
    var profilePic:String?
    var firstName:String?
    var lastName:String?
    var userId:String?
    enum CodingKeys: String, CodingKey {
        case userName  = "userName"
        case profilePic = "profilePic"
        case firstName = "firstName"
        case lastName = "lastName"
        case userId = "userId"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        
        firstName =   try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName =   try values.decodeIfPresent(String.self, forKey: .lastName)
        userName =   try values.decodeIfPresent(String.self, forKey: .userName)
       profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
    }
}





struct DMTagSearchResponse:Mappable {
    let status : Int?
    let message : String?
    let tagSearch:DMTagSearchData?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case tagSearch = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        tagSearch =  values.contains(.tagSearch) == true ? try values.decodeIfPresent(DMTagSearchData.self, forKey: .tagSearch) : nil
    }
}



struct DMTagSearchData:Mappable {
    
    var totalCount:Int?
    var hashList:[DMTagSearchModel] = [DMTagSearchModel]()
    enum CodingKeys: String, CodingKey {
        
        case hashList = "hashList"
        case totalCount = "totalCount"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        guard let hashList = try values.decodeIfPresent([DMTagSearchModel].self, forKey: .hashList) else{return}
        self.hashList = hashList
        
    }
}
struct DMTagSearchModel:Mappable {
  

    var hashName:String?
    var dumpCount:Int?
   
    enum CodingKeys: String, CodingKey {
        case hashName  = "hashName"
        case dumpCount = "dumpCount"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        
      
        hashName =   try values.decodeIfPresent(String.self, forKey: .hashName)
        dumpCount = try values.decodeIfPresent(Int.self, forKey: .dumpCount)
    }
}
