
import Foundation
struct DMStoriesModel:Mappable {
    let totalCount:Int?
    let storiesData:[DMStoriesModelData]?
    enum CodingKeys:String,CodingKey {
       case storiesData = "data"
       case totalCount = "totalCount"
    }
init(from decoder:Decoder) throws {
         let values = try decoder.container(keyedBy:CodingKeys.self)
         storiesData =  values.contains(.storiesData) == true ? try values.decodeIfPresent([DMStoriesModelData].self, forKey: .storiesData) : nil
         totalCount = (values.contains(.totalCount) == true) ? try values.decodeIfPresent(Int.self, forKey: .totalCount) : 0
    }
}

struct DMStoriesModelData:Mappable {
    let sire:String?
    enum CodingKeys:String,CodingKey {
        case sire = "sire"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        sire = (values.contains(.sire) == true) ? try values.decodeIfPresent(String.self, forKey: .sire) : ""
    }
}

struct DMStoriesResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    let stories:DMStoriesModel?
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case stories = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
         stories =  values.contains(.stories) == true ? try values.decodeIfPresent(DMStoriesModel.self, forKey: .stories) : nil
    }
}
