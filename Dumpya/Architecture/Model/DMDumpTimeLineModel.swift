//
//  DMDumpTimeLineModel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//


import Foundation

struct DMDumpTimeLineModel:Mappable {
    let totalCount : Int?
    var feedTimeLine:[DMFeedTimeLineModel] = [DMFeedTimeLineModel]()
    var categoryTimeLine:[DMCategoryTimeLineModel] = [DMCategoryTimeLineModel]()
    let defaultLanguage:String?
    enum CodingKeys: String, CodingKey {
        case totalCount       = "totalCount"
        case feedTimeLine     = "feedList"
        case categoryTimeLine = "categoryList"
          case defaultLanguage = "defaultLanguage"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        defaultLanguage =  try values.decodeIfPresent(String.self, forKey: .defaultLanguage)
        guard let feedTimeLine =  try values.decodeIfPresent([DMFeedTimeLineModel].self, forKey: .feedTimeLine) else{return}
        self.feedTimeLine = feedTimeLine
        guard let categoryTimeLine = try values.decodeIfPresent([DMCategoryTimeLineModel].self, forKey: .categoryTimeLine) else{return}
        self.categoryTimeLine = categoryTimeLine
       
    }
}
struct DMFeedTimeLineModel:Mappable {
    var states:Bool?
    var dumpTime:String?
    var topicName:String?
    var media:String?
    var isShowLocation:Bool?
    var type:String?
    var videoThumbnail:String?
    var bgCode:String?
    var content:String?
    var locationName:String?
    var isRedump:Bool?
    var redumpCount:Int?
    var commentCount:Int?
    var dumpFeedID:String?
    var dumpeeId:String?
    var feedPromotion:String?
    var source:String?
    var newsUrl:String?
    var feedPromotionCount:Int?
    var isFollowed:Int?
    var commentObj:DMCommentObjInfo?
    var imageinfo:DMTimeLineImageInfo?
    var userInfo:DMTimeLineUserInfoModel?
    var categoryInfo:DMCategoryModel?
    var commentPromotionalText:String?
   
    var feedType:DumpFeedType{
        guard let type = self.type else { return .gradient }
        return DumpFeedType.getFeedType(value: type)
        
    }
    enum CodingKeys:String,CodingKey {
        case dumpTime = "dumpTime"
        case topicName = "dumpeeName"
        case media = "media"
        case videoThumbnail = "videoThumbnail"
        case isShowLocation = "isShowLocation"
        case commentPromotionalText = "commentPromotionalText"
        case type = "dumpType"
        case isFollowed = "isFollowed"
        case locationName = "locationName"
        case bgCode = "bgCode"
        case content = "content"
        case feedPromotionCount = "feedPromotionCount"
        case redumpCount = "redumpCount"
        case commentCount = "commentCount"
        case userInfo = "userInfo"
        case imageinfo = "dimensions"
        case isRedump = "isRedumpedByMe"
        case dumpFeedID = "dumpId"
        case dumpeeId = "dumpeeId"
        case categoryInfo = "categoryInfo"
        case feedPromotion = "feedPromotion"
        case source = "source"
        case newsUrl = "newsUrl"
        case commentObj = "commentObj"
       
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dumpTime = try values.decodeIfPresent(String.self, forKey: .dumpTime)
        topicName =   try values.decodeIfPresent(String.self, forKey: .topicName)
        videoThumbnail =   try values.decodeIfPresent(String.self, forKey: .videoThumbnail)
        commentPromotionalText = try values.decodeIfPresent(String.self, forKey: .commentPromotionalText)
        isShowLocation = try values.decodeIfPresent(Bool.self, forKey: .isShowLocation)
        newsUrl =   try values.decodeIfPresent(String.self, forKey: .newsUrl)
        locationName =   try values.decodeIfPresent(String.self, forKey: .locationName)
        feedPromotionCount =   try values.decodeIfPresent(Int
            .self, forKey: .feedPromotionCount)
        feedPromotion = try values.decodeIfPresent(String.self, forKey: .feedPromotion)
        dumpeeId =   try values.decodeIfPresent(String.self, forKey: .dumpeeId)
        isRedump = try values.decodeIfPresent(Bool.self, forKey: .isRedump)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        dumpFeedID = try values.decodeIfPresent(String.self, forKey: .dumpFeedID)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        bgCode =   try values.decodeIfPresent(String.self, forKey: .bgCode)
        content =   try values.decodeIfPresent(String.self, forKey: .content)
        redumpCount =   try values.decodeIfPresent(Int.self, forKey: .redumpCount)
        commentCount =   try values.decodeIfPresent(Int.self, forKey: .commentCount)
        isFollowed =   try values.decodeIfPresent(Int.self, forKey: .isFollowed)
        userInfo =   values.contains(.userInfo) == true ? try values.decodeIfPresent(DMTimeLineUserInfoModel.self, forKey: .userInfo) : nil
        
        
        commentObj =   values.contains(.commentObj) == true ? try values.decodeIfPresent(DMCommentObjInfo.self, forKey: .commentObj) : nil
        
        
        imageinfo = values.contains(.imageinfo) == true ? try values.decodeIfPresent(DMTimeLineImageInfo.self, forKey: .imageinfo) : nil
        
        categoryInfo = values.contains(.categoryInfo) == true ? try values.decodeIfPresent(DMCategoryModel.self, forKey: .categoryInfo) : nil
        states = true
       
    }

}

struct DMCommentObjInfo:Mappable{
    
    var commentMessage:String?
    
    
    enum CodingKeys:String,CodingKey {
        case commentMessage = "commentMessage"
     
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        commentMessage = try values.decodeIfPresent(String.self, forKey: .commentMessage)
    }
}

struct DMTimeLineImageInfo:Mappable{
    
    var height:Int?
    var width:Int?
    var imageSize:CGSize{
        return CGSize(width: self.width ?? 0, height: self.height ?? 0)
    }
    
    enum CodingKeys:String,CodingKey {
        case height = "height"
        case width = "width"
        
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        height = try values.decodeIfPresent(Int.self, forKey: .height)
        width =   try values.decodeIfPresent(Int.self, forKey: .width)
        
    }
}


struct DMTimeLineUserInfoModel:Mappable{
    
    var firstName:String?
    var lastName:String?
    var locationName:String?
    var profilePic:String?
    var userId:String?
    var userName:String?
    var settingPrefrence : SettingPrefrence?
    var isFollowed:Int?
    enum CodingKeys:String,CodingKey {
        case firstName = "firstName"
        case lastName = "lastName"
        case locationName = "locationName"
        case profilePic = "profilePic"
        case userId = "userId"
        case userName = "userName"
        case settingPrefrence = "settingPrefrence"
        case isFollowed = "isFollowed"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName =   try values.decodeIfPresent(String.self, forKey: .lastName)
        settingPrefrence =  values.contains(.settingPrefrence) == true ? try values.decodeIfPresent(SettingPrefrence.self, forKey: .settingPrefrence) : nil
        locationName =   try values.decodeIfPresent(String.self, forKey: .locationName)
        profilePic =   try values.decodeIfPresent(String.self, forKey: .profilePic)
        userId =   try values.decodeIfPresent(String.self, forKey: .userId)
        
        userName =   try values.decodeIfPresent(String.self, forKey: .userName)
        isFollowed = try values.decodeIfPresent(Int.self, forKey: .isFollowed)
    }
    
}


struct DMCategoryTimeLineModel:Mappable {
    var categoryId:String?
    var categoryName:String?
    enum CodingKeys:String,CodingKey {
        case categoryId = "categoryId"
        case categoryName = "categoryName"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
        categoryName =   try values.decodeIfPresent(String.self, forKey: .categoryName)
    }
}

struct DMDumpTimeLineResponse:Mappable {
    let status : Int?
    let message : String?
    let dumpData:DMDumpTimeLineModel?
    
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case dumpData = "data"
       
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
       
        dumpData =  values.contains(.dumpData) == true ? try values.decodeIfPresent(DMDumpTimeLineModel.self, forKey: .dumpData) : nil
    }
}


struct DMRedumpResponseModel:Mappable {
    let status : Int?
    let message : String?
    let reDumpData:DMDumpRedumpResponse?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case reDumpData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        reDumpData =  values.contains(.reDumpData) == true ? try values.decodeIfPresent(DMDumpRedumpResponse.self, forKey: .reDumpData) : nil
    }
}

struct DMDumpRedumpResponse:Mappable {
    let isRedumpedByMe : Bool?
    let redumpCount:Int?
    let commentCount:Int?
    let feedPromotion:String?
    enum CodingKeys: String, CodingKey {
        case isRedumpedByMe  = "isRedumpedByMe"
        case redumpCount = "redumpCount"
        case commentCount = "commentCount"
        case feedPromotion = "feedPromotion"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //       
    //        
    //        isRedumpedByMe = try values.decodeIfPresent(Bool.self, forKey: .isRedumpedByMe)
    //        feedPromotion = try values.decodeIfPresent(String.self, forKey: .feedPromotion)
    //        redumpCount = try values.decodeIfPresent(Int.self, forKey: .redumpCount)
    //        commentCount = try values.decodeIfPresent(Int.self, forKey: .commentCount)
    //    }
}



struct DumpDeleteResponseModel:Mappable {
    let status : Int?
    let message : String?
    let reDumpData:String?
    enum CodingKeys: String, CodingKey {
        case status  = "statusCode"
        case message = "message"
        case reDumpData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        reDumpData =   try values.decodeIfPresent(String.self, forKey: .reDumpData)
        
        
    }
}
