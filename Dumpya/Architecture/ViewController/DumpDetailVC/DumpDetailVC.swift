//
//  DumpDetailVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
class DumpDetailVC: UIViewController {

    @IBOutlet weak fileprivate var scrollView: UIScrollView!
    @IBOutlet weak var commentPromotionalDetailBtn: UIButton!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var commentPromotionalTextLbl: UILabel!
    @IBOutlet weak fileprivate var readMoreLbl: UILabel!
    @IBOutlet weak fileprivate var privateUserLbl: UILabel!
    @IBOutlet weak fileprivate var readMoreBtn: UIButton!
    @IBOutlet weak var commentPromotionalHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak fileprivate var commentPromotionalView: UIView!
    @IBOutlet weak fileprivate var feedPromotionBtn: UIButton!
    @IBOutlet weak var gradientLbl: UILabel!
    @IBOutlet weak fileprivate var mediaImageRedumpBtn: UIButton!
    @IBOutlet weak var gradientLblHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var dumpuserName: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var imageheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileBtn: DMButton!
    @IBOutlet var objDumpDetailViewModel: DumpDetailViewModel!
    @IBOutlet weak var promotionalTextLbl: UILabel!
    @IBOutlet weak var comeemntCountLbl: UILabel!
    @IBOutlet weak var redumpCountlbl: UILabel!
    @IBOutlet weak fileprivate var contentLBl: ActiveLabel!
    @IBOutlet weak fileprivate var dumpeeName: UIButton!
    @IBOutlet weak fileprivate var timeLbl: UILabel!
    @IBOutlet weak var promotionalTextHeight: NSLayoutConstraint!
    
    @IBOutlet weak fileprivate var gradientView: DMGradientCard!
    @IBOutlet weak var locationNameLbl: UILabel!
    @IBOutlet weak var promotionalView: UIView!
    
    var notificationDumpId:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dumpDetail()
    }
    func refreshDumpDetail(){
        dumpDetail() 
    }
    
   fileprivate func dumpDetail()  {
    
    
        objDumpDetailViewModel.getDumpDetail(dumpId:notificationDumpId , onSuccess: {
            
              let source =   self.objDumpDetailViewModel.dumpDetailInfo()?.source ?? ""
            
            if source == ""
            {
                self.showAlert(message: "noFeedAvailable".localized, completion: { (index) in
                    self.navigationController?.popViewController(animated: true)
                })
               
            }
            
            if userModel?.userId == self.objDumpDetailViewModel.dumpDetailInfo()?.userInfo?.userId{
                
                
                self.privateUserLbl.isHidden = true
                self.scrollView.isHidden = false
                self.mediaImageRedumpBtn.isHidden = true
            }else{
                
                if self.objDumpDetailViewModel.dumpDetailInfo()?.userInfo?.settingPrefrence?.isPrivateProfile ?? false && self.objDumpDetailViewModel.dumpDetailInfo()?.isFollowed != 2 {
                    self.scrollView.isHidden = true
                    self.privateUserLbl.isHidden = false
                }
               
                else{
                    self.scrollView.isHidden = false
                    self.privateUserLbl.isHidden = true
                    self.mediaImageRedumpBtn.isHidden = false
                    if self.objDumpDetailViewModel.dumpDetailInfo()?.isRedump ?? false{
                        self.mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                    }
                    else{
                        self.mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                    }
                }
            }
            
            self.promotionalTextLbl.text = self.objDumpDetailViewModel.dumpDetailInfo()?.feedPromotion ?? ""
            self.commentPromotionalTextLbl.text = self.objDumpDetailViewModel.dumpDetailInfo()?.commentPromotionalText ?? ""
            self.contentLBl.handleHashtagTap { hashtag in
               
                let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
                
                
                vc?.hashTagName = hashtag
                
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            if self.objDumpDetailViewModel.dumpDetailInfo()?.feedPromotionCount ?? 0 > 2{
                
                self.feedPromotionBtn.isHidden = false
                
            }else{
                self.feedPromotionBtn.isHidden = true
            }
            
            let promotionalText =   self.objDumpDetailViewModel.dumpDetailInfo()?.feedPromotion ?? ""
            if promotionalText.isEmpty{
                UIView.performWithoutAnimation {
                   self.promotionalTextHeight.constant = 0
                }
            }else{
                UIView.performWithoutAnimation{
                     self.promotionalTextHeight.constant = 30.0
                }
            }
            
           
            let commentPromotionalText =   self.objDumpDetailViewModel.dumpDetailInfo()?.commentPromotionalText ?? ""
            if commentPromotionalText.isEmpty{
                UIView.performWithoutAnimation {
                    self.commentPromotionalHeightConstraint.constant = 0.0
                }
                
            }else{
                
                UIView.performWithoutAnimation {
                    self.commentPromotionalHeightConstraint.constant = 30.0
                }
            }
            
            
            
            if self.objDumpDetailViewModel?.dumpDetailInfo()?.type == "textImage"{
                self.videoBtn.isHidden = true
                guard let imageFile  = self.objDumpDetailViewModel?.dumpDetailInfo()?.media  else { return}
                guard let height2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.height else{return}
                guard let width2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.width else{return}
                
                if height2 > 0 && width2 > 0{
                    let w1 = self.feedImageView.bounds.size.width
                    let h1  = CGFloat(height2)/CGFloat(width2) * w1
                    self.imageheightConstraint.constant = h1
                    
                    
                    UIView.performWithoutAnimation {
                         self.view.layoutIfNeeded()
                    }
                   
                }
              
                
                
                self.feedImageView.loadImage(filePath: imageFile)
                self.contentLBl.text = self.objDumpDetailViewModel?.dumpDetailInfo()?.content ?? ""
                
                if self.contentLBl.countLines>3{
                    self.contentLBl.numberOfLines = 3
                    self.readMoreLbl.isHidden = false
                }else{
                    self.contentLBl.numberOfLines = 3
                    self.readMoreLbl.isHidden = true
                }
                
                self.gradientLblHeight.constant = 0.0
                
                UIView.performWithoutAnimation {
                     self.view.layoutIfNeeded()
                }
                
            }else if self.objDumpDetailViewModel?.dumpDetailInfo()?.type == "textVideo"{
                self.videoBtn.isHidden = false
                    guard let imageFile  = self.objDumpDetailViewModel?.dumpDetailInfo()?.videoThumbnail  else { return}
                    guard let height2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.height else{return}
                    guard let width2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.width else{return}
                    
                    if height2 > 0 && width2 > 0{
                        let w1 = self.feedImageView.bounds.size.width
                        let h1  = CGFloat(height2)/CGFloat(width2) * w1
                        self.imageheightConstraint.constant = h1
                        
                        
                        UIView.performWithoutAnimation {
                            self.view.layoutIfNeeded()
                        }
                        
                    }
                    
                    
                    
                    self.feedImageView.loadImage(filePath: imageFile)
                    self.contentLBl.text = self.objDumpDetailViewModel?.dumpDetailInfo()?.content ?? ""
                    
                    if self.contentLBl.countLines>3{
                        self.contentLBl.numberOfLines = 3
                        self.readMoreLbl.isHidden = false
                    }else{
                        self.contentLBl.numberOfLines = 3
                        self.readMoreLbl.isHidden = true
                    }
                    
                    self.gradientLblHeight.constant = 0.0
                    
                    UIView.performWithoutAnimation {
                        self.view.layoutIfNeeded()
                    }
                    
                }else if self.objDumpDetailViewModel?.dumpDetailInfo()?.type == "text"{
                
                self.imageheightConstraint.constant = 0.0
                
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
                
                
                self.videoBtn.isHidden = true
                
                self.contentLBl.text = self.objDumpDetailViewModel?.dumpDetailInfo()?.content ?? ""
                
                if self.contentLBl.countLines>3{
                    self.contentLBl.numberOfLines = 3
                    self.readMoreLbl.isHidden = false
                }else{
                    self.contentLBl.numberOfLines = 3
                    self.readMoreLbl.isHidden = true
                }
                
                self.gradientLblHeight.constant = 0.0
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
                
            }else if self.objDumpDetailViewModel?.dumpDetailInfo()?.type == "image"{
                self.videoBtn.isHidden = true
                guard let imageFile  = self.objDumpDetailViewModel?.dumpDetailInfo()?.media  else { return}
                guard let height2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.height else{return}
                guard let width2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.width else{return}
                let w1 = self.feedImageView.bounds.size.width
                let h1  = CGFloat(height2)/CGFloat(width2) * w1
                self.imageheightConstraint.constant = h1
                
                
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
               
                
                
                self.feedImageView.loadImage(filePath: imageFile)
                self.contentLBl.text = ""
                self.gradientLblHeight.constant = 0.0
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
                
            }else if self.objDumpDetailViewModel?.dumpDetailInfo()?.type == "video"{
                self.videoBtn.isHidden = false
                guard let imageFile  = self.objDumpDetailViewModel?.dumpDetailInfo()?.videoThumbnail  else { return}
                guard let height2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.height else{return}
                guard let width2 = self.objDumpDetailViewModel?.dumpDetailInfo()?.imageinfo?.width else{return}
                let w1 = self.feedImageView.bounds.size.width
                let h1  = CGFloat(height2)/CGFloat(width2) * w1
                self.imageheightConstraint.constant = h1
                
                
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
                
                
                
                self.feedImageView.loadImage(filePath: imageFile)
                self.contentLBl.text = ""
                self.gradientLblHeight.constant = 0.0
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                self.videoBtn.isHidden = true
                guard let gradientColor = self.objDumpDetailViewModel?.didSelect()?.bgCode else{return}
                
                self.gradientLbl.text = "Dumped \(self.objDumpDetailViewModel?.didSelect()?.topicName ?? "")"
                if gradientColor == "green"{
                    
                    self.gradientView.set(gradientColors: DMColor.deepGreenColor, secondColor: DMColor.deepGreenColor)
                    
                    
                }
                else if gradientColor == "purple" {
                    
                    self.gradientView.set(gradientColors: DMColor.deepBlueColor, secondColor:DMColor.lightBlueColor)
                    
                } else if gradientColor == "yellow"{
                    
                    self.gradientView.set(gradientColors: DMColor.deepYellowColor, secondColor:DMColor.deepYellowColor)
                    
                    
                }
                    
                else if gradientColor == "dark_red"{
                    
                    self.gradientView.set(gradientColors: DMColor.deepRedColor, secondColor:DMColor.lightRedColor)
                    
                    
                }
                    
                else{
                    
                    
                    self.gradientView.set(gradientColors: DMColor.deepRedColor, secondColor:DMColor.lightRedColor)
                    
                    
                    
                }
                
                self.imageheightConstraint.constant = 0.0
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
               
                self.contentLBl.text = ""
                self.gradientLblHeight.constant = 188.0
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
               
            }
            self.dumpuserName.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.userInfo?.userName ?? "")"
            
            self.locationNameLbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.locationName ?? "")"
            
            
            
            if source == "news"{
                self.timeLbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.dumpTime ?? "") \u{2022} Dumped this "
            }else{
                self.timeLbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.dumpTime ?? "") \u{2022} Dumped "
            }
            
            self.dumpeeName.setTitle( "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.topicName ?? "")", for: .normal)
            
            let redumpCount = self.objDumpDetailViewModel?.dumpDetailInfo()?.redumpCount ?? 0
            let commentCount = self.objDumpDetailViewModel?.dumpDetailInfo()?.commentCount ?? 0
            if redumpCount < 2{
                self.redumpCountlbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.redumpCount ?? 0) Redump"
            }
            else{
                self.redumpCountlbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.redumpCount ?? 0) Redumps"
            }
            
            if  commentCount < 2{
                self.comeemntCountLbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.commentCount ?? 0) " + "dumpComment".localized
            }
            else{
                self.comeemntCountLbl.text = "\(self.objDumpDetailViewModel?.dumpDetailInfo()?.commentCount ?? 0) " + "dumpComments".localized
            }
            
            
            guard let profilePic  = self.objDumpDetailViewModel?.dumpDetailInfo()?.userInfo?.profilePic  else { return}
            self.profileBtn.loadImage(filePath: profilePic, for: .normal)
            
            
            
        }, onFailure: {
            
        })
   
   
    
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let dumpInfo = objDumpDetailViewModel.didSelect() else{return}
            let userId = dumpInfo.userInfo?.userId ?? ""
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userId)
        }else if segue.identifier == SegueIdentity.kCommentedUserListSegue{
            
            
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
           
           objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:objDumpDetailViewModel.dumpDetailInfo()?.dumpFeedID ?? "")
        }else if segue.identifier == SegueIdentity.kNewsDetailSegue{
            let objNewsDetailVC : NewsDetailVC = segue.destination as! NewsDetailVC
           
            objNewsDetailVC.newsUrl =  objDumpDetailViewModel.dumpDetailInfo()?.newsUrl ?? ""
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:objDumpDetailViewModel.dumpDetailInfo()?.dumpFeedID ?? "")
            
        }
        else if segue.identifier ==  SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
           
            let dumpInfo = objDumpDetailViewModel.didSelect()
            objDumpeeDetail.objDumpeeDetail.set(userId: dumpInfo?.userInfo?.userId ?? "", dumpeeId: dumpInfo?.dumpeeId ?? "", dumpeeName: dumpInfo?.topicName ?? "")
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo?.dumpeeId ?? "")
            
        }
        else if segue.identifier == SegueIdentity.kCommentSegue{
            guard let dumpInfo = objDumpDetailViewModel.didSelect() else{return}
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: dumpInfo)
            
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let dumpInfo = objDumpDetailViewModel.didSelect() else{return}
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedID ?? "")
            
        }
    }
    
    @IBAction fileprivate func onClickPlay(_ sender: UIButton) {
        guard let obj  = self.objDumpDetailViewModel else{return}
       
        if let  url  = obj.dumpeeDetailInfo()?.videoThumbnail{
            _ = DMPicker.streamVideo(file: url)
        }
    }
    @IBAction func onClickDumpProfile(_ sender: UIButton) {
        performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
    }
    @IBAction fileprivate func onClickDumpName(_ sender: UIButton) {
        
        
        let source =   self.objDumpDetailViewModel.dumpDetailInfo()?.source ?? ""
        
        if source == "news"{
            performSegue(withIdentifier: SegueIdentity.kNewsDetailSegue, sender: sender)
        }else{
            performSegue(withIdentifier: SegueIdentity.kDumpeeDetailSegue, sender: sender)
        }
        
    }

    @IBAction func onClickDumpComment(_ sender: UIButton) {
performSegue(withIdentifier:SegueIdentity.kCommentSegue, sender: sender)
    }
    
    @IBAction func onClickCommentDetail(_ sender: UIButton) {
        performSegue(withIdentifier:SegueIdentity.kCommentSegue, sender: sender)
    }
    @IBAction fileprivate func onClickDumpFollowingDetail(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDumpDots(_ sender: UIButton) {
        dumpSetting(dumpId:objDumpDetailViewModel.didSelect()?.dumpFeedID ?? "",userID:objDumpDetailViewModel.didSelect()?.userInfo?.userId ?? "",source:objDumpDetailViewModel.didSelect()?.source ?? "",dumpeeName:objDumpDetailViewModel.didSelect()?.topicName ?? "", media: objDumpDetailViewModel.didSelect()?.media ?? "", content: objDumpDetailViewModel.didSelect()?.content ?? "")
    
        
        
    }
    
    @IBAction fileprivate func onClickCommentUserList(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentedUserListSegue , sender: sender)
        
    }
    

    @IBAction func onClickGradientRedump(_ sender: UIButton) {
    
        guard let dumpInfo = objDumpDetailViewModel.didSelect() else{return}
        
        
        if userModel?.userId == dumpInfo.userInfo?.userId{
            mediaImageRedumpBtn.isUserInteractionEnabled = false
        }else{
           
            self.objDumpDetailViewModel.dumpDetailRedump(dumpFeedId: dumpInfo.dumpFeedID ?? "",countryName: countryNameFromLocation ,onSuccess: {
                
                guard let feedPromotion =  self.objDumpDetailViewModel?.didSelectDumpDetailRedump() else{return}
                if feedPromotion.feedPromotion == ""{
                    
                    UIView.performWithoutAnimation {
                        self.promotionalTextHeight.constant = 0
                    }
                    
                    
                }else{
                    UIView.performWithoutAnimation {
                        self.promotionalTextHeight.constant = 55
                    }
                    
                }
                
                
                let redumpCount = self.objDumpDetailViewModel?.didSelectDumpDetailRedump()?.redumpCount ?? 0
                
                if redumpCount < 2{
                    self.redumpCountlbl.text = "\(self.objDumpDetailViewModel?.didSelectDumpDetailRedump()?.redumpCount ?? 0) Redump"
                }
                else{
                    self.redumpCountlbl.text = "\(self.objDumpDetailViewModel?.didSelectDumpDetailRedump()?.redumpCount ?? 0) Redumps"
                }
                
                
                if userModel?.userId == dumpInfo.userInfo?.userId{
                    self.mediaImageRedumpBtn.isUserInteractionEnabled = false
                    self.mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                }
                else{
                    self.mediaImageRedumpBtn.isUserInteractionEnabled = true
                    guard let isRedump = self.objDumpDetailViewModel?.didSelectDumpDetailRedump() else {
                        return
                    }
                    
                    if isRedump.isRedumpedByMe ?? false{
                        self.mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedText"), for:.normal)
                    }
                    else{
                        self.mediaImageRedumpBtn.setImage(UIImage(named: "icon_FeedTextMedia"), for:.normal)
                    }
                    
                }
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        
    }
    
    
    @IBAction func onClickRedumpCount(_ sender: Any) {
      performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    @IBAction func onClickReadMore(_ sender: Any) {
        contentLBl.numberOfLines = 0
        readMoreLbl.isHidden = true
    }
    fileprivate  func
        dumpSetting(dumpId:String,userID:String,source:String,dumpeeName:String = "",media:String,content:String) {
    
        
        if userID == userModel?.userId ?? ""{
            
            if source == "news"{
                let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle:.actionSheet)
                 let cancelActionButton = UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (UIAlertAction)in
                })
                actionSheetAlertController.addAction(cancelActionButton)
             var actions :[AlertActionModel] = [AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                 ]
                        
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    
                    if indexofSheet == 2{
                        self.objDumpDetailViewModel.deleteDumpDetail(dumpFeedId: dumpId, onSuccess: {
                            
                           self.navigationController?.popViewController(animated: true)
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }
                    
                   else if indexofSheet == 3{
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                    }else if indexofSheet == 4{
                        //copylink
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
           }else{
                
                var actions :[AlertActionModel] = [
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        //Edit
                        
                        let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                        dumpCreateVC?.isEdit = true
                        
                        guard let dumpeeInfo = self.objDumpDetailViewModel?.dumpeeDetailInfo() else{return}
                        dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID ?? ""
                        dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId ?? ""
                        dumpCreateVC?.dumpName = dumpeeInfo.categoryInfo?.categoryName ?? ""
                        let dumpeeType = dumpeeInfo.type ?? ""
                        let content = dumpeeInfo.content ?? ""
                        let media = dumpeeInfo.media ?? ""
                        let height = dumpeeInfo.imageinfo?.height ?? 0
                        let width = dumpeeInfo.imageinfo?.width ?? 0
                        dumpCreateVC?.topicName = dumpeeInfo.topicName ?? ""
                        let type = DumpFeedType.getFeedType(value: dumpeeType)
                        let size  = CGSize(width: width, height: height)
                        if type == .text{
                            dumpCreateVC?.params.type = .text(text:dumpeeInfo.content ?? "")
                        }
                        else if type == .textImage{
                            
                            dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                        }
                        else if type == .image{
                            dumpCreateVC?.params.type = .image(item: media, size: size)
                        }
                        else if type == .video{
                            dumpCreateVC?.params.type = .video(item: media, size: size)
                        } else if type == .textVideo{
                            dumpCreateVC?.params.type = .textVideo(item: media, text: content, size: size)
                            
                        }
                        else{
                            let bgCode = dumpeeInfo.bgCode ?? ""
                            var gradientColor:GradientColor = .red
                            if GradientColor.red.name == bgCode{
                                gradientColor = .red
                            }else if GradientColor.green.name == bgCode{
                                gradientColor = .green
                            }else if GradientColor.purple.name == bgCode{
                                gradientColor = .purple
                            }else if GradientColor.yellow.name == bgCode{
                                gradientColor = .yellow
                            }else if GradientColor.darkRed.name == bgCode{
                                gradientColor = .darkRed
                            }
                            dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                        }
                        
                        self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                    }else if indexofSheet == 3{
                        self.objDumpDetailViewModel.deleteDumpDetail(dumpFeedId: dumpId, onSuccess: {
                            self.navigationController?.popViewController(animated: true)
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                    }else if indexofSheet == 4{
                        //Share
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media, viewController: self,copyLink:false)
                    }else if indexofSheet == 5{
                        //Copy
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
            }
        }
            
        else{
            var actions :[AlertActionModel] = [
                
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Report
                   self.objDumpDetailViewModel.reportDumpDetail(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                }else if indexofSheet == 3{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                }else if indexofSheet == 4{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            }
            
        }
        
    }
}
