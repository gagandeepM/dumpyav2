//
//  CreateDumpeeVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 19/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class CreateDumpeeVC: UIViewController,UITextViewDelegate {
    @IBOutlet var objDumpeeUserDetail: DumpeeDetailViewModel!
    @IBOutlet weak fileprivate var textView: UITextView!
    @IBOutlet weak fileprivate var navigationHeading: UILabel!
    @IBOutlet var objUserViewModel: DMUserViewModel!
    fileprivate var coverPicURL:String = ""
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var selectCategoryBTN: DMButton!
    @IBOutlet var objDumpFeed: DMFeedViewModel!
    @IBOutlet weak var selectCategoryTF: DMTextField!
    @IBOutlet weak fileprivate var dumpeesTwitterAcoountTF: DMTextField!
    @IBOutlet weak fileprivate var dumpeesWebsiteTF: DMTextField!
    @IBOutlet weak fileprivate var descriptionTV: THTextView!
    @IBOutlet weak fileprivate var dumpeeNameTF: DMTextField!
   
    var isFromDumpeeDetail = false
    var dumpeeId:String?
    var category:String = ""
    var name:String = ""
    var catId:String  = ""
   fileprivate let termsAndConditionsURL = kTerms
   fileprivate let privacyURL = kPrivacy
   fileprivate var categoryId:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = "dumpeeTerms".localized
        descriptionTV.placeholder = "dumpeeDescription".localized
        self.textView.delegate = self
        let attributedString = NSMutableAttributedString(string: textView.text)
        let foundRange = attributedString.mutableString.range(of:"termsOfService".localized)
        attributedString.addAttribute(.link, value: termsAndConditionsURL, range: foundRange)
        attributedString.addAttribute(.underlineStyle, value: 1, range: foundRange)
        let foundRange1 = attributedString.mutableString.range(of:"privacyPolicy".localized)
        attributedString.addAttribute(.link, value: privacyURL, range: foundRange1)
        attributedString.addAttribute(.underlineStyle, value: 1, range: foundRange1)
        textView.attributedText = attributedString
       
        if isFromDumpeeDetail
        {
            navigationHeading.text = "editDumpee".localized
            self.dumpeeNameTF.isUserInteractionEnabled = false
            self.selectCategoryBTN.isUserInteractionEnabled = false
            loadDumpeeProfile()
        }
        else{
            navigationHeading.text = "createDumpee".localized
            self.dumpeeNameTF.isUserInteractionEnabled = true
            self.selectCategoryBTN.isUserInteractionEnabled = true
            loadInfo()
        }
    }

    
   fileprivate func loadInfo()  {
     //self.category = self.selectCategoryTF.text ?? ""
      //let dumpInfo = objDumpFeed.getDumpInfo()
    //print(dumpInfo)
      dumpeeNameTF.text = name
      selectCategoryTF.text = category
    categoryId = catId
  
    }
    func loadDumpeeProfile()
    {
        self.objDumpeeUserDetail.dumpeeId = self.dumpeeId ?? ""
        objDumpeeUserDetail.getDumpeeDetail(onSuccess: {
            DispatchQueue.main.async {
                self.setUI()
            }
            
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    func setUI()
    {
        DispatchQueue.main.async {
            let model = self.objDumpeeUserDetail.categoryData()
            self.dumpeeNameTF.text = model?.dumpeeName ?? ""
            self.selectCategoryTF.text = model?.dumpeeCategoryIdModel?.categoryName ?? ""
            self.categoryId = model?.dumpeeCategoryIdModel?.categoryId ?? ""
            self.dumpeesWebsiteTF.text = model?.website ?? ""
            self.dumpeesTwitterAcoountTF.text = model?.twitterAccount ?? ""
            self.descriptionTV.text = model?.description ?? ""
            if let image = model?.image {
                self.coverPicURL = image
                if !image.isEmpty{
                    self.coverImage.loadImage(filePath:image)
                }
            }
        }
    }
    @IBAction func onClickCamera(_ sender: Any) {
        
        self.showPicker(message: "") { (isPicked, image, filename) in
            if isPicked{
                
               self.coverImage.image =  image
                self.objUserViewModel.imageUpload(image: image!, onSuccess: { url in
                   
                    self.coverPicURL = url
                    
                }, onFailure: {
                    
                })
                
            }
        }
        
        
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(value: 1), range: NSMakeRange(44, 18))
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if (URL.absoluteString == termsAndConditionsURL) {
            let termsVC = UIStoryboard.init(name: "Privacy", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsVC") as? TermsVC
            self.navigationController?.pushViewController(termsVC!, animated: true)
        } else if (URL.absoluteString == privacyURL) {
           let policyVC = UIStoryboard.init(name: "Terms", bundle: Bundle.main).instantiateViewController(withIdentifier: "PolicyVC") as? PolicyVC
            self.navigationController?.pushViewController(policyVC!, animated: true)
        }
        return false
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        //self.navigationController.
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func onClickCreate(_ sender: Any) {
        if isFromDumpeeDetail
        {
            guard let description
                = descriptionTV.text else {return}
            guard let dumpeeWebsite = dumpeesWebsiteTF.text else {return}
            var str = dumpeeWebsite
            if let httpRange = str.range(of: "https?://", options: .regularExpression, range: nil, locale: nil) {
                str.removeSubrange(httpRange)
            
            }
                        guard let dumpeeTwitterAcoount = dumpeesTwitterAcoountTF.text else {return}
            objDumpeeUserDetail.updateDumpee(updateDumpeeId: self.dumpeeId ?? "", image:self.coverPicURL,description: description, website: str, twitterAccount: dumpeeTwitterAcoount, onSuccess: {
                 self.navigationController?.popViewController(animated: true)
            },onFailure: {
                
            }) { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            }
            
        }
        else
        {
        guard let dumpeename = dumpeeNameTF.text?.removeWhiteSpace else {return}
        guard let dumpeeCatergory = selectCategoryTF.text else {return}
        guard let description
            = descriptionTV.text else {return}
        guard let dumpeeWebsite = dumpeesWebsiteTF.text else {return}
            var str = dumpeeWebsite
            if let httpRange = str.range(of: "https?://", options: .regularExpression, range: nil, locale: nil) {
                str.removeSubrange(httpRange)
                
            }
            
        guard let dumpeeTwitterAcoount = dumpeesTwitterAcoountTF.text else {return}
//        guard let imageUrl = coverPicURL else{return}
            
        objDumpFeed.createDumpee(categoryId: categoryId, name: dumpeename,description: description, website: str, twitterAccount: dumpeeTwitterAcoount,image:coverPicURL ,dumpeeCatergory:dumpeeCatergory, onSuceess: {
            self.showAlert(message:"Dumpee created successfully", completion: { (index) in
            self.performSegue(withIdentifier:SegueIdentity.kDumpCreateSegue, sender: sender)
               
            })
        }, onFailure: {
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        }
    }
    
    fileprivate func showPicker(controller:CategoryPickerControllerVC, sourceView:DMButton){
        if let popoverController = controller.popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = sourceView.bounds
            popoverController.delegate = self
            let height:CGFloat = 190
            let width:CGFloat = 200
           controller.preferredContentSize = CGSize(width: width, height: height)
        }
    }
    
    //*** Set storyboard id same as VC name
//        self.navigationController!.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "TheLastViewController"))! as UIViewController, animated: true)
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kCategoryPickerSegue{
            let controller = segue.destination as! CategoryPickerControllerVC
            controller.delegate = self
            guard let souceView = sender as? DMButton else{return}
            self.showPicker(controller: controller, sourceView:souceView)
        }else if segue.identifier == SegueIdentity.kDumpCreateSegue{
             let objDumpeCreate : DumpCreateVC = segue.destination as! DumpCreateVC
            objDumpeCreate.objDumpVM.setDumpeeDetail(objModel: objDumpFeed.getDumpeeDetail())
        }
    }
 
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

extension CreateDumpeeVC:CategoryPickerControllerDelegate{
    func picker(pikcer: CategoryPickerControllerVC, didSelectCategory: String, categoryId: String) {
        print("THE STRING IS--------->",didSelectCategory)
        print("THE STRING IS--------->",categoryId)
        self.categoryId = categoryId
        selectCategoryTF.text = didSelectCategory
    }
   
}
  


extension CreateDumpeeVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController)-> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
}
