//
//  LeaderBoardVC.swift
//  Dumpya
//
//  Created by Chandan on 01/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
enum TimeTypes:String{
    case today = "today"
    case month = "month"
    case week = "week"
    case allTime = "allTime"
 }
class LeaderBoardVC: UIViewController {
    @IBOutlet weak fileprivate var searchBar: UISearchBar!
    @IBOutlet weak var allTimeLBL: UILabel!
    @IBOutlet weak var weekLBL: UILabel!
    @IBOutlet weak var monthLBL: UILabel!
    @IBOutlet weak var todayLBL: UILabel!
    @IBOutlet var objLeaderBoard: LeaderBoardViewModel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var timeDurationCV: UICollectionView!
    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate var timeDurations = ["today".localized,"week".localized,"month".localized,"allTime".localized]
    fileprivate var categoryId:String = ""
    fileprivate var dayType:TimeTypes = .today
    fileprivate var index:Int = 0
    fileprivate var indexTime:Int = 0
    fileprivate var searchText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        getLeaderBoard(dayType:dayType.rawValue)
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(LeaderBoardVC.searchText(notification:)), name: Notification.Name("isFromDumpeedetail"), object: nil)
    }
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
//    @objc fileprivate func refreshLeaderBaord() {
//
//        getLeaderBoard(search:"", categoryId: categoryId, dayType: dayType.rawValue)
//
//    }
    
    
    
    @objc func searchText(notification:NSNotification)
    {
        self.searchBar.text = self.searchText
        
    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    fileprivate func getLeaderBoard(search:String="",categoryId:String="",dayType:String=""){
        objLeaderBoard.getLeaderBoard(isPull:self.refreshControl.isRefreshing,search: search,categoryId:categoryId,dayType:dayType,onSuccess: {
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                self.tableView.reloadData()
                self.collectionView.reloadData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
  
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let dumpInfo = objLeaderBoard.dumpeeLeaderBoardInfo(at: selectedIndexPath.row)
            objDumpeeDetail.objDumpeeDetail.set(dumpeeId: dumpInfo.dumpeeId, dumpeeName: dumpInfo.dumpeeName)
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo.dumpeeId)
        }
    }
}
extension LeaderBoardVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if objLeaderBoard.numberOfRows() > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "noDumpee".localized
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objLeaderBoard.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let leaderBoardCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kLeaderBoardCell , for: indexPath) as!  LeaderBoardCell
        leaderBoardCell.objLeaderModel = objLeaderBoard.cellForRowAt(at:indexPath)
        return leaderBoardCell
    }
}
extension LeaderBoardVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == timeDurationCV{
            return timeDurations.count
        }else{
            return objLeaderBoard.numberOfRowsForCategory()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == timeDurationCV{
            let leaderBoardTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kLeaderBoardTimeCell, for: indexPath) as! LeaderBoardTimeCell
            leaderBoardTimeCell.timeDurationLBL.text = timeDurations[indexPath.row]
            if indexTime == indexPath.row{
                leaderBoardTimeCell.selectedLBL.isHidden = false
            }else{
                leaderBoardTimeCell.selectedLBL.isHidden = true
            }
            return leaderBoardTimeCell
        }else{
            let leaderBoardCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kLeaderBoardCategoryCell, for: indexPath) as! LeaderBoardCategoryCell
            leaderBoardCategoryCell.categoryLBL.text = objLeaderBoard.cellForRowAtForCategory(at: indexPath).categoryName
            if index == indexPath.row{
                leaderBoardCategoryCell.selectedLBL.isHidden = false
            }else{
                leaderBoardCategoryCell.selectedLBL.isHidden = true
            }
            return leaderBoardCategoryCell
        }
    }
}
extension LeaderBoardVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == timeDurationCV{
            let size = (timeDurations[indexPath.row]).size(withAttributes: nil)
           
            return CGSize(width:size.width + 60, height: timeDurationCV.bounds.height)
        }else{
            let size = (objLeaderBoard.cellForRowAtForCategory(at: indexPath).categoryName ?? ("" as NSString) as String).size(withAttributes: nil)
            
            return CGSize(width:size.width+40, height: collectionView.bounds.height)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == timeDurationCV {
            indexTime = indexPath.row
            
            if indexTime == 0{
                dayType = .today
            }else if indexTime == 1 {
                dayType = . week
            }else if indexTime == 2{
                dayType = .month
            }else{
                dayType = .allTime
            }
            getLeaderBoard(categoryId:categoryId,dayType:dayType.rawValue)
            timeDurationCV.reloadData()
        }else{
            index = indexPath.row
            categoryId = objLeaderBoard.cellForRowAtForCategory(at: indexPath).categoryId ?? ""
            getLeaderBoard(categoryId:categoryId,dayType:dayType.rawValue)
            collectionView.reloadData()
        }
    }
}


extension LeaderBoardVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
         self.searchText = searchText.removeWhiteSpace
        
        
        if searchText.count>0{
            getLeaderBoard(search: searchText, categoryId: categoryId, dayType: dayType.rawValue)
        }else{
            getLeaderBoard(categoryId: categoryId, dayType: dayType.rawValue)
        }
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
      
        searchBar.resignFirstResponder()
          searchBar.text = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
        getLeaderBoard(categoryId: categoryId, dayType: dayType.rawValue)
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        
    }
}



