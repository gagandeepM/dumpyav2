//
//  FollowingListVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class FollowingListVC: UIViewController {

    @IBOutlet var objFollowingListViewModel: RedumpedUserViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    var userId:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        redumpUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
    }
   
    fileprivate func redumpUser(){
        print(self.userId)
        objFollowingListViewModel.getRedumpUserList(userId: self.userId, page:0, onSuccess: {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) {
            
        }
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
        guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
        
        
        let searchUserId = objFollowingListViewModel.didSelectRedumpUser(at: selectedIndexPath.row)
        
        objProfileDetail.objUserDetailViewModel.set(currentUserId: searchUserId)
    
    
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension FollowingListVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {

            var numOfSections: Int = 0
            if objFollowingListViewModel.numberOfRows() > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections            = 1
                tableView.backgroundView = nil
                tableView.separatorStyle  = .none
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "noRedumpUser".localized
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
    
     
        
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objFollowingListViewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let redumpUserCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kRedumpUserCell, for: indexPath) as! RedumpUserCell
        
            redumpUserCell.objRedumpUser =   objFollowingListViewModel.cellForRowAt(indexPath: indexPath)
            
            return redumpUserCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return 75.0
    }
}

