
//
//  ChatVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//


import UIKit
import FirebaseDynamicLinks
import GrowingTextView
import IQKeyboardManagerSwift
import Alamofire
protocol CopyLinkDelegate : class {
   func copyLinkMessageCell(_ sender:MessageCell,copyLink:URL)
}
class MessageVC: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak fileprivate var emptyView: UIView!
    @IBOutlet weak var chatUserLbl: UILabel!
    @IBOutlet weak var viewMessage: DMCardView!
    @IBOutlet weak fileprivate var tableView:UITableView!
    @IBOutlet var objChatViewModel: ChatViewModel!
    @IBOutlet weak fileprivate var textView: GrowingTextView!{
        didSet{
            textView.trimWhiteSpaceWhenEndEditing = false
            textView.placeholder = "typeMessage".localized
            textView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
            textView.minHeight = 30.0
            textView.maxHeight = 80.0
            textView.backgroundColor = .white
            textView.layer.cornerRadius = 4.0
            textView.layer.borderWidth = 1.0
            textView.layer.borderColor = UIColor.gray.cgColor
            textView.clipsToBounds  = true
            textView.layer.masksToBounds = true
           
        }
    }
    @IBOutlet fileprivate var clipBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var sendBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstriant: NSLayoutConstraint!
    @IBOutlet fileprivate var clipBtn: DMButton!
    @IBOutlet fileprivate var sendBtn: DMButton!
    @IBOutlet fileprivate weak var attachmentView: UIView!
    @IBOutlet fileprivate weak var attachmentViewWidthConstraint: NSLayoutConstraint!
    fileprivate var objImageUpload:DMUserViewModel = DMUserViewModel()
    @IBOutlet fileprivate weak var inputBarHeightConstraint: NSLayoutConstraint!//49
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh),
                                 for:.valueChanged)
        refreshControl.tintColor = DMColor.redColor
        refreshControl.layoutIfNeeded()
        return refreshControl
    }()
    
    
    fileprivate var isEnableIQKeyobard:Bool = true{
        didSet{
            IQKeyboardManager.shared.enable = isEnableIQKeyobard
            IQKeyboardManager.shared.enableAutoToolbar = false
            IQKeyboardManager.shared.shouldResignOnTouchOutside = isEnableIQKeyobard
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
       tableView.addSubview(refreshControl)
        
        // *** Listen for keyboard show / hide ***
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    
    func refreshChatRoom(other:Bool = false){
       willEnterForeground()
    }
    
    @objc func didEnterBackground() {
        self.objChatViewModel.userOnlineObserver(isOnline: false)
        SocketIOManager.shared.socketDisconnect()
    }
    
    @objc func willEnterForeground() {
        
        SocketIOManager.shared.socketConnect(completion: {
            self.objChatViewModel.socketToggleObserver()
            })
        
         self.addSocketObserver()
    }
    
    //MARK: ViewController lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageNumber = 0
        totalRecords = 0
        textView.enablesReturnKeyAutomatically = true
        self.isEnableIQKeyobard = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
           willEnterForeground()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.objChatViewModel.userOnlineObserver(isOnline: false)
        SocketIOManager.shared.socketDisconnect()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        self.textView.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Methods
    func addSocketObserver() {
         objChatViewModel.set(reciverlbl: chatUserLbl)
         getConversationId()
    }
    
    
    //MARK: NotificationCenter handlers
    
    @objc func showKeyboard(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            _ = keyboardRectangle.height
            bottomConstriant?.constant = keyboardFrame.cgRectValue.height
            self.view.layoutIfNeeded()
            UIView.performWithoutAnimation {
                
                // make tableview scroll bottom
               self.tableView.scrollToBottomRow()
                
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstriant.constant = 0
        print("HIDE KEYBOARD")
        self.view.layoutIfNeeded()
        UIView.performWithoutAnimation {
            // make tableview scroll bottom
            self.tableView.scrollToBottomRow()
        }
    }
    @IBAction func onClickSend(_ sender: DMButton) {
        guard let textMessage = textView.text else{return}
        let trimText = textMessage.trimWhiteSpace
        if trimText.count > 0 {
            self.textView.text = ""
            sendMessageObserver(content: textMessage, messageType: .text)
        }else{
            alertMessage = "enterMessage".localized
         }
        
    }
    
    @objc func handleRefresh() {

        if objChatViewModel.serviceType == .pagging {
            pageNumber += 1
            self.getConversationlist(isLoader: false)
        }else{
            refreshControl.endRefreshing()
        }

    }
    
    
    //MARK:- BACK BUTTON
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickMedia(_ sender: DMButton) {
        
        self.showPicker(message: "") { (isPicked, image, filename) in
            if isPicked{
                self.objImageUpload.imageUpload(image: image!, onSuccess: { url in
                    print(url)
                    
                    self.sendMessageObserver(content: url, messageType: .photo)
                }, onFailure: {
                    
                })
                
            }
        }
        
    }
    
    
}
extension MessageVC:GrowingTextViewDelegate{
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}



extension MessageVC:UITableViewDataSource,UITableViewDelegate,CellSubclassDelegate{
    func buttonTapped(cell: MessageImageCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            // Note, this shouldn't happen - how did the user tap on a button that wasn't on screen?
            return
        }
        let model = self.objChatViewModel.didSelectForRowAt(at: indexPath)
        let type = model.type
        let isIcomming = model.isIncomming
        
        if isIcomming{
            if type == .photo{
                let newImageView = UIImageView()
                newImageView.loadImage(filePath: model.media ?? "")
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
        }else{
            if type == .photo{
                let newImageView = UIImageView()
                newImageView.loadImage(filePath: model.media ?? "")
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
        }
    }
    
    
    //MARK: Delegates
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = DMDateLabel()
        label.backgroundColor = UIColor.gray
        label.text = objChatViewModel.titleForHeaderInSection(section:section)
        label.textColor = .white
        label.font = Roboto.Regular.font(size: 12.0)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let containerView = UIView()
        containerView.addSubview(label)
        label.centerXAnchor.constraint(equalTo:containerView.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        return containerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objChatViewModel.numberOfSection()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objChatViewModel.numberOfRows(InSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = self.objChatViewModel.cellForRowAt(at: indexPath)
        let isIcomming = model.isIncomming
        let type = model.type
        
        if isIcomming{
            if type == .text{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverCell", for: indexPath) as! MessageCell
                
                cell.delegateCopyLink = self
                cell.model = model
                
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverPhotoCell", for: indexPath) as! MessageImageCell
                cell.model = model
                cell.delegate = self
                return cell
            }
            
        }else{
            if type == .text{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! MessageCell
                cell.delegateCopyLink = self
                cell.model = model
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderPhotoCell", for: indexPath) as! MessageImageCell
                cell.model = model
                cell.delegate = self
                return cell
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = self.objChatViewModel.cellForRowAt(at: indexPath)
        
        let type = model.type
        
        switch type {
        case .photo:
            return 230.0
        default:
            return 78.0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.textView.resignFirstResponder()
        }
        
    }
    
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        sender.view?.removeFromSuperview()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let model = self.objChatViewModel.cellForRowAt(at: indexPath)
        let isIcomming = model.isIncomming
        if isIcomming{
            return  false
        }else{
            return true
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete,objChatViewModel.numberOfRows(InSection: indexPath.section)>0{
            objChatViewModel.deleteMessage(indexPath: indexPath, tableView: tableView)
         }
    }
}

//MARK:- API FUNCTIONS-

extension MessageVC{
    
    
    fileprivate func getConversationId(){
        
        objChatViewModel.getConversationId( onSuccess: {
            self.objChatViewModel.userOnlineObserver()
            self.getConversationlist()
            self.readMessageObserver()
            self.getInstanceMessageObservar()
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    fileprivate  func sendMessageObserver(content:Any,messageType:MessageType) {
        objChatViewModel.send(content: content, messageType: messageType, completion: {(isChatRefresh)in
            DispatchQueue.main.async {
                if isChatRefresh{
                    self.getConversationlist(isLoader: false)
                }else{
                     self.tableView.reloadData()
                    self.tableView.scrollToBottomRow()
                }
               
                
                
            }
        })
        
    }
    
    fileprivate func getInstanceMessageObservar(){
        
        self.objChatViewModel.getInstanceMessageObserver {(_) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.scrollToBottomRow()
                //                if self.objChatViewModel.numberOfSection() > 0{
                //                    let indexPath = IndexPath(
                //                        row: self.objChatViewModel.numberOfRows(InSection:self.objChatViewModel.numberOfSection() - 1) - 1,
                //                        section: self.objChatViewModel.numberOfSection() - 1)
                //                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                //                }
            }
        }
    }
    
    fileprivate func readMessageObserver(){
        self.objChatViewModel.readNotify(completion: {(_) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.scrollToBottomRow()
                //                if self.objChatViewModel.numberOfSection() > 0{
                //                    let indexPath = IndexPath(
                //                        row: self.objChatViewModel.numberOfRows(InSection:self.objChatViewModel.numberOfSection() - 1) - 1,
                //                        section: self.objChatViewModel.numberOfSection() - 1)
                //                    self.tableView.beginUpdates()
                //                    self.tableView.reloadRows(at: [indexPath], with: .none)
                //                    self.tableView.endUpdates()
                //                }
            }
        })
    }
    //MARK:- CHAT HISTORY AND PAGINATION FUNCTIONALITY
    fileprivate func getConversationlist(isLoader:Bool = true) {
        
        objChatViewModel.getChat(page: pageNumber,isLoader:isLoader,onSuccess: {(_) in
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing{
                    let topIndex = IndexPath(row: 0, section: 0)
                    self.tableView.scrollToRow(at: topIndex, at: .top, animated: true)
                    self.refreshControl.endRefreshing()
                }
                else{
                    self.tableView.scrollToBottomRow()

                }
                self.tableView.reloadData()
     
                

            }
        })
        
    }
}
extension MessageVC:CopyLinkDelegate{
    func copyLinkMessageCell(_ sender: MessageCell, copyLink: URL) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath.row)
        print("THE CopyLink Is-------->",copyLink)
        self.deeplinking(incomingURL: copyLink)
    }
}
//MARK: Specific corner radius extension
extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


extension UITableView {
    
    
    func scrollToBottomRow() {
        DispatchQueue.main.async {
            guard self.numberOfSections > 0 else { return }
            
            // Make an attempt to use the bottom-most section with at least one row
            var section = max(self.numberOfSections - 1, 0)
            var row = max(self.numberOfRows(inSection: section) - 1, 0)
            var indexPath = IndexPath(row: row, section: section)
            
            // Ensure the index path is valid, otherwise use the section above (sections can
            // contain 0 rows which leads to an invalid index path)
            while !self.indexPathIsValid(indexPath) {
                section = max(section - 1, 0)
                row = max(self.numberOfRows(inSection: section) - 1, 0)
                indexPath = IndexPath(row: row, section: section)
                
                // If we're down to the last section, attempt to use the first row
                if indexPath.section == 0 {
                    indexPath = IndexPath(row: 0, section: 0)
                    break
                }
            }
            
            // In the case that [0, 0] is valid (perhaps no data source?), ensure we don't encounter an
            // exception here
            guard self.indexPathIsValid(indexPath) else { return }
            
            self.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    func indexPathIsValid(_ indexPath: IndexPath) -> Bool {
        let section = indexPath.section
        let row = indexPath.row
        return section < self.numberOfSections && row < self.numberOfRows(inSection: section)
    }
}

extension MessageVC{
    
    func deeplinking(incomingURL:URL){
        
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
            // ...
            if let dynamicLink = dynamicLink, let _ = dynamicLink.url {
                self.handleDynamicLink(dynamicLink)
            } else {
                // Check for errors
                
            }
            
        }
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        
        if dynamicLink.matchType == .weak{
        }else {
            
            guard let dynamickLink = dynamicLink.url else{return}
            let dumpId = self.getQueryStringParameter(url: "\(dynamickLink)", param: "dumpId") ?? ""
            
           
            guard let controller = dumpDetailStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDumpDetailVC) as? DumpDetailVC else{return}
            
                    controller.notificationDumpId = dumpId
            self.navigationController?.pushViewController(controller, animated: true)
             }
            
            
        }
    
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        print(url)
        guard let url1 = URLComponents(string: url) else { return nil }
        return url1.queryItems?.first(where: { $0.name == param })?.value
        
    }
}
