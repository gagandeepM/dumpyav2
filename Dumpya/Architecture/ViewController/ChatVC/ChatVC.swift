//
//  ChatVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class MessageVC: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak fileprivate var emptyView: UIView!
    @IBOutlet fileprivate var inputTextView: GrowingTextView!
    @IBOutlet weak fileprivate var tableView:UITableView!
    @IBOutlet weak fileprivate var bottomConstraint: NSLayoutConstraint! //*** Bottom Constraint of userTagView ***
    @IBOutlet fileprivate var clipBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var sendBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var clipBtn: JKButton!
    @IBOutlet fileprivate var sendBtn: JKButton!
    @IBOutlet fileprivate weak var attachmentView: UIView!
    @IBOutlet fileprivate weak var attachmentViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var inputbar: JKCardView!
    @IBOutlet fileprivate weak var inputBarHeightConstraint: NSLayoutConstraint!//49
    @IBOutlet fileprivate weak var userTagView: UserTagView!
    @IBOutlet fileprivate weak  var tagViewHeightConstraint: NSLayoutConstraint!
    fileprivate var attibutelist:[TagUserModel] = [TagUserModel]()
    fileprivate var items = [Message]()
    fileprivate var refreshControl:UIRefreshControl!
    
    var projectId:String = ""
    var taskId:String = ""
    
    fileprivate var isEnableIQKeyobard:Bool = true{
        didSet{
            IQKeyboardManager.shared.enable = isEnableIQKeyobard
            IQKeyboardManager.shared.enableAutoToolbar = false
            IQKeyboardManager.shared.shouldResignOnTouchOutside = isEnableIQKeyobard
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.isEnableIQKeyobard = false
        DispatchQueue.main.async {
            
            self.showUserTagView()
            self.loadTagView()
            self.customization()
            //self.refreshChat()
            self.updateChatAPNS()
        }
        
        
    }
    //MARK:- disableInputBar -
    func disableInputBar(){
        
        if screanType == .taskChat {
            if taskModel.TaskStatusId == TASKSTATUS.COMPLETED_ID  {
                clipBtn.isHidden = true
                clipBtnWidthConstraint.constant = 0
                attachmentViewWidthConstraint.constant = 85-32
                self.inputbar.layoutIfNeeded()
                self.sendBtnlayout(isHide: true)
                self.inputTextView.isEditable = false
                
                
                
            }else{
                clipBtn.isHidden = false
                clipBtnWidthConstraint.constant = 32
                attachmentViewWidthConstraint.constant = 85
                self.inputbar.layoutIfNeeded()
                self.sendBtnlayout(isHide: false)
                self.inputTextView.isEditable = true
                
                
            }
            
        }else  if screanType == .projectChat {
            
            if projectModel.ProjectStatusId == PROJECT_STATUS.CLOSED_ID {
                clipBtn.isHidden = true
                clipBtnWidthConstraint.constant = 0
                attachmentViewWidthConstraint.constant = 85-32
                self.inputbar.layoutIfNeeded()
                self.sendBtnlayout(isHide: true)
                self.inputTextView.isEditable = false
                
                
                
            }else{
                clipBtn.isHidden = false
                clipBtnWidthConstraint.constant = 32
                attachmentViewWidthConstraint.constant = 85
                self.inputbar.layoutIfNeeded()
                self.sendBtnlayout(isHide: false)
                self.inputTextView.isEditable = true
                
                
            }
            
        }
        
    }
    //MARK:- loadTagView -
    func loadTagView(){
        if userlist.count > 0 {
            
            
            userTagView.loadList(screanType: screanType, list: userlist) { (users:[TagUserModel]) in
                if attibutelist.count > 0 {
                    attibutelist.removeAll()
                }
                attibutelist = users
            }
            
            
            userTagView.didSelectedTagUser { (model:TagUserModel?, isFilter) in
                
                if isFilter , let obj = model,let str = obj.userName, !str.isEmpty  {
                    
                    var arrayWords = [String]()
                    arrayWords = self.inputTextView.text.components(separatedBy: " ")
                    arrayWords.remove(at: arrayWords.count-1)
                    arrayWords.append("@\(str)")
                    let text  = arrayWords.joined(separator: " ")
                    
                    self.inputTextView.text  = "\(text) "
                    self.showUserTagView()
                    if  IQKeyboardManager.shared.shouldResignOnTouchOutside == false {
                        
                        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
                    }
                    
                    
                }else{
                    
                }
            }
            
            inputTextView.textViewDidChange { (inputText:String!, filter:Bool) in
                if filter == true , inputText.count > 0{
                    self.clipBtnlayout()
                    
                    let arrayWords = inputText!.components(separatedBy: " ")
                    let lastWord = "\(arrayWords.last!)"
                    if  lastWord.hasPrefix("@"){
                        
                        self.showUserTagView(isShow: true)
                        let str = lastWord.replacingOccurrences(of: "@", with: "")
                        if  str.count > 0 {
                            
                            self.userTagView.filter(search: str)
                        }
                        
                    }else{
                        self.showUserTagView()
                    }
                    
                    
                }else{
                    self.showUserTagView()
                    self.clipBtnlayout(isHide: false)
                    
                }
                
                
                
            }
        }
    }
    //MARK:- showUserTagView -
    func showUserTagView(isShow:Bool = false){
        if isShow == true {
            
            self.view.layoutIfNeeded()
            self.tagViewHeightConstraint.constant = 120
            UIView.animate(withDuration: 0.6, animations: {
                self.userTagView.alpha = 1.0
                
                self.userTagView.reloadData()
                self.view.layoutIfNeeded()
            })
        }else{
            self.view.layoutIfNeeded()
            self.tagViewHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.userTagView.alpha = 0.0
                self.userTagView.reloadData()
                self.view.layoutIfNeeded()
            })
        }
    }
    //MARK:- loadChat -
//    func loadChat(){
//
//
//        DispatchQueue.main.async {
//            if  self.items.count > 0  {
//                for (idx, obj) in self.items.enumerated() {
//
//                    if obj.owner == .sender{
//                        if obj.status == .Sending {
//                            obj.status = .Sent
//                            self.items[idx] = obj
//                        }else if obj.status == .Sent {
//                            obj.status = .Read
//                            self.items[idx] = obj
//                        }
//
//                    }
//
//                }
//
//                self.tableView.reloadData()
//                if self.items.count>0{
//                    let lastindexPath = IndexPath(row: self.items.count - 1, section: 0)
//                    self.tableView.scrollToRow(at: lastindexPath, at: .bottom, animated: false)
//                }
//
//            }
//        }
//    }
    //MARK: ViewController lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.disableInputBar()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.isEnableIQKeyobard = true
        self.inputTextView.resignFirstResponder()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: Methods
    func customization() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshChat), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.addSubview(refreshControl)
        
        self.tableView.tableFooterView = UIView(frame: CGRect(x:0, y:0,width:self.view.frame.size.width,height:10))
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        
        // *** Listen for keyboard show / hide ***
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        
    }
    
    
    func updateChatAPNS(){
        refreshChat()
        if screanType == .projectChat {
            
            
            if userlist.count == 0 {
                
                self.loadProjectTeamlist(onComletion: { (teams:[ProjectTeamTaskModel], isSuccess) in
                    if isSuccess {
                        userlist = teams
                        self.loadTagView()
                    }
                })
            }else{
                self.loadTagView()
            }
            
        }else if screanType == .taskChat {
            if userlist.count == 0 {
                self.getTaskAssigneeList(onCompletion: { (assigneeTeams:[TaskAssigneeTeamModel], isSuccess) in
                    if isSuccess {
                        userlist = assigneeTeams
                        self.loadTagView()
                    }
                })
                
            }else{
                self.loadTagView()
            }
        }
        
    }
    func refreshChat(){
        
        if notiModel.notifType == .message {
            
            if screanType == .taskChat {
                if let container = self.navigationController?.visibleViewController as? TaskDetailContainer {
                    
                    container.updateTaskControllerTitle()
                }
                
            }else{
                
                if let container = self.navigationController?.visibleViewController as? ProjectDetailContainer {
                    
                    container.updateProjectControllerTitle()
                }
            }
            notiModel.notifType = .none
            
            
        }
        if !projectId.isEmpty {
            
            self.fetchData(projectId: projectId)
        }else{
            if self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }
        }
        
        
    }
    //MARK: - Downloads messages
    func fetchData(projectId:String) {
        //ChatManager.playReceivedSound()
        ChatManager.downloadAllMessages(from: self, projectId: projectId, ProjectTaskId: self.taskId) { (messages:[Message], isSuccess) in
            if isSuccess {
                
                if self.items.count > 0 {
                    self.items.removeAll()
                }
                self.items = messages
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                
                self.loadChat()
                
                ChatManager.readAllChatMessages(from: self, projectId: projectId, ProjectTaskId: self.taskId, completion: { (isRead:Bool) in
                    if isRead {
                        
                        self.loadChat()
                    }
                    
                    
                })
            }
            else{
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
            }
            
        }
        
        
    }
    
    //MARK: - composeMessage
    @IBAction func resendFailedMessage(_ sender:JKButton) {
        if self.items.count == 0 {return}
        let idx  = sender.tag
        let model  = items[idx]
        if model.status == .Failed {
            self.composeMessage(type: model.type, content: model.content!)
            items.remove(at: idx)
            self.tableView.reloadData()
        }
        
    }
    func composeMessage(type: MessageType, content: Any)  {
        
        self.clipBtnlayout(isHide: false)
        
        let message = Message(type: type, content: content, projectId: "\(projectId)", ProjectTaskId: "\(taskId)")
        message.owner = .sender
        message.status = .Sending
        if type == .document {
            let docUrl = content as? URL
            message.fileName =   docUrl?.lastPathComponent
        }
        
        items.append(message)
        self.tableView.reloadData()
        if self.items.count>0  {
            let lastindexPath = IndexPath(row: self.items.count - 1, section: 0)
            self.tableView.scrollToRow(at: lastindexPath, at: .bottom, animated: false)
            if let cell  = self.tableView.cellForRow(at: lastindexPath) as? MessageCell {
                cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
                UIView.animate(withDuration: 0.3, animations: {
                    cell.transform = CGAffineTransform.identity
                })
            }
            
        }
        
        ChatManager.playSendSound()
        ChatManager.send(from: self, message: message) { (json:JSON) in
            let status = json["Key"].intValue
            switch status {
            case 1:
                self.refreshChat()
                
                break
            default:
                
                if let idx = self.items.index(of: message){
                    message.status = .Failed
                    self.items[idx] = message
                    self.tableView.reloadData()
                }
                break
            }
        }
        
        
        
    }
    // MARK - Actions
    var documentfiles:[URL] = [URL]()
    @IBAction func showMessageMedia(_ sender: JKButton) {
        if self.items.count == 0 {return}
        let model = self.items[sender.tag]
        let type = model.type
        
        switch type {
        case .photo:
            var imageUrls = [String]()
            var images = [UIImage]()
            if let image = model.placeholderImage {
                
                
                images.append(image)
                
            }else if let imageFile =  model.content as? String {
                imageUrls.append(imageFile)
                
            }
            if images.count > 0 {
                
                // 2. create PhotoBrowser Instance, and present from your viewController.
                guard let originImage = sender.normalBackgroundImage else{return}
                self.photoBrowse(originImage: originImage, photolist: images, initializePageIndex: sender.tag, animatedFromView: sender)
            }else if imageUrls.count>0{
                guard let originImage = sender.normalBackgroundImage else{return}
                self.photoBrowse(originImage: originImage, photoUrls: imageUrls, initializePageIndex: sender.tag, animatedFromView: sender)
            }
            
            
        case .document:
            
            if let docStr =  model.content as? String  , !docStr.isEmpty, docStr.isVerifyURL {
                
                
                FileManager.getlocalfile(filename: "\(model.fileName!)", OnCompletion: { (destinationURL:URL?, isfileExists:Bool, filemanager:FileManager) in
                    documentfiles.removeAll()
                    if isfileExists ,(destinationURL != nil) {
                        
                        documentfiles.append(destinationURL!)
                        
                        if QLPreviewController.canPreview(destinationURL! as QLPreviewItem) {
                            
                            let quickLookController = QLPreviewController()
                            quickLookController.dataSource = self
                            quickLookController.delegate = self
                            quickLookController.currentPreviewItemIndex = 0
                            quickLookController.modalTransitionStyle = .crossDissolve
                            self.showTabbar(isShow: false)
                            
                            self.present(quickLookController, animated: true, completion: {
                                
                            })
                            
                        }
                        
                        
                    }else{
                        self.showAlert(message: "Please download the file first.")
                        
                    }
                })
                
            }
            
            
            
        default: break
        }
        
        
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.showUserTagView()
        self.clipBtnlayout(isHide: false)
        self.view.endEditing(true)
    }
    
    
    //MARK: - showOptions
  
    
    //MARK: - sendMessage
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextView.text {
            if text.count > 0 {
                self.composeMessage(type: .text, content: self.inputTextView.text!)
                self.inputTextView.text = ""
            }
        }
    }
    
    
    
    
    //MARK: NotificationCenter handlers
    
    func showKeyboard(notification: Notification) {
        
        if  let frame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height - 49
            print("SHOW KEYBOARD")
            bottomConstraint.constant = height
            self.view.layoutIfNeeded()
            UIView.performWithoutAnimation {
                
                self.setNeedsStatusBarAppearanceUpdate()
                if self.items.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
                }
                
            }
            
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        print("HIDE KEYBOARD")
        self.view.layoutIfNeeded()
        UIView.performWithoutAnimation {
            
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
            
        }
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "MessageMediaSegue" {
//
//            let controller = segue.destination as! MessageMediaVC
//            controller.projectId = "\(projectId)"
//            controller.taskId = "\(taskId)"
//            self.showTabbar(isShow: false)
//            controller.onCancel(completion: { (isCancel) in
//                if isCancel{
//
//                    self.showTabbar(isShow: true)
//
//                }
//            })
//        }
//    }
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
}
extension MessageVC:GrowingTextViewDelegate{
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
extension MessageVC:UITextViewDelegate{
    
    
    //MARK:- clipBtnlayout -
    func clipBtnlayout(isHide:Bool = true) {
        if isHide == true{
            /// self.clipBtnWidthConstraint.constant = 0
            self.attachmentViewWidthConstraint.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.attachmentView.alpha = 0.0
                self.inputbar.layoutIfNeeded()
            })
        }else{
            //self.clipBtnWidthConstraint.constant = 32
            self.attachmentViewWidthConstraint.constant = 85
            UIView.animate(withDuration: 0.3, animations: {
                self.attachmentView.alpha = 1.0
                self.inputbar.layoutIfNeeded()
            })
        }
    }
    //MARK:- sendBtnlayout -
    func sendBtnlayout(isHide:Bool = true) {
        if isHide == true {
            self.sendBtnWidthConstraint.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.sendBtn.alpha = 0.0
                self.inputbar.layoutIfNeeded()
            })
        }else{
            self.sendBtnWidthConstraint.constant = 32
            UIView.animate(withDuration: 0.3, animations: {
                self.sendBtn.alpha = 1.0
                self.inputbar.layoutIfNeeded()
            })
        }
    }
    
    
}




extension MessageVC:UITableViewDataSource,UITableViewDelegate{
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging ,self.items.count>0{
            let lastIndexPath = IndexPath.init(row: self.items.count - 1, section: 0)
            if lastIndexPath.compare(indexPath) == .orderedSame {
                cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
                UIView.animate(withDuration: 0.3, animations: {
                    cell.transform = CGAffineTransform.identity
                })
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.items[indexPath.row]
        let owner = model.owner
        let type = model.type
        
        switch owner {
        case .receiver:
            switch type {
            case .text:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverCell", for: indexPath) as! MessageCell
                cell.loadMessageTextCellData(model: model, tagUserList: attibutelist)
                return cell
                
           default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverPhotoCell", for: indexPath) as! MessageCell
                cell.rowIndex = indexPath.row
                cell.loadMessagePhotoCellData(model: model)
                
                return cell
                
            }
            
            
        case .sender:
            
            switch type {
            case .text:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! MessageCell
                cell.loadMessageTextCellData(model: model, tagUserList: attibutelist)
                cell.resendButton.tag = indexPath.row
                
                return cell
                
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderPhotoCell", for: indexPath) as! MessageCell
                
                cell.rowIndex = indexPath.row
                cell.resendButton.tag = indexPath.row
                cell.loadMessagePhotoCellData(model: model)
                return cell
                
                
           
                
            }
            
            
            
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      //  let model = self.items[indexPath.row]
        
//        let type = model.type
//
let type = "photo"
        switch type {
        case .photo:
            return 184
        default:
            return 97
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.inputTextView.resignFirstResponder()
//            self.showUserTagView()
//            self.clipBtnlayout(isHide: false)
            
        }
    }
    
    
    
}
