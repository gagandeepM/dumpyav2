//
//  SettingsVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 30/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var termsOfServiceView: UIView!
    @IBOutlet weak var privacyPolicyView: UIView!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var linkedAccountView: UIView!
    @IBOutlet weak var commentControlView: UIView!
    @IBOutlet weak var BlockAccountView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var langugaeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contactsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pushNotificationView: UIView!
    @IBOutlet weak var chnagePasswordHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak fileprivate var privateAccountSwitch: UISwitch!
    @IBOutlet var objSettings: SettingsViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIView.animate(withDuration: 0.4) {
            self.langugaeHeightConstraint.constant = 55.0
             self.contactsHeightConstraint.constant = 0.0
        }
        
        if  userModel?.socialType == "normal"{
            pushNotificationView.backgroundColor = UIColor(hexString: "F6F6F6")
            
            
            UIView.animate(withDuration: 0.4) {
                self.chnagePasswordHeightConstraint.constant = 55.0
            }
        }
        else{
           pushNotificationView.backgroundColor = UIColor.white
            BlockAccountView.backgroundColor = UIColor(hexString: "F6F6F6")
            commentControlView.backgroundColor = UIColor.white
            linkedAccountView.backgroundColor = UIColor(hexString: "F6F6F6")
            languageView.backgroundColor = UIColor.white
            helpView.backgroundColor = UIColor(hexString: "F6F6F6")
            privacyPolicyView.backgroundColor = UIColor.white
            termsOfServiceView.backgroundColor = UIColor(hexString: "F6F6F6")
            aboutView.backgroundColor = UIColor.white
            logoutView.backgroundColor = UIColor(hexString: "F6F6F6")
            UIView.animate(withDuration: 0.4) {
               self.chnagePasswordHeightConstraint.constant = 0.0
            }
        }
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
         self.getData()
    }
   fileprivate func getData() {
        
        objSettings.getSettingPrefrence(onSuccess: { (isPrivateAccount,isRedump,isComment,acceptedFollowRequestPushStatus,directMessagePushStatus,directMessageRequestPushStatus,dumpsPushStatus,newFollowersPushStatus,commnetControl) in
            if isPrivateAccount{
                self.privateAccountSwitch.isOn = true
            }else{
                self.privateAccountSwitch.isOn = false
            }
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @IBAction func onClickClearHistory(_ sender: Any) {
        clearSearchHistory()
       
        
    }
    
    
    
    func clearSearchHistory(title:String = kAppTitle,message:String = "clearHistory".localized){
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: message, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.DeepOrange)
        let cancel = AlertActionModel(image: nil, title:"NO".localized, color: DMColor.ACRed, style: .cancel)
        let destructive = AlertActionModel(image: nil, title:"YES".localized, color: DMColor.Red, style: .destructive)
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: [cancel,destructive]) { (alert:UIAlertController, action:UIAlertAction, buttonIndex:Int) in
            
            switch buttonIndex {
            case 1:
                self.objSettings.clearSearchHistory(onSuccess: {
                    
                }, onFailure: {
                    
                    
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                
                break
                
            default:
                break
            }
        }
    }
    @IBAction func onClickPrivateAccount(_ sender: UISwitch) {
        
        
        if sender.isOn{
           objSettings.setSettingPrefrence(type:"isPrivateProfile", typeValue: sender.isOn, onSuccess: {
                
            }, onFailure: {
                
           }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
           })
        }
        else{
            objSettings.setSettingPrefrence(type:"isPrivateProfile", typeValue: sender.isOn, onSuccess: {
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
    }
    
    @IBAction func onClickChangePassword(_ sender: Any) {
         performSegue(withIdentifier: SegueIdentity.kChangePassword, sender: sender)
    }
    
    @IBAction func onClickPushNotifications(_ sender: Any) {
        
         performSegue(withIdentifier: SegueIdentity.kPushNotification, sender: sender)
    }
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    
    @IBAction func onClickCommentControl(_ sender: Any) {
    }
    @IBAction func onClickLogout(_ sender: Any) {
        
        let facebookLogon = FacebookLogin()
                    facebookLogon.logoutFB()
                    JKGoogleManager.shared.googleSingOut()
                    self.showLogoutAlert()
    }
    
    
}


