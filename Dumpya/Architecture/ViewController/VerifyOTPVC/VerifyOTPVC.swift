//
//  VerifyOTPVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 18/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class VerifyOTPVC: UIViewController {
    @IBOutlet weak fileprivate var otpFirstTF:DMTextField!
    @IBOutlet weak fileprivate var otpSecondTF:DMTextField!
    @IBOutlet weak fileprivate var otpThirdTF:DMTextField!
    @IBOutlet weak fileprivate var otpFourthTF:DMTextField!
    @IBOutlet weak fileprivate var sendEmailBTN: UIButton!
    @IBOutlet weak fileprivate var resetLoginBTN: UIButton!
    @IBOutlet var objVerifyOtp: DMUserViewModel!
    fileprivate var otp:String?
    let yourAttributes : [NSAttributedStringKey: Any] = [
        
        NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14),
        
        NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributeString = NSMutableAttributedString(string: "resendOTP".localized,
                                                        
                                                        attributes: yourAttributes)
        
        sendEmailBTN.setAttributedTitle(attributeString, for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction fileprivate func textFieldDidChanged(_ sender: DMTextField) {
        let text = sender.text
        if text?.utf16.count == 1{
            switch sender{
            case otpFirstTF:
                otpSecondTF.becomeFirstResponder()
            case otpSecondTF:
                otpThirdTF.becomeFirstResponder()
            case otpThirdTF:
                otpFourthTF.becomeFirstResponder()
            case otpFourthTF:
                otpFourthTF.resignFirstResponder()
            default:
                break
            }
            
            
        }
        else if text?.utf16.count == 0{
            switch sender{
            case otpFirstTF:
                otpFirstTF.resignFirstResponder()
            case otpSecondTF:
                otpFirstTF.becomeFirstResponder()
            case otpThirdTF:
                otpSecondTF.becomeFirstResponder()
            case otpFourthTF:
                otpThirdTF.becomeFirstResponder()
            default:
                break
            }
           
        }
        else  {
            
        }
    }
    
    @IBAction func onClickVerify(_ sender: Any) {
       
        guard let otp1 = otpFirstTF.text,let otp2 = otpSecondTF.text,let otp3 = otpThirdTF.text,let otp4 = otpFourthTF.text else {return}
        otp = otp1+otp2+otp3+otp4
        guard let otp = otp else {return}
        if otp.count == 4{
         
            //call api
            self.objVerifyOtp.verifyOTPLogin(otp: otp, onSuccess: {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier:SegueIdentity.kProfileSegue, sender: sender)

                }
            }, onFailure: {

                self.otpFirstTF.text = ""
                self.otpSecondTF.text = ""
                self.otpThirdTF.text = ""
                self.otpFourthTF.text = ""

            }, onUserSuspend: { (message) in

                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else{
            alertMessage = "Please enter OTP"
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {  navigationController?.popToRootViewController(animated: true)}
    
    @IBAction fileprivate func onClickSendEmailAgain(_ sender: UIButton) {
        
        objVerifyOtp.resendOTP(onSuccess: {
            
           // alertMessage = message ?? ""
            
        }, onFailure: {
        })
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//         Get the new view controller using segue.destinationViewController.
//         Pass the selected object to the new view controller.
        if segue.identifier  == SegueIdentity.kProfileSegue {
            if let vc  = segue.destination as? ProfileVC{
                vc.isFromOTP = true
                print(vc.isFromOTP)
            }
            
        }
    }
    

}
