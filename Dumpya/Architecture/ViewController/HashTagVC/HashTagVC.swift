//
//  HashTagVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 04/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
protocol SwiftyHashTagTableViewCellDelegate : class {
    func swiftyTableViewCellDidTapHeart(_ sender: FeedMediaCell,hashName:String)
    func clickHashTextCell(_ sender:FeedTextCell,hashName:String)
}
class HashTagVC: UIViewController {

    @IBOutlet weak var lblTagName: UILabel!
    @IBOutlet var objHashTag: DumpTimeLineViewModel!
    @IBOutlet weak var tableView: UITableView!
    var hashTagName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTagName.text = "#\(hashTagName)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         NotificationCenter.default.post(name: Notification.Name("isFromTags"), object: nil)
        loadTagData()
    }
    
    fileprivate func loadTagData(){
        
        objHashTag.getTimeLineDump(refreshControl: UIRefreshControl(), search:hashTagName,searchType:"hashTag", onSuccess: { (records) in
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
     }

    
    
    @IBAction fileprivate func onClickUserProfile(_ sender: UIButton) {
        
      
            performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
      
    }
    
    
    @IBAction fileprivate func onClickDumpeeName(_ sender: UIButton) {
        performSegue(withIdentifier: SegueIdentity.kDumpeeDetailSegue, sender: sender)
    }
    
    @IBAction fileprivate func onClickDots(_ sender: UIButton) {
        let index = sender.tag
        let obj = self.objHashTag.dumpTimeLineInfo(at: index)
      
        
        dumpSetting(dumpId: obj.dumpFeedId,userID:obj.userId,source:obj.source,index: index,dumpeeName: obj.dumpeeName,media: obj.media,content: obj.content)
    }
    
    @IBAction func onClickCommentCount(_ sender: Any) {
        performSegue(withIdentifier: SegueIdentity.kCommentSegue , sender: sender)
    }
    
    @IBAction func onClickComment(_ sender: DMCardView) {
        performSegue(withIdentifier: SegueIdentity.kCommentViewSegue , sender: sender)
    }
    
    @IBAction func onClickCommentPromotionalText(_ sender: DMCardView) {
        performSegue(withIdentifier:SegueIdentity.kCommentedUserViewListSegue, sender: sender)
    }
    
    
   
    @IBAction fileprivate func onClickFollowingDetail(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    
    @IBAction fileprivate func  onClickRedumpCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        
        let index = sender.tag
        objHashTag.didUpdateReadMore(at: index, states:false)
        
        
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        
    }
    
   
    @IBAction func onClickRedump(_ sender: UIButton) {
        
        let index = sender.tag
        self.objHashTag.redumpUser(dumpFeedId: self.objHashTag.dumpTimeLineInfo(at: index).dumpFeedId, onSuccess: { (isRedumpedByMe,redumpCount) in
            self.objHashTag.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
            let indexPath = IndexPath(item: index, section: 0)
            UIView.performWithoutAnimation {
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    
    fileprivate  func dumpSetting(dumpId:String,userID:String,source:String,index:Int,dumpeeName:String = "",media:String,content:String)  {
        
        if userID == userModel?.userId ?? ""{
          
            var actions :[AlertActionModel] = [
                AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Edit
                    let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                    dumpCreateVC?.isEdit = true
                    
                    guard let dumpeeInfo = self.objHashTag?.dumpeeInfo(at:index) else{return}
                    
                    dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                    dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                    dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                    dumpCreateVC?.isShowLocation = dumpeeInfo.isShowLocation
                    let dumpeeType = dumpeeInfo.type
                    dumpCreateVC?.topicName = dumpeeInfo.topicName
                    let content = dumpeeInfo.content
                    let media = dumpeeInfo.media
                    let videoThumbnail = dumpeeInfo.videoThumbnail
                    let size  = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                    switch dumpeeType{
                    case .image:
                        dumpCreateVC?.params.type = .image(item: media, size: size)
                    case .textImage:
                        dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                    case .video:
                        dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                    case .textVideo:
                        dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                    case .gradient:
                        let bgCode  = dumpeeInfo.bgCode
                        var gradientColor:GradientColor = .red
                        if GradientColor.red.name == bgCode{
                            gradientColor = .red
                        }else if GradientColor.green.name == bgCode{
                            gradientColor = .green
                        }else if GradientColor.purple.name == bgCode{
                            gradientColor = .purple
                        }else if GradientColor.yellow.name == bgCode{
                            gradientColor = .yellow
                        }else if GradientColor.darkRed.name == bgCode{
                            gradientColor = .darkRed
                        }
                        dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                    case .text:
                        dumpCreateVC?.params.type = .text(text: content)
                    }
                    
                    self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                    
                }else if indexofSheet == 3{
                    //Delete
                    
                   
                    self.objHashTag.deleteDump(dumpFeedId: dumpId, onSuccess: {
                        
                        self.loadTagData()
                        
                    }, onFailure: {
                        
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                    
                }else if indexofSheet == 4{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media, viewController: self,copyLink: false)
                }else if indexofSheet == 5{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            
            
            
            
        }
        }
        else{
            
          var actions :[AlertActionModel] = [
                
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Report
                    self.objHashTag.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                }else if indexofSheet == 3{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                }else if indexofSheet == 4{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
        }
    }
        }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let sender = sender as? Any else{return}
            
            let dumpInfo = objHashTag.dumpTimeLineInfo(at: (sender as AnyObject).tag)
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }else if segue.identifier == SegueIdentity.kCommentViewSegue{
            guard let commentBtn = sender as? DMCardView else{return}
            let index  = commentBtn.tag
            let obj = self.objHashTag.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
            
        }else if segue.identifier == SegueIdentity.kCommentSegue{
            guard let commentBtn = sender as? UIButton else{return}
            let index  = commentBtn.tag
            let obj = self.objHashTag.didSelectAtRedump(at: index)
            
            let objComment : CommentVC = segue.destination as! CommentVC
            
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
        }else if segue.identifier ==  SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let sender = sender as? UIButton else{return}
            
            let dumpInfo = objHashTag.dumpTimeLineInfo(at: sender.tag)
            objDumpeeDetail.objDumpeeDetail.set(userId: dumpInfo.userId, dumpeeId: dumpInfo.dumpeeId, dumpeeName: dumpInfo.dumpeeName)
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo.dumpeeId)
        }else  if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let sender = sender as? UIButton else{return}
            let dumpInfo = objHashTag.dumpTimeLineInfo(at: sender.tag)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.userId)
        }
        else if segue.identifier == SegueIdentity.kCommentedUserViewListSegue{
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
            guard let sender = sender as? DMCardView else{return}
            
            let dumpInfo = objHashTag.dumpTimeLineInfo(at: sender.tag)
            objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
        }
        
    }
    

}
extension HashTagVC:UITableViewDataSource,UITableViewDelegate,SwiftyTableViewCellDelegate{
    
    func clickHashTextCell(_ sender: FeedTextCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath.row)
        print("THE HASHNAME",hashName)
        let hashNameWithTag = "#\(hashName)"
        if hashNameWithTag != self.lblTagName.text
        {
            let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
            
            
            vc?.hashTagName = hashName
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }
       
    }
    
    
    func swiftyTableViewCellDidTapHeart(_ sender: FeedMediaCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        
        
        let hashNameWithTag = "#\(hashName)"
        if hashNameWithTag != self.lblTagName.text
        {
             let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
            vc?.hashTagName = hashName
            self.navigationController?.pushViewController(vc!, animated: true)
        }
       
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objHashTag.numberOfRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let obj  = objHashTag.cellForRowAt(at:indexPath)
        let feedType =  obj.feedType
       
        switch feedType{
        case .textImage,.textVideo:
            let feedMediaCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaCell , for: indexPath) as! FeedMediaCell
            feedMediaCell.delegateFeedMedia = self
            feedMediaCell.rowIndex = indexPath.row
            feedMediaCell.objFeedTimeLine = objHashTag.cellForRowAt(at: indexPath)
            return feedMediaCell
             case .image,.video:
                let  feedMediaImageCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaImageCell , for: indexPath) as!  FeedMediaImageCell
                feedMediaImageCell.objImage = objHashTag.cellForRowAt(at: indexPath)
                feedMediaImageCell.rowIndex = indexPath.row
                return feedMediaImageCell
             case .text:
                let feedTextCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedTextCell , for: indexPath) as! FeedTextCell
                feedTextCell.delegateFeedText = self
                feedTextCell.rowIndex = indexPath.row
                feedTextCell.objText = objHashTag.cellForRowAt(at: indexPath)
                return feedTextCell
            
             case .gradient:
                let feedGradientCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedGradientCell , for: indexPath) as! FeedGradientCell
                feedGradientCell.rowIndex = indexPath.row
                feedGradientCell.objGradient = objHashTag.cellForRowAt(at: indexPath)
                return feedGradientCell
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let obj  = objHashTag.cellForRowAt(at:indexPath)
        let feedType =  obj.feedType
        
        switch feedType{
        case .textImage,.textVideo:
          return 369.0
        case .image,.video:
            return 320.0
        case .text:
           return  180.0
        case .gradient:
           return  322.0
        }
        
        
    }
   
}

