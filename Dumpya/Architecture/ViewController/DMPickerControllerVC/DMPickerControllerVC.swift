//
//  DMPickerControllerVC.swift
//  Dumpya
//
//  Created by Chander on 05/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
enum DMPickerType:Int {
    case countryCode = 0
    case dob
    case gender
}
protocol DMPickerControllerDelegate:NSObjectProtocol{
    func picker(pikcer:DMPickerControllerVC, didSelectDate:String)
    func picker(pikcer:DMPickerControllerVC, didSelectCountry:CountryModel)
    func picker(pikcer:DMPickerControllerVC, didSelectGender:GenderModel)
    
}
class DMPickerControllerVC: UIViewController {
    
    @IBOutlet  fileprivate var objCountryCodeVM: DMCountryCodeViewModel!
    @IBOutlet  fileprivate var objGenderVM: DMGenderViewModel!
    @IBOutlet weak fileprivate var datePicker: UIDatePicker!
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak fileprivate var countrySearchBar: UISearchBar!
    @IBOutlet weak var heightConstraintCountrySearch: NSLayoutConstraint!
    
    var pickerType:DMPickerType = .countryCode
    var delegate:DMPickerControllerDelegate?
    var filterdata:[CountryModel] = [CountryModel]()
    var rowCount:Int{
        return pickerType == .gender ? objGenderVM.numberOfRow() : objCountryCodeVM.numberOfRow()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         datePicker.datePickerMode = .date
         self.definesPresentationContext = true
        // Do any additional setup after loading the view.
        self.loadPickerTypeData()
    }
    
    fileprivate func loadPickerTypeData(){
        if pickerType == .dob {
            datePickerView.isHidden = false
             datePicker.set18YearValidation()
            countryView.isHidden = true
        }else if pickerType == .gender{
            countryView.isHidden = false
            tableView.isHidden = false
            objGenderVM.getgender {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.heightConstraintCountrySearch.constant = 0
                    })
                    self.tableView.reloadData()
                }
            }
        }else{
            tableView.isHidden = false
            objCountryCodeVM.getcountryNamesByCode {
                
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.heightConstraintCountrySearch.constant =  40
                    })
                    self.tableView.reloadData()
                }
            }
        }
   }
    
    @IBAction func onClickDateSelect(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        // Now we specify the display format, e.g. "27-08-2015
        dateFormatter.dateFormat = "MM/dd/YYYY"
      
        let strDate = dateFormatter.string(from: datePicker.date)
        delegate?.picker(pikcer: self, didSelectDate:  strDate)
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        self.preferredContentSize.height = self.tableView.contentSize.height + 10
    }
    @IBAction fileprivate func onClickDateSelect(_sender :DMButton){
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DMPickerControllerVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if pickerType == .gender {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
            cell.objGenderModel = objGenderVM.cellForItem(at: indexPath)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeCell", for: indexPath) as! CountryCodeCell
            cell.objCountryModel = objCountryCodeVM.cellForItem(at: indexPath)
            return cell
        }
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        guard let delegate = self.delegate else { return  }
        if pickerType == .countryCode {
            guard let obj = objCountryCodeVM.cellForItem(at: indexPath) else{return}
            delegate.picker(pikcer: self, didSelectCountry: obj)
            dismiss(animated: true, completion: nil)
        }else if pickerType == .gender {
            guard let obj = objGenderVM.cellForItem(at: indexPath) else{return}
            delegate.picker(pikcer: self, didSelectGender: obj)
            dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if pickerType == .countryCode{
            return UITableViewAutomaticDimension
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if pickerType == .countryCode{
            return 60
        }else{
            return 60
        }
    }
 }

extension DMPickerControllerVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension DMPickerControllerVC:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count>0{
        objCountryCodeVM.getcountryNamesBySearch(searchText, OnCompletion: {
            self.tableView.reloadData()
        })}else{
            searchBar.resignFirstResponder()
            loadPickerTypeData()
       
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
        print("searchBarCancelButtonClicked")
        loadPickerTypeData()
    }
 }
extension UIDatePicker {
    func set18YearValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -18
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -150
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    } }
