//
//  RegisterVC.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController,UITextViewDelegate{
    
    //MARK:- Outlets
    @IBOutlet weak fileprivate var firstNameTF: DMTextField!
    @IBOutlet weak fileprivate var lastNameTF: DMTextField!
    @IBOutlet weak fileprivate var userNameTF: DMTextField!
    @IBOutlet weak fileprivate var emailAddressTF: DMTextField!
    @IBOutlet weak fileprivate var passwordTF: DMTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: DMTextField!
    @IBOutlet weak fileprivate var eyeConfirmPasswordBTN: UIButton!
    @IBOutlet weak fileprivate var eyePasswordBtn: UIButton!
    @IBOutlet weak fileprivate var textView: UITextView!
    @IBOutlet fileprivate var objCreateAccountVM: DMUserViewModel!
    fileprivate var iconPassword = true
    fileprivate let termsAndConditionsURL = kTerms
    fileprivate let privacyURL = kPrivacy
    fileprivate var iconConfirmPassowrd = true
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = "createAccountTerms".localized
        let attributedString = NSMutableAttributedString(string: textView.text)
        let foundRange = attributedString.mutableString.range(of:"termsOfService".localized)
        attributedString.addAttribute(.link, value: termsAndConditionsURL, range: foundRange)
        attributedString.addAttribute(.underlineStyle, value: 1, range: foundRange)
        let foundRange1 = attributedString.mutableString.range(of:"privacyPolicy".localized)
        attributedString.addAttribute(.link, value: privacyURL, range: foundRange1)
        attributedString.addAttribute(.underlineStyle, value: 1, range: foundRange1)
        textView.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if (URL.absoluteString == termsAndConditionsURL) {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "Terms_ConditionsVC") as! Terms_ConditionsVC
            navigationController?.pushViewController(vc,
                                                     animated: true)
        } else if (URL.absoluteString == privacyURL) {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginPrivacyVC") as! LoginPrivacyVC
            navigationController?.pushViewController(vc,
                                                     animated: true)
        }
        return false
    }
    
    //MARK:- Actions
    @IBAction fileprivate func onClickCreateAccount(_ sender: UIButton) {
         guard let firstName = self.firstNameTF.text?.removeWhiteSpace else { return }
         guard let lastName = self.lastNameTF.text?.removeWhiteSpace else { return }
         guard let userName = self.userNameTF.text?.removeWhiteSpace else { return }
         guard let emailAddress = self.emailAddressTF.text else { return }
         guard let password = self.passwordTF.text else { return }
         guard let confirmPassword = self.confirmPasswordTF.text else { return }
        
        objCreateAccountVM.createAccount(firstName: firstName, lastName: lastName, userName: userName, email: emailAddress, password: password, confirmPassword: confirmPassword, onSuccess: { (message) in
          self.performSegue(withIdentifier:SegueIdentity.kOTPSegue , sender: sender)
        }, onFailure: {
        })
     }
    
    @IBAction func onClieckEyePassword(_ sender: Any) {
        if(iconPassword == true) {
            passwordTF.isSecureTextEntry = false
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
            
        } else {
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
       iconPassword = !iconPassword
    }
    
    @IBAction func onClickEyeConfirmPassword(_ sender: Any) {
        if(iconConfirmPassowrd == true) {
            confirmPasswordTF.isSecureTextEntry = false
           eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
        } else {
         eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            confirmPasswordTF.isSecureTextEntry = true
        }
        iconConfirmPassowrd = !iconConfirmPassowrd
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {  navigationController?.popViewController(animated: true)}
    
    
}

