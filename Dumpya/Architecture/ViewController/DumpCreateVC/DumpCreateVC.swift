//
//  DumpCreateVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 16/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

enum GradientColor:Int {
    case red = 0
    case green
    case purple
    case yellow
    case darkRed
    var name:String{
        switch self {
        case .red    : return "red"
        case .green  : return "green"
        case .purple : return "purple"
        case .yellow : return "yellow"
        case .darkRed: return "dark_red"
            
        }
    }
    var colors:[UIColor]{
        switch self {
        case .red    : return  [DMColor.deepRedColor,DMColor.lightRedColor]
        case .green  : return  [DMColor.deepGreenColor,DMColor.deepGreenColor]
        case .purple : return  [DMColor.deepBlueColor, DMColor.lightBlueColor]
        case .yellow : return  [DMColor.deepYellowColor, DMColor.deepYellowColor]
        case .darkRed: return  [DMColor.deepRedColor,DMColor.lightRedColor]
            
        }
    }
    
}
typealias DumpCreateParameter = (mediaItem:DMMediaItem?,mediaFile:String,type:DumpCreateFeedType)
class DumpCreateVC: UIViewController{
    
    @IBOutlet weak var widthConstraintBtn: NSLayoutConstraint!
    @IBOutlet weak fileprivate var locationTF: DMTextField!
    @IBOutlet var objDumpVM: DMFeedViewModel!
    @IBOutlet weak fileprivate var cancelBTN: DMButton!
    @IBOutlet weak fileprivate var videoThumbnailBTN: DMButton!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    @IBOutlet weak fileprivate var dumpTypeLBL: UILabel!
    @IBOutlet weak fileprivate var descriptionTV: THTextView!
    @IBOutlet weak fileprivate var collectionView: UICollectionView!
    @IBOutlet weak var imgSelect: DMButton!
    @IBOutlet weak var heightConstraintBtn: NSLayoutConstraint!
    @IBOutlet weak var profilePicBtn: DMButton!
    @IBOutlet weak var showLocationSwitch: UISwitch!
    fileprivate var objImageUpload:DMUserViewModel = DMUserViewModel()
    var isShowLocation:Bool = true
    
    
    var searchString:String = ""
    var catagory:String = ""
    
    var topicCreate:Bool = false
    let selectedImageV = UIImageView()
    var isEdit:Bool = false
    var topicName:String = ""
    fileprivate var gradientColor:GradientColor = .red{
        didSet{
            params.type = DumpCreateFeedType.gradient(color: gradientColor, text: "")
        }
    }
    // var media:String = ""
    var dumpId:String = ""
    var dumpeeId:String = ""
    var dumpName:String = ""
    var locationName:String = ""
    var params:DumpCreateParameter = DumpCreateParameter(nil,"",DumpCreateFeedType.gradient(color:.red, text: ""))
    fileprivate var gradientList:[GradientColor] = [.red,.green,.purple,.yellow,.darkRed]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isDumpySaved = false
        //self.fetchCurrentLoction()
        descriptionTV.placeholder = NSLocalizedString("placeHolder",comment:"")
        if isEdit{
            dumpInfo()
        }
        else{
            heightConstraintBtn.constant = 0
            loadInfo()
        }
        
        // Do any additional setup after loading the view.
    }
    
    fileprivate func  dumpInfo(){
        userNameLBL.text =  "\(userModel?.firstName ?? "") \(userModel?.lastName ?? "")"
        profilePicBtn.loadImage(filePath:userModel?.profilePic ?? "", for: .normal)
        
        
        self.showLocationSwitch.isOn = isShowLocation ? true : false
        
        let combination = NSMutableAttributedString(attributedString:"Dumped".attributedString(textColor:dumpTypeLBL.textColor))
        if !topicName.isEmpty {
            let nameAttributed = " \(topicName)".attributedString(fontType:.Bold,textColor:dumpTypeLBL.textColor)
            combination.append(nameAttributed)
        }
        if !dumpName.isEmpty {
            let dumpNameAttributed = " \(dumpName)".attributedString(textColor:dumpTypeLBL.textColor)
            combination.append(dumpNameAttributed)
        }
        
        
        dumpTypeLBL.attributedText = combination
        let type = params.type
        switch type {
        case .text(let text):
            descriptionTV.text = text
             self.videoThumbnailBTN.isHidden = true
            heightConstraintBtn.constant = 0
            self.cancelBTN.isHidden = true
            
        case .image(let item, _),.gif(let item,_):
            params.mediaFile = item
            self.imgSelect.loadImage(filePath: item, for: .normal, progressBlock: nil) { (image , error) in
                if let image  = image{
                    self.cancelBTN.isHidden = false
                    self.updateSelectImageSize(image: image)
                     self.videoThumbnailBTN.isHidden = true
                }
            }
        case .textImage( let item,let text,_),.textGif( let item,let text,_) :
            descriptionTV.text = text
            params.mediaFile = item
            self.imgSelect.loadImage(filePath: item, for: .normal, progressBlock: nil) { (image , error) in
                if let image  = image{
                    self.descriptionTV.text = text
                    self.cancelBTN.isHidden = false
                     self.videoThumbnailBTN.isHidden = true
                    self.updateSelectImageSize(image: image)
                    
                }
            }
        case .video(let item,_):
            params.mediaFile = item
            self.imgSelect.loadImage(filePath: item, for: .normal, progressBlock: nil) { (image , error) in
                if let image  = image{
                    
                    self.cancelBTN.isHidden = false
                    self.videoThumbnailBTN.isHidden = false
                    self.updateSelectImageSize(image: image)
                    
                }else{
                    self.videoThumbnailBTN.isHidden = false
                    self.heightConstraintBtn.constant = 180
                }
            }
            
        case .textVideo( let item,let text,_):
            params.mediaFile = item
            self.descriptionTV.text = text
                self.imgSelect.loadImage(filePath: item, for: .normal, progressBlock: nil) { (image , error) in
                if let image  = image{
                self.descriptionTV.text = text
                     self.videoThumbnailBTN.isHidden = false
                self.cancelBTN.isHidden = false
                self.updateSelectImageSize(image: image)
                }else{
                    self.videoThumbnailBTN.isHidden = false
                    self.heightConstraintBtn.constant = 180
                    }
            }
            
        case .gradient(let color, _):
            self.cancelBTN.isHidden = true
            gradientColor = color
            heightConstraintBtn.constant = 0
        }
    }
    
    func loadInfo(){
        userNameLBL.text =  "\(userModel?.firstName ?? "") \(userModel?.lastName ?? "")"
        profilePicBtn.loadImage(filePath:userModel?.profilePic ?? "", for: .normal)
        var dumpeeName:String = ""
        var categoryName:String = ""
        if topicCreate{
            let data =  objDumpVM?.getDumpeeCategoryInfo()
            dumpeeName = data?.dumpeeName ?? ""
            categoryName = data?.categoryName ?? ""
        }
        else{
            let data = objDumpVM?.getTopicInfo()
            dumpeeName = data?.topicName ?? ""
            categoryName = data?.categoryName ?? ""
        }
        
        let combination = NSMutableAttributedString(attributedString:"Dumped".attributedString(textColor:dumpTypeLBL.textColor))
        if !dumpeeName.isEmpty {
            let nameAttributed = " \(dumpeeName)".attributedString(fontType:.Bold,textColor:dumpTypeLBL.textColor)
            combination.append(nameAttributed)
        }
        if !categoryName.isEmpty {
            let categoryNameAttributed = " \(categoryName)".attributedString(textColor:dumpTypeLBL.textColor)
            combination.append(categoryNameAttributed)
        }
        
        dumpTypeLBL.attributedText = combination
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- updateSelectImageSize
    fileprivate func updateSelectImageSize(image:UIImage){
        let w1  = self.imgSelect.bounds.size.width
        let w2 = image.size.width
        let h2 = image.size.height
        let h1 = CGFloat(h2/w2)*w1
        self.heightConstraintBtn.constant = h1
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
            
        })
    }
    //MARK:- onClickPhoto
    @IBAction fileprivate func onClickPhoto(_ sender: Any) {
        DMPicker.camera(onPicked: { (item) in
            self.cancelBTN.isHidden = false
            self.refreshPickerData(item: item)
        }).picker(from: self)
    }
    //MARK:- onClickLibrary
    @IBAction fileprivate func onClickLibrary(_ sender: Any) {
        DMPicker.library(onPicked: { (item) in
            self.cancelBTN.isHidden = false
            self.refreshPickerData(item: item)
        }).picker(from: self)
    }
    //MARK:- refreshPickerData
    fileprivate func refreshPickerData(item:DMMediaItem){
        let size = item.itemSize ?? self.view.bounds.size
        self.params.mediaItem = item
        
        switch item{
        case .photo(let p):
             self.videoThumbnailBTN.isHidden = true
            self.imgSelect.setImage(p.image, for: .normal)
            self.updateSelectImageSize(image: p.image)
            self.params.type = .image(item: "",size: size)
            
        case .video(let v):
            self.videoThumbnailBTN.isHidden = false
            self.params.type = .video(item:"",size: size)
            self.imgSelect.setImage(v.thumbnail, for: .normal)
            self.updateSelectImageSize(image: v.thumbnail)
            
        }
        
    }
    
    @IBAction func onclickCancel(_ sender: Any) {
        UIView.animate(withDuration: 1.0, animations: {
            let content = self.descriptionTV.text ?? ""
            let trimText = content.trimWhiteSpace
            self.params.mediaFile = ""
            if self.imgSelect.nornamlImage != nil , trimText.count>0{
                switch self.params.type{
                case .image,.video,.textVideo,.textImage,.gif,.textGif:
                    self.params = DumpCreateParameter(nil,"",.text(text: content))
                default:
                    self.params = DumpCreateParameter(nil,"",.gradient(color: .red, text: content))
                }
                
            }else{
                self.params = DumpCreateParameter(nil,"",.gradient(color: .red, text: content))
            }
            
            self.heightConstraintBtn.constant = 0
            self.cancelBTN.isHidden = true
            self.videoThumbnailBTN.isHidden = true
        })
    }
    
    
    
    @IBAction func onClickGigBTN(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GifVC") as? GifVC
        vc?.delegate = self
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction fileprivate func onClickBack(_ sender: UIButton) {
        isDumpySaved = false
        navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK:- onClickPost
    @IBAction fileprivate func onClickPost(_ sender: Any) {
        let content = self.descriptionTV.text ?? ""
        let trimText = content.trimWhiteSpace
        
        if let item = params.mediaItem {
            //PICKER DATA
            let size = item.itemSize ?? self.view.bounds.size
            var folderType:AWSS3UploadFolder = .images
            let uploadItme:Any
            switch item{
            case .photo(let p):
                folderType = .images
                uploadItme = p.image
            case .video(let v):
                folderType = .videos
                uploadItme = v.url
                
            }
            objDumpVM.uploadDumpMediaData(item: uploadItme, folderType: folderType, completionHandler: { (url) in
                //self.params.mediaItem?.awsUploadedUrl = url
                if trimText.count>0{
                    switch item{
                    case .photo:
                        self.params.type  = .textImage(item: url, text: content, size: size)
                    case .video:
                        self.params.type  = .textVideo(item: url, text: content,size: size)
                        
                    }
                }else{
                    switch item{
                    case .photo:
                        self.params.type  = .image(item: url,size: size)
                    case .video:
                        self.params.type  = .video(item: url,size: size)
                        
                    }
                }
                
                self.postCreateDumpData()
                
            })
            
        }else {
            
            if trimText.count>0{
                if !params.mediaFile.isEmpty{
                    switch params.type{
                    case .gif(let item,let size), .image(let item,let size):
                        let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                        params.type =  .textImage(item: item, text: content, size: imageSize)
                    case .video(let item,let size):
                        let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                        params.type = .textVideo(item: item, text: content, size: imageSize)
                    case .textVideo(let item, _, let size):
                        let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                        params.type = .textVideo(item: item, text: content, size: imageSize)
                    case .textImage(let item, _, let size):
                        let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                        params.type =  .textImage(item: item, text: content, size: imageSize)
                    
                    default:break
                        
                    }
                    
                }else{
                     params.type = .text(text: content)
                    
                }
                
                self.postCreateDumpData()
            }else{
                switch params.type{
                case .gif(let item,let size):
                    let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                    params.type =  .image(item: item, size: imageSize)
                case .image(let item,let size),.textImage(let item,_,let size):
                    let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                    params.type =  .image(item: item, size: imageSize)
                case .video(let item,let size), .textVideo(let item,_,let size):
                    let imageSize = size == .zero ? imgSelect.nornamlImage?.size ?? self.view.bounds.size :size
                    params.type =  .video(item: item, size: imageSize)
                case .gradient:
                    params.type = .gradient(color: gradientColor, text: "")

                default:break
                }
                self.postCreateDumpData()
            }
            
            
        }
        
        
    }
    
    //MARK:- postCreateDumpData
    fileprivate func postCreateDumpData(){
        if isEdit{
            
            objDumpVM?.editDump(dumpId: dumpId, dumpeeId: dumpeeId,params: params ,locationName:locationNameLocation,countryName:countryNameFromLocation,isShowLocation:isShowLocation,onSuceess: {
                 isDumpySaved = true
                self.navigationController?.popViewController(animated:true)
            }, onFailure: {
                isDumpySaved = false
            }, onUserSuspend: { (message) in
                isDumpySaved = false
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else{
            
            objDumpVM?.createDump(isDumpStatus:topicCreate,params: params,locationName:locationNameLocation,countryName:countryNameFromLocation,isShowLocation:isShowLocation,onSuceess: {
                isDumpySaved = false
                AppDelegate.sharedDelegate.setUpSideMenu()
            }, onFailure: {
                isDumpySaved = false
            }, onUserSuspend: { (message) in
                isDumpySaved = false
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        
    }
    
    
    
    @IBAction func onClickShowLocation(_ sender: UISwitch) {
        isShowLocation =  sender.isOn ? true:false
    }
}


extension DumpCreateVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gradientList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dumpCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kDumpCategoryCell, for: indexPath) as! DumpCategoryCell
        let gradentColor = gradientList[indexPath.row]
        dumpCategoryCell.gradientColor = gradientList[indexPath.row]
        dumpCategoryCell.isMakeDefault = (gradentColor.rawValue == self.gradientColor.rawValue) ? true : false
        
        //dumpCategoryCell.dumpColors(cellIndex: indexPath.row,selectedIndex:self.gradientColor)
        
        
        
        return dumpCategoryCell
    }
}

extension DumpCreateVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:100, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        // selectedIndex = indexPath.row
        self.gradientColor = gradientList[indexPath.row]
        collectionView.reloadData()
    }
}

extension DumpCreateVC:GifVCDelegate{
    
    @IBAction func onClickRight(_ sender: Any)
    {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if nextItem.row < gradientList.count {
            self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
            
        }
    }
    
    @IBAction func onClickLeft(_ sender: Any)
    {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        if nextItem.row < gradientList.count && nextItem.row >= 0{
            self.collectionView.scrollToItem(at: nextItem, at: .right, animated: true)
            
        }
    }
    func didTapOnGifImage(_ url: String, height: Int, width: Int) {
        params.mediaFile = url
        params.type = .gif(item: url, size: CGSize(width: width, height: height))
        self.imgSelect.loadImage(filePath: url, for: .normal, progressBlock: nil) { (image , error) in
            if let image  = image{
                self.cancelBTN.isHidden = false
                self.updateSelectImageSize(image: image)
                
            }
        }
    }
}

