//
//  ForgotPasswordVC.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    
    @IBOutlet fileprivate var forgotPasswordViewModel: DMUserViewModel!
    @IBOutlet weak fileprivate var emailTF:DMTextField!
    fileprivate var otp:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickReturnLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction fileprivate func onforgotPassword(_ sender: Any){
        
        guard let email  = emailTF.text else{return}
        
        forgotPasswordViewModel.forgotPassword(email: email, onSuccess: { (message) in
            DispatchQueue.main.async {
                
                self.performSegue(withIdentifier: SegueIdentity.KResetPasswordSegue, sender: sender)
            }
            
        }, onFailure: {
            
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kResetPasswordSegue {
            let objResetPassword : ResetPasswordVC = segue.destination as! ResetPasswordVC
            objResetPassword.verifyOTP = forgotPasswordViewModel
            objResetPassword.userEmail = emailTF.text
        }
    }
}


