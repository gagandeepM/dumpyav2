//
//  DirectMessageVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DirectMessageVC: UIViewController {
    
    @IBOutlet var objSearchViewModel: SearchViewModel!
    
    @IBOutlet weak var requestHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var noDirectMessageLbl: UILabel!
    @IBOutlet weak var privateMessagesText: UILabel!
    @IBOutlet weak fileprivate var requestMessageBtn: UIButton!
    @IBOutlet fileprivate var objDirectMessageViewModel: DirectMessageViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    var searchString:String = ""
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(self.refreshDirectMessage),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
        
    }
    
    @objc fileprivate func refreshDirectMessage() {
        
        getChatUsers()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getChatUsers()
        
        
    }
    
    fileprivate func getChatUsers(){
        objDirectMessageViewModel.getChatUsersList(isPull:self.refreshControl.isRefreshing,onSuccess: {
            DispatchQueue.main.async {
                if self.objDirectMessageViewModel.totalRequestCount > 0{
                    UIView.animate(withDuration:0.3, animations: {
                        self.requestMessageBtn.setTitle("\(String(describing: self.objDirectMessageViewModel?.totalRequestCount ?? 0)) " + "request".localized, for: .normal)
                        self.requestHeightConstraint.constant = 20.0
                    })
                }else{
                    UIView.animate(withDuration:0.3, animations: {
                        self.requestHeightConstraint.constant = 0.0
                    })
                }
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                if self.objDirectMessageViewModel.numberOfRows() > 0 {
                    
                    self.tableView.reloadData()
                    self.noDirectMessageLbl.isHidden = true
                    self.privateMessagesText.isHidden = true
                    self.tableView.isHidden = false
                }else{
                    self.noDirectMessageLbl.isHidden = false
                    self.privateMessagesText.isHidden = false
                    self.tableView.isHidden = true
                }
                
                
            }
        }, onFailure: {
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SideBar Click
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kChatSegue{
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let objMessageVC = segue.destination as! MessageVC
            let result = objDirectMessageViewModel.didSelect(index: selectedIndexPath.row)
            objMessageVC.objChatViewModel.set(friendId: result.friendId, friendName: result.friendName)
        }
        else if segue.identifier == SegueIdentity.kChatUserSearchSegue
        {
            let objMessageVC = segue.destination as! MessageVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let userId = objSearchViewModel.didSelect(index: selectedIndexPath.row).0
            let userName = objSearchViewModel.didSelect(index: selectedIndexPath.row).1
            objMessageVC.objChatViewModel.set(friendId: userId, friendName: userName)
        }
        
    }
    
    
}

extension DirectMessageVC:UITableViewDelegate,UITableViewDataSource{
    
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //
    //
    //        var numOfSections: Int = 0
    //        if objDirectMessageViewModel.numberOfRows() > 0
    //        {
    //            tableView.separatorStyle = .singleLine
    //            numOfSections            = 1
    //            tableView.backgroundView = nil
    //            tableView.separatorStyle  = .none
    //        }
    //        else
    //        {
    //            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
    //            noDataLabel.text          = "No direct message found"
    //            noDataLabel.textColor     = UIColor.black
    //            noDataLabel.textAlignment = .center
    //            tableView.backgroundView  = noDataLabel
    //            tableView.separatorStyle  = .none
    //        }
    //        return numOfSections
    //
    //    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !searchString.isEmpty
        {
            print(objSearchViewModel.numberOfRows())
            return objSearchViewModel.numberOfRows()
        }
        return objDirectMessageViewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !searchString.isEmpty
        {
            let userSearchListCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kUserSearchListCell, for: indexPath) as! UserSearchListCell
            userSearchListCell.objUserSearch =   objSearchViewModel.cellForRowAt(at: indexPath)
            return userSearchListCell
        }
        else
        {
            let directMessageCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kDirectMessageCell , for: indexPath) as!  DirectMessageCell
            directMessageCell.objDirectMessage = objDirectMessageViewModel.cellForRowAt(indexPath: indexPath)
            return directMessageCell
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if !searchString.isEmpty
        {
            return
        }
        else if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            objDirectMessageViewModel.onDeleteConversationThread(indexPath: indexPath)
            objDirectMessageViewModel.deleteConversationThread( onSuccess: {
                self.objDirectMessageViewModel.deleteParticularConversationThread(indexPath: indexPath)
                
                if self.objDirectMessageViewModel.numberOfRows() > 0 {
                    self.tableView.reloadData()
                    self.noDirectMessageLbl.isHidden = true
                    self.privateMessagesText.isHidden = true
                    self.tableView.isHidden = false
                }else{
                    self.noDirectMessageLbl.isHidden = false
                    self.privateMessagesText.isHidden = false
                    self.tableView.isHidden = true
                }
            }, onFailure: {
            })
            
        }
    }
}
extension DirectMessageVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText.removeWhiteSpace
        //searchBar.showsCancelButton = true
        if searchText.count > 0
        {
            
            objSearchViewModel.removeObjects()
            objSearchViewModel.getSearchUsers(search: searchText, page:pageNumber, reqType: "chat",onSuccess: { (records) in
                
                DispatchQueue.main.async {
                    if self.objSearchViewModel.numberOfRows() > 0 {
                        self.tableView.reloadData()
                        self.noDirectMessageLbl.isHidden = true
                        self.privateMessagesText.isHidden = true
                        self.tableView.isHidden = false
                    }else{
                        self.noDirectMessageLbl.isHidden = false
                        self.privateMessagesText.isHidden = false
                        self.tableView.isHidden = true
                    }
                    
                }
            }, onFailure: {
                
            }) { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            }
        }
        else
            
        {
            getChatUsers()
        }
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
        getChatUsers()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.text = ""
        searchBar.resignFirstResponder()
        print("searchBarCancelButtonClicked")
        searchString = ""
        getChatUsers()
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchString = ""
        searchBar.text = ""
        getChatUsers()
    }
}
