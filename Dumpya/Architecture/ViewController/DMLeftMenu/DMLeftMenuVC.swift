//
//  DMLeftMenuVC.swift
//  Dumpya
//
//  Created by Chander on 17/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import Foundation
enum DMLeftMenu:Int {
    case Dump = 0
    case Dumpfeed = 1
    case News = 2
    case Leaderboard = 3
    case Notifications = 4
    case Connections = 5
    case DirectMessage = 6
    case Settings = 7
    
    var title:String{
        switch self {
        case .Dump:
            
            return NSLocalizedString("Dump", comment: "Dump")
            
            
        case .Dumpfeed:
            return NSLocalizedString("Dumpfeed",comment:"Dumpfeed")
            
        case .News:
            return NSLocalizedString("News",comment:"News")
            
        case .Leaderboard:
            return NSLocalizedString("Leaderboard",comment:"Leaderboard")
        case .Notifications:
            return NSLocalizedString("Notifications",comment:"Notifications")
        case .Connections:
            return NSLocalizedString("Connections",comment:"Connections")
        case .DirectMessage:
            return NSLocalizedString("DirectMessage",comment:"Direct Message")
            
        default:
            return NSLocalizedString("Settings",comment:"Settings")
            
            
            
        }
    }
}
struct DMLeftMenuModel {
    var title:String?
    var image:UIImage?
}
class DMLeftMenuVC: UIViewController {
    
    
    @IBOutlet fileprivate var objprofileDetail: ProfileDetailViewModel!
    @IBOutlet weak fileprivate var profileImage: UIImageView!
    @IBOutlet weak fileprivate var userNameLBL: UILabel!
    
    @IBOutlet weak var bioLBL: UILabel!
    @IBOutlet weak var profileImgBtn: DMButton!
    var objUser:DMUserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    
        self.profileImage.setRounded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getProfileData()
    }
    fileprivate func getProfileData(){
        self.userNameLBL.text = "\(userModel?.firstName ?? "") \(userModel?.lastName ?? "")"
        self.bioLBL.text = userModel?.bio ?? ""
        guard let imageFile  = userModel?.profilePic else{return}
        
        
        
                if imageFile.isEmpty{
                   profileImage.image = UIImage(named: "profile_placeholder")
                }else{
                    profileImage.loadImage(filePath: imageFile)
                    
        }
        
    }
    
    @IBAction func onClickProfileImage(_ sender: Any) {
        
        
        
        let profileDetailController  = profileStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kProfileDetailNavigation) as! UINavigationController
        if let topViewController = profileDetailController.topViewController as? ProfileDetailVC{
            topViewController.isFromSideMenu = true
            topViewController.objUserDetailViewModel.set(currentUserId:userModel?.userId ?? "")
            topViewController.handler = {
                if self.mainController.isdupFeedController , let feedController = self.mainController.dumpFeedController{
                    feedController.isRefreshFeed = true
                }
            }
        }
        setChangeMainController(controller: profileDetailController)
    }
    
    
    
    var MenuItems:[DMLeftMenuModel]  {
        
        
        
        let itemTitle:[String] =
            [NSLocalizedString("Dump", comment: "Dump"),NSLocalizedString("Dumpfeed", comment: "Dumpfeed"),NSLocalizedString("News", comment: "News"),NSLocalizedString("Leaderboard", comment: "Leaderboard"),NSLocalizedString("Notifications", comment: "Notifications"),NSLocalizedString("Connections", comment: "Connections"),NSLocalizedString("DirectMessage", comment: "Direct Message"),NSLocalizedString("Settings", comment: "Settings")]
        var items = [DMLeftMenuModel]()
        
        for item in itemTitle {
            var title:String = DMLeftMenu.Settings.title
            var image:UIImage = #imageLiteral(resourceName: "icon_LeftBarSetting")
            if item == DMLeftMenu.Dump.title{
                image = #imageLiteral(resourceName: "icon_Dump")
                title = DMLeftMenu.Dump.title
            }else if item == DMLeftMenu.Dumpfeed.title{
                image = #imageLiteral(resourceName: "icon_DumpFeed")
                title = DMLeftMenu.Dumpfeed.title
            }else if item == DMLeftMenu.News.title{
                image = #imageLiteral(resourceName: "icon_News")
                title = DMLeftMenu.News.title
                
            }else if item == DMLeftMenu.Leaderboard.title{
                image = #imageLiteral(resourceName: "icon_LeaderBoard")
                title = DMLeftMenu.Leaderboard.title
                
            }else if item == DMLeftMenu.Notifications.title{
                image = #imageLiteral(resourceName: "icon_Notification")
                title = DMLeftMenu.Notifications.title
            }else if item == DMLeftMenu.Connections.title{
                image = #imageLiteral(resourceName: "icon_Connections")
                title = DMLeftMenu.Connections.title
            }else if item == DMLeftMenu.DirectMessage.title{
                image = #imageLiteral(resourceName: "icon_Message")
                title = DMLeftMenu.DirectMessage.title
            }else  {
                
                image = #imageLiteral(resourceName: "icon_LeftBarSetting")
                title = DMLeftMenu.Settings.title
                
            }
            let model  = DMLeftMenuModel(title: title, image: image)
            items.append(model)
        }
        return items
    }
    
    private func setChangeMainController(controller:UIViewController){
        slideMenuController()?.changeMainViewController(mainViewController: controller, close: true)
    }
    
    private var mainController:(isdupFeedController:Bool, dumpFeedController:DumpFeedVC?){
        if let navigationController = slideMenuController()?.mainViewController as? UINavigationController {
            if let feedController = navigationController.topViewController as? DumpFeedVC{
                return(true,feedController)
            }else{
                return(false,nil)
            }
        }else{
            return(false,nil)
        }
    }
    
    //    private var searchController:(isSearchController:Bool, dumpFeedController:DumpFeedVC?){
    //        if let navigationController = slideMenuController()?.mainViewController as? UINavigationController {
    //            if let feedController = navigationController.topViewController as? DumpFeedVC{
    //                return(true,feedController)
    //            }else{
    //                return(false,nil)
    //            }
    //        }else{
    //            return(false,nil)
    //        }
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickSearch(_ sender: Any) {
        //        performSegue(withIdentifier: SegueIdentity.kSearchSegue, sender: sender)
        
        let searchController  = searchStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kSearchNavigation) as! UINavigationController
        setChangeMainController(controller: searchController)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        
        
        
    }
    
    func setDumpFeedVC() {
        let dumpController  = dashBoardStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDashBoardNavigation) as! UINavigationController
        self.setChangeMainController(controller: dumpController)
    }
    
}

extension DMLeftMenuVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kMenuCell, for: indexPath) as! MenuCell
        cell.model = MenuItems[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type : DMLeftMenu = DMLeftMenu(rawValue: indexPath.row)!
        switch type {
        case .Dump:
            
            let dumpController  = dashBoardStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDashBoardNavigation) as! UINavigationController
            setChangeMainController(controller: dumpController)
        case .Dumpfeed:
            let dumpFeedController  = dumpFeedStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDumpFeedNavigation) as! UINavigationController
            setChangeMainController(controller: dumpFeedController)
            break
            
        case .News:
            let newsController  = newsStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kNewsNavigation) as! UINavigationController
            setChangeMainController(controller: newsController)
            break
            
            
        case .Leaderboard:
            
            let leaderBoardController  = leaderBoardStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kLeaderBoardNavigation) as! UINavigationController
            setChangeMainController(controller: leaderBoardController)
            break
        case .Connections:
            let connectionController  = connectionStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kConnectionNavigation) as! UINavigationController
            setChangeMainController(controller: connectionController)
            break
            
        case .Notifications:
            let notificationController = notificationListStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kNotificationNavigation) as! UINavigationController
            setChangeMainController(controller: notificationController)
            break
        case .DirectMessage:
            let directMessageController = directMessageStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kDirectMessageNavigation) as! UINavigationController
            setChangeMainController(controller: directMessageController)
            break
            
            
        case .Settings:
            
            
            let settingsController  = settingsStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.kSettingsNavigation) as! UINavigationController
            setChangeMainController(controller:settingsController)
            
            break
            
            //        case .Logout:
            //            let facebookLogon = FacebookLogin()
            //            facebookLogon.logoutFB()
            //            JKGoogleManager.shared.googleSingOut()
            //            self.showLogoutAlert()
            //            break
            
            
        }
    }
}
extension String {
    
    func changesString(key:String,comment:String)-> String{
        return NSLocalizedString(key, comment: comment)
    }
}
extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
        self.layer.masksToBounds = true
    }
}
