//
//  ConnectionsVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
enum ConnectionType:Int,CustomStringConvertible{
    case followers = 0
    case following = 1
    case requests  = 2
    case facebook  = 3
    var description:String{
        switch self {
        case .followers:
            return "followers"
        case .following:
            return "following"
        case .requests:
            return "requests"
        case .facebook:
            return "facebook"
            
        }
    }
    
}
class ConnectionsVC: UIViewController {
    
    @IBOutlet weak var searchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak fileprivate var searchBar: UISearchBar!
    @IBOutlet fileprivate var objFollowers: FollowersViewModel!
    @IBOutlet weak fileprivate var collectionView: UICollectionView!
    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate var connections:[ConnectionType] = [.followers,.following,.requests,.facebook]
    fileprivate var connectionType:ConnectionType = .followers
    var index = 0
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        
        return refresh
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
            
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        }
        else{
            self.tableView.addSubview(refreshControl)
        }
        //self.refreshControl.beginRefreshing()
        refreshControl.addTarget(self, action:
            #selector(self.refreshFeed),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
    }
    @objc fileprivate func refreshFeed() {
        self.loadConnectionData(type:self.connectionType)
        refreshControl.endRefreshing()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    lazy var noDataLabel:UILabel = {
        
        let label    = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        
        label.textColor     = UIColor.black
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        loadConnectionData(type: connectionType)
        
    }
    
    var getConnectionInfo:(rowCount:Int,noData:String){
        switch connectionType {
        case .facebook: return (objFollowers.numberOfRowsForFacebookFollower(),"noFacebookUser".localized)
        case .following: return (objFollowers.numberOfRows(),"startFollowing".localized)
        case .requests: return (objFollowers.numberOfRowsForRequested(),"noFollowRequests".localized)
        default:return (objFollowers.numberOfRows(),"startDumpingFollowers".localized)
        }
    }
    fileprivate func showNoData(){
        let result  = getConnectionInfo
        if result.rowCount>0{
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
            
        }else{
            noDataLabel.text = result.noData
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        self.tableView.reloadData()
    }
    fileprivate func loadConnectionData(type:ConnectionType , searchKey text:String = ""){
        objFollowers.removeAll()
        self.tableView.reloadData()
        
        
        switch type {
        case .following:
            loadFollowing(search: text)
        case .requests:
            loadRequested(search: text)
        case .facebook:
            if userModel?.settingPrefrence?.isFacebookLinkedAccount ?? false{
                getFacebookFriend()
                tableView.isHidden = false
                facebookView.isHidden = true
            }else{
                tableView.isHidden = true
                // self.searchBar.resignFirstResponder()
                facebookView.isHidden = false
                
                
            }
        default:
            loadFollowers(search: text)
        }
    }
    fileprivate func refreshView(){
        switch connectionType {
        case .following,.followers,.requests:
            UIView.animate(withDuration: 0.3) {
                self.searchHeightConstraint.constant = 56.0
            }
            tableView.isHidden = false
            //searchBar.text = ""
            
            facebookView.isHidden = true
        case .facebook:
            // searchBar.text = ""
            self.searchBar.resignFirstResponder()
            UIView.animate(withDuration: 0.3) {
                self.searchHeightConstraint.constant = 0.0
            }
        }
    }
    fileprivate func loadFollowers(search:String="")  {
        objFollowers.getFollowFollwers(search:search, userId: userModel?.userId ?? "", page: 0, type:connectionType.description, onSuccess: {
            DispatchQueue.main.async {
                self.showNoData()
                self.collectionView.reloadData()
                
            }}, onFailure: {
                
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    fileprivate func loadFollowing(search:String="")  {
        objFollowers.getFollowFollwers(search:search, userId: userModel?.userId ?? "", page: 0, type:connectionType.description, onSuccess: {
            DispatchQueue.main.async {
                self.showNoData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    fileprivate func loadRequested(search:String = "")  {
        objFollowers.getRequesteded(search:search, userId: userModel?.userId ?? "", page: 0,  onSuccess: {
            DispatchQueue.main.async {
                self.showNoData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    fileprivate func getFacebookFriend()  {
        objFollowers.getFacebookFriend(onSuccess: {
            
            
            DispatchQueue.main.async {
                self.showNoData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail = segue.destination as! ProfileDetailVC
            objProfileDetail.index = self.index
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let userInfo = objFollowers.followFollowingInfo(at: selectedIndexPath.row)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userInfo.userId)
            objProfileDetail.handler = {
                
            }
        }
        else if segue.identifier == SegueIdentity.kRequestedProfileDetailSegue{
            let objProfileDetail = segue.destination as! ProfileDetailVC
            objProfileDetail.index = self.index
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let userInfo = objFollowers.requestedInfo(at: selectedIndexPath.row)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userInfo.userId)
        }
    }
    
    
    @IBAction func onClickFollower(_ sender: DMButton) {
        let index = sender.tag
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        objFollowers.setFollowerId(at:index)
        if followerStatus == .follower{
            objFollowers.sendFollowerRequest(onSuceess: {
                self.loadFollowers()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followerStatus == .following{
            let alert = UIAlertController(title: nil, message: "unFollowUserMeesage".localized, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Unfollow", style: .default , handler:{ (UIAlertAction)in
                self.objFollowers.sendUnfollowRequest( onSuceess: {
                    self.loadFollowers()
                    
                }, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            self.present(alert, animated: true, completion: {
                
            })
            
        }
    }
    
    @IBAction func onClickRemove(_ sender: UIButton) {
        
        let index = sender.tag
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        objFollowers.setFollowerId(at:index)
        let alert = UIAlertController(title: nil, message: "removeFollower".localized, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
            self.objFollowers.sendUnfollowRequest(type:"follower", onSuceess: {
                self.loadFollowers()
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    @IBAction func onClickFollowing(_ sender: UIButton) {
        let index = sender.tag
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        objFollowers.setFollowerId(at:index)
        if followerStatus == .follower{
            objFollowers.sendFollowerRequest(onSuceess: {
                self.loadFollowers()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followerStatus == .following{
            let alert = UIAlertController(title: nil, message: "unFollowUserMeesage".localized, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "unfollow".localized, style: .default , handler:{ (UIAlertAction)in
                self.objFollowers.sendUnfollowRequest( onSuceess: {
                    self.loadFollowers()
                    
                }, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
                print("User click Dismiss button")
            }))
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        }
    }
    
    
    
    @IBAction func onClickFacebookFollowing(_ sender: UIButton) {
        let index = sender.tag
        var followerStatus:FollowState {
            return objFollowers.setFacebookFollowedState(at: index) ?? .none
        }
        objFollowers.setFacebookFollowerId(at:index)
        if followerStatus == .follower{
            objFollowers.sendFollowerRequest(onSuceess: {
                self.getFacebookFriend()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followerStatus == .following{
            let alert = UIAlertController(title: nil, message: "unFollowUserMeesage".localized, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Unfollow", style: .default , handler:{ (UIAlertAction)in
                self.objFollowers.sendUnfollowRequest( onSuceess: {
                    self.getFacebookFriend()
                    
                }, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            self.present(alert, animated: true, completion: {
                
            })
            
        }
    }
    
    @IBAction func onclickAccept(_ sender: DMButton) {
        
        let index = sender.tag
        
        objFollowers.selectedAt(index: index)
        objFollowers.acceptRejectFollowRequest(isAccepted: true, onSuccess: {
            
            self.loadRequested()
            
        }, onFailure: {
            
        })
    }
    
    
    @IBAction func onClickReject(_ sender: DMButton) {
        let index = sender.tag
        objFollowers.selectedAt(index: index)
        objFollowers.acceptRejectFollowRequest(isAccepted: false, onSuccess: {
            self.loadRequested()
        }, onFailure: {
            
        })
    }
   
}

extension ConnectionsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getConnectionInfo.rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if connectionType == .following {
            let followingCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFollowingCell , for: indexPath) as!  FollowingCell
            followingCell.followingBtn.tag = indexPath.row
            followingCell.objFollowingModel = objFollowers.cellForRowAt(at:indexPath)
            return followingCell
            
        }else if connectionType == .requests{
            
            let requestCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kRequestCell , for: indexPath) as!  RequestCell
            
            requestCell.objRequestedModel = objFollowers.cellForRowAtRequested(at: indexPath)
            
            requestCell.acceptBtn.tag = indexPath.row
            requestCell.rejectBtn.tag = indexPath.row
            return requestCell
            
        }else if connectionType == .facebook{
            let followingCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFaceBookConnectionCell , for: indexPath) as!  FaceBookConnectionCell
            followingCell.followingBtn.tag = indexPath.row
            followingCell.objFollowingModel = objFollowers.cellForRowAtFacebookFollower(at:indexPath)
            return followingCell
        }else{
            let followersCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFollowersCell , for: indexPath) as!  FollowersCell
            followersCell.followersBtn.tag = indexPath.row
            followersCell.removeBtn.tag =  indexPath.row
            followersCell.removeFollowwidthConstraint.constant = 30
            followersCell.objFollowerModel = objFollowers.cellForRowAt(at:indexPath)
            return followersCell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
        
    }
    
}

extension ConnectionsVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return connections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let leaderBoardCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kLeaderBoardCategoryCell, for: indexPath) as! LeaderBoardCategoryCell
        leaderBoardCategoryCell.type = connections[indexPath.row]
        leaderBoardCategoryCell.isSelectedTab = connectionType == connections[indexPath.row] ? false :true
        return leaderBoardCategoryCell
    }
}
extension ConnectionsVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w =  connections[indexPath.row].description.width(withConstraintedHeight: collectionView.bounds.height, font: Roboto.Regular.font(size: 15) ?? UIFont.systemFont(ofSize: 15)) + 40
        return CGSize(width:w, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        connectionType = connections[indexPath.row]
        self.index = indexPath.row
        loadConnectionData(type: connectionType)
        refreshView()
        collectionView.reloadData()
    }
}
extension ConnectionsVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        guard  let text = searchBar.text?.removeWhiteSpace , !text.isEmpty else{return}
        
        let urlString = text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
            ServerManager.shared.cancelledRequest(isAll: true)
        self.loadConnectionData(type: self.connectionType, searchKey: urlString!)
        })
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
        self.loadConnectionData(type: connectionType)
        
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        
    }
}
