
//
//  ViewController.swift
//  Dumpya
//
//  Created by Chandan Taneja on 06/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

protocol GifVCDelegate {
    func didTapOnGifImage(_ url:String,height:Int,width:Int)
    
}
class GifVC: UIViewController{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var gifViewModel: DMGifViewModel!
    fileprivate var loadmoreCell:LoadMoreCollectionViewCell!

    var itemCount:Int {return gifViewModel.numberOfRows()}
    var searchString:String = ""
    var delegate:GifVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.searchBarStyle = .minimal
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.searchBar.delegate = self
        self.definesPresentationContext = true
     }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        gifViewModel.serviceType = .none
        pageNumber = 0
        totalRecords = 0
        loadGifImages()
    }
    //MARK: private

    
    func loadGifImages()
    {
    
        gifViewModel.getTrendingGif(offset: pageNumber, onSuccess: {
            
            DispatchQueue.main.async {
                if self.gifViewModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                         cell.isShowLoader = false
                       
                    }
                }
                self.collectionView.reloadData()
            }
        },onFailure: {
            DispatchQueue.main.async {
                
                if self.gifViewModel.serviceType == .pagging {
                    if pageNumber>0{
                        pageNumber -= 1
                    }
                    if let cell = self.loadmoreCell{
                        
                         cell.isShowLoader = false
                    }
                    
                }
            }
            
        })
    }
    func loadSearchData()
    {
        gifViewModel.getGifBySearch(searchText: self.searchString, offset: pageNumber, onSuccess: {
            DispatchQueue.main.async {
                if self.gifViewModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                        
                    }
                }
                self.collectionView.reloadData()
            }
        }) {
            DispatchQueue.main.async {
                
                if self.gifViewModel.serviceType == .pagging {
                    if pageNumber>0{
                        pageNumber -= 1
                    }
                    if let cell = self.loadmoreCell{
                        
                        cell.isShowLoader = false
                    }
                    
                }
            }
        }
    }
    @IBAction func backBTN(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GifSegue"
        {
            
        }
    }
    

}
extension GifVC :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return gifViewModel.serviceType == .pagging ? itemCount+1 :itemCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if indexPath.row<itemCount {
             let obj  = gifViewModel.cellForRowAt(at:indexPath)
            let gifCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.KGifCell, for: indexPath) as! GifCell
           
            gifCell.image = obj.gifUrl// obj
            
            return gifCell
        }
        else
         {
            loadmoreCell = collectionView.dequeueReusableCell(withReuseIdentifier: TVCellIdentifier.KLoadCollectionMoreCell, for: indexPath) as! LoadMoreCollectionViewCell
            if ServerManager.shared.CheckNetwork(){
                if self.itemCount<totalRecords {
                    gifViewModel.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        if !(self.searchBar.text?.isEmpty)!{
                            
                            self.loadSearchData()
                        }else{
                            self.loadGifImages()
                        }
                        
                    }
                }else{
                    gifViewModel.serviceType = .none
                }
            }else{
                gifViewModel.serviceType = .none
            }
            
            return loadmoreCell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let image = gifViewModel.didSelectAtGifImage(at: indexPath)
//        self.delegate?.didTapOnGifImage(imageUrl, height: 200, width: 300)
        self.delegate?.didTapOnGifImage(image.gifUrl ?? "", height: image.gifHeight, width: image.gifWidth)
        self.navigationController?.popViewController(animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let swidth  = collectionView.bounds.width
        if indexPath.row < self.itemCount {
            let width  = swidth/2-6
            return CGSize(width: width, height: width+20)
        }else{
            return CGSize(width: swidth, height: 60)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 0, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

extension GifVC:UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
         self.searchString = searchText.removeWhiteSpace
     
        if searchText.count>0{
            DispatchQueue.main.async {
                self.gifViewModel.removeAll()
            }
            gifViewModel.serviceType = .none
            pageNumber = 0
            totalRecords = 0
            loadSearchData()
            
        }else{
            DispatchQueue.main.async {
                self.gifViewModel.removeAll()
            }
            gifViewModel.serviceType = .none
            self.loadGifImages()
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let text =  searchBar.text,text.count < 1{
           
        }
        
         searchBar.resignFirstResponder()
//       // Hide Keyboard
//       searchBar.resignFirstResponder()
////        DispatchQueue.main.async {
////            self.gifViewModel.removeAll()
////        }
//        //loadSearchData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
      searchBar.endEditing(true)
      searchBar.resignFirstResponder()
        
        
        gifViewModel.serviceType = .none
        DispatchQueue.main.async {
            self.gifViewModel.removeAll()
            self.loadGifImages()
        }
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
       
      //searchBar.text = ""
        
        
    }

}

