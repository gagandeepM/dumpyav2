//
//  ResetPasswordVC.swift
//  Dumpya
//
//  Created by Chander on 18/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak fileprivate var otpFirstTF:DMTextField!
    @IBOutlet weak fileprivate var otpSecondTF:DMTextField!
    @IBOutlet weak fileprivate var otpThirdTF:DMTextField!
    @IBOutlet weak fileprivate var otpFourthTF:DMTextField!
    @IBOutlet weak fileprivate var passwordTF: DMTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: DMTextField!
    @IBOutlet weak fileprivate var otpSuceesImgView: UIImageView!
    @IBOutlet  var verifyOTP: DMUserViewModel!
    @IBOutlet weak fileprivate var resetLoginBTN: UIButton!
    @IBOutlet weak fileprivate var  createPasswordLBL: UILabel!
    let yourAttributes : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14),
         NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
    
     @IBOutlet weak var sendEmailBTN: UIButton!
    @IBOutlet weak fileprivate var sendEmailHeightConstraint: NSLayoutConstraint!
    fileprivate var isEmptyField:Bool = false{
        didSet{
            if isEmptyField {
                otpFirstTF.text =  ""
                otpSecondTF.text = ""
                otpThirdTF.text =  ""
                otpFourthTF.text  =  ""
            }
          
        }
    }
    
    //MARK:- Properties
    var userEmail:String?
    var otp:String?
    
    @IBOutlet weak fileprivate var eyeConfirmPasswordBTN: UIButton!
    @IBOutlet weak fileprivate var eyePasswordBtn: UIButton!
    fileprivate var iconPassword = true
    fileprivate var iconConfirmPassowrd = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let attributeString = NSMutableAttributedString(string: "sendNewEmail".localized,
                                                        
                                                        attributes: yourAttributes)
        
        sendEmailBTN.setAttributedTitle(attributeString, for: .normal)
        
       
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction fileprivate func textFieldDidChanged(_ sender: DMTextField) {
        let text = sender.text
        if text?.utf16.count == 1{
            switch sender{
            case otpFirstTF:
                otpSecondTF.becomeFirstResponder()
            case otpSecondTF:
                otpThirdTF.becomeFirstResponder()
            case otpThirdTF:
                otpFourthTF.becomeFirstResponder()
            case otpFourthTF:
                otpFourthTF.resignFirstResponder()
            default:
                break
            }
            self.verifyOtp()
            
        }
        else if text?.utf16.count == 0{
            switch sender{
            case otpFirstTF:
                otpFirstTF.resignFirstResponder()
            case otpSecondTF:
                otpFirstTF.becomeFirstResponder()
            case otpThirdTF:
                otpSecondTF.becomeFirstResponder()
            case otpFourthTF:
                otpThirdTF.becomeFirstResponder()
            default:
                break
            }
            self.verifyOtp()
        }
        else  {
            
        }
    }
    
    fileprivate func verifyOtp(){
        guard let otp1 = otpFirstTF.text,let otp2 = otpSecondTF.text,let otp3 = otpThirdTF.text,let otp4 = otpFourthTF.text else {return}
        otp = otp1+otp2+otp3+otp4
        guard let otp = otp else {return}
        if otp.count == 4{
            //call api
            self.verifyOTP.verifyOTP(otp: otp, onSuccess: {
                DispatchQueue.main.async {
                    self.otpSuceesImgView.isHidden = false
                    self.resetPasswordEnable(userInteraction: true, alpha: 1.0)
                    self.otpFieldDisable(userinteraction:false)
                    self.sendEmailBTN.isUserInteractionEnabled = false
                    self.sendEmailBTN.alpha = 0.5
                }
            }, onFailure: {

                self.isEmptyField = true
            }, onUserSuspend: { (message) in
                 self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else{
            self.otpSuceesImgView.isHidden = true
        }
    }

    
    fileprivate func resetPasswordEnable(userInteraction:Bool,alpha:CGFloat){
        passwordTF.isUserInteractionEnabled = userInteraction
        confirmPasswordTF.isUserInteractionEnabled = userInteraction
        resetLoginBTN.isUserInteractionEnabled = userInteraction
        passwordTF.alpha = alpha
        confirmPasswordTF.alpha = alpha
        resetLoginBTN.alpha = alpha
        createPasswordLBL.alpha = alpha
    }
     fileprivate func otpFieldDisable(userinteraction:Bool){
     self.otpFirstTF.isUserInteractionEnabled = userinteraction
     self.otpSecondTF.isUserInteractionEnabled = userinteraction
     self.otpThirdTF.isUserInteractionEnabled = userinteraction
     self.otpFourthTF.isUserInteractionEnabled = userinteraction
    }
    //MARK:- Actions
    
    
    @IBAction func onClieckEyePassword(_ sender: Any) {
        if(iconPassword == true) {
            passwordTF.isSecureTextEntry = false
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
            
        } else {
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        iconPassword = !iconPassword
    }
    
    @IBAction func onClickEyeConfirmPassword(_ sender: Any) {
        if(iconConfirmPassowrd == true) {
            confirmPasswordTF.isSecureTextEntry = false
            eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
        } else {
            eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            confirmPasswordTF.isSecureTextEntry = true
        }
        iconConfirmPassowrd = !iconConfirmPassowrd
    }
    
    
    @IBAction func onClickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickReset(_ sender: UIButton) {
        
        guard let otp = otp,let confirmPassword = confirmPasswordTF.text,let password = passwordTF.text else{return}
        verifyOTP.resetPassword(otp: otp, confirmPassword:confirmPassword, password: password, onSuccess: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                self.navigationController?.popToRootViewController(animated: true)
            })
            
            
        }, onFailure: {
            
        })
        
        
    }
    
    
    @IBAction func onClickSendEmailAgain(_ sender: UIButton) {
        guard let email = userEmail else { return }
        verifyOTP.forgotPassword(email: email, onSuccess: { (message) in
            
            alertMessage = message ?? ""
            
        }, onFailure: {
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

