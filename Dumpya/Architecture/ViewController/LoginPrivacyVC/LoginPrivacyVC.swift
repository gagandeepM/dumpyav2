//
//  LoginPrivacyVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 15/04/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class LoginPrivacyVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ServerManager.shared.CheckNetwork(){
            webView.delegate = self
            if let url = URL(string: kPrivacy) {
                let request = URLRequest(url: url)
                webView.loadRequest(request)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        ServerManager.shared.showHud()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        ServerManager.shared.hideHud()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
