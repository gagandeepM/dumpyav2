
//
//  CreatePickerControllerVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 20/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
protocol CategoryPickerControllerDelegate:NSObjectProtocol{
    func picker(pikcer:CategoryPickerControllerVC, didSelectCategory:String,categoryId:String)
    
}


class CategoryPickerControllerVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet  fileprivate var objCategoryModel: DMFeedViewModel!
    var delegate:CategoryPickerControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
       loadCategory()
   // Do any additional setup after loading the view.
        
    }

    
    func loadCategory() {
        objCategoryModel.getDumpFeed(search: "", page: 0, onSuccess: { (records) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onFailure: {
            
        }) { (message) in
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
           loadCategory()
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}

extension CategoryPickerControllerVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
         return objCategoryModel.numberOfRowsForCatagory()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let categoryCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kCategoryCell, for: indexPath) as!  CategoryCell
        categoryCell.objCategory = objCategoryModel.cellForRowAtCategory(at: indexPath)
        
        return categoryCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         guard let delegate = self.delegate else { return  }
        
       let categoryName = objCategoryModel.cellForRowAtCategory(at: indexPath).categoryName
        let categoryId = objCategoryModel.cellForRowAtCategory(at: indexPath).categoryId
         delegate.picker(pikcer: self, didSelectCategory: categoryName ?? "" ,categoryId:categoryId ?? "")
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
     }
}
