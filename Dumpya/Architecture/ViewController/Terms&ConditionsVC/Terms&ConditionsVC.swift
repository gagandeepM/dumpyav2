//
//  Terms&ConditionsVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 10/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class Terms_ConditionsVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ServerManager.shared.CheckNetwork(){
            webView.delegate = self
            if let url = URL(string: kTerms) {
                let request = URLRequest(url: url)
                webView.loadRequest(request)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickDone(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        ServerManager.shared.showHud()
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        ServerManager.shared.hideHud()
    }
    

}
