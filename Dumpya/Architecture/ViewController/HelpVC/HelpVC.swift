//
//  HelpVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 13/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {

    @IBOutlet weak var subjectTF: THTextView!
    @IBOutlet var objHelpViewModel: DMUserViewModel!
    @IBOutlet weak var descriptionTV: THTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
     subjectTF.placeholder = "subject".localized
        descriptionTV.placeholder = "description".localized
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func onClickSend(_ sender: Any) {
       self.view.endEditing(true)
        guard let subject = subjectTF.text?.removeWhiteSpace else{return}
        guard let description = descriptionTV.text?.removeWhiteSpace else{return}
        objHelpViewModel.help(subject:subject , description: description, onSuccess: { (message) in
            self.showAlert(message: message, completion: { (index) in
            self.navigationController?.popViewController(animated: true)
            })
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
}
