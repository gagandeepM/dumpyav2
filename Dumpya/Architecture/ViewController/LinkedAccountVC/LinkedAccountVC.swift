//
//  LinkedAccountVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 24/04/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class LinkedAccountVC: UIViewController {
let facebookLinkAccount = FacebookLogin()
    
    @IBOutlet fileprivate var settingViewModel: SettingsViewModel!
    @IBOutlet weak fileprivate var linkedAccountSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userModel?.settingPrefrence?.isFacebookLinkedAccount ?? false{
            self.linkedAccountSwitch.isOn = true
        }else{
            self.linkedAccountSwitch.isOn = false
        }
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func onClickLinkedAccount(_ sender: UISwitch) {
        if sender.isOn{
           facebookLinkAccount.facebookLogin(withController:self) { (success,user) in
            
            self.settingViewModel.linkedAccount(isFacebookLinkedAccount: true, fbAccessToken: user.fbAccessToken,fbUserId:user.id, onSuccess: {
                
            }, onFailure: {
                
            })
            
            }
        }
        else{
            self.settingViewModel.linkedAccount( onSuccess: {
                
            }, onFailure: {
                
            })
        }
    }
}
