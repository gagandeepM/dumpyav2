//
//  ProfileVC.swift
//  Dumpya
//
//  Created by Chander on 27/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import CoreLocation
class ProfileVC: UIViewController,DMPickerControllerDelegate,CLLocationManagerDelegate {
    
    //MARK:- DMPickerControllerDelegate Methods
    func picker(pikcer: DMPickerControllerVC, didSelectDate: String) {
        
        birthDateTF.text = didSelectDate
        
    }
    
    func picker(pikcer: DMPickerControllerVC, didSelectCountry: CountryModel) {
        countryNameLBL.text = didSelectCountry.name ?? ""
        countryCodeLBL.text = "(\(didSelectCountry.phoneCode ?? ""))"
    }
    
    func picker(pikcer: DMPickerControllerVC, didSelectGender: GenderModel) {
        genderSelectionTF.text = didSelectGender.gender
        
    }
    
    //MARK:- Outlets
    @IBOutlet weak fileprivate var firstNameTF: DMTextField!
    @IBOutlet weak fileprivate var userNameTF: DMTextField!
    @IBOutlet weak fileprivate var locationTF: DMTextField!
    @IBOutlet weak fileprivate var locationNameLBL: UILabel!
    @IBOutlet var profileViewModel: DMUserViewModel!
    @IBOutlet weak fileprivate var nameLBL: UILabel!
    @IBOutlet weak fileprivate var mobileTF: DMTextField!
    @IBOutlet weak fileprivate var emailTF: DMTextField!
    @IBOutlet weak fileprivate var birthDateTF: UITextField!
    @IBOutlet weak fileprivate var genderSelectionTF:UITextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var coverImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak fileprivate var profileImgBTN: UIButton!
    @IBOutlet weak fileprivate var countryCodeLBL: UILabel!
    @IBOutlet weak var bioTxtView: UILabel!
    @IBOutlet weak var bioTV: THTextView!
    @IBOutlet weak var coverImage: UIImageView!
    
    @IBOutlet weak fileprivate var countryNameLBL: UILabel!
    
    //MARK:- Properties
    var isFromOTP:Bool = false
    
    fileprivate let geneder = ["Male","Female","Other"]
    fileprivate var latitude:Double?
    fileprivate var longitude:Double?
    fileprivate var coverPicURL:String?
    fileprivate var profilePicURL:String = ""
    fileprivate var countryName:String = ""
    fileprivate var cityName:String = ""
    fileprivate var stateName:String = ""
    var timeInterval:Int64 = 0
    fileprivate var isProfileUpdateStatus :Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchCurrentLoction()
        setUpUI()
        getProfileData()
    }
    func setUpUI()
    {
        if isFromOTP 
        {
        self.backBtn.isHidden = true
        }
        else
        {
           self.backBtn.isHidden = false
        }
    }
   
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kDMCountryCodePickerSegue {
            let controller = segue.destination as! DMPickerControllerVC
            controller.delegate = self
            guard let souceView = sender as? DMButton else{return}
            controller.pickerType = .countryCode
            self.showPicker(controller: controller, sourceView:souceView , pickerType: .countryCode)
            
        }else if segue.identifier == SegueIdentity.kDMDatePickerSegue {
            let controller = segue.destination as! DMPickerControllerVC
            controller.delegate = self
            guard let souceView = sender as? DMButton else{return}
            controller.pickerType = .dob
            self.showPicker(controller: controller, sourceView:souceView , pickerType: .dob)
        }else if segue.identifier == SegueIdentity.kDMGenderPickerSegue{
            let controller = segue.destination as! DMPickerControllerVC
            controller.delegate = self
            guard let souceView = sender as? DMButton else{return}
            controller.pickerType = .gender
            self.showPicker(controller: controller, sourceView:souceView , pickerType: .gender)
            
        }
    }
    
    fileprivate func getProfileData(){
        self.profileViewModel.getProfileData(userId:userModel?.userId ?? "",onSuccess: { () in
            DispatchQueue.main.async {
                self.updateData()
            }
        }, onFailure: {
            
        })
    }
    
    fileprivate func updateData() {
        
        self.userNameTF.text = self.profileViewModel.username
        self.nameLBL.text = "\(self.profileViewModel.firstname) \(self.profileViewModel.lastname)"
        self.firstNameTF.text = "\(self.profileViewModel.firstname) \(self.profileViewModel.lastname)"
       
        self.emailTF.text = self.profileViewModel.email
        
        if isProfileUpdate{
            emailTF.isUserInteractionEnabled = false
            isProfileUpdateStatus = isProfileUpdate
            backBtn.isHidden = false
            let imageFile  = self.profileViewModel.profilePic
            profilePicURL = imageFile
            self.profileImgBTN.loadImage(filePath: imageFile, for: .normal);
            locationTF.text = self.profileViewModel.locationName
            bioTV.text = self.profileViewModel.bio
            countryNameLBL.text = self.profileViewModel.countryobj
            countryCodeLBL.text = self.profileViewModel.countryCode
            mobileTF.text = self.profileViewModel.mobileNumber
            bioTxtView.text = self.profileViewModel.bio
            locationNameLBL.text = self.profileViewModel.locationName
           
            let date =  Date(milliseconds: self.profileViewModel.birthDate) // "Dec 31, 1969, 4:00 PM" (PDT variant of 1970 UTC)
            
            
            guard let coverImageFile  = profileViewModel?.coverPic  else { return}
            coverPicURL = coverImageFile
            countryName = profileViewModel.countryName
            self.coverImage.loadImage(filePath: coverImageFile)
            
            let date1 = date
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy" // change format as per needs
            let result = formatter.string(from: date1)
            
            birthDateTF.text = "\(result)"
            
            if self.profileViewModel.gender == "Male"{
               genderSelectionTF.text = "male".localized
            }else if self.profileViewModel.gender == "Female"{
                genderSelectionTF.text = "female".localized
            }else{
                 genderSelectionTF.text = "other".localized
            }
       
        }
        else{
            isProfileUpdateStatus = isProfileUpdate
            backBtn.isHidden = false
            
            if  profileViewModel.socialType == "normal" {
                self.emailTF.isUserInteractionEnabled = false
            }
            else{
                self.emailTF.isUserInteractionEnabled = true
                let imageFile  = self.profileViewModel.profilePic
                profilePicURL = imageFile
                self.profileImgBTN.loadImage(filePath: imageFile, for: .normal);
            }
            
        }
    }
    
    fileprivate func showPicker(controller:DMPickerControllerVC, sourceView:DMButton, pickerType:DMPickerType){
        
        if let popoverController = controller.popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = sourceView.bounds
            popoverController.delegate = self
            var height:CGFloat = 200
            var width:CGFloat = 300
            if pickerType == .countryCode{
                height = 50*254
                width = 300
            }else if pickerType == .gender {
                height = 190
                width = 200
            }
            controller.preferredContentSize = CGSize(width: width, height: height)
        }
        
    }
    //MARK:- Actions
    
    @IBAction func onClickBack(_ sender: UIButton) {
        //dismiss(animated:false,completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickProfile(_ sender: Any) {
        self.showPicker(message: "") { (isPicked, image, filename) in
            if isPicked{
                self.profileImgBTN.setImage(image, for: .normal)
                self.profileViewModel.imageUpload(image: image!, onSuccess: { url in
                    self.profilePicURL = url
                    
                }, onFailure: {
                    
                })
                
            }
        }
        
    }
    
    @IBAction func onClickChangeBackground(_ sender: Any) {
        self.showPicker(message: "") { (isPicked, image, filename) in
            if isPicked{
                
           
                self.coverImage.image =  image
                self.profileViewModel.imageUpload(image: image!, onSuccess: { url in
                    
                    self.coverPicURL = url
                }, onFailure: {
                    
                })
            }
        }
    }
    
    @IBAction func onClickLocation(_ sender: Any) {
        
        if ServerManager.shared.CheckNetwork() {
            
            if !JKLocationManager.shared.checkEnableLocation {
                return
            }
            ServerManager.shared.showHud()
            JKGoogleManager.shared.showGooglePlacePicker(from: self, center: currentLocation.coordinate) { (place, error, isCancel) in
                ServerManager.shared.hideHud()
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else { return}
                if let arrays = place.addressComponents{
                    let country:String = arrays.country ?? ""
                    var state:String = arrays.state ?? ""
                    var city:String = arrays.city ?? ""
                    
                    if arrays.isEmptyAddress{
                        alertMessage = FieldValidation.kLocationNotFound
                    }else{
                        
                        var  state1 = ""
                        if country.isUnitedStates{
                            state1 = !state.isEmpty ? "\(state)" : ""
                        }else{
                            state = !state.isEmpty ? "\(state)," : ""
                        }
                        
                        
                        if !state.isEmpty{
                            city = !city.isEmpty ? "\(city)," : ""
                        }else{
                            city = !city.isEmpty ? "\(city)" : ""
                        }
                        
                        self.locationTF.text = country.isUnitedStates ? "\(city) \(state1)" : "\(state) \(country)"
                        self.countryName = country
                        
//                        UserDefaults.standard.set(self.countryName, forKey: "currentLocation")
//                        UserDefaults.standard.set(self.countryName, forKey: "profileLocation")
                        self.stateName = state
                        self.cityName = city
                        self.locationNameLBL.text = country.isUnitedStates ? "\(city) \(state1)" : "\(state) \(country)"
                    }
                }else {
                    JKGoogleManager.shared.fetchloaction(from: place.coordinate, completion: { (city, country, state, error) in
                        if let err = error{
                            print(err.localizedDescription)
                            alertMessage = "Not able to find any location"
                        }else{
                            let country:String = country ?? ""
                            var state:String =  state ?? ""
                            self.countryName = country
                            var city = city ?? ""
                            if country.isEmpty && state.isEmpty && city.isEmpty{
                                alertMessage = FieldValidation.kLocationNotFound
                                
                            }
                            else{
                                //UserDefaults.standard.set(self.countryName, forKey: "currentLocation")
                                //UserDefaults.standard.set(self.countryName, forKey: "profileLocation")
                                self.stateName = state
                                self.cityName = city
                                var  state1 = ""
                                if country.isUnitedStates{
                                    state1 = !state.isEmpty ? "\(state)" : ""
                                }else{
                                    state = !state.isEmpty ? "\(state)," : ""
                                }
                                
                                if !state.isEmpty{
                                    city = !city.isEmpty ? "\(city)," : ""
                                }else{
                                    city = !city.isEmpty ? "\(city)" : ""
                                }
                                self.locationTF.text =  country.isUnitedStates ? "\(city) \(state1)" : "\(state) \(country)"
                                self.locationNameLBL.text = country.isUnitedStates ? "\(city) \(state1)" : "\(state) \(country)"
                            }
                            
                        }
                    })
                }
                
                
                userCoordinate = place.coordinate
                self.latitude =  userCoordinate.latitude
                self.longitude = userCoordinate.longitude
            }
        }
    }
    
     @IBAction func onClickSave(_ sender: Any) {
        if cityName.isEmpty{
            cityName = stateName
        }
        guard let firstName = self.firstNameTF.text?.removeWhiteSpace else { return }
        guard let userName = self.userNameTF.text?.removeWhiteSpace else { return }
        guard let emailAddress = self.emailTF.text else { return }
        guard let location = self.locationTF.text else { return }
        guard let mobileNumber = self.mobileTF.text else { return }
        guard let birthDate = self.birthDateTF.text else { return }
        guard let genderSelection = self.genderSelectionTF.text else { return }
         var genderSelected:String = "Other"
        if genderSelection == DMGenderSelected.male.genderType{
            genderSelected = "Male"
        }else if genderSelection == DMGenderSelected.female.genderType{
            genderSelected = "Female"
        }else{
             genderSelected = "Other"
        }
        guard let bio = self.bioTV.text else{return}
        
        let fullName = firstName
        var fullNameArr =  fullName.components(separatedBy: " ")
        let name: String = fullNameArr[0]
        let lastName: String? = fullNameArr.count > 1 ? fullNameArr[1] : ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy" // change format as per needs
        if !birthDate.isEmpty{
            guard let dateFromString = formatter.date(from: birthDate)else{return}
            print("THE Date from string is---------->",dateFromString)
            timeInterval = dateFromString.millisecondsSince1970
        }
        
        // Save location for check location change or not
        
         UserDefaults.standard.set(self.profileViewModel.currentLocation, forKey: "currentLocation")
       
     
  /* ------------------------------------------------------------------------------------------------------*/
        profileViewModel.editProfile(firstName: name ,lastName:lastName ?? "" ,userName: userName ?? "", email: emailAddress,location:location,mobileNumber:mobileNumber,birthDate:birthDate ,genderType:genderSelected,countryName:countryNameLBL.text ?? "",countryCode:countryCodeLBL.text ?? "",latitude:latitude ?? 0.0 ,longitude:longitude ?? 0.0  ,coverPicUrl:coverPicURL ?? "" ,profilePicUrl:profilePicURL ?? "",bio:bio,timeInterval:timeInterval,countryNameFull:countryName ,cityName:cityName, stateName:stateName,onSuccess: { (message) in
            isDumpySaved = true
            if self.isProfileUpdateStatus{
                self.navigationController?.popViewController(animated: true)
//                self.showAlert(message:message , completion: { (index) in
//                    self.dismiss(animated: true, completion: nil)
//                })
//
            }
            else{
                AppDelegate.sharedDelegate.showMainController()
            }
        }, onFailure: {
            isDumpySaved = false
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        JKLocationManager.shared.stopUpdatingLocation()
    }
}
extension ProfileVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController)-> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
}
extension ProfileVC:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        // Run code here for when user begins type into the text view
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        bioTxtView.text = bioTV.text
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        // Run code here for when user ends editing text view
        
    }
}


