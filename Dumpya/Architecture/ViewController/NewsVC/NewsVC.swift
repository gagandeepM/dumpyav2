//
//  NewsVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 13/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {

    @IBOutlet var objNewsModel: NewsViewModel!
    @IBOutlet weak var tableView: UITableView!
     fileprivate var loadmoreCell:LoadMoreCell!
    var itemCount:Int {return objNewsModel.numberOfRows()}
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
      
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        }
        else{
            self.tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action:
            #selector(self.refreshNews),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
//        self.refreshControl.beginRefreshing()
    
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
        
        refreshControl.layoutIfNeeded()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageNumber = 0
        totalRecords = 0
        objNewsModel.serviceType = .none
       newsData()
    }
    
    @objc fileprivate func refreshNews() {
        
        newsData()
       
    }
    
    @IBAction func onClickReadMore(_ sender: Any) {
   
     performSegue(withIdentifier: SegueIdentity.kNewsDetailSegue, sender: sender)
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func newsData() {
        objNewsModel.getNews(isPull:self.refreshControl.isRefreshing,onSuccess: {
            DispatchQueue.main.async {
                
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                if self.objNewsModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                self.tableView.reloadData()
             }
        }, onFailure: {
            if self.objNewsModel.serviceType == .pagging {
                if pageNumber>0{
                    pageNumber -= 1
                }
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
                
            }
        })
    }
    
    
    @IBAction func onClickNewsRedump(_ sender: UIButton) {
        
        let index = sender.tag
        let newsModel  =  objNewsModel.redumpStatus(index:index)
        let dumpStatus =  newsModel.0


        let newsId     =  newsModel.1
        if dumpStatus{
        alertMessage = "You've already dump this news"
        }else{
            objNewsModel.didUpdateRedumpNews(at: index)
            self.objNewsModel.redumpNews(newsId: newsId,countryName:countryNameFromLocation ,locationName:locationNameLocation,onSuccess: { (redumpCount) in
                print("THE REDUMP COUNT IS-------->",redumpCount)
                self.objNewsModel.didUpdateRedumpNews(at: index,isDump: !dumpStatus,redumpCount: redumpCount)
                let indexPath = IndexPath(item: index, section: 0)
                UIView.performWithoutAnimation {
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }

        
        
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    // MARK: - Navigation

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == SegueIdentity.kNewsDetailSegue{
            let objNewsDetailVC : NewsDetailVC = segue.destination as! NewsDetailVC
            guard let sender = sender as? UIButton else{return}
            objNewsDetailVC.newsUrl = objNewsModel.didSelect(index: sender.tag)
        }
    }
}

extension NewsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objNewsModel.serviceType == .pagging ? itemCount+1 :itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if indexPath.row<itemCount {
        let newsCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kNewsCell , for: indexPath) as! NewsCell
        newsCell.objNewsModel = objNewsModel.cellForRowAt(at: indexPath)
        newsCell.readMoreBtn.tag = indexPath.row
        newsCell.mediaRedumpBtn.tag = indexPath.row
        return newsCell
        }
        else
         {
            
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(){
                if self.itemCount>0,self.itemCount<totalRecords {
                    objNewsModel.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        self.newsData()
                    }
                }else{
                    objNewsModel.serviceType = .none
                }
            }else{
                objNewsModel.serviceType = .none
            }
            
            return loadmoreCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 310.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

}
