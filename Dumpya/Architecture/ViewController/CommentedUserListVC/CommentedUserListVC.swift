//
//  CommentedUserListVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 21/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class CommentedUserListVC: UIViewController {
    @IBOutlet var objCommentedUserListViewModel: CommentedUserListViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.commentedUserList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func commentedUserList(){
        objCommentedUserListViewModel.getCommentedUsersList(onSuccess: {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            
        }, onFailure: {
            
        })
    }

    
    // MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
        guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
        
        
        let searchUserId = objCommentedUserListViewModel.didSelectRedumpUser(at: selectedIndexPath.row)
        
        objProfileDetail.objUserDetailViewModel.set(currentUserId: searchUserId)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CommentedUserListVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        if objCommentedUserListViewModel.numberOfRows() > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No User Found"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objCommentedUserListViewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let commentedUserListCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kCommentedUserListCell, for: indexPath) as! CommentedUserListCell
        commentedUserListCell.objCommentedUserInfo =   objCommentedUserListViewModel.cellForRowAt(indexPath: indexPath)
        
        return commentedUserListCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
   
}
