       //
//  NotificationVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    @IBOutlet weak var noNotificationLbl: UILabel!
    @IBOutlet weak var deleteNotificationBtn: UIButton!
    @IBOutlet var objNotificationViewModel: NotificationViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate var loadmoreCell:LoadMoreCell!
    var itemCount:Int {return objNotificationViewModel.numberOfRows()}
//    fileprivate lazy var refreshControl:UIRefreshControl = {
//        let refresh = UIRefreshControl()
//        refresh.tintColor = DMColor.redColor
//        return refresh
//    }()
      let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        refreshControl.tintColor = DMColor.redColor
        self.tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action:
            #selector(self.loadNotification),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       if totalRecords > 0
        {
        self.objNotificationViewModel.removeObjects()
       }
        pageNumber = 0
        totalRecords = 0
        objNotificationViewModel.serviceType = .none
        loadNotification()
    }
    func refreshNotificationList() {
        if totalRecords > 0
        {
            objNotificationViewModel.removeObjects()
        }
        pageNumber = 0
        totalRecords = 0
        objNotificationViewModel.serviceType = .none
        loadNotification()
        
    }
    var isRefreshFeed:Bool = false{
        didSet{
            if isRefreshFeed {
                self.loadNotification()
            }
        }
    }
    @objc fileprivate func loadNotification()  {
       
         objNotificationViewModel.getNotification(isPull: self.refreshControl.isRefreshing, page: pageNumber,onSuccess: {
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                if self.objNotificationViewModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                
                if self.objNotificationViewModel.numberOfRows() > 0 {
                    self.tableView.reloadData()
                    self.deleteNotificationBtn.isHidden = false
                    self.noNotificationLbl.isHidden = true
                    self.tableView.isHidden = false
                }else{
                    self.noNotificationLbl.isHidden = false
                    self.tableView.isHidden = true
                    self.deleteNotificationBtn.isHidden = true
                }
                
            }
        }, onFailure: {
            DispatchQueue.main.async {
                
                if self.objNotificationViewModel.serviceType == .pagging {
                    if pageNumber>0{
                        pageNumber -= 1
                    }
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                    
                }
            }
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let dumpInfo = objNotificationViewModel.didSelect(at: selectedIndexPath)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.senderInfoModel?.senderId ?? "")
        }
        else if segue.identifier ==  SegueIdentity.kDumpDetailSegue{
                 let objDumpDetailVC : DumpDetailVC = segue.destination as! DumpDetailVC
                 guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
                let dumpInfo = objNotificationViewModel.didSelect(at: selectedIndexPath)
                objDumpDetailVC.notificationDumpId = dumpInfo.dumpId ?? ""
          }
     }
    
    @IBAction fileprivate func onclickAccept(_ sender: DMButton) {
       
        let index = sender.tag
        
        objNotificationViewModel .selectedAt(index: index)
        objNotificationViewModel.acceptRejectNotificationFollowRequest(isAccepted: true, onSuccess: {
             self.objNotificationViewModel.removeObjects()
            self.loadNotification()
            
        }, onFailure: {
            
        })
    }
    @IBAction func onClickDeleteNotification(_ sender: Any) {
        clearNotificationHistory()
    }
    
    
    func clearNotificationHistory(title:String = kAppTitle,message:String = "clearNotifications".localized){
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: message, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.DeepOrange)
        let cancel = AlertActionModel(image: nil, title:"NO".localized, color: DMColor.ACRed, style: .cancel,alignment: .none)
        let destructive = AlertActionModel(image: nil, title:"YES".localized, color: DMColor.Red, style: .destructive)
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: [cancel,destructive]) { (alert:UIAlertController, action:UIAlertAction, buttonIndex:Int) in
            
            switch buttonIndex {
            case 1:
                self.objNotificationViewModel.deleteNotification(onSuccess: {
                    self.objNotificationViewModel.removeObjects()
                    self.loadNotification()
                }, onFailure: {
                })
                
                break
                
            default:
                break
            }
        }
    }
    /*
     let index = sender.tag
     
     objNotificationViewModel .selectedAt(index: index)
     objNotificationViewModel.acceptRejectNotificationFollowRequest(isAccepted: true, onSuccess: {
     
     self.loadNotification()
     
     }, onFailure: {
     
     })
     
     
     */
    @IBAction fileprivate func onClickReject(_ sender: DMButton) {
       
        let index = sender.tag
        objNotificationViewModel.selectedAt(index: index)
        objNotificationViewModel.acceptRejectNotificationFollowRequest(isAccepted: false, onSuccess: {
              self.objNotificationViewModel.removeObjects()
            self.loadNotification()
        }, onFailure: {
        })
    }
    
}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("number of rows -------- \(objNotificationViewModel.serviceType == .pagging ? itemCount+1 :itemCount)")
        return objNotificationViewModel.serviceType == .pagging ? itemCount+1 :itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.row<itemCount {
            
            let notificationListCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kNotificationListCell , for: indexPath) as!  NotificationListCell
            notificationListCell.acceptBtn.tag = indexPath.row
            notificationListCell.rejectBtn.tag = indexPath.row
            notificationListCell.objNotificationList =  objNotificationViewModel.cellForRowAt(at:indexPath)
            return notificationListCell
        }
        else{
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(){
                
                 if self.itemCount>0,self.itemCount<totalRecords {
                    objNotificationViewModel.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        self.loadNotification()
                    }
                }else{
                    objNotificationViewModel.serviceType = .none
                }
            }else{
                objNotificationViewModel.serviceType = .none
            }
            
            return loadmoreCell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if indexPath.row<itemCount {
            let notificationType = objNotificationViewModel.cellForRowAt(at:indexPath).notificationType
            if notificationType == 1 || notificationType == 2 || notificationType == 3{
                performSegue(withIdentifier: SegueIdentity.kDumpDetailSegue, sender: self)
            }else{
                performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: self)
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            let notificationId = objNotificationViewModel.didSelect(at: indexPath).notificationId ?? ""
            objNotificationViewModel.deleteNotification(notificationId: notificationId, onSuccess: {
                self.objNotificationViewModel.deleteParticularNotification(indexPath: indexPath)
                
                
                if self.objNotificationViewModel.numberOfRows() > 0 {
                    self.tableView.reloadData()
                    self.noNotificationLbl.isHidden = true
                    self.tableView.isHidden = false
                }else{
                    self.noNotificationLbl.isHidden = false
                    self.tableView.isHidden = true
                }
                
                
                //  tableView.deleteRows(at: [indexPath], with: .none)
                
                
                
            }, onFailure: {
            })
            
        }
    }
}

