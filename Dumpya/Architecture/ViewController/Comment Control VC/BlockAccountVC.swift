//
//  BlockAccountVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 12/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class BlockAccountVC: UIViewController {
    
    @IBOutlet weak var noBlockUser: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var objSettingViewModel: SettingsViewModel!
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        blockUserList()
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func blockUserList(){
        objSettingViewModel.getBlockAccount(onSuccess: {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickUnblock(_ sender: UIButton) {
        
        let blockUserId =  objSettingViewModel.unBlockUser(at: sender.tag)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "unblockUser".localized, style: .default , handler:{ (UIAlertAction)in
            self.objSettingViewModel.userUnblockRequest(blockUserId:blockUserId, blockType: false , onSuceess: {
                self.blockUserList()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BlockAccountVC:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if objSettingViewModel.numberOfRows() > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "noBlockedUser".localized
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objSettingViewModel.numberOfRows()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let blockAccountCell = tableView.dequeueReusableCell(withIdentifier:"BlockAccountCell", for: indexPath) as! BlockAccountCell
        
        blockAccountCell.objModel = objSettingViewModel.cellForRowAt(at:indexPath)
        blockAccountCell.unBlockBtn.tag = indexPath.row
        return blockAccountCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
}

extension BlockAccountVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchText = searchText.removeWhiteSpace
        
        
        if searchText.count>0{
           
            objSettingViewModel.getBlockAccount(seacrh:searchText,onSuccess: {
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }else{
            objSettingViewModel.getBlockAccount(onSuccess: {
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
        print("searchBarCancelButtonClicked")
        objSettingViewModel.getBlockAccount(onSuccess: {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
}
