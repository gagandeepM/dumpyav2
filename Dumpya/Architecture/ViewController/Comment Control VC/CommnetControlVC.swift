//
//  CommnetControlVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 12/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class CommnetControlVC: UIViewController {
    @IBOutlet weak var noOneImgView: UIImageView!
    @IBOutlet weak var followImgView: UIImageView!
    @IBOutlet weak var followMeImgView: UIImageView!
    @IBOutlet weak var everyOneImgView: UIImageView!
    @IBOutlet var objCommentViewModel: SettingsViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
self.getData()
        // Do any additional setup after loading the view.
    }

    
    fileprivate func getData() {
        objCommentViewModel.getSettingPrefrence(onSuccess: { (isPrivateAccount,isRedump,isComment,acceptedFollowRequestPushStatus,directMessagePushStatus,directMessageRequestPushStatus,dumpsPushStatus,newFollowersPushStatus,commnetControl) in
            
            if commnetControl == 3{
                self.followMeImgView.isHidden = false
            } else if commnetControl == 1{
                self.everyOneImgView.isHidden = false
            } else if commnetControl == 2{
                self.followImgView.isHidden = false
            }else{
                self.noOneImgView.isHidden = false
            }
            
        }
            , onFailure: {
                
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickEveryOne(_ sender: Any) {
        self.noOneImgView.isHidden = true
        self.followImgView.isHidden = true
        self.followMeImgView.isHidden = true
        self.everyOneImgView.isHidden = false
        self.objCommentViewModel.setPushNotificationSetting(type:"commentControlStatus", typeValue:1, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
        
    }
    
    @IBAction func onClickFollowMe(_ sender: Any) {
        
        self.noOneImgView.isHidden = true
        self.followImgView.isHidden = true
        self.followMeImgView.isHidden = false
        self.everyOneImgView.isHidden = true
        self.objCommentViewModel.setPushNotificationSetting(type:"commentControlStatus", typeValue:3, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    @IBAction func onClickFollow(_ sender: Any) {
        
        self.noOneImgView.isHidden = true
        self.followImgView.isHidden = false
        self.followMeImgView.isHidden = true
        self.everyOneImgView.isHidden = true
        self.objCommentViewModel.setPushNotificationSetting(type:"commentControlStatus", typeValue:2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    @IBAction func onClickNoOne(_ sender: Any) {
        
        self.noOneImgView.isHidden = false
        self.followImgView.isHidden = true
        self.followMeImgView.isHidden = true
        self.everyOneImgView.isHidden = true
        self.objCommentViewModel.setPushNotificationSetting(type:"commentControlStatus", typeValue:0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
}
