//
//  ViewController.swift
//  Dumpya
//
//  Created by Gagandeep Mishra on 21/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit
import MobileCoreServices
class LibraryVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet var videoBtn: UIButton!
    @IBOutlet var photoBtn: UIButton!
    var imagePickerController = UIImagePickerController()
    var videoURL: URL?
    override func viewDidLoad() {
        super.viewDidLoad()
       photoLibrary()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.photoBtn.tintColor = .black
    }
    
    @IBAction func onClickBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
           
        }
    }
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // To handle image
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           print(image)
        } else{
            print("Something went wrong in  image")
        }
    
    self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickedVideoBtn(_ sender: UIButton) {
        self.videoBtn.tintColor = .black
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoVC") as? VideoVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
