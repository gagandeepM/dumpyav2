//
//  VideoVC.swift
//  Dumpya
//
//  Created by Gagandeep Mishra on 22/02/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit
import MobileCoreServices
class VideoVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet var photoBTn: UIButton!
    @IBOutlet var videoBtn: UIButton!
    var imagePickerController = UIImagePickerController()
    var videoURL: URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoBtn.tintColor = .black
         videoLibrary()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    @IBAction func onClickBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            self.present(myPickerController, animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? NSURL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl as URL)!
            let size = Double(data.length / 1048576)
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            print(videoUrl,size)
            
        }
        else{
            print("Something went wrong in  video")
        }
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickedPhotoBTn(_ sender: UIButton) {
        self.photoBTn.tintColor = .black
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LibraryVC") as? LibraryVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}
