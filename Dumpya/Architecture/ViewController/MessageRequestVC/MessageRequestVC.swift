//
//  MessageRequestVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/04/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit

class MessageRequestVC: UIViewController {
    @IBOutlet fileprivate var objRequestViewModel: DirectMessageViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(self.refreshMessageRequest),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
        getMessageRequestUser()
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshMessageRequest() {
        getMessageRequestUser()
        
    }
    fileprivate func getMessageRequestUser(){
        objRequestViewModel.getChatUsersList(isPull:self.refreshControl.isRefreshing,isRequest:true,onSuccess: {
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                if self.objRequestViewModel.numberOfRows() > 0 {
                    self.tableView.reloadData()
                    
                    self.tableView.isHidden = false
                }else{
                    
                    self.tableView.isHidden = true
                }
                
                
            }
        }, onFailure: {
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MessageRequestVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objRequestViewModel.numberOfRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kMessageRequestCell, for: indexPath) as! MessageRequestCell
        
        cell.objMessageRequest = objRequestViewModel.cellForRowAt(indexPath: indexPath)
        
        cell.acceptBtn.tag = indexPath.row
        cell.rejectBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }
}

//MARK:- Actions
extension MessageRequestVC{
    @IBAction fileprivate func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func onclickAccept(_ sender: DMButton) {
        
        let index = sender.tag
        
        objRequestViewModel.onMessageRequest(index: index)
        objRequestViewModel.acceptRejectMessageRequest(isAccepted: "Accepted", onSuccess: {
            //
           self.navigationController?.popViewController(animated: true)
            //
        }, onFailure: {
            //
        })
    }
    
    
    @IBAction fileprivate func onClickReject(_ sender: DMButton) {
        
        let index = sender.tag
        objRequestViewModel.onMessageRequest(index: index)
        objRequestViewModel.acceptRejectMessageRequest(isAccepted: "Rejected", onSuccess: {
            
            self.getMessageRequestUser()
        }, onFailure: {
        })
    }
}
