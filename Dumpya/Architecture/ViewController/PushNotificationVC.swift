//
//  PushNotificationVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class PushNotificationVC: UIViewController {

    @IBOutlet var objSettingViewModel: SettingsViewModel!
    @IBOutlet weak var commnetFromEveryoneImgView: UIImageView!
    @IBOutlet weak var commentFromPeopleImgView: UIImageView!
    @IBOutlet weak var commentFromPeople: UILabel!
    @IBOutlet weak var commentOffImgView: UIImageView!
    @IBOutlet weak var redumpEveryoneImgView: UIImageView!
    @IBOutlet weak var redumpFromPeopleImgView: UIImageView!
    @IBOutlet weak var redumpOffImgview: UIImageView!
    
    @IBOutlet weak var dumpsIcon_off: UIImageView!
    
    @IBOutlet weak var dumpsIcon_Follow: UIImageView!
    
    
    @IBOutlet weak var newFollowers_OffIcon: UIImageView!
    
    @IBOutlet weak var newFollowers_EveryoneIcon: UIImageView!
    
    
    @IBOutlet weak var acceptedFollowRequest_FromEveryoneIcon: UIImageView!
    @IBOutlet weak var acceptedFollowRequest_officon: UIImageView!
    
    @IBOutlet weak var directMessageRequest_FromEveryoneIcon: UIImageView!
    
    @IBOutlet weak var directMessageRequest_offIcon: UIImageView!
    
    
    @IBOutlet weak var directMessage_everyoneIcon: UIImageView!
    @IBOutlet weak var direcrtMessage_offIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
getData()
        // Do any additional setup after loading the view.
    }

   fileprivate func getData() {
        objSettingViewModel.getSettingPrefrence(onSuccess: { (isPrivateAccount,isRedump,isComment,acceptedFollowRequestPushStatus,directMessagePushStatus,directMessageRequestPushStatus,dumpsPushStatus,newFollowersPushStatus,commnetControl) in
            
            
            if isRedump == 0{
               self.redumpOffImgview.isHidden = false
            } else if isRedump == 1{
                self.redumpFromPeopleImgView.isHidden = false
            }else{
                self.redumpEveryoneImgView.isHidden = false
            }
            
            if isComment == 0{
                self.commentOffImgView.isHidden = false
            } else if isComment == 1{
                self.commentFromPeopleImgView.isHidden = false
            }else{
                self.commnetFromEveryoneImgView.isHidden = false
            }
            
            
            
            if acceptedFollowRequestPushStatus == 0{
                self.acceptedFollowRequest_officon.isHidden = false
            } else{
                self.acceptedFollowRequest_FromEveryoneIcon.isHidden = false
            }
            
            if directMessagePushStatus == 0{
                self.direcrtMessage_offIcon.isHidden = false
            } else{
                self.directMessage_everyoneIcon.isHidden = false
            }
            if directMessageRequestPushStatus == 0{
                self.directMessageRequest_offIcon.isHidden = false
            } else{
                self.directMessageRequest_FromEveryoneIcon.isHidden = false
            }
            if dumpsPushStatus == 0{
                self.dumpsIcon_off.isHidden = false
            } else{
                self.dumpsIcon_Follow.isHidden = false
            }
            
            if newFollowersPushStatus == 0 {
                self.newFollowers_OffIcon.isHidden = false
            } else{
                self.newFollowers_EveryoneIcon.isHidden = false
            }
            
            }
        , onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func onClickRedumpOff(_ sender: Any) {
        
       self.redumpOffImgview.isHidden = false
         self.redumpFromPeopleImgView.isHidden = true
         self.redumpEveryoneImgView.isHidden = true
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.redumpPushStatus", typeValue:0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickRedumpFromPeople(_ sender: Any) {
        
       
        self.redumpOffImgview.isHidden = true
        self.redumpFromPeopleImgView.isHidden = false
        self.redumpEveryoneImgView.isHidden = true
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.redumpPushStatus", typeValue: 1, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickRedumpFromEveryone(_ sender: Any) {
        
        self.redumpOffImgview.isHidden = true
        self.redumpFromPeopleImgView.isHidden = true
        self.redumpEveryoneImgView.isHidden = false
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.redumpPushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
   
    
    @IBAction func onClickCommmentOff(_ sender: Any) {
        
       self.commentOffImgView.isHidden = false
      self.commentFromPeopleImgView.isHidden = true
       self.commnetFromEveryoneImgView.isHidden = true
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.commentPushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickCommmentFromPeople(_ sender: Any) {
        
       
        self.commentOffImgView.isHidden = true
        self.commentFromPeopleImgView.isHidden = false
        self.commnetFromEveryoneImgView.isHidden = true
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.commentPushStatus", typeValue: 1, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickCommmentFromEveryone(_ sender: Any) {
        
        self.commentOffImgView.isHidden = true
        self.commentFromPeopleImgView.isHidden = true
        self.commnetFromEveryoneImgView.isHidden = false
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.commentPushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickDumpsoff(_ sender: Any) {
        
        dumpsIcon_off.isHidden = false
        dumpsIcon_Follow.isHidden = true
        
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.dumpsPushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickDumpsFollow(_ sender: Any) {
        dumpsIcon_off.isHidden = true
        dumpsIcon_Follow.isHidden = false

        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.dumpsPushStatus", typeValue: 1, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })

    }


    @IBAction func onClickNewFollowerOff(_ sender: Any) {
        
       
        newFollowers_OffIcon.isHidden = false
        newFollowers_EveryoneIcon.isHidden = true
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.newFollowersPushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
        
        
    }
    
    @IBAction func onClickNewFollowerEveryone(_ sender: Any) {
        newFollowers_OffIcon.isHidden = true
        newFollowers_EveryoneIcon.isHidden = false
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.newFollowersPushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickAcceptFollowOff(_ sender: Any) {
        acceptedFollowRequest_officon.isHidden = false
        acceptedFollowRequest_FromEveryoneIcon.isHidden = true
        
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.acceptedFollowRequestPushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickAcceptFollowEveryone(_ sender: Any) {
        
        acceptedFollowRequest_officon.isHidden = true
        acceptedFollowRequest_FromEveryoneIcon.isHidden = false
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.acceptedFollowRequestPushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
   
    @IBAction func onClickMessageRequestOff(_ sender: Any) {
        
        directMessageRequest_offIcon.isHidden = false
        
        directMessageRequest_FromEveryoneIcon.isHidden = true
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.directMessageRequestPushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    
    @IBAction func onClickMessageRequestEveryone(_ sender: Any) {
        
        directMessageRequest_offIcon.isHidden = true
        
        directMessageRequest_FromEveryoneIcon.isHidden = false
        
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.directMessageRequestPushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickDirectMessage(_ sender: Any) {
        
        direcrtMessage_offIcon.isHidden = false
        
        directMessage_everyoneIcon.isHidden = true
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.directMessagePushStatus", typeValue: 0, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    
    
    @IBAction func onClickDirectMessageEveryone(_ sender: Any) {
        
        direcrtMessage_offIcon.isHidden = true
        
        directMessage_everyoneIcon.isHidden = false
        self.objSettingViewModel.setPushNotificationSetting(type:"pushSetting.directMessagePushStatus", typeValue: 2, onSuccess: {
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
}
