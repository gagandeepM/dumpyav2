//
//  DumpVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 17/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class DumpVC: UIViewController {
    @IBOutlet weak fileprivate var searchBar: UISearchBar!
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak var lineLBL: UILabel!
    @IBOutlet weak var heightConstraintView: NSLayoutConstraint!
    @IBOutlet fileprivate var objDumpFeed: DMFeedViewModel!
    fileprivate var categoryName:String = ""
    fileprivate var searchText:String = ""
    fileprivate var historyText:String = ""
    fileprivate var loadmoreCell:LoadMoreCell!
    var catagoryNameWithNoRcord:String = ""
    var searchNameWithNoRcord:String = ""
    var itemCount:Int {
        
        if objDumpFeed.screenApiType == .category  {
            
            return objDumpFeed.numberOfRowsForCatagory()
        }else{
            
            return objDumpFeed.numberOfRows()
        }
        
        
    }
    var searchStringValue:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 1.0, animations: {
            self.heightConstraintView.constant = 0
            self.lineLBL.isHidden = true
        })
        
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        pageNumber = 0
        totalRecords = 0
       
        if !searchStringValue.isEmpty
        {
            objDumpFeed.screenApiType = .search
             getDumpFeed()
        }
        else
        {
            objDumpFeed.screenApiType = .history
            objDumpFeed.serviceType = .none
             getDumpFeed()
        }
       
    }
    
    //MARK:- getDumpFeed
    fileprivate func getDumpFeed(){
        
//        self.view.isUserInteractionEnabled = false
        objDumpFeed.getDumpFeed(search: searchStringValue,onSuccess: { (records) in
            DispatchQueue.main.async{
//                  self.view.isUserInteractionEnabled = true
                if self.objDumpFeed.serviceType == .pagging{
                    if let cell = self.loadmoreCell, cell.isShowLoader == true{
                        cell.isShowLoader = false
                    }
                }
                
                print(self.itemCount)
                self.tableView.reloadData()
            }
        }, onFailure: {
            if self.objDumpFeed.serviceType == .pagging {
                if pageNumber>0{
                    pageNumber -= 1
                }
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
                
            }
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kDumpTopicSeque{
            //            let controller = segue.destination as! DumpCreateVC
            //            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            //            self.searchBar.text = searchBar.text ?? ""
            //            controller.objDumpFeed = self.objDumpFeed
            //            controller.objDumpFeed?.set(at: selectedIndexPath, searchString: self.searchBar.text ?? "")
            //            self.searchBar.text = ""
            let controller = segue.destination as! DumpCreateVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            controller.objDumpVM = self.objDumpFeed
            controller.topicCreate = false
            controller.objDumpVM?.setTopic(at: selectedIndexPath)
        }
        
        if segue.identifier == "categorySegue"
        {
            let controller = segue.destination as! CreateDumpeeVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            controller.objDumpFeed = self.objDumpFeed
            controller.category = self.objDumpFeed.categoryList[selectedIndexPath.row].categoryName ?? ""
            controller.catId = self.objDumpFeed.categoryList[selectedIndexPath.row].categoryId ?? ""
            controller.name = self.searchBar.text ?? ""
            
        }
        
    }
}
extension DumpVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
        
        if objDumpFeed.screenApiType == .category {
            
            return itemCount
        }else{
            
            return objDumpFeed.serviceType == .pagging ? itemCount+1 :itemCount
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("-------------   ---------  \(indexPath.row)\(itemCount)")
        
        if objDumpFeed.screenApiType == .category {
            let noDataRecordCell = tableView.dequeueCell(reuseIdentifier: TVCellIdentifier.kDumpListEmptyCell, for: indexPath) as! DumpListEmptyCell
            noDataRecordCell.suggestionListModel = objDumpFeed.cellForRowAtCategory(at: indexPath)
            return noDataRecordCell
        }else{
            
            if  indexPath.row < itemCount {
                
                
                let dumpListCell =  tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kDumpListCell  , for: indexPath) as! DumpListCell
                dumpListCell.dumpListModel = objDumpFeed.cellForRowAt(at: indexPath)
                return dumpListCell
                
                
            }else{
                loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
                if  !ServerManager.shared.CheckNetwork(){
                    objDumpFeed.serviceType = .none
                    loadmoreCell.isShowLoader = false
                    return loadmoreCell
                }
                if self.itemCount<totalRecords
                {
                    objDumpFeed.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5, execute: {
                        
                        if let searchKeyword  = self.searchBar.text
                        {
                            self.searchStringValue = searchKeyword
                            self.getDumpFeed()
                        }else
                        {
                            self.searchStringValue = ""
                            self.getDumpFeed()
                        }
                        
                    })
                    
                }else{
                    objDumpFeed.serviceType = .none
                    loadmoreCell.isShowLoader = false
                    
                }
                
                return loadmoreCell
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if objDumpFeed.screenApiType == .category {
            let cell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kDumpListEmptyCell) as! DumpListEmptyCell
            let label = UILabel()//UILabel(frame: CGRect(x: 20, y: 20, width: 50, height: 50))
            label.text = "TEST TEXT"
            label.textColor = .black
            cell.contentView.addSubview(label)
            return cell
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if objDumpFeed.screenApiType == .category {
            return 60
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if objDumpFeed.screenApiType == .category {
            
        }
        else{
            let obj = objDumpFeed.cellForRowAt(at: indexPath)
            if let jsonObject =  obj.jsonObject as? [String:Any]{
                
                let searchDumpeeId:String = obj.dumpeeId ?? ""
                let searchName:String = (self.searchBar.text?.isEmpty == true) ? (obj.topicName ?? "") : (self.searchBar.text ?? "")
                
                objDumpFeed.saveDumpeeHistory(searchUserId:"" , searchDumpeeId: searchDumpeeId, searchName: obj.topicName ?? "", historyType: "dumpee", otherInfo: jsonObject, progressHud: true, onSuccess: {
                    
                    
                })
            }
            
        }
        
    }
    
}

extension DumpVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//    guard let searchText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
        self.searchStringValue = searchText
        
        if pageNumber>0
        {
            pageNumber = 0
            totalRecords = 0
        }
        if !self.searchStringValue.isEmpty{

            objDumpFeed.screenApiType = .search
            objDumpFeed.serviceType = .none

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                print("--------Searchif -----\(pageNumber)")
                ServerManager.shared.cancelledRequest(isAll: true)
                self.getDumpFeed()
            })
        }
        else{
            objDumpFeed.serviceType = .none
            objDumpFeed.screenApiType = .history
            self.searchStringValue = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                print("--------Search -----\(pageNumber)")
                pageNumber = 0
                ServerManager.shared.cancelledRequest(isAll: true)
                searchBar.resignFirstResponder()
                self.searchBar.endEditing(true)
                self.getDumpFeed()
            })
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
 
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        if pageNumber>0
        {
            
            pageNumber = 0
            totalRecords = 0
        }
        print("--------CancelButtonClicked -----\(pageNumber)")
        searchBar.resignFirstResponder()
        objDumpFeed.removeDumpList()
        objDumpFeed.serviceType = .none
        objDumpFeed.screenApiType = .history
        searchStringValue = ""
        searchBar.text = ""
        print(pageNumber)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
            ServerManager.shared.cancelledRequest(isAll: true)
            searchBar.resignFirstResponder()
            self.getDumpFeed()
            
        })
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("--------did end editing -----\(pageNumber)")
        
    }
    
    
}
