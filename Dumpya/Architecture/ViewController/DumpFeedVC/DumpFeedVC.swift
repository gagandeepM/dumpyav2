
protocol SwiftyTableViewCellDelegate : class {
    func swiftyTableViewCellDidTapHeart(_ sender: FeedMediaCell,hashName:String)
    func clickHashTextCell(_ sender:FeedTextCell,hashName:String)
}

import UIKit
import ExpandableLabel
import CoreLocation
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
class DumpFeedVC: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak fileprivate var noFeedImgView: UIImageView!
    @IBOutlet weak fileprivate var noDumpFoundLBL: UILabel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var objDumpFeed: DumpTimeLineViewModel!
    fileprivate  var type:String = ""
    fileprivate var loadmoreCell:LoadMoreCell!
    fileprivate var locationInfo:String = ""
    fileprivate var countryName:String = ""
    fileprivate var stateName:String = ""
    fileprivate var cityName:String = ""
    fileprivate var latitude:Double =  0
    fileprivate var longitude:Double = 0
    fileprivate var locationManager: CLLocationManager!
    fileprivate var locationCoordinates:CLLocationCoordinate2D?
    var itemCount:Int {return objDumpFeed.numberOfRows()}
    lazy var noDataLabel:UILabel = {
        
        let label    = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        label.text          = "exploreDumpya".localized
        label.textColor     = UIColor.black
        label.textAlignment = .center
        
        return label
    }()
    
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        
        return refresh
    }()
    let incomingURL = "https://dumpya.page.link/v5CYt9jiMiZKhg7MA"
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        self.deeplinking()
        locationManager = CLLocationManager()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //getAddressFromLatLon(pdblLatitude: self.locationManager.location?.coordinate.latitude ?? 0.0, withLongitude: self.locationManager.location?.coordinate.longitude ?? 0.0)
        subscribeTopic = "Dumpya_\(userModel?.userId ?? "")"
        kAppDelegate.fireBasesubscribeToTopic(subscribeToTopic: true)
        self.tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(self.refreshFeed),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
         tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
        
        
        //MK moved from viewWillAppear to here, not to reload data when coming back from dumpee detail
        if totalRecords > 0
        {
            objDumpFeed.removeObjects()
        }
        
        pageNumber = 0
        totalRecords = 0
        objDumpFeed.serviceType = .none
        loadTimeLineData()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
         //MK refresh view if dump editted/saved
        if(isDumpySaved)
        {
            isDumpySaved = false
            if totalRecords > 0
            {
                objDumpFeed.removeObjects()
            }
            
            pageNumber = 0
            totalRecords = 0
            objDumpFeed.serviceType = .none
            loadTimeLineData()
        }
        
        
     }
    
    @IBAction func noDataFountBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "SearchBar", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func rightSideBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DumpVC") as? DumpVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //MARK: Change location change
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.locationCoordinates = locValue
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        getAddressFromLatLon(pdblLatitude: self.locationCoordinates?.latitude ?? 0.0, withLongitude: self.locationCoordinates?.longitude ?? 0.0)
        locationManager.stopUpdatingLocation()
    }
    func validateLocation()
    {
        
        if  self.countryName != getUserCurrentLocation()
        {
           
           
            let alert = UIAlertController(title: "roaming".localized, message: "movedTo".localized + " \(self.countryName), " + "serveBetter".localized + " \(self.countryName)? " + "updateLeaderboard".localized + " \(self.countryName)'s " + "leaderboardDumps".localized + " \(self.countryName) " +  "leaderboardLocation".localized, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "YES".localized, style: .default , handler:{ (UIAlertAction)in
                
                self.objDumpFeed.checkCurrentLocation(locationName: self.locationInfo, countryName: self.countryName, onSuccess: {
                    print("Location update")
                    
                }, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO".localized, style: .default , handler:{ (UIAlertAction)in
                
                self.showAlert(message: "Logout and sign back in if you change your mind")
                self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
        else
        {
            print("same location ")
            return
        }
        
    }
    
    //MARK:- Get user current location
    
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                guard let pm = placemarks else{return}
                if pm.count > 0 {
                    let pm = placemarks![0]
                    self.countryName = pm.country ?? ""
                    
                    if let country = pm.country,let state = pm.administrativeArea,let city = pm.locality
                    {
                        self.locationInfo = country.isUnitedStates ? "\(city), \(state)" : "\(city), \(country)"
                        locationNameLocation = self.locationInfo
                        countryNameFromLocation = country
                        self.validateLocation()
                    }
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                }
        })
    }
    
    @objc fileprivate func refreshFeed() {
        if !ServerManager.shared.CheckNetwork(){
            refreshControl.endRefreshing()
            return
        }
        if totalRecords > 0
        {
            objDumpFeed.removeObjects()
        }
        pageNumber = 0
        totalRecords = 0
        objDumpFeed.serviceType = .none
        loadTimeLineData()
            
        
    }
    
    var isRefreshFeed:Bool = false{
        didSet{
            if isRefreshFeed {
                self.loadTimeLineData()
            }
        }
    }
    
    fileprivate func loadTimeLineData() {
        
        objDumpFeed.getTimeLineDump(refreshControl:self.refreshControl,onSuccess: { (records) in
            DispatchQueue.main.async {
                
                totalRecords = records
                if self.objDumpFeed.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                
                
                
                if self.itemCount>0{
                    self.tableView.backgroundView = nil
                    
                }else{
                    self.tableView.backgroundView  = self.noDataLabel
                }
                self.tableView.reloadData()
            }
            
        }, onFailure: {
            DispatchQueue.main.async {
                
                if self.objDumpFeed.serviceType == .pagging {
                    if pageNumber>0{
                        pageNumber -= 1
                    }
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                    
                }
            }
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let sender = sender as? UIButton else{return}
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.userId)
        }
        else if segue.identifier ==  SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let sender = sender as? UIButton else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            objDumpeeDetail.objDumpeeDetail.set(userId: dumpInfo.userId, dumpeeId: dumpInfo.dumpeeId, dumpeeName: dumpInfo.dumpeeName)
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo.dumpeeId)
            
        }
        else if SegueIdentity.kCommentSegue == segue.identifier{
            guard let commentBtn = sender as? UIButton else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpFeed.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
        }else if segue.identifier == SegueIdentity.kCommentViewSegue{
            guard let commentBtn = sender as? DMCardView else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpFeed.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let sender = sender as? Any else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: (sender as AnyObject).tag)
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }
            
        else if segue.identifier == SegueIdentity.kCommentedUserViewListSegue{
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
            guard let sender = sender as? DMCardView else{return}
            
            let dumpInfo = objDumpFeed.dumpTimeLineInfo(at: sender.tag)
            objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
        }
        else if segue.identifier == SegueIdentity.kNewsDetailSegue{
            let objNewsDetailVC : NewsDetailVC = segue.destination as! NewsDetailVC
            guard let sender = sender as? UIButton else{return}
            objNewsDetailVC.newsUrl =  objDumpFeed.dumpTimeLineInfo(at: sender.tag).newsUrl
        }
    }
    
    
    @IBAction func onClickGradientRedump(_ sender: UIButton) {
        
        let index = sender.tag
        let data = self.objDumpFeed.redumpUpdate(indexPath: index)
        let redump = (data.redumpCount ?? 0) + 1
        let undump = (data.redumpCount ?? 0) - 1
        if data.isRedump!
        {
               self.objDumpFeed.didUpdateRedump(at: index, isRedump: false,redumpCount:undump)
        }
        else{
               self.objDumpFeed.didUpdateRedump(at: index, isRedump: true,redumpCount:redump)
        }
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        DispatchQueue.global(qos: .userInitiated).async {
            
            self.objDumpFeed.redumpUser(dumpFeedId: self.objDumpFeed.dumpTimeLineInfo(at: index).dumpFeedId,countryName: countryNameFromLocation ,locationName:locationNameLocation ,onSuccess: { (isRedumpedByMe,redumpCount) in
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
//        self.objDumpFeed.redumpUser(dumpFeedId: self.objDumpFeed.dumpTimeLineInfo(at: index).dumpFeedId,countryName: countryNameFromLocation ,locationName:locationNameLocation ,onSuccess: { (isRedumpedByMe,redumpCount) in
//
//            self.objDumpFeed.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
//            let indexPath = IndexPath(item: index, section: 0)
//            UIView.performWithoutAnimation {
//                self.tableView.reloadRows(at: [indexPath], with: .none)
//            }
//
//        }, onFailure: {
//
//        }, onUserSuspend: { (message) in
//
//            self.showAlert(message: message, completion: { (index) in
//                AppDelegate.sharedDelegate.logoutUser()
//            })
//        })
    }
    
    
    @IBAction func onClickMediaText(_ sender: UIButton) {
        performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
    }
    
    @IBAction func onClickDumpeeName(_ sender: UIButton) {
        
        let index = sender.tag
        let obj = self.objDumpFeed.dumpTimeLineInfo(at: index)
        if obj.source == "news"{
            performSegue(withIdentifier: SegueIdentity.kNewsDetailSegue, sender: sender)
        }else{
            performSegue(withIdentifier: SegueIdentity.kDumpeeDetailSegue, sender: sender)
        }
        
        
    }
    
   
    @IBAction func onClickDots(_ sender: UIButton) {
        let index = sender.tag
        let obj = self.objDumpFeed.dumpTimeLineInfo(at: index)
        dumpSetting(dumpId: obj.dumpFeedId,userID:obj.userId,source:obj.source,index: index,dumpeeName: obj.dumpeeName,media: obj.media,content: obj.content)
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    @IBAction func onClickCommentPromotionalText(_ sender: DMCardView) {
        performSegue(withIdentifier:SegueIdentity.kCommentedUserViewListSegue, sender: sender)
    }
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        let index = sender.tag
        objDumpFeed.didUpdateReadMore(at: index, states:false)
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
     }
    
     @IBAction fileprivate func onClickFollowingDetail(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    
    @IBAction func onClickCommentCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentSegue , sender: sender)
        
    }
    
    @IBAction func onClickComment(_ sender: DMCardView) {
        performSegue(withIdentifier: SegueIdentity.kCommentViewSegue , sender: sender)
    }
    
    
    @IBAction fileprivate func  onClickRedumpCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    fileprivate  func dumpSetting(dumpId:String,userID:String,source:String,index:Int,dumpeeName:String = "",media:String,content:String)  {
        
        if userID == userModel?.userId ?? ""{
            
            if source == "news"{
                var actions :[AlertActionModel] = [
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                            
                            self.objDumpFeed.removeObjects()
                            self.loadTimeLineData()
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }else if indexofSheet == 3{
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                    }else if indexofSheet == 4{
                        //copylink
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
                
                
            }else{
                
               var actions :[AlertActionModel] = [
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        //Edit
                        
                        let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                        dumpCreateVC?.isEdit = true
                        
                        guard let dumpeeInfo = self.objDumpFeed?.dumpeeInfo(at:index) else{return}
                        
                        dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                        dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                        dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                        dumpCreateVC?.isShowLocation = dumpeeInfo.isShowLocation
                        let dumpeeType = dumpeeInfo.type
                        dumpCreateVC?.topicName = dumpeeInfo.topicName
                        let size  = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                        let content = dumpeeInfo.content
                        let media = dumpeeInfo.media
                        let videoThumbnail = dumpeeInfo.videoThumbnail
                        dumpCreateVC?.locationName = dumpeeInfo.locationName
                        switch dumpeeType{
                        case .image:
                            dumpCreateVC?.params.type = .image(item: media, size: size)
                        case .textImage:
                            dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                        case .video:
                            dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                        case .textVideo:
                            dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                        case .gradient:
                            let bgCode  = dumpeeInfo.bgCode
                            var gradientColor:GradientColor = .red
                            if GradientColor.red.name == bgCode{
                                gradientColor = .red
                            }else if GradientColor.green.name == bgCode{
                                gradientColor = .green
                            }else if GradientColor.purple.name == bgCode{
                                gradientColor = .purple
                            }else if GradientColor.yellow.name == bgCode{
                                gradientColor = .yellow
                            }else if GradientColor.darkRed.name == bgCode{
                                gradientColor = .darkRed
                            }
                            dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                        case .text:
                            dumpCreateVC?.params.type = .text(text: content)
                        }
                        
                        
                        self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                        
                    }else if indexofSheet == 3{
                        //Delete
                        
                        
                        self.objDumpFeed.deleteDump(dumpFeedId: dumpId, onSuccess: {
                            self.objDumpFeed.removeObjects()
                            self.loadTimeLineData()
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })

                    }else if indexofSheet == 4{
                        //Share
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media, viewController: self,copyLink: false)
                    }else if indexofSheet == 5{
                        //Copy
                         JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
            }
        }
            
        else{
            
          var actions :[AlertActionModel] = [
            
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
            
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Report
                   self.objDumpFeed.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                    
                }else if indexofSheet == 3{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                }else if indexofSheet == 4{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            }
        }
    }
}

extension DumpFeedVC:UITableViewDataSource,UITableViewDelegate,SwiftyTableViewCellDelegate,ExpandableLabelDelegate{
    
    func clickHashTextCell(_ sender: FeedTextCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath.row)
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        
        
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func swiftyTableViewCellDidTapHeart(_ sender: FeedMediaCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath)
        
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        vc?.hashTagName = hashName
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objDumpFeed.serviceType == .pagging ? itemCount+1 :itemCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row<itemCount {
            let obj  = objDumpFeed.cellForRowAt(at:indexPath)
            let feedType =  obj.feedType
            switch feedType{
            case .textImage,.textVideo:
                let feedMediaCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaCell , for: indexPath) as! FeedMediaCell
                feedMediaCell.rowIndex = indexPath.row
                feedMediaCell.delegateFeedMedia = self
                feedMediaCell.objFeedTimeLine = obj
                return feedMediaCell
            case .image,.video:
                let  feedMediaImageCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaImageCell , for: indexPath) as!  FeedMediaImageCell
                feedMediaImageCell.objImage = obj
                feedMediaImageCell.rowIndex = indexPath.row
                return feedMediaImageCell
            case .text:
                let feedTextCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedTextCell , for: indexPath) as! FeedTextCell
                feedTextCell.delegateFeedText = self
                feedTextCell.rowIndex = indexPath.row
                feedTextCell.objText = obj
                return feedTextCell
            case .gradient:
                let cell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedGradientCell , for: indexPath) as! FeedGradientCell
                cell.rowIndex = indexPath.row
                cell.objGradient = obj
                return cell
            }
           
        }else{
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(),!self.refreshControl.isRefreshing{
                if self.itemCount<totalRecords {
                    objDumpFeed.serviceType = .pagging
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        self.loadTimeLineData()
                    }
                }else{
                    objDumpFeed.serviceType = .none
                }
            }else{
                objDumpFeed.serviceType = .none
            }
            
            return loadmoreCell
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row<itemCount {
            let obj  = objDumpFeed.cellForRowAt(at:indexPath)
            let feedType =  obj.feedType
            switch feedType {
            case .gradient:
                return 322.0
            case .textImage,.textVideo:
                return 369.0
            case .image,.video:
                return 320.0
            case .text:
                return  180.0
            }
        }else{
            return 50
            
        }
        
    }
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if let temp = cell as? FeedMediaImageCell {
    //            if !tableView.isDecelerating{
    //                temp.didUpdateImage = {
    //                    tableView.beginUpdates()
    //                    tableView.reloadRows(at: [indexPath], with: .fade)
    //                    tableView.endUpdates()
    //                }
    //            }
    //        }else if let temp  = cell as? FeedMediaCell{
    //            if !tableView.isDecelerating{
    //                temp.didUpdateImage = {
    //                    tableView.beginUpdates()
    //                    tableView.reloadRows(at: [indexPath], with: .fade)
    //                    tableView.endUpdates()
    //                }
    //            }
    //        }
    //
    //    }
    //    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    ////        if let temp = cell as? FeedMediaImageCell {
    ////            temp.cancelImageRequest()
    ////        }else if let temp  = cell as? FeedMediaCell{
    ////            temp.cancelImageRequest()
    ////        }
    //    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        let offset = scrollView.contentOffset.y
        //
        //        if scrollView.contentOffset.y < 0 {
        //            topConstraint.constant = -offset/2
        //        } else {
        //            topConstraint.constant = 0
        //        }
    }
    
    // MARK:- ExpandableLabel Delegate
    
    func willExpandLabel(_ label: ExpandableLabel) {
        
        tableView.beginUpdates()
        
    }
    
    
    
    func didExpandLabel(_ label: ExpandableLabel) {
        
        let point = label.convert(CGPoint.zero, to: tableView)
        
        
        
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        tableView.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableView.beginUpdates()
    }
    
    
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        
        let point = label.convert(CGPoint.zero, to: tableView)
        
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            
            
            objDumpFeed.didUpdateReadMore(at: indexPath.row, states:true)
            DispatchQueue.main.async { [weak self] in
                
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                
            }
            
        }
        
        tableView.endUpdates()
        
    }
}

//extension DumpFeedVC{
//    
//    func deeplinking(){
//        
//        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
//            // ...
//            if let dynamicLink = dynamicLink, let _ = dynamicLink.url {
//                self.handleDynamicLink(dynamicLink)
//            } else {
//                // Check for errors
//                
//            }
//            
//        }
//    }
//    
//    func handleDynamicLink(_ dynamicLink: DynamicLink) {
//        
//        if dynamicLink.matchType == .weak{
//        }else {
//            
//            guard let dynamickLink = dynamicLink.url else{return}
//            let dumpId = self.getQueryStringParameter(url: "\(dynamickLink)", param: "dumpId") ?? ""
//            
//            }
//            
//            
//        }
//    
//    
//    func getQueryStringParameter(url: String, param: String) -> String? {
//        print(url)
//        guard let url1 = URLComponents(string: url) else { return nil }
//        return url1.queryItems?.first(where: { $0.name == param })?.value
//        
//    }
//}
