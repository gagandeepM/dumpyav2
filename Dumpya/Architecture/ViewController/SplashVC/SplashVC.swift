//
//  SplashVC.swift
//  Dumpya
//
//  Created by Chander on 19/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak fileprivate var logoImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration:1.5, delay:0, options: [.curveEaseIn], animations: {
            self.logoImgView.transform = CGAffineTransform(translationX: 0, y: -(self.view.frame.height / 2 - self.logoImgView.frame.height/2))
            
        }, completion: { (finished:Bool) -> Void in
           
            let isProfileUpdate = userModel?.isProfileUpdate ?? false
            if isProfileUpdate{
                AppDelegate.sharedDelegate.showMainController()
            }else{
            Thread.sleep(forTimeInterval: 1.0)
            let loginController = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController")
             loginController.modalTransitionStyle = .crossDissolve
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
          
            UIView.transition(from: (appDelegate.window?.rootViewController!.view)!, to: loginController.view, duration: 5.0, options: [.transitionCrossDissolve], completion: {
                _ in
                appDelegate.window?.rootViewController = loginController
            })
            }
            
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
