//
//  WalkThroughVC.swift
//  Dumpya
//
//  Created by Chander on 14/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class WalkThroughVC: UIViewController {

    @IBOutlet weak fileprivate var pageControl: UIPageControl!
    @IBOutlet weak fileprivate var btnSkip: UIButton!
    
   fileprivate var logoImage: [UIImage] = [
        UIImage(named: "Tutorial.png")!,
        UIImage(named: "Tutorial2.png")!, UIImage(named: "Tutorial3.png")!
       
        
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func onClickSkip(_ sender: UIButton) {
        UserDefaults.DMDefault(setBool:true, forKey: kWalkThroughtSkip)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension WalkThroughVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return logoImage.count
   }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let walkThroughCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kWalkThroughCell, for: indexPath) as! WalkThroughCell
        walkThroughCell.walkThroughImgView.image = logoImage[indexPath.row]
        return walkThroughCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if let collectionView = scrollView as? UICollectionView,
            let section = collectionView.indexPathForItem(at: targetContentOffset.pointee)?.row {
            print("THE VALUE OF THE SECTION IS------>",section)
            btnSkip.isSelected = section == 2 ? true:false
            self.pageControl.currentPage = section
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}
