//
//  LoginVC.swift
//  Dumpya
//
//  Created by Chander on 13/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import FBSDKCoreKit
import CoreLocation
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

class LoginVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak fileprivate  var emailTF:DMTextField!
    @IBOutlet weak fileprivate  var passwordTF:DMTextField!
    @IBOutlet weak fileprivate var centerConstraint_Password: NSLayoutConstraint!
    @IBOutlet weak fileprivate var centerConstraint_UserName: NSLayoutConstraint!
    @IBOutlet fileprivate var loginViewModel: DMUserViewModel!
    
    @IBOutlet weak var eyeBtn: UIButton!
    //MARK:- Properties
    fileprivate var userLoginType:Int?
    let facebookLogon = FacebookLogin()
    fileprivate var iconClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
//        if UserDefaults.DMDefault(boolForKey: kWalkThroughtSkip) == false{
//            let initialVC = walkThroughStoryboard.instantiateViewController(withIdentifier: "WalkThroughVC")
//            self.navigationController?.present(initialVC,animated: false)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- ACTIONS
    
    @IBAction  func onClickSignIn(_ sender: DMButton) {
        self.view.endEditing(true)
        guard let email = emailTF.text,let password = passwordTF.text else {
            return
        }
      
        loginViewModel.login(email:email,password:password, onSuceess: {
           DispatchQueue.main.async {
                guard let isVerified = userModel?.isVerified else{return}
                guard let isProfileUpdate = userModel?.isProfileUpdate else{return}
                if isVerified{
                    if isProfileUpdate{
                     AppDelegate.sharedDelegate.showMainController()
                    }else{
                         self.performSegue(withIdentifier:SegueIdentity.kProfileSegue, sender: sender)
                    }
                }else{
                   self.performSegue(withIdentifier:SegueIdentity.kOTPSegue, sender: sender)
                }
        }
        }, onFailure:{
            print("API Failure")
        })
    }
    @IBAction  func onClickFacebook(_ sender: UIButton) {
        facebookLogon.facebookLogin(withController:self) { (success,user) in
            self.loginViewModel.loginWithFacebook(firstName: user.firstName ?? "", lastName: user.lastName ?? "",email: user.email ?? "", socialId: user.id ?? "", profilePic: user.profilePic ?? "", onSuccess: {
                DispatchQueue.main.async {
                    
                    
                    guard let isVerified = userModel?.isVerified else{return}
                    guard let isProfileUpdate = userModel?.isProfileUpdate else{return}
                    if isVerified{
                        if isProfileUpdate{
                            AppDelegate.sharedDelegate.showMainController()
                        }else{
                            self.performSegue(withIdentifier:SegueIdentity.kProfileSegue, sender: sender)
                        }
                    }
                    
                    
                 
                }
            }, onFailure: {
                
            })
          }
     }
    
    @IBAction  func onClickGoogle(_ sender: UIButton) {
        JKGoogleManager.shared.GIDSignInUI(from: self, onSuccess: { (user) in
            
            var firstName:String = ""
            var lastName:String = ""
            
            let url = user.name
            let whiteSpace = " "
            if let hasWhiteSpace = url?.contains(whiteSpace) {
            var fullNameArr =  url?.components(separatedBy: " ")
                firstName = fullNameArr?[0] ?? ""
                lastName  = fullNameArr?[1] ?? ""
                print ("has whitespace")
            } else {
                firstName = url ?? ""
                lastName = ""
            }
            self.loginViewModel.loginWithGmail(firstName: firstName,lastName:lastName, userName: user.givenName ?? "", email: user.email ?? "", socialId: user.userGmailID ?? "", profilePic: user.hasImage ?? "", onSuccess: {
                
                guard let isVerified = userModel?.isVerified else{return}
                guard let isProfileUpdate = userModel?.isProfileUpdate else{return}
                if isVerified{
                    if isProfileUpdate{
                        AppDelegate.sharedDelegate.showMainController()
                    }else{
                        self.performSegue(withIdentifier:SegueIdentity.kProfileSegue, sender: sender)
                    }
                }
                
              
            }, onFailure: {
                
            })
            print("Sucessssss")
        }, onCancel: { (isCancelled) in
            
        }, onFailure: { (error) in
            
        })
      }
    
    
    @IBAction func onClickEye(_ sender: Any) {
        
        if(iconClick == true) {
            passwordTF.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(named:"icon_eyeWhite"), for: .normal)
            
        } else {
           eyeBtn.setImage(UIImage(named:"icon_EyeWhiteClosed"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

