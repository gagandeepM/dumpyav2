//
//  LanguageVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/03/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit
import UserNotifications
class LanguageVC: UIViewController {
    fileprivate var index:Int = 0
    @IBOutlet var objSettingViewModel: SettingsViewModel!
    @IBOutlet weak var tableView:UITableView!
    var languages:[Language] = [.english,.french]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
       
       
        // Do any additional setup after loading the view.
    }
 
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LanguageVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLanguageCell, for: indexPath) as! LanguageCell
        let lang  =  languages[indexPath.row]
        cell.languageSelectionImgView.isHidden =  Language.language == lang ? false : true
        cell.languageLbl.text = languages[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.reloadData()
                let language = self.languages[indexPath.row]
        
                ChangeLanguageAlert.showAction(language: language, onCompletion: {
                    
                    self.objSettingViewModel.changeLanguage(languageKey: language.rawValue, onSuccess: {
                        LocalNotification.sent(message:
                            "\(kAppTitle) " + "thisPhone".localized + " \(language.name). " + "tapToOpen".localized
                            
                        )
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            exit(0)
                            
                        }
                    }, onFailure: {
                        
                    })
                    
                    
                })
               
        
        
    }
 
}


