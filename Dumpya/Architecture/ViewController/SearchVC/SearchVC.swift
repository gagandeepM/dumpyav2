//
//  SearchVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 31/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

protocol HashTagDelegateSearch : class {
    func hashTagMediaCell(_ sender: FeedMediaCell,hashName:String)
    func hashTagTextCell(_ sender:FeedTextCell,hashName:String)
}
import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
class SearchVC: UIViewController {
    @IBOutlet weak fileprivate var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var objDumpee: DMFeedViewModel!
    @IBOutlet var objDumpeeSeacrhModel: DumpTimeLineViewModel!
    @IBOutlet var objSearchModel: SearchViewModel!
    fileprivate var loadmoreCell:LoadMoreCell!
    var itemCount:Int {
        
        if optionTypeSelect == .user{
            return objSearchModel.numberOfRows()
            
        }
        else if optionTypeSelect == .dumpee{
            return objDumpee.numberOfRows()
            
        }
        else if optionTypeSelect == .tags{
            return objSearchModel.numberOfRowsForTag()
        }
        else {
            return objDumpeeSeacrhModel.numberOfRows()
        }
        
        
    }
    
    enum DMSegmentedTab:Int{
        case dumps  = 0
        case tags   = 1
        case user   = 2
        case dumpee = 3
    }
    
    
    @IBOutlet  fileprivate var tableView: UITableView!
    var type:String = ""
    var searchString:String = ""
    var optionTypeSelect:DMSegmentedTab = .user
    fileprivate var searchType = ["DUMPS","tags".localized,"users".localized,"DUMPEE"]
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registeredTableProtocol()
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
        tableView.addSubview(refreshControl)
        
        refreshControl.addTarget(self, action:
            #selector(self.refreshDetail),
                                 for:.valueChanged)
        
        refreshControl.layoutIfNeeded()
        
    }
    
    
    fileprivate func registeredTableProtocol()
    {
        if tableView.dataSource == nil
        {
            self.tableView.dataSource = self
        }
        if tableView.delegate == nil{
            self.tableView.delegate = self
        }
    }
    @objc fileprivate func refreshDetail() {
        
        if !ServerManager.shared.CheckNetwork(){
            refreshControl.endRefreshing()
            return
        }
        
        pageNumber = 0
        totalRecords = 0
        objDumpeeSeacrhModel.serviceType = .none
        
        
        loadDataAccordingToSelection()
        
        
    }
    func loadDataAccordingToSelection()
    {
        switch optionTypeSelect {
        case .dumps:
            loadTimeLineData()
        case .tags:
            loadTags()
        case .user:
            loadUsers()
        case .dumpee:
            loadDumpee()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)//isFromSearchFormDumpeeDetail
        //
        //        NotificationCenter.default.addObserver(self, selector: #selector(refreshUser), name:Notification.Name("isFromSearch"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(refreshDumpee), name:Notification.Name("isFromSearchFormDumpeeDetail"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(refreshTags), name:Notification.Name("isFromTags"), object: nil)
        
        //        collectionView.reloadData()
        
        if self.itemCount>0 {
            
            self.tableView.reloadData()
        }else{
            pageNumber = 0
            totalRecords = 0
            objDumpeeSeacrhModel.serviceType = .none
        }
        loadDataAccordingToSelection()
    }
    //MARK: selection in serach
    //    @objc  func refreshUser(_ notification: Notification) {
    //        self.optionTypeSelect = .user
    //    }
    //    @objc  func refreshDumpee(_ notification: Notification) {
    //        self.optionTypeSelect = .dumpee
    //    }
    //    @objc  func refreshTags(_ notification: Notification) {
    //        self.optionTypeSelect = .tags
    //    }
    //    override func didReceiveMemoryWarning() {
    //        super.didReceiveMemoryWarning()
    //        // Dispose of any resources that can be recreated.
    //    }
    //    deinit {
    //        NotificationCenter.default.removeObserver(self)
    //    }
    
    fileprivate func loadTimeLineData() {
        
        
        objDumpeeSeacrhModel.getTimeLineDump(refreshControl:self.refreshControl, page: pageNumber, search: "",searchType: "global",onSuccess: { (records) in
            DispatchQueue.main.async {
                
                totalRecords = records
                if self.objDumpeeSeacrhModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                
                self.optionTypeSelect = .dumps
                self.tableView.reloadData()
                
            }
        }, onFailure: {
            DispatchQueue.main.async {
                
                if self.objDumpeeSeacrhModel.serviceType == .pagging {
                    if pageNumber>0{
                        pageNumber -= 1
                    }
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                    
                }
            }
            
        })
        { (message) in
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        }
        
    }
    
    func  loadUsers()
    {
        
        
        
        objSearchModel.getSearchUsers(onSuccess: {(records) in
            
            DispatchQueue.main.async {
                
                totalRecords = records
                if self.objSearchModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                self.optionTypeSelect = .user
                self.searchBar.text = ""
                self.searchBar.resignFirstResponder()
                self.tableView.reloadData()
                
            }
        }, onFailure: {
            print("THE API NOT SUCEESFULL HIT")
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    @IBAction func onClickUsers(_ sender: Any) {
        
        
        //        objSearchModel.removeObjects()
        //        pageNumber = 0
        //        totalRecords = 0
        //        objDumpeeSeacrhModel.serviceType = .none
        //        loadUsers()
        
    }
    
    @IBAction func onClickTags(_ sender: Any) {
        
        //
        //        objSearchModel.removeObjects()
        //        pageNumber = 0
        //        totalRecords = 0
        //        objDumpeeSeacrhModel.serviceType = .none
        //        loadTags()
    }
    func loadTags()
    {
        objSearchModel.getSearchTags(refreshControl:self.refreshControl,search:"",onSuccess: {(records) in
            
            DispatchQueue.main.async {
                totalRecords = records
                if self.objSearchModel.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                self.optionTypeSelect = .tags
                self.searchBar.text = ""
                self.searchBar.resignFirstResponder()
                self.tableView.reloadData()
            }
        }, onFailure: {
            print("THE API NOT SUCEESFULL HIT")
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickDumpee(_ sender: Any) {
        
        
        //
        //        objDumpee.removeObjects()
        //        pageNumber = 0
        //        totalRecords = 0
        //        objDumpeeSeacrhModel.serviceType = .none
        //        loadDumpee()
        
    }
    
    func loadDumpee()
    {
        self.registeredTableProtocol()
        objDumpee.getDumpFeed(refreshControl:self.refreshControl,search:"",  onSuccess: {(records) in
            
            DispatchQueue.main.async {
                if self.objDumpee.serviceType == .pagging{
                    if let cell = self.loadmoreCell{
                        cell.isShowLoader = false
                    }
                }
                self.optionTypeSelect = .dumpee
                self.searchBar.text = ""
                self.searchBar.resignFirstResponder()
                self.tableView.reloadData()
                
            }
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    @IBAction func onClickDumps(_ sender: Any) {
        //
        //
        //         self.searchBar.text = ""
        //         self.searchBar.resignFirstResponder()
        //        pageNumber = 0
        //        totalRecords = 0
        //        objDumpeeSeacrhModel.serviceType = .none
        //        loadTimeLineData()
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        self.toggleLeft()
    }
    
    
    @IBAction func onClickGradientRedump(_ sender: UIButton) {
        if itemCount == 0 {
            return
        }
        let index = sender.tag
        
        self.objDumpeeSeacrhModel.redumpUser(dumpFeedId: self.objDumpeeSeacrhModel.dumpTimeLineInfo(at: index).dumpFeedId,countryName:countryNameFromLocation,locationName: locationNameLocation  ,onSuccess: { (isRedumpedByMe,redumpCount) in
            self.objDumpeeSeacrhModel.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
            let indexPath = IndexPath(item: index, section: 0)
            print(indexPath)
            
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
            //            UIView.performWithoutAnimation {
            //                self.tableView.reloadRows(at: [indexPath], with: .none)
            //            }
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickDots(_ sender: UIButton) {
        
        let index = sender.tag
        let obj = self.objDumpeeSeacrhModel.dumpTimeLineInfo(at: index)
        
        dumpSetting(dumpId: obj.dumpFeedId,userID:obj.userId,source:obj.source,index: index,dumpeeName: obj.dumpeeName,media: obj.media,content: obj.content)
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        self.toggleLeft()
    }
    
    @IBAction func onClickCommentPromotionalText(_ sender: DMCardView) {
        performSegue(withIdentifier:SegueIdentity.kCommentedUserViewListSegue, sender: sender)
    }
    
    @IBAction func onClickComment(_ sender: DMCardView) {
        performSegue(withIdentifier: SegueIdentity.kCommentViewSegue , sender: sender)
    }
    
    @IBAction func onClickCommentCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentSegue , sender: sender)
        
    }
    
    
    
    @IBAction func onClickUserImage(_ sender: UIButton) {
        
        performSegue(withIdentifier: SegueIdentity.kProfileDetailSearchSegue, sender: sender)
    }
    
    
    @IBAction func onClickDumpeeName(_ sender: UIButton) {
        
        
        let index = sender.tag
        let obj = self.objDumpeeSeacrhModel.dumpTimeLineInfo(at: index)
        if obj.source == "news"{
            performSegue(withIdentifier: SegueIdentity.kNewsDetailSegue, sender: sender)
        }else{
            performSegue(withIdentifier: SegueIdentity.kDumpeeDetailSearchSegue, sender: sender)
        }
    }
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        
        let index = sender.tag
        objDumpeeSeacrhModel.didUpdateReadMore(at: index, states:false)
        
        
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        
    }
    
    @IBAction fileprivate func onClickFollowingDetail(_ sender: Any) {
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    
    @IBAction fileprivate func  onClickRedumpCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    @IBAction fileprivate func onClickCommentUserList(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentedUserListSegue , sender: sender)
        
    }
    fileprivate  func dumpSetting(dumpId:String,userID:String,source:String,index:Int,dumpeeName:String = "",media:String,content:String)   {
        
        if userID == userModel?.userId ?? ""{
            if source == "news"{
                var actions :[AlertActionModel] = [
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        self.objDumpeeSeacrhModel.deleteDump(dumpFeedId: dumpId, onSuccess: {
                            
                            self.objDumpeeSeacrhModel.remove(at: index)
                            self.tableView.reloadData()
                            // self.loadTimeLineData()
                            
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }else if indexofSheet == 3{
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                    }else if indexofSheet == 4{
                        //copylink
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
            }else{
                
                var actions :[AlertActionModel] = [
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                    AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                    
                ]
                self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                    if indexofSheet == 2{
                        //Edit
                        
                        let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                        dumpCreateVC?.isEdit = true
                        
                        guard let dumpeeInfo = self.objDumpeeSeacrhModel?.dumpeeInfo(at:index) else{return}
                        
                        dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                        dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                        dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                        dumpCreateVC?.isShowLocation = dumpeeInfo.isShowLocation
                        let dumpeeType = dumpeeInfo.type
                        dumpCreateVC?.topicName = dumpeeInfo.topicName
                        let content = dumpeeInfo.content
                        let media = dumpeeInfo.media
                        let videoThumbnail = dumpeeInfo.videoThumbnail
                        let size = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                        switch dumpeeType{
                        case .image:
                            dumpCreateVC?.params.type = .image(item: media, size: size)
                        case .textImage:
                            dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                        case .video:
                            dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                        case .textVideo:
                            dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                        case .gradient:
                            let bgCode  = dumpeeInfo.3
                            var gradientColor:GradientColor = .red
                            if GradientColor.red.name == bgCode{
                                gradientColor = .red
                            }else if GradientColor.green.name == bgCode{
                                gradientColor = .green
                            }else if GradientColor.purple.name == bgCode{
                                gradientColor = .purple
                            }else if GradientColor.yellow.name == bgCode{
                                gradientColor = .yellow
                            }else if GradientColor.darkRed.name == bgCode{
                                gradientColor = .darkRed
                            }
                            dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                        case .text:
                            dumpCreateVC?.params.type = .text(text: content)
                        }
                        
                        self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                        
                    }else if indexofSheet == 3{
                        //Delete
                        self.objDumpeeSeacrhModel.deleteDump(dumpFeedId: dumpId, onSuccess: {
                            
                            self.loadTimeLineData()
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
                        
                    }else if indexofSheet == 4{
                        //Share
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media, viewController: self,copyLink: false)
                    }else if indexofSheet == 5{
                        //Copy
                        JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                    }
                }
            }
        }
            
        else{
            
            
            var actions :[AlertActionModel] = [
                
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Report
                    self.objDumpeeSeacrhModel.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                    
                }else if indexofSheet == 3{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                }else if indexofSheet == 4{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            }
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            
            
            let searchUserId = objSearchModel.didSelectUser(index: selectedIndexPath.row)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: searchUserId)
        }else if segue.identifier ==  SegueIdentity.kDumpeeDetailSegue{
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let dumpeeInfo = objDumpee.searchDumpeeInfo(at: selectedIndexPath)
            if !dumpeeInfo.dumpeeId.isEmpty{
            objDumpeeDetail.objDumpeeDetail.set(userId: "", dumpeeId: dumpeeInfo.dumpeeId, dumpeeName: dumpeeInfo.dumpeeName)
            saveDumpeeHistory(dumpeeId:dumpeeInfo.dumpeeId,dumpeeName:dumpeeInfo.dumpeeName)
                objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpeeInfo.dumpeeId)}
            
        }else if segue.identifier == SegueIdentity.kCommentedUserListSegue{
            
            
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
            guard let sender = sender as? DMCardView else{return}
            
            let dumpInfo = objDumpeeSeacrhModel.dumpTimeLineInfo(at: sender.tag)
            objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
        }else if segue.identifier == SegueIdentity.kCommentViewSegue{
            guard let commentBtn = sender as? DMCardView else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpeeSeacrhModel.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
        }
            
            
        else if
            segue.identifier ==  SegueIdentity.kHashTagSegue{
            let objHashTag : HashTagVC = segue.destination as! HashTagVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            objHashTag.hashTagName = objSearchModel.didSelectTag(index: selectedIndexPath.row)
            
        }else if segue.identifier == SegueIdentity.kCommentSegue{
            guard let commentBtn = sender as? UIButton else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpeeSeacrhModel.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
        }else if segue.identifier == SegueIdentity.kProfileDetailSearchSegue{
            
            let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
            guard let sender = sender as? UIButton else{return}
            let dumpInfo = objDumpeeSeacrhModel.dumpTimeLineInfo(at: sender.tag)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.userId)
        }else if segue.identifier == SegueIdentity.kDumpeeDetailSearchSegue{
            
            let objDumpeeDetail : DumpeeDetailVC = segue.destination as! DumpeeDetailVC
            guard let sender = sender as? UIButton else{return}
            
            let dumpInfo = objDumpeeSeacrhModel.dumpTimeLineInfo(at: sender.tag)
            objDumpeeDetail.objDumpeeDetail.set(userId: dumpInfo.userId, dumpeeId: dumpInfo.dumpeeId, dumpeeName: dumpInfo.dumpeeName)
            objDumpeeDetail.objDumpeeUserDetail.set(dumpeeId: dumpInfo.dumpeeId)
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let sender = sender as? Any else{return}
            
            let dumpInfo = objDumpeeSeacrhModel.dumpTimeLineInfo(at: (sender as AnyObject).tag)
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }
        else if segue.identifier == SegueIdentity.kNewsDetailSegue{
            let objNewsDetailVC : NewsDetailVC = segue.destination as! NewsDetailVC
            guard let sender = sender as? UIButton else{return}
            objNewsDetailVC.newsUrl =  objDumpeeSeacrhModel.dumpTimeLineInfo(at: sender.tag).newsUrl
        }
    }
    
    func saveDumpeeHistory(dumpeeId:String,dumpeeName:String)
    {
        guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
        let dumpeeInfo = objDumpee.cellForRowAt(at: selectedIndexPath)
        if let jsonObject =  dumpeeInfo.jsonObject as? [String:Any]{
            objSearchModel.saveUserHistory(searchUserId: "", searchDumpeeId: dumpeeId, searchName:dumpeeInfo.topicName ?? ""
            , historyType: "dumpee", otherInfo: jsonObject, progressHud: true) {
                
            }
        }
    }
    
    
}
extension SearchVC:UITableViewDelegate,UITableViewDataSource,HashTagDelegateSearch{
    
    
    func hashTagTextCell(_ sender: FeedTextCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath.row)
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        
        
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func hashTagMediaCell(_ sender: FeedMediaCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath)
        
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        
        
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if optionTypeSelect == .dumpee{
            var numOfSections: Int = 0
            if objDumpee.numberOfRows() > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections            = 1
                tableView.backgroundView = nil
                tableView.separatorStyle  = .none
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "noDumpee".localized
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
        else if optionTypeSelect == .user{
            var numOfSections: Int = 0
            if objSearchModel.numberOfRows() > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections            = 1
                tableView.backgroundView = nil
                tableView.separatorStyle  = .none
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "noUsers".localized
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }else if optionTypeSelect == .tags{
            var numOfSections: Int = 0
            if objSearchModel.numberOfRowsForTag() > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections            = 1
                tableView.backgroundView = nil
                tableView.separatorStyle  = .none
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "noTags".localized
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
        else if optionTypeSelect == .dumps{
            
            
            var numOfSections: Int = 0
            if objDumpeeSeacrhModel.numberOfRows() > 0
            {
                tableView.separatorStyle  = .singleLine
                numOfSections             = 1
                tableView.backgroundView  = nil
                tableView.separatorStyle  = .none
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "noFeeds".localized
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
            
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if optionTypeSelect == .user{
            return objSearchModel.serviceType == .pagging ? itemCount+1 : itemCount
        }else if  optionTypeSelect == .dumpee{
            return objDumpee.serviceType == .pagging ? itemCount+1 : itemCount
        }else    if optionTypeSelect == .tags{
            return objSearchModel.serviceType == .pagging ? itemCount+1 : itemCount
        }
        
        return objDumpeeSeacrhModel.serviceType == .pagging ? itemCount+1 : itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.row<itemCount {
            print("------- \(indexPath.row)")
            
            if optionTypeSelect == .user{
                
                let userSearchListCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kUserSearchListCell, for: indexPath) as! UserSearchListCell
                userSearchListCell.objUserSearch =   objSearchModel.cellForRowAt(at: indexPath)
                return userSearchListCell
            }
            else if optionTypeSelect == .tags{
                let tagSearchListCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kTagSearchListCell, for: indexPath) as! TagSearchListCell
                tagSearchListCell.objTagSearch =   objSearchModel.cellForRowAtTag(at: indexPath)
                return tagSearchListCell
            }
            else if optionTypeSelect == .dumpee{
                let dumpeeSearchListCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kDumpeeSearchListCell, for: indexPath) as! DumpeeSearchListCell
                dumpeeSearchListCell.objDumpeeSearch =   objDumpee.cellForRowAt(at: indexPath)
                return dumpeeSearchListCell
            }
                
            else {
                let obj  = objDumpeeSeacrhModel.cellForRowAt(at:indexPath)
                let feedType =  obj.feedType
                
                switch feedType{
                case .textImage,.textVideo:
                    let feedMediaCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaCell , for: indexPath) as! FeedMediaCell
                    
                    feedMediaCell.tagDelegateSeacrhMedia = self
                    feedMediaCell.rowIndex = indexPath.row
                    feedMediaCell.objFeedTimeLine = objDumpeeSeacrhModel.cellForRowAt(at: indexPath)
                    
                    return feedMediaCell
                case .image,.video:
                    let  feedMediaImageCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaImageCell , for: indexPath) as!  FeedMediaImageCell
                    feedMediaImageCell.rowIndex = indexPath.row
                    feedMediaImageCell.objImage = objDumpeeSeacrhModel.cellForRowAt(at: indexPath)
                    return feedMediaImageCell
                case .text:
                    let feedTextCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedTextCell , for: indexPath) as! FeedTextCell
                    
                    feedTextCell.tagDelegateSeacrh = self
                    feedTextCell.rowIndex = indexPath.row
                    feedTextCell.objText = objDumpeeSeacrhModel.cellForRowAt(at: indexPath)
                    return feedTextCell
                case .gradient:
                    let feedGradientCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedGradientCell , for: indexPath) as! FeedGradientCell
                    
                    feedGradientCell.rowIndex = indexPath.row
                    feedGradientCell.objGradient = objDumpeeSeacrhModel.cellForRowAt(at: indexPath)
                    return feedGradientCell
                }
                
                
            }
            
            
        }
        else
        {
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(){
                
                if self.itemCount>0,self.itemCount<totalRecords {
                    
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        if self.optionTypeSelect == .dumps
                        {
                            self.objDumpeeSeacrhModel.serviceType = .pagging
                            self.loadTimeLineData()
                        }else if self.optionTypeSelect == .tags
                        {
                            self.objSearchModel.serviceType = .pagging
                            self.loadTags()
                        }else if self.optionTypeSelect == .user
                        {
                            self.objSearchModel.serviceType = .pagging
                            self.loadUsers()
                        }else{
                            self.objDumpee.serviceType = .pagging
                            self.loadDumpee()
                        }
                    }
                }else{
                    objDumpeeSeacrhModel.serviceType = .none
                }
            }else{
                objDumpeeSeacrhModel.serviceType = .none
            }
            return loadmoreCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if optionTypeSelect == .dumps{
            if indexPath.row<itemCount {
                let obj  = objDumpeeSeacrhModel.cellForRowAt(at:indexPath)
                let feedType =  obj.feedType
                switch feedType {
                case .gradient:
                    return 322.0
                case .textImage,.textVideo:
                    return 369.0
                case .image,.video:
                    return 320.0
                case .text:
                    return 180.0
                }
            }
            return 416.0
        }
        else{
            
            return 75.0
        }
    }
}


extension SearchVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let searchCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CVCellIdentifier.kSearchCategoryCell, for: indexPath) as! SearchCategoryCell
        searchCategoryCell.categoryLBL.text = searchType[indexPath.row]
        if let tab =  DMSegmentedTab(rawValue: indexPath.row) {
            searchCategoryCell.selectedLBL.isHidden =  optionTypeSelect == tab ? false : true
        }
        return searchCategoryCell
    }
}

extension SearchVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = (searchType[indexPath.row] as NSString).size(withAttributes: nil)
        
        return CGSize(width:size.width+60, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        guard let tab =  DMSegmentedTab(rawValue: indexPath.row) else{return}
        self.optionTypeSelect = tab
        switch self.optionTypeSelect {
        case .dumps:
            self.searchBar.text = ""
            self.searchBar.resignFirstResponder()
            pageNumber = 0
            totalRecords = 0
            objDumpeeSeacrhModel.serviceType = .none
            loadTimeLineData()
            
        case .tags:
            objSearchModel.removeObjects()
            pageNumber = 0
            totalRecords = 0
            objDumpeeSeacrhModel.serviceType = .none
            loadTags()
        case .user:
            objSearchModel.removeObjects()
            pageNumber = 0
            totalRecords = 0
            objDumpeeSeacrhModel.serviceType = .none
            loadUsers()
        case .dumpee:
            objDumpee.removeObjects()
            pageNumber = 0
            totalRecords = 0
            objDumpeeSeacrhModel.serviceType = .none
            loadDumpee()
        }
        
        collectionView.reloadData()
    }
}

extension SearchVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        
        if pageNumber>0
        {
            pageNumber = 0
            totalRecords = 0
            
        }
       
//        guard let searchText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
        
        self.searchString = searchText
      
    
        if searchText.count > 0{
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                  ServerManager.shared.cancelledRequest(isAll: true)
            if self.optionTypeSelect == .user{
                
                    self.objSearchModel.serviceType = .none
                    self.objSearchModel.removeObjects()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                    self.objSearchModel.getSearchUsers(search:self.searchString,onSuccess: {(records) in
                        self.optionTypeSelect = .user
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }, onFailure: {
                        print("THE API NOT SUCEESFULL HIT")
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
            })
                
            }else if self.optionTypeSelect == .dumpee{
                self.objDumpee.serviceType = .none
                self.objDumpee.removeDumpList()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                    self.objDumpee.getDumpFeed(search:self.searchString,  onSuccess: {(records) in
                        DispatchQueue.main.async {
                            self.optionTypeSelect = .dumpee
                            self.tableView.reloadData()
                        }
                        
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
            })
                
                
            }else if self.optionTypeSelect == .tags{
                 self.objSearchModel.serviceType = .none
                self.objSearchModel.removeObjects()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                    self.objSearchModel.getSearchTags(search:self.searchString,onSuccess: { (records) in
                        self.optionTypeSelect = .tags
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }, onFailure: {
                        print("THE API NOT SUCEESFULL HIT")
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
            })
               
            }else if self.optionTypeSelect == .dumps{
                self.objDumpeeSeacrhModel.serviceType = .none
                self.objDumpeeSeacrhModel.removeObjects()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                    self.objDumpeeSeacrhModel.getTimeLineDump(refreshControl:self.refreshControl,search:self.searchString,searchType:"global",onSuccess: { (records) in
                        
                        DispatchQueue.main.async {
                            self.optionTypeSelect = .dumps
                            self.tableView.reloadData()
                        }
                        
                    }, onFailure: {
                        
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
               })
            }
           })
        }
        else{
            
            searchBar.resignFirstResponder()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 , execute: {
                ServerManager.shared.cancelledRequest(isAll: true)
            
            if self.optionTypeSelect == .user{
                 self.objSearchModel.serviceType = .none
                self.objSearchModel.removeObjects()
                DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                self.objSearchModel.getSearchUsers(onSuccess: { (records) in
                    self.optionTypeSelect = .user
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }, onFailure: {
                    print("THE API NOT SUCEESFULL HIT")
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
           })
            }
                
                
            else if self.optionTypeSelect == .dumpee{
                  self.objDumpee.serviceType = .none
                self.objDumpee.removeDumpList()
                  DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                self.objDumpee.getDumpFeed(search:"",  onSuccess: { (records) in
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }, onFailure: {
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                      })
                
            }else if self.optionTypeSelect == .tags{
                 self.objSearchModel.serviceType = .none
                self.objSearchModel.removeObjects()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                self.objSearchModel.getSearchTags(onSuccess: { (records) in
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }, onFailure: {
                    print("THE API NOT SUCEESFULL HIT")
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                    })
            }
            else if self.optionTypeSelect == .dumps{
                self.objDumpeeSeacrhModel.serviceType = .none
                
                self.objDumpeeSeacrhModel.removeObjects()
                 DispatchQueue.main.asyncAfter(deadline:.now() + 0.1 , execute: {
                self.loadTimeLineData()
                })
            }
        })
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        //MK Hide keyboard always
        //if let text =  searchBar.text,text.count < 1{
        searchBar.resignFirstResponder()
        //}
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
     
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //MK Hide keyboard always, but to retain text
        //searchBar.text = ""
        //searchBar.showsCancelButton = true
    }
}
