//
//  FollowingVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 23/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class FollowingVC: UIViewController {
    @IBOutlet var objFollowRequest: ProfileDetailViewModel!
    @IBOutlet weak var headingLBL: UILabel!
    @IBOutlet fileprivate var objFollowers: FollowersViewModel!
    @IBOutlet weak fileprivate var tableView:UITableView!
     @IBOutlet weak var searchBar: UISearchBar!
      fileprivate var loadmoreCell:LoadMoreCell!
    var userId:String = ""
    var followerType:String = ""
    var followHeading:String = ""
    var serachString:String = ""
   var isFromMutualText = false
    
     var itemCount:Int {
         return objFollowers.numberOfRows()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.itemCount>0 {
            
            self.tableView.reloadData()
        }else{
            pageNumber = 0
            totalRecords = 0
            objFollowers.serviceType = .none
        }
        
        if isFromMutualText
        {
            self.headingLBL.text = "mutual".localized
            loadMutualFollowers()
        }
        else{
            
             self.headingLBL.text = "following".localized
            self.loadFollowers()
        }
    }
    
    func loadMutualFollowers()
    {
        
        objFollowers.getMutualFollowFollwers(search:self.serachString, userId: userId, page:pageNumber, onSuccess: {
            
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
   fileprivate func loadFollowers()  {
        objFollowers.getFollowFollwers(search:"", userId: userId, page: pageNumber, type:followerType, onSuccess: {
            if self.objFollowers.serviceType == .pagging{
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
            }
          
            
                    self.tableView.reloadData()
            
            
        }, onFailure: {
            if self.objFollowers.serviceType == .pagging {
                if pageNumber>0{
                    pageNumber -= 1
                }
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
                
            }
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail = segue.destination as! ProfileDetailVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let userInfo = objFollowers.followFollowingInfo(at: selectedIndexPath.row)
            
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userInfo.userId)
            //            controller.objDumpFeed = self.objDumpFeed
            //            controller.objDumpFeed?.set(at: selectedIndexPath, searchString: self.searchBar.text ?? "")
            
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickFollowing(_ sender: UIButton) {
       
       
        let index = sender.tag
        
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        
        objFollowers.setFollowerId(at:index)
        if followerStatus == .follower{
            objFollowers.sendFollowerRequest(onSuceess: {
                
                
                
                self.loadFollowers()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followerStatus == .following{
            let alert = UIAlertController(title: nil, message: "unFollowUserMeesage".localized, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "unfollow".localized, style: .default , handler:{ (UIAlertAction)in
                self.objFollowers.sendUnfollowRequest( onSuceess: {
                    if self.isFromMutualText
                    {
                        self.loadMutualFollowers()
                    }else{
                    self.loadFollowers()
                    }
                }, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
                
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            self.present(alert, animated: true, completion: {
               
            })
            
        }
    }
}

extension FollowingVC : UITableViewDataSource,UITableViewDelegate{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if objFollowers.numberOfRows() > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "startFollowing".localized
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objFollowers.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if  indexPath.row<itemCount {
        
        let followingCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kFollowingCell, for: indexPath) as! FollowingCell
        followingCell.followingBtn.tag = indexPath.row
        followingCell.objFollowingModel = objFollowers.cellForRowAt(at:indexPath)
        return followingCell
        }
        else
         {
            loadmoreCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kLoadMoreCell, for: indexPath) as! LoadMoreCell
            if ServerManager.shared.CheckNetwork(){
                if self.itemCount>0,self.itemCount<totalRecords {
                    pageNumber += 1
                    loadmoreCell.isShowLoader = true
                    DispatchQueue.main.after(0.5) {
                        self.objFollowers.serviceType = .pagging
                        if self.isFromMutualText
                        {
                            self.loadMutualFollowers()
                        }
                        else{
                            
                            self.loadFollowers()
                        }
                        
                    }
                
                }
                else{
                    self.objFollowers.serviceType = .none
                }
            }
            else{
             self.objFollowers.serviceType = .none
            }
            return loadmoreCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension FollowingVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
         guard let searchText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
      
        if searchText.count>0{
            
            if isFromMutualText
            {
                self.objFollowers.removeAll()
                self.serachString = searchText
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                     self.loadMutualFollowers()
                    
                }
               
                
            }
            else{
            objFollowers.getFollowFollwers(search:searchText, userId: userId, page: pageNumber, type:followerType, onSuccess: {
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    
                    
                }}, onFailure: {
                    
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
            }
        }else{
            
            if self.isFromMutualText
            {
                self.objFollowers.removeAll()
                self.serachString = ""
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.loadMutualFollowers()
                    
                }
            }
              else{
            objFollowers.getFollowFollwers(search:"", userId: userId, page: pageNumber, type:followerType, onSuccess: {
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    
                    
                }}, onFailure: {
                    
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        }
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
      
        if isFromMutualText
        {
            self.objFollowers.removeAll()
            self.serachString = ""
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.loadMutualFollowers()
                
            }
        }
        objFollowers.getFollowFollwers(search:"", userId: userId, page: pageNumber, type:followerType, onSuccess: {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
                
            }}, onFailure: {
                
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
}
