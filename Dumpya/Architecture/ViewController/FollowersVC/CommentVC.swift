//
//  CommentVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 23/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit
import GrowingTextView
import IQKeyboardManagerSwift
import NotificationCenter
class CommentVC: UIViewController {
    
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var viewMessage: DMCardView!
    @IBOutlet var objCommentViewModel: CommentViewModel!
    @IBOutlet weak var bottomConstriant: NSLayoutConstraint!
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak fileprivate var tableView: UITableView!
    //    fileprivate lazy var refreshControl:UIRefreshControl = {
    //        let refresh = UIRefreshControl()
    //        refresh.tintColor = DMColor.redColor
    //        return refresh
    //    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        //        tableView.addSubview(refreshControl)
        //
        //        refreshControl.addTarget(self, action:
        //            #selector(self.refreshComment),
        //                                 for:.valueChanged)
        //
        //        refreshControl.layoutIfNeeded()
    }
    
    //    @objc fileprivate func refreshComment() {
    //
    //        getComment()
    //
    //    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        textView.trimWhiteSpaceWhenEndEditing = false
        textView.placeholder = "addComment".localized
        textView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        textView.minHeight = 30.0
        textView.maxHeight = 80.0
        textView.backgroundColor = .white
        textView.layer.cornerRadius = 4.0
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.clipsToBounds  = true
        textView.layer.masksToBounds = true
        customization()
        getComment()
        
    }
    fileprivate func getComment(){
        objCommentViewModel.getComment(onSuccess: {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
                if self.objCommentViewModel.numberOfRows() > 0{
                    let count = self.objCommentViewModel.numberOfRows()
                    let index = IndexPath(row: count-1, section: 0)
                    print("Suceess")
                    self.tableView.scrollToRow(at: index , at: .bottom , animated: false)
                }
            }
        }, onFailure: {
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
    }
    
    @IBAction func onClickComment(_ sender: Any) {
        
        
        objCommentViewModel.addComment(comment:textView.text.removeWhiteSpace, onSuceess: {
            self.textView.text = ""
            self.textView.minHeight = 30.0
            self.getComment()
            isDumpySaved = true
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    func customization() {
        // *** Listen for keyboard show / hide ***
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstriant?.constant = keyboardFrame.cgRectValue.height
            self.view.layoutIfNeeded()
            UIView.performWithoutAnimation {
                
                if self.objCommentViewModel.numberOfRows() > 0{
                    let count = self.objCommentViewModel.numberOfRows()
                    let index = IndexPath(row: count-1, section: 0)
                    
                    self.tableView.scrollToRow(at: index , at: .bottom , animated: false)
                }
            }
            
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstriant.constant = 0
        print("HIDE KEYBOARD")
        self.view.layoutIfNeeded()
        UIView.performWithoutAnimation {
            
            if self.objCommentViewModel.numberOfRows() > 0{
                let count = self.objCommentViewModel.numberOfRows()
                let index = IndexPath(row: count-1, section: 0)
                
                self.tableView.scrollToRow(at: index , at: .bottom , animated: false)
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickProfile(_ sender: DMButton) {
        
        performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail = segue.destination as! ProfileDetailVC
            
            guard let sender = sender as? UIButton else{return}
            let userId = objCommentViewModel.didSelect(index:sender.tag)
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userId)
        }
    }
    
    
}

extension CommentVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if objCommentViewModel.numberOfRows() > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
            tableView.separatorStyle  = .none
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "noComments".localized
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return objCommentViewModel.numberOfRows()
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if  userModel?.userId ?? "" == objCommentViewModel.didSelect(index: indexPath.row){
            return true
        }
        else{
            return false
        }
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        
        if (editingStyle == .delete)
        {
            objCommentViewModel.getCommnetId(at: indexPath)
            let commecntId = objCommentViewModel.getCommId(at: indexPath)
            self.objCommentViewModel.deleteComment(commentId: commecntId, onSuccess: {
                DispatchQueue.main.async {
                    self.objCommentViewModel.objCommentDetail.remove(at: indexPath.row)
                    
                    tableView.reloadData()
                }
                
            }, onFailure: {
                
            }) { (message) in
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(CommentVC.longTap))
        
        
        let commentCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kCommentCell, for: indexPath) as! CommentCell
        commentCell.objComment = objCommentViewModel.cellForRowAt(at: indexPath)
        
        commentCell.userProfile.tag = indexPath.row
        
        commentCell.addGestureRecognizer(longGesture)
        return commentCell
    }
    
    
    @objc func longTap(gestureReconizer: UILongPressGestureRecognizer) {
        
        print("Long tap")
        
        let longPress = gestureReconizer as UILongPressGestureRecognizer
        _ = longPress.state
        let locationInView = longPress.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: locationInView)
        
        guard let index = indexPath else { return }
        
        objCommentViewModel.getCommnetId(at: index)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //
        //        alert.addAction(UIAlertAction(title: "Copy", style: .default , handler:{ (UIAlertAction)in
        //            UIPasteboard.general.string = "Hello"
        //         }))
        //
        //
        alert.addAction(UIAlertAction(title: "Report Comment", style: .default , handler:{ (UIAlertAction)in
            
            
            self.objCommentViewModel.reportComment( onSuccess: {
                
                
            }, onFailure: {
                
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 65.0
    }
    
    
    
}

