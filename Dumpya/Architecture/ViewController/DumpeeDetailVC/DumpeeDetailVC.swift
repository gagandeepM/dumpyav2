//
//  DumpeeDetailVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 19/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

protocol HashTagDelegateDumpeeDetail : class {
    func hashTagMediaDumpeeDetailCell(_ sender: FeedMediaCell,hashName:String)
    func hashTagTextDumpeeDetailCell(_ sender:FeedTextCell,hashName:String)
}
import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
class DumpeeDetailVC: UIViewController {
    @IBOutlet weak var editDumpee_btn: UIButton!
    
    @IBOutlet weak var dumpeeDescriptionLBL: UILabel!
    @IBOutlet var objDumpeeUserDetail: DumpeeDetailViewModel!
    @IBOutlet var objDumpeeDetail: DumpTimeLineViewModel!
    @IBOutlet weak var NoDumpView: UIView!
    @IBOutlet weak var dumpeeTitleLBL: UILabel!
    @IBOutlet weak fileprivate var noDumpFoundLBL: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
     @IBOutlet weak var dumpeeImageView: DMImageView!
     @IBOutlet weak var dumpeeName: UILabel!
    @IBOutlet weak var gradientUpperView: DMGradientCard!
    @IBOutlet weak var gradientLowerView: DMGradientCard!
    var handler:(()->Void)?
    var type:String = ""
    var followStatus:Int = 0
    var isFromLeaderboard = false
    @IBOutlet weak var isPrivateView: UIView!
    @IBOutlet weak var privateAccountLBL: NSLayoutConstraint!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak fileprivate var bio: UILabel!
    @IBOutlet weak fileprivate var locationName: UILabel!
    @IBOutlet weak fileprivate var userName: UILabel!
    @IBOutlet var objUserDetailViewModel: ProfileDetailViewModel!
    @IBOutlet weak fileprivate var tableView: UITableView!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var todayCountLBL: UILabel!
    @IBOutlet weak var allTimeCountLBL: UILabel!
    @IBOutlet weak var dumpCountLBL: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var followHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImgBTN: DMButton!
    @IBOutlet weak var profileViewHeightConstraint: NSLayoutConstraint!
    var count:Int = 0
    var tempCount = 3
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    fileprivate lazy var refreshControl:UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = DMColor.redColor
        return refresh
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.post(name:Notification.Name("isFromDumpeedetail"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("isFromSearchFormDumpeeDetail"), object: nil)
        getDumpeeDetail()
          self.objDumpeeDetail.set(dumpeeNameLbl: dumpeeTitleLBL)
         self.dumpeeName.text = dumpeeTitleLBL.text
         tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(self.refreshDumpeeDetailList),
                                 for:.valueChanged)
        refreshControl.layoutIfNeeded()
    }
  
    @objc fileprivate func refreshDumpeeDetailList() {
        
        getDumpeeDetail()
        
    }
    
    
    
    @IBAction func onClickedWebsiteBtn(_ sender: UIButton) {
        
        
        let websiteUrl = self.objDumpeeUserDetail.categoryData()?.website ?? ""
        
        if websiteUrl.isEmpty{
            alertMessage =  FieldValidation.kEmptyWebsite
        }else{
            let storyboard = UIStoryboard(name:"Website", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "WebsiteVC") as? WebsiteVC
            vc?.url = self.objDumpeeUserDetail.categoryData()?.website ?? ""
            self.navigationController?.pushViewController(vc!, animated: true)
        }
       
    }
    @IBAction func editDunpeeClicked(_ sender: UIButton) {
        print (count,tempCount)
        if count < tempCount
        {
       
            let updateRemains =  tempCount - count
            self.showAlertAction(title: "Update Dumpee", message: "You have \(String(describing: updateRemains)) update attempts left.Do you want to proceed?", cancelTitle: "CANCEL", otherTitle: "PROCEED") { (index) in
                if index == 2
                {
                    let storyboard = UIStoryboard(name:"CreateDumpee", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateDumpeeVC") as? CreateDumpeeVC
                    vc?.isFromDumpeeDetail = true
                    
                    vc?.dumpeeId = self.objDumpeeUserDetail.dumpeeId ?? ""
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
                else
                {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else{
            
            self.showAlert(message: "It seems you are out of your update limit. Please contact admin for further information.")
            return
        }
     
        
    }
    fileprivate func setupEditBtn(){
      
        
        self.editDumpee_btn.isHidden = objDumpeeUserDetail.userId == userModel?.userId ? false : true
       
    }
    fileprivate func getDumpeeDetail(){
        objDumpeeUserDetail.getDumpeeDetail(isPull:self.refreshControl.isRefreshing,onSuccess: {
            DispatchQueue.main.async {
                if self.refreshControl.isRefreshing{
                    self.refreshControl.endRefreshing()
                }
                self.setupEditBtn()
                
            }
           
            self.count = self.objDumpeeUserDetail.categoryData()?.updateCount ?? 0
            self.objDumpeeUserDetail.setDumpeeDetail(dumpsCount: self.dumpCountLBL, todayCount: self.todayCountLBL, allTimeCount: self.allTimeCountLBL, coverImageView: self.coverImageView,dumpeeDescription:self.dumpeeDescriptionLBL,dumpeeImage:self.dumpeeImageView )
            
           print(self.dumpeeImageView.image)
            
            //MK Assign page no 0 to initially load first page data
            pageNumber = 0
            self.loadDumpeeTimeLineData()
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    @IBAction func onClickPost(_ sender: UIButton) {
        
         performSegue(withIdentifier: SegueIdentity.kDumpCreateSeque, sender: sender)
        
        
    }
    
    fileprivate func loadDumpeeTimeLineData() {
        
        objDumpeeDetail.getTimeLineDump(refreshControl: self.refreshControl, onSuccess: { (records) in
             self.tableView.isHidden = false
        DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
        
           
        }, onFailure: {
            
     }, onUserSuspend: { (message) in
        
        self.showAlert(message: message, completion: { (index) in
            AppDelegate.sharedDelegate.logoutUser()
        })
     })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickGradientRedump(_ sender: UIButton) {
        
        let index = sender.tag
        self.objDumpeeDetail.redumpUser(dumpFeedId: self.objDumpeeDetail.dumpTimeLineInfo(at: index).dumpFeedId,countryName:countryNameFromLocation ,locationName: locationNameLocation ,onSuccess: { (isRedumpedByMe,redumpCount) in
            self.objDumpeeDetail.didUpdateRedump(at: index, isRedump: isRedumpedByMe,redumpCount: redumpCount)
            let indexPath = IndexPath(item: index, section: 0)
            UIView.performWithoutAnimation {
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }, onFailure: {

        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    }
    
    
    @IBAction func onClickUserProfile(_ sender: UIButton) {
        
        performSegue(withIdentifier: SegueIdentity.kProfileDetailSegue, sender: sender)
    }
    
    
 
    @IBAction func onClickDots(_ sender: UIButton) {
        
        let index = sender.tag
        let obj = self.objDumpeeDetail.dumpTimeLineInfo(at: index)
       dumpSetting(dumpId: obj.dumpFeedId,userID:obj.userId,source:obj.source,index: index,dumpeeName: obj.dumpeeName,media: obj.media,content: obj.content)
    }
    
    @IBAction func onClickCommentPromotionalText(_ sender: DMCardView) {
        performSegue(withIdentifier:SegueIdentity.kCommentedUserViewListSegue, sender: sender)
    }
    
    @IBAction func onClickCommentCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentSegue , sender: sender)
        
    }
    
    @IBAction func onClickComment(_ sender: DMCardView) {
        performSegue(withIdentifier: SegueIdentity.kCommentViewSegue , sender: sender)
    }
    
    @IBAction func onClickCommentDetail(_ sender: UIButton) {
        performSegue(withIdentifier:SegueIdentity.kCommentSegue, sender: sender)
    }
    fileprivate  func dumpSetting(dumpId:String,userID:String,source:String,index:Int,dumpeeName:String = "",media:String,content:String)   {
        
        if userID == userModel?.userId ?? ""{
          var actions :[AlertActionModel] = [
                AlertActionModel(image: #imageLiteral(resourceName: "icon_EditDump"), title: "editDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Delete"), title: "deleteDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left), AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Edit
                    
                    let dumpCreateVC = UIStoryboard.init(name: "DumpCreate", bundle: Bundle.main).instantiateViewController(withIdentifier: "DumpCreateVC") as? DumpCreateVC
                    dumpCreateVC?.isEdit = true
                    
                    
                    guard let dumpeeInfo = self.objDumpeeDetail?.dumpeeInfo(at:index) else{return}
                    
                    dumpCreateVC?.dumpId = dumpeeInfo.dumpFeedID
                    dumpCreateVC?.dumpeeId = dumpeeInfo.dumpeeId
                    dumpCreateVC?.dumpName = dumpeeInfo.categoryName
                    let dumpeeType = dumpeeInfo.type
                    dumpCreateVC?.topicName = dumpeeInfo.topicName
                    let content = dumpeeInfo.content
                    let media = dumpeeInfo.media
                    let videoThumbnail = dumpeeInfo.videoThumbnail
                    let size = CGSize(width: dumpeeInfo.width, height: dumpeeInfo.height)
                    switch dumpeeType{
                    case .image:
                        dumpCreateVC?.params.type = .image(item: media, size: size)
                    case .textImage:
                        dumpCreateVC?.params.type = .textImage(item: media, text: content, size: size)
                    case .video:
                        dumpCreateVC?.params.type = .video(item: videoThumbnail, size: size)
                    case .textVideo:
                        dumpCreateVC?.params.type = .textVideo(item: videoThumbnail, text: content, size: size)
                    case .gradient:
                        let bgCode  = dumpeeInfo.3
                        var gradientColor:GradientColor = .red
                        if GradientColor.red.name == bgCode{
                            gradientColor = .red
                        }else if GradientColor.green.name == bgCode{
                            gradientColor = .green
                        }else if GradientColor.purple.name == bgCode{
                            gradientColor = .purple
                        }else if GradientColor.yellow.name == bgCode{
                            gradientColor = .yellow
                        }else if GradientColor.darkRed.name == bgCode{
                            gradientColor = .darkRed
                        }
                        dumpCreateVC?.params.type = .gradient(color: gradientColor, text: content)
                    case .text:
                        dumpCreateVC?.params.type = .text(text: content)
                    }
                    self.navigationController?.pushViewController(dumpCreateVC!, animated: true)
                }else if indexofSheet == 3{
                    //Delete
                    self.objDumpeeDetail.deleteDump(dumpFeedId: dumpId, onSuccess: {
                        self.loadDumpeeTimeLineData()
                    }, onFailure: {
                        
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                }else if indexofSheet == 4{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media, viewController: self,copyLink: false)
                }else if indexofSheet == 5{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            }
            
            
            
        }
            
        else{
           var actions :[AlertActionModel] = [
                
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Report"), title: "reportDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_Share"), title: "shareDump".localized, color: .black, style: .default, alignment: .left),
                AlertActionModel(image: #imageLiteral(resourceName: "icon_CopyLink"), title: "copyLink".localized, color: .black, style: .default, alignment: .left)
                
            ]
            self.shareActionSheet(otherAction:&actions) { (indexofSheet) in
                if indexofSheet == 2{
                    //Report
                   self.objDumpeeDetail.reportDump(dumpId: dumpId, userId: userID, reportType: "dump", onSuccess: {
                    }, onFailure: {
                    }, onUserSuspend: { (message) in
                        
                        self.showAlert(message: message, completion: { (index) in
                            AppDelegate.sharedDelegate.logoutUser()
                        })
                    })
                }else if indexofSheet == 3{
                    //Share
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: false)
                }else if indexofSheet == 4{
                    //Copy
                    JKGoogleManager.shared.deeplinking(dumpId: dumpId, dumpeeName: dumpeeName, content: content, media: media,viewController: self,copyLink: true)
                }
            }
            
            
        }
        
    }
    
    
    
    @IBAction fileprivate func onClickFollowingDetail(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
        
    }
    
    
    @IBAction fileprivate func onClickCommentUserList(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kCommentedUserListSegue , sender: sender)
        
    }
    
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        
        let index = sender.tag
        objDumpeeDetail.didUpdateReadMore(at: index, states:false)
        
        
        let indexPath = IndexPath(item: index, section: 0)
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        
    }
    
    @IBAction fileprivate func  onClickRedumpCount(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIdentity.kFollowingListSegue , sender: sender)
    }
    
    
    
    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == SegueIdentity.kDumpCreateSeque {
           return self.objDumpeeUserDetail.categoryData() == nil ? false : true
        }else{
            return true
        }
        
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kDumpCreateSeque{
           
            let objDumpCreate : DumpCreateVC = segue.destination as! DumpCreateVC
            objDumpCreate.topicCreate = true
            objDumpCreate.objDumpVM?.set(objDumpbCategory: self.objDumpeeUserDetail.categoryData()!)
        }else if segue.identifier == SegueIdentity.kCommentViewSegue{
            guard let commentBtn = sender as? DMCardView else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpeeDetail.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
            
            
        }else if segue.identifier == SegueIdentity.kProfileDetailSegue{
                let objProfileDetail : ProfileDetailVC = segue.destination as! ProfileDetailVC
                guard let sender = sender as? UIButton else{return}
                let dumpInfo = objDumpeeDetail.dumpTimeLineInfo(at: sender.tag)
                objProfileDetail.objUserDetailViewModel.set(currentUserId: dumpInfo.userId)
        }else if segue.identifier == SegueIdentity.kCommentedUserListSegue{
            let objCommentedUserListVC : CommentedUserListVC = segue.destination as! CommentedUserListVC
            
            guard let sender = sender as? DMCardView else{return}
            let dumpInfo = objDumpeeDetail.dumpTimeLineInfo(at: sender.tag)
            print(dumpInfo.dumpeeId)
objCommentedUserListVC.objCommentedUserListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            print(dumpInfo.dumpeeId)
        } else if segue.identifier == SegueIdentity.kCommentSegue{
            guard let commentBtn = sender as? UIButton else{return}
            let index  = commentBtn.tag
            let obj = self.objDumpeeDetail.didSelectAtRedump(at: index)
            let objComment : CommentVC = segue.destination as! CommentVC
            objComment.objCommentViewModel.set(dumpFeedTimeLine: obj)
        }else if segue.identifier == SegueIdentity.kFollowingListSegue{
            
            let objFollowingList : FollowingListVC = segue.destination as! FollowingListVC
            guard let sender = sender as? Any else{return}
            
            let dumpInfo = objDumpeeDetail.dumpTimeLineInfo(at: (sender as AnyObject).tag)
            objFollowingList.objFollowingListViewModel.setFollowingDumpId(dumpId:dumpInfo.dumpFeedId)
            
        }
     }
 }

extension DumpeeDetailVC:UITableViewDataSource,UITableViewDelegate,HashTagDelegateDumpeeDetail{
    
    
   
    func hashTagTextDumpeeDetailCell(_ sender: FeedTextCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath.row)
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        vc?.hashTagName = hashName
       self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func hashTagMediaDumpeeDetailCell(_ sender: FeedMediaCell,hashName:String) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Heart", sender, tappedIndexPath)
        
        print("THE HASHNAME",hashName)
        let vc = UIStoryboard.init(name: "HashTag", bundle: Bundle.main).instantiateViewController(withIdentifier: "HashTagVC") as? HashTagVC
        
        
        vc?.hashTagName = hashName
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objDumpeeDetail.numberOfRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let obj  = objDumpeeDetail.cellForRowAt(at:indexPath)
        let feedType =  obj.feedType
        switch feedType{
        case .textImage,.textVideo:
            let feedMediaCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaCell , for: indexPath) as! FeedMediaCell
            
            feedMediaCell.rowIndex = indexPath.row
            feedMediaCell.tagDelegateDumpeeDetailMedia = self
            
            feedMediaCell.objFeedTimeLine = objDumpeeDetail.cellForRowAt(at: indexPath)
            
            return feedMediaCell
             case .image,.video:
                let  feedMediaImageCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedMediaImageCell , for: indexPath) as!  FeedMediaImageCell
                
                feedMediaImageCell.rowIndex = indexPath.row
                feedMediaImageCell.objImage = objDumpeeDetail.cellForRowAt(at: indexPath)
                return feedMediaImageCell
            case .text:
                let feedTextCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedTextCell , for: indexPath) as! FeedTextCell
                feedTextCell.rowIndex = indexPath.row
                feedTextCell.tagDelegateDumpeeDetailText = self
                feedTextCell.objText = objDumpeeDetail.cellForRowAt(at: indexPath)
                return feedTextCell
             case .gradient:
                let feedGradientCell = tableView.dequeueReusableCell(withIdentifier:TVCellIdentifier.kFeedGradientCell , for: indexPath) as! FeedGradientCell
                feedGradientCell.rowIndex = indexPath.row
                
                feedGradientCell.objGradient = objDumpeeDetail.cellForRowAt(at: indexPath)
                return feedGradientCell
        }
     }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            
            return UITableViewAutomaticDimension
       
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let obj  = objDumpeeDetail.cellForRowAt(at:indexPath)
        let feedType =  obj.feedType
        switch feedType{
        case .image,.video:
            return 321.0
        case .text:
             return  180.0
        case .textImage,.textVideo:
             return 369.0
        case .gradient:
             return 322.0
        }
    }
}
