//
//  ChangePasswordVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet var objChangePassword: DMUserViewModel!
    @IBOutlet weak fileprivate var oldPasswordTF: DMTextField!
     @IBOutlet weak fileprivate var newPasswordTF: DMTextField!
     @IBOutlet weak fileprivate var confirmPasswordTF: DMTextField!
    
    @IBOutlet weak fileprivate var eyeConfirmPasswordBTN: UIButton!
    @IBOutlet weak fileprivate var eyePasswordBtn: UIButton!
     @IBOutlet weak fileprivate var eyeOldPasswordBtn: UIButton!
    
    
    //MARK:- Properties
  
    fileprivate var iconPassword = true
    fileprivate var iconOldPassword = true
    fileprivate var iconConfirmPassowrd = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickChangePassword(_ sender: Any) {
    
    guard let oldPassword = oldPasswordTF.text else {return}
    guard let newPassword = newPasswordTF.text else { return }
    guard let confirmPassword = confirmPasswordTF.text else {return}
        
        objChangePassword.changePassword(oldPassword: oldPassword, password: newPassword, confirmPassword: confirmPassword, onSuccess: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                 AppDelegate.sharedDelegate.logoutUser()
            })
            
            
        }, onFailure: {
            
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
        
    }
    @IBAction func onClickEyeOldPassword(_ sender: Any) {
        if(iconOldPassword == true) {
            oldPasswordTF.isSecureTextEntry = false
    eyeOldPasswordBtn.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
            
        } else {
           eyeOldPasswordBtn.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            oldPasswordTF.isSecureTextEntry = true
        }
        iconOldPassword = !iconOldPassword
    }
    
    @IBAction func onClickEyeNewPassword(_ sender: Any) {
        if(iconPassword == true) {
            newPasswordTF.isSecureTextEntry = false
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
            
        } else {
            eyePasswordBtn.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            newPasswordTF.isSecureTextEntry = true
        }
        iconPassword = !iconPassword
    }
    
    @IBAction func onClickEyeConfirmPassword(_ sender: Any) {
        if(iconConfirmPassowrd == true) {
            confirmPasswordTF.isSecureTextEntry = false
            eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeGray"), for: .normal)
        } else {
            eyeConfirmPasswordBTN.setImage(UIImage(named:"icon_eyeoffGray"), for: .normal)
            confirmPasswordTF.isSecureTextEntry = true
        }
        iconConfirmPassowrd = !iconConfirmPassowrd
    }

}
