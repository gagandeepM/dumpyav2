//
//  SocketIOManager.swift
//  Upstix
//
//  Created by Yogesh on 24/03/18.
//  Copyright © 2018 Mobilyte. All rights reserved.
//
import UIKit
import SocketIO
enum DMSocketEvent:String ,CustomStringConvertible{
    case receiveMessage
    case readMessage
    case sendMessage
    case socketRegister
    case userOnlineStatus
    var description: String{
        switch self {
        case .receiveMessage:
            return "receive_message"
        case .readMessage:
            return "messageRead"
        case .sendMessage:
            return "send_message"
        case .socketRegister:
            return "socketRegister"
        case .userOnlineStatus:
            return "chatWith"
        }
    }
}
class SocketIOManager: NSObject {
    typealias DidBecomeActiveHandler = ()->Void
    typealias DidEnterBackgroundHandler = ()->Void
    
    fileprivate var didBecomeActiveCompletion:DidBecomeActiveHandler?
    fileprivate var didEnterBackgroundCompletion:DidEnterBackgroundHandler?
    var s3URL:String?
    class var shared:SocketIOManager{
        struct  Singlton{
            static let instance = SocketIOManager()
            
        }
        return Singlton.instance
    }
    lazy var manager:SocketManager = {
        return SocketManager(socketURL:  URL(string:kBaseUrl)!, config: [.log(true), .forceWebsockets(true), .compress])
        
        //SocketManager(socketURL:  URL(string:kBaseUrl)!, config: [.log(true), .forceNew(true), .reconnectAttempts(10), .reconnectWait(6000), .connectParams(["key":"value"]), .forceWebsockets(true), .compress])
    }()
    var socket:SocketIOClient!
    //  let socket:SocketIOClient = shared.manager.defaultSocket
    func setup() {
        //socket = manager.defaultSocket
        // Add my handlers
    }
    override init() {
        super.init()
        socket =  manager.defaultSocket
        // self.addObserver()
        
    }
    //    func addObserver(){
    //        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (noti) in
    //            self.establishConnection()
    //          //  self.didBecomeActiveCompletion?()
    //
    //        }
    //        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: OperationQueue.main) { (noti) in
    //            self.closeConnection()
    //          //  self.didEnterBackgroundCompletion?()
    //
    //        }
    //    }
    
    func socketConnect(completion:(()->())? = nil) {
        
        if socket.status == .notConnected{
            socket.on(clientEvent: .connect) { (data, ack) in
                print("socket connected",data)
                completion?()
            }
            
            
        }
        if socket.status == .connected {
            getMessage()
        }
        //socketReconnect()
        socket.connect()
        
        
    }
    
    func socketDisconnect(completion:(()->())? = nil) {
        if socket.status == .connected{
            socket.on(clientEvent: .disconnect) { (data, ack) in
                print("socket disconnect",data)
                completion?()
            }
            socket.disconnect()
        }
        
        
    }
    func socketReconnect(){
        if socket.status == .disconnected {
            socket.on(clientEvent: .reconnectAttempt) { (data, ack) in
                print("socket Reconnect attempt **** ",data)
            }
            socket.on(clientEvent: .reconnect) { (data, ack) in
                print("socket starting reconnect **** ",data)
            }
        }
        
    }
    func getMessage(){
        socket.on(DMSocketEvent.receiveMessage.description) { (data, ack) in
            
        }
        socket.on(DMSocketEvent.readMessage.description) { (data, ack) in
            
        }
    }
    func sendMessageNew(message: [String:Any]) {
        socket.emit(DMSocketEvent.sendMessage.description, message)
    }
    
    func userOnline(message: [String:Any]) {
        socket.emit(DMSocketEvent.userOnlineStatus.description, message)
    }
    
    
    
    func socketOn(userID: String) {
      
             socket.emit(DMSocketEvent.socketRegister.description, userID)
      
       
    }
    func readMessage(completionHandler: @escaping (_ messageInfo: [String: Any]) -> Void){
        socket.on(DMSocketEvent.readMessage.description) { (result, socketAck) -> Void in
            print(result)
            let receiveData = result[0] as? [String:Any] ?? [:]
            completionHandler(receiveData)
        }
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: [String: Any]) -> Void) {
        
        socket.on(DMSocketEvent.receiveMessage.description) { (dataArray, socketAck) -> Void in
            
          
            
            let receiveData = dataArray[0] as? [String:Any] ?? [:]
            
            completionHandler(receiveData)
        }
        
        
    }
    //    func didBecomeActive(onComletion:@escaping DidBecomeActiveHandler){
    //       didBecomeActiveCompletion = onComletion
    //    }
    //    func didEnterBackground(onComletion:@escaping DidEnterBackgroundHandler){
    //        didEnterBackgroundCompletion = onComletion
    //    }
    
    private func listenForOtherMessages() {
        //        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
        //            NSNotificationCenter.defaultCenter().postNotificationName("userWasConnectedNotification", object: dataArray[0] as! [String: AnyObject])
        //        }
        //
        //        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
        //            NSNotificationCenter.defaultCenter().postNotificationName("userWasDisconnectedNotification", object: dataArray[0] as! String)
        //        }
        //
        //        socket.on("userTypingUpdate") { (dataArray, socketAck) -> Void in
        //            NSNotificationCenter.defaultCenter().postNotificationName("userTypingNotification", object: dataArray[0] as? [String: AnyObject])
        //        }
    }
    
    
    //    func sendStartTypingMessage(nickname: String) {
    //        socket.emit("send_message", nickname)
    //    }
    //
    //
    //    func sendStopTypingMessage(nickname: String) {
    //        socket.emit("send_message", nickname)
    //    }
    
}


