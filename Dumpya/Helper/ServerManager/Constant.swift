//
//  Constant.swift
//  Dumpya
//
//  Created by Chander on 14/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//
import Foundation
import UIKit
import CoreLocation
let kWalkThroughtSkip = "WalkThroughSkip"

//let kAppImage:UIImage = appIcon ?? #imageLiteral(resourceName: "logo_orange_text")
let kAppTitle             =   (AppInfo.kAppTitle.isEmpty == true) ? "Dumpya":AppInfo.kAppTitle
let kAppLanguageNotificationIdentifier = "AppLanguageNotificationIdentifier"
let kOops                   = "Oops!!☹"
let kconnectionError        = "No Internet Connection!☹"
let KSuccess                = "Success!!😀"
let kDeviceToken            = "device_token"
let kInstanceToken = "instance_Token"
let kUserDataKey            = "UserData"
let kAuthTokenKey           = "AuthToken"
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
//Web Service Constant String-
let kDevelopmentUrl =  "http://34.210.71.81:4003/"

//let kDevelopmentUrl =  "http://45.33.68.242:4003/"
let kStagingURL = "http://45.33.68.242:4000/"
let kLocalUrl = "http://10.20.3.214:4003/"
var pageNumber:Int = 0
var totalRecords:Int = 0
var isDumpySaved:Bool = false
var locationNameLocation:String = ""
var countryNameFromLocation:String = ""

//MARK:- Dynamic Links for Sharing and Copy

let  kDeepLinkUrl = "https://dumpya.page.link/?dumpId="
let  kDynamicDomainLink = "dumpya.page.link"


//MARK: Set url here
let kBaseUrl = kDevelopmentUrl

//USER MANAGE MODULE
let ksignup        = kBaseUrl + "users/registration"
let kcheckOtp      = kBaseUrl + "users/checkOtp"
let keditProfile   = kBaseUrl + "users/updateUser"
let kUpdateToken = kBaseUrl + "users/updateToken"
let kAcceptRejectFollowRequest = kBaseUrl + "follows/responseFollowRequest"
let kSetSettingPrefrence = kBaseUrl + "users/setSettingPrefrence"

let kLanguageChange = kBaseUrl + "users/changeLanguage"
let kClearSeacrh = kBaseUrl + "dumps/deleteHistory"
let kChangePassword = kBaseUrl + "users/changePassword"
let klogin = kBaseUrl + "users/loginUser"
let kSendFollowRequest = kBaseUrl + "follows/sendFollowRequest"
let kSendUnfollowRequest = kBaseUrl + "follows/unFollowUser"
let kRedump = kBaseUrl + "dumps/makeReDump"
let kNewsRedump = kBaseUrl + "dumps/createNewsDump"

let kEditDump = kBaseUrl + "dumps/editDump"
let kDeleteDump = kBaseUrl + "dumps/deleteDump?dumpId="
let kReportUser = kBaseUrl + "users/reportContent"
let KDeleteComment = kBaseUrl + "dumps/deleteComment?commentId="
let kBlockUnblockUser  = kBaseUrl + "users/blockUnblockUser"
let kUploadProfileImage = kBaseUrl + "users/upload"
let kResetPassword = kBaseUrl+"users/resetPassword"
let kHelpMessage = kBaseUrl+"users/helpMessage"
let kPrivacy = kBaseUrl+"privacy/\(Language.language.rawValue)"
let kPrivacyPolicy = kBaseUrl+"privacy/\(Language.language.rawValue)"
let kTerms = kBaseUrl + "terms/\(Language.language.rawValue)"
let kAbout = kBaseUrl+"about/\(Language.language.rawValue)"
let kgetUserDetail = kBaseUrl + "users/userDetails?userId="
let kgetDumpeeDetail = kBaseUrl + "dumps/getDumpeeProfile?dumpeeId="

let kGetSettingsPrefrence = kBaseUrl + "users/getSettingsPrefrence"
let kgetBlockAccount = kBaseUrl + "users/blockUserList?searchString="
let kForgotPassword = kBaseUrl+"users/forgotPasswordOtp"
let kVerifyOtp = kBaseUrl + "users/checkOtp"
let kVerifySignupOtp = kBaseUrl + "users/verifySignupOtp"
let kresendOtpSignup = kBaseUrl + "users/resendOtpSignup"
let kUserSearch = kBaseUrl + "users/searchUser?searchString="
let kGetChat = kBaseUrl + "users/getChatHistory?conversationId="
let kGetConversationId = kBaseUrl + "users/getConversationId?userId="
let kDumpSearch = kBaseUrl + "dumps/searchDumpee?searchString="
let kSearchSuggestion =  kBaseUrl   + "dumps/searchDumpee?searchString="
let kLeaderBoard = kBaseUrl + "dumps/getLeaderBord?categoryId="
let kTagSearch = kBaseUrl + "dumps/searchHash?searchText="
let kcreateTopic = kBaseUrl + "dumps/createTopic"
let kCreateDump = kBaseUrl + "dumps/createDump"
let kCreateDumpee = kBaseUrl + "dumps/createDumpee"
let kFollwers = kBaseUrl + "follows/getFollowFollwers?"
let KMutualFollowing = kBaseUrl + "follows/getMutualFollowedBy?"
let kDumpDetail = kBaseUrl + "dumps/getDumpDetail?dumpId="
let kNotificationList = kBaseUrl + "users/notificationsList?page="
let kRequested = kBaseUrl + "follows/getFollowRequests?searchString="
let kFacebookFriendList = kBaseUrl + "users/facebookFriendList?page="

let kEditDumps = kBaseUrl + "dumps/editDump"
let kCommentGet = kBaseUrl + "dumps/getComments?searchText"
let kAddComment = kBaseUrl + "dumps/addCommentOnDump"
let kTimeLineDump = kBaseUrl + "dumps/getDumpFeed?"
let kTimeLineDumpForProfile = kBaseUrl + "dumps/getDumpFeedForProfile?"

let kCommentedUsersList = kBaseUrl + "dumps/getCommentedUsers?page="
let kRedumpUsers =  kBaseUrl + "dumps/getRedumpedUsers?page="
let kChatUsersList = kBaseUrl + "users/getChatList?isRequest="
let kDeleteMessage = kBaseUrl + "users/deleteMessage?messageId="
let kDeleteNotification = kBaseUrl + "users/deleteNotification?notificationId="
let kDeleteConversationThread = kBaseUrl + "users/deleteMessageConversation?conversationId="
let kAcceptRejectMessageRequest = kBaseUrl + "users/messageRequestAcceptReject"
let kNews  = kBaseUrl + "dumps/getNews"
let KSaveHistory = kBaseUrl + "users/saveHistory"
let KUpdateLocation = kBaseUrl + "users/updateLocation"
let KGetDumpeeProfile =  kBaseUrl + "dumps/getDumpeeProfile?dumpeeId"
let KUpdateDumpeeProfile = kBaseUrl + "dumps/updateDumpee"
var userCoordinate:CLLocationCoordinate2D!
var currentLocation:CLLocation!
//
var walkThroughStoryboard:UIStoryboard{
    return UIStoryboard(name: "WalkThrough", bundle: Bundle.main)
}
var mainStoryboard:UIStoryboard{
    return UIStoryboard(name: "Main", bundle: Bundle.main)
}
var leftMenuStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "DMLeftMenu", bundle: Bundle.main)
}()
var dashBoardStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "DashBoard", bundle: Bundle.main)
}()
var dumpFeedStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "DumpFeed", bundle: Bundle.main)
}()

var newsStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "News", bundle:Bundle.main)
}()

var leaderBoardStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "LeaderBoard", bundle: Bundle.main)
}()

var profileStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "Profile", bundle: Bundle.main)
}()

var connectionStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "Connections", bundle: Bundle.main)
}()
var dumpDetailStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "DumpDetail", bundle: Bundle.main)
}()
var chatStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "Chat", bundle: Bundle.main)
}()
var directMessageStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "DirectMessage", bundle: Bundle.main)
}()
var notificationListStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "NotificationList", bundle: Bundle.main)
}()

var settingsStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "Settings", bundle: Bundle.main)
}()

var searchStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "SearchBar", bundle: Bundle.main)
}()
var accessToken:String{
    get{
        guard let accessToken = UserDefaults.DMDefault(objectForKey: kAuthTokenKey) as? String else { return "" }
        return accessToken
    }
    set{
        UserDefaults.DMDefault(setObject: newValue, forKey: kAuthTokenKey )
    }
}
var isLogin:Bool{
    get{
        return accessToken.isEmpty ? false:true
    }
}
var isProfileUpdate:Bool{
    get{
        return userModel?.isProfileUpdate ?? false
    }
}
var isAccountVerified:Bool {
    get{
        return userModel?.isVerified ?? false
    }
}
var userModel:DMUserModel?{
    get{
        guard let model = UserDefaults.DMDefault(DMUserModel.self, forKey: kUserDataKey) else { return nil }
        return model
    }
    set{
        guard let obj = newValue else { return  }
        UserDefaults.DMDefault(set: obj, forKey: kUserDataKey)
    }
}


var deviceToken:String{
    set{
        UserDefaults.DMDefault(setObject: newValue, forKey: kDeviceToken)
    }
    get{
        guard let deviceToken = UserDefaults.DMDefault(objectForKey: kDeviceToken) as? String else { return "" }
        return deviceToken
    }
}

var instanceToken:String{
    set{
        UserDefaults.DMDefault(setObject: newValue, forKey: kInstanceToken)
        UserDefaults.standard.synchronize()
        
    }
    get{
        guard let instanceToken = UserDefaults.DMDefault(objectForKey: kInstanceToken) as? String else { return "" }
        return instanceToken
    }
}

var rootController:UIViewController?{
    return AppDelegate.sharedDelegate.window?.rootViewController
}
var currentController:UIViewController?{
    
    if let navController  =  rootController as? UINavigationController {
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return navController
        }
    }else if let sideController  =  rootController as? THSlideMenuController, let navController = sideController.mainViewController as? UINavigationController{
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return sideController
        }
    }else if let tabBarController  =  rootController as? UITabBarController, let navController = tabBarController.selectedViewController as? UINavigationController{
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return tabBarController
        }
    }
    return nil
}
var alertMessage: String? {
    didSet{
        DispatchQueue.main.async {
            guard let controller = currentController else{return}
            controller.showAlert(message: alertMessage)
        }
    }
}
var suspendMessage: String? {
    didSet{
        DispatchQueue.main.async {
            guard let controller = currentController else{return}
            controller.showAlert( message: suspendMessage, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        }
    }
}
var subscribeTopic:String?{
    get{
        guard let subscribeTopic = kUserDefaults.object(forKey: kFCMSubscriptionKey) as? String else { return "" }
        return subscribeTopic
    }
    set{
        kUserDefaults.set(newValue, forKey: kFCMSubscriptionKey)
    }
}

//MARK:- UserDefault Constant

let kUserDefaults = UserDefaults.standard
let kFCMSubscriptionKey = "FCMSubscription"
//MARK:- Userdefault Retrive data for location

func getLocation() -> String
{
    let userDefault = UserDefaults()
    let location = userDefault.object(forKey: "profileLocation") as? String
    return location ?? ""
}
func getUserCurrentLocation() ->String
{
    let userDefault = UserDefaults()
    let location = userDefault.object(forKey: "currentLocation") as? String
    return location ?? ""
}

