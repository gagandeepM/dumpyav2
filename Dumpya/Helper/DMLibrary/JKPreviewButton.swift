//
//  JKPreviewButton.swift
//  Dumpya
//
//  Created by Chandan Taneja on 27/12/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

@IBDesignable
class JKPreviewButton: UIButton {
    

    @IBInspectable public var masksToBounds : Bool = false
        {
        didSet
        {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable public var clipsToBound : Bool = false
        {
        didSet
        {
            self.clipsToBounds = clipsToBound
        }
    }
    
    
    var shouldUpdateSize: Bool = false
    
    // MARK - initilization
    override public init(frame: CGRect) {
        super.init(frame: frame)
      
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
         self.setNeedsLayout()
        print(self.frame.width)
    }
    override func setBackgroundImage(_ image: UIImage?, for state: UIControlState) {
        super.setBackgroundImage(image, for: state)
        if let image = image, image.size != imageSize {
            imageSize = image.size
            self.invalidateIntrinsicContentSize()
            shouldUpdateSize = true
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    override func setImage(_ image: UIImage?, for state: UIControlState) {
        super.setImage(image, for: state)
        if let image = image, image.size != imageSize {
            imageSize = image.size
            self.invalidateIntrinsicContentSize()
            shouldUpdateSize = true
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //h1 = h2/w2*w1
    }
    private var imageSize: CGSize = CGSize(width: 1920, height: 1080)
    
    open override var intrinsicContentSize: CGSize {
        let aspect = imageSize.width/imageSize.height
        print(self.frame.width)
        return CGSize(width: self.bounds.width, height: self.bounds.width/aspect)
    }
    
}
class PRImageView: UIImageView {
    // MARK - initilization
     private var imageSize: CGSize = CGSize(width: 1920, height: 1080)
    @IBInspectable public var masksToBounds : Bool = false
        {
        didSet
        {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable public var clipsToBound : Bool = false
        {
        didSet
        {
            self.clipsToBounds = clipsToBound
        }
    }
    
    
    var shouldUpdateSize: Bool = false
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setNeedsLayout()
        print(self.frame.width)
    }
    override init(image: UIImage?) {
        super.init(image: image)
    }
    override var image: UIImage?{
        get{
            return super.image
        }
        set{
            if let image = newValue, image.size != imageSize {
                imageSize = image.size
                self.invalidateIntrinsicContentSize()
                shouldUpdateSize = true
                self.setNeedsLayout()
                self.layoutIfNeeded()
            }
        }
    }
    open override var intrinsicContentSize: CGSize {
        let aspect = imageSize.width/imageSize.height
        print(self.frame.width)
        return CGSize(width: self.bounds.width, height: self.bounds.width/aspect)
    }
}

