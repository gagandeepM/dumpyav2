//
//  JKFacebook.swift
//  OHP
//
//  Created by Ankit chauhan on 12/05/17.
//  Copyright © 2017 Manpreet. All rights reserved.
//

import Foundation
import GoogleSignIn
import GooglePlacePicker
import GoogleMaps

import Firebase
import GooglePlaces

typealias JKGoogleManagerSuccess = (_ user: DMGoogleUser)->Void
typealias JKGoogleManagerFailure=(_ error:Error?)->Void
typealias JKGoogleManagerOnCancel=(_ isCancel:Bool)->Void
typealias JKGoogleManagerOnPlacePicker=(_ place: GMSPlace?,_  error:Error?, _ isCancel:Bool)->Void
class JKGoogleManager: NSObject
{
    var currentController:UIViewController!
    var successBlock:JKGoogleManagerSuccess!
    var failureBlock : JKGoogleManagerFailure!
    var cancelBlock: JKGoogleManagerOnCancel!
    var googlePlaceHanlder :JKGoogleManagerOnPlacePicker!
    class var shared:JKGoogleManager{
        
        struct  Singlton{
            
            static let instance = JKGoogleManager()
            
        }
        
        return Singlton.instance
    }
    func GSSetup(){
        // Initialize sign-in
        //"AIzaSyCOb1IAjpqN0EdDxJIbsyySqtod0f8fEwI"
        
       
        let kapiKey :String =  FirebaseApp.app()?.options.apiKey == "AIzaSyCPQKV96v7N0BWxqo2DOgKmM2wo8HcJnT0" ? (FirebaseApp.app()?.options.apiKey)! : "AIzaSyCPQKV96v7N0BWxqo2DOgKmM2wo8HcJnT0"
        GMSPlacesClient.provideAPIKey(kapiKey)
        GMSServices.provideAPIKey(kapiKey)
        
        var configureError: NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        GIDSignIn.sharedInstance().clientID = "518587413677-26ao0dkv10eb7irkm9aeebh63qu3jmfo.apps.googleusercontent.com" //FIRApp.defaultApp()?.options.clientID
        //"518587413677-8sejr631vca4655obeh2tsbo5m8mrjln.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        
    }
    //handleURL:(NSURL *)url
  
    func handle (url:URL,options: [UIApplicationOpenURLOptionsKey : Any])->Bool{
       return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
    }
    func googleSingOut(){
        if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            GIDSignIn.sharedInstance().signOut()
        }
    }
    func GIDSignInUI(from viewController:UIViewController,onSuccess:@escaping JKGoogleManagerSuccess,onCancel:@escaping JKGoogleManagerOnCancel,onFailure:@escaping JKGoogleManagerFailure){
        GIDSignIn.sharedInstance().uiDelegate = self
        currentController = viewController
        successBlock = onSuccess
        failureBlock = onFailure
        cancelBlock = onCancel
        GIDSignIn.sharedInstance().signIn()
    }
    
    func showGooglePlacePicker(from controller:UIViewController! ,center:CLLocationCoordinate2D!,callback:@escaping JKGoogleManagerOnPlacePicker){
        googlePlaceHanlder = callback
        if (center != nil) {
            
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
                                                   longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
                                                   longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            
            let placePicker = GMSPlacePickerViewController(config:config)// GMSPlacePicker(config: config)
            placePicker.delegate = self
            placePicker.navigationController?.navigationBar.barTintColor = UIColor.red
            placePicker.navigationController?.navigationBar.isTranslucent = false
            
            //controller.present(placePicker, animated: true, completion: nil)
            controller.present(placePicker, animated: true, completion: {
                ServerManager.shared.hideHud()
            })
            //placePicker.pickPlace(callback: callback)
        }else{
            ServerManager.shared.hideHud()
        }
    }
    func fetchloaction(from coordinate: CLLocationCoordinate2D, completion: @escaping (_ city: String?, _ country:  String?,_ state:String?, _ error: Error?) -> ()) {
        let location : CLLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,placemarks?.first?.administrativeArea,
                       error)
        }
    }
}

extension JKGoogleManager : GMSPlacePickerViewControllerDelegate {
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
       
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: {
            // Create the next view controller we are going to display and present it.
            if (self.googlePlaceHanlder != nil) {
                self.googlePlaceHanlder(place,nil,false)
            }
            
        })
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        // In your own app you should handle this better, but for the demo we are just going to log
        // a message.
        ServerManager.shared.hideHud()
        if (self.googlePlaceHanlder != nil) {
            googlePlaceHanlder(nil,error,true)
        }
        NSLog("An error occurred while picking a place: \(error)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        NSLog("The place picker was canceled by the user")
        ServerManager.shared.hideHud()
        if (self.googlePlaceHanlder != nil) {
            googlePlaceHanlder(nil,nil,true)
        }
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension JKGoogleManager:GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error != nil) {
            failureBlock(error)
        }else{
            
            let strImg = user?.profile.imageURL(withDimension: 200).absoluteString
            let userGmailId = user?.userID ?? ""
            let email = user?.profile.email ?? ""
            let givenName = user?.profile.givenName ?? ""
            let name = user?.profile.name ?? ""
            let user = DMGoogleUser(email: email, name: name, hasImage: strImg, userGmailID: userGmailId, givenName: givenName)
            successBlock(user)
        }
        // Perform any operations on signed in user here.
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        if (error != nil) {
            failureBlock(error)
        }else{
            failureBlock(nil)
        }
    }
}

extension JKGoogleManager:GIDSignInUIDelegate{
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if (error != nil) {
            failureBlock(error)
        }else{
            failureBlock(nil)
        }
        
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        if (currentController != nil)
        {
            currentController.present(viewController, animated: true, completion: nil)
        }
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        cancelBlock(true)
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
}
struct DMGoogleUser {
    var email:String?
    var name:String?
    var hasImage:String?
    var userGmailID:String?
    var givenName:String?
  
}

extension Collection where Element == GMSAddressComponent {
    var streetAddress: String? {
        return "\(valueForKey(key: "street_number") ?? "") \(valueForKey(key: kGMSPlaceTypeRoute) ?? "")"
    
    }
    var city: String? {
        return valueForKey(key: kGMSPlaceTypeLocality)
    }
    
    var state: String? {
        return valueForKey(key: kGMSPlaceTypeAdministrativeAreaLevel1)
    }
    
    var zipCode: String? {
        return valueForKey(key: kGMSPlaceTypePostalCode)
    }
    
    var country: String? {
        return valueForKey(key: kGMSPlaceTypeCountry)
    }
    
    func valueForKey(key: String) -> String? {
        return filter { $0.type == key }.first?.name
    }
    var isEmptyAddress:Bool{
        let country:String = self.country ?? ""
        let state:String = self.state ?? ""
        let city:String = self.city ?? ""
        return country.isEmpty && state.isEmpty && city.isEmpty
    }
}
extension String{
    func isEqual(to string:String)->Bool{
        return !self.isEmpty && self.compare(string) == .orderedSame ? true : false
    }
    var isUnitedStates:Bool{
    return self.isEqual(to: "United States")
    }
    var isIndia:Bool{
        return self.isEqual(to: "India")
    }
}


extension JKGoogleManager{
    
    func deeplinking(dumpId:String,dumpeeName:String = "",content:String = "",media:String = "",viewController:UIViewController,copyLink:Bool = false){
        let urlString = "\(kDeepLinkUrl)\(dumpId)"
        guard let link = URL(string:urlString ) else { return }
        let dynamicLinksDomain = kDynamicDomainLink
        let linkBuilder = DynamicLinkComponents(link: link, domain: dynamicLinksDomain)
        let bundleId = Bundle.kBundleID.isEmpty ?  "com.Bertrand.dumpya" :  Bundle.kBundleID
        linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleId)
        linkBuilder.iOSParameters?.appStoreID = "1034690299"
        
        linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.dumpya")
        linkBuilder.androidParameters?.minimumVersion = Int(5.0)
        
        linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder.socialMetaTagParameters?.title = "Dumped \(dumpeeName)"
        linkBuilder.socialMetaTagParameters?.descriptionText = content
        linkBuilder.socialMetaTagParameters?.imageURL =  URL(string: media)
        
        guard let longDynamicLink = linkBuilder.url else { return }
        DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
            guard let url = url, error == nil else { return }
        }
     
        linkBuilder.options = DynamicLinkComponentsOptions()
        linkBuilder.options?.pathLength = .short
        if copyLink{
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                guard let url = url, error == nil else { return }
                let objectsToShare = [url] as [Any]
                UIPasteboard.general.urls = objectsToShare as? [URL]
            }
        }else{
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                guard let shortUrl = url else { return }
                debugPrint("The short URL is: \(shortUrl)")
                let activityViewController = UIActivityViewController(activityItems: ["\(shortUrl)"], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = viewController.view
                viewController.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = { activity, success, items, error in
                    debugPrint("activity: \(String(describing: activity)), success: \(success), items: \(String(describing: items)), error: \(String(describing: error?.localizedDescription))")
                    if success{
                        debugPrint("SuCCEss Print")
                    }
                }
                viewController.present(activityViewController, animated: true, completion: nil )
            }
        }
       
    }
    
}
