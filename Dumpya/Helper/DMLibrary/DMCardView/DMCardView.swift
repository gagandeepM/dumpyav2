//
//  DMCardView.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class DMCardView: UIControl{
    
    /// The color of the drop-shadow, defaults to black.
    @IBInspectable public var shadowColor: UIColor = UIColor.black {
        didSet {
            
            if isShadow == true {
                layer.shadowColor = shadowColor.cgColor
            }
            self.setNeedsDisplay()
        }
    }
    /// Whether to display the shadow, defaults to false.
    @IBInspectable public var isShadow: Bool = false{
        didSet{
            self.setNeedsDisplay()
        }
    }
    /// The opacity of the drop-shadow, defaults to 0.5.
    @IBInspectable public var shadowOpacity: Float = 0.5 {
        didSet {
            if isShadow == true {
                layer.shadowOpacity = shadowOpacity
            }
            
            self.setNeedsDisplay()
        }
    }
    /// The x,y offset of the drop-shadow from being cast straight down.
    @IBInspectable public var shadowOffset: CGSize = CGSize(width:0,height:3) {
        didSet {
            if isShadow == true {
                layer.shadowOffset = shadowOffset
            }
            self.setNeedsDisplay()
        }
    }
    /// The blur radius of the drop-shadow, defaults to 3.
    @IBInspectable public var shadowRadius : CGFloat = 3.0
        {
        didSet
        {   if isShadow == true {
            layer.shadowRadius = shadowRadius
            }
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 2.5 {
        didSet {
            layer.cornerRadius = cornerRadius
            
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var borderColor: UIColor =  UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
            self.setNeedsDisplay()
            
        }
    }
    @IBInspectable public var borderWidth: CGFloat =  0 {
        didSet {
            layer.borderWidth = borderWidth
            self.setNeedsDisplay()
            
        }
    }
    @IBInspectable public var masksToBounds : Bool = false
        {
        didSet
        {
            if isShadow == true {
                layer.masksToBounds = false
            }
            else{
                layer.masksToBounds = masksToBounds
            }
            self.setNeedsDisplay()
        }
        
    }
    @IBInspectable public var clipsToBound : Bool = false
        {
        didSet
        {
            self.clipsToBounds = clipsToBound
            self.setNeedsDisplay()
        }
    }
    
    
    
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if isShadow == true
        {
            
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            layer.masksToBounds = masksToBounds
            layer.shadowColor = shadowColor.cgColor
            layer.shadowOffset = shadowOffset
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = shadowPath.cgPath
        }
        else{
            // Disable the shadow.
            layer.shadowRadius = 0
            layer.shadowOpacity = 0
        }
        self.setNeedsDisplay()
    }
    
    
}
/// Clear the child views.
/// Clear the child views.
extension CALayer {
    func rectCornerRadius(rectCornerRadiusType type: RectCornerRadiusType, cornerRadius radius: CGFloat = 3.0) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: type.rectCorner,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        mask = shape
    }
}
/// Clear the child views.
public  extension UIView {
    
    public  func clearChildViews(){
        subviews.forEach({ $0.removeFromSuperview() })
    }
    
    func rectCornerRadius(rectCornerRadiusType type: RectCornerRadiusType, cornerRadius radius: CGFloat = 3.0) {
        self.layer.rectCornerRadius(rectCornerRadiusType: type, cornerRadius: radius)
        self.setNeedsDisplay()
    }
}


@IBDesignable
class DMGradientCard: UIView {
    @IBInspectable public var isGradient : Bool = false{
        didSet{
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var isHorizontalGradient : Bool = true{
        didSet{
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var firstColor : UIColor = DMColor.DeepOrange{
        didSet{
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var secondColor : UIColor = DMColor.yellowColor{
        didSet{
            self.setNeedsDisplay()
        }
    }
    lazy  var gradientLayer: CAGradientLayer = {
        return CAGradientLayer()
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    func set(gradientColors firstColor:UIColor , secondColor:UIColor){
        self.firstColor = firstColor
        self.secondColor = secondColor
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isGradient == true
        {
            gradientLayer.frame = self.bounds
            gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
            if isHorizontalGradient{
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            }
            self.layer.insertSublayer(gradientLayer, at: 0)
            //            self.layer.addSublayer(gradientLayer)
        }
        else{
            gradientLayer.removeFromSuperlayer()
        }
        self.setNeedsDisplay()
    }
}


@IBDesignable
class DMNavigationController: UINavigationController {
    
    @IBInspectable public var isGradient : Bool = false{
        didSet{
            self.view.setNeedsDisplay()
        }
    }
    @IBInspectable public var isHorizontalGradient : Bool = true{
        didSet{
            self.view.setNeedsDisplay()
        }
    }
    @IBInspectable public var firstColor : UIColor = DMColor.DeepOrange{
        didSet{
            self.view.setNeedsDisplay()
        }
    }
    @IBInspectable public var secondColor : UIColor = DMColor.yellowColor{
        didSet{
            self.view.setNeedsDisplay()
        }
    }
    lazy  var gradientLayer: CAGradientLayer = {
        return CAGradientLayer()
    }()
    override func loadView() {
        super.loadView()
    }
    
    var gradientImage:UIImage?{
        let updatedFrame = self.navigationBar.bounds
        //updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        if isHorizontalGradient {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let image = self.gradientImage
        self.navigationBar.setBackgroundImage(image, for: .default)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

