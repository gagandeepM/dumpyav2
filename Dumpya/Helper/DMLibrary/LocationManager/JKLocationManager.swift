//
//  JKLocationManager.swift
//  JKMaterialKit
//
//  Created by Jitendra Kumar on 20/01/17.
//  Copyright © 2017 Jitendra. All rights reserved.
//

import UIKit
import CoreLocation
typealias JKUpdateLocationsHanlder = ( _ locations: [CLLocation],_ manager: CLLocationManager)->Void
typealias JKFailureLocationsHanlder = (_ error :Error,_ manager: CLLocationManager)->Void

class JKLocationManager: NSObject {
    
    var updateLocationBlock : JKUpdateLocationsHanlder!
    var failureLocationBlovk :JKFailureLocationsHanlder!
    var locationManager: CLLocationManager!
    class var shared:JKLocationManager {
        struct Singleton {
            static let instance = JKLocationManager()
        }
        return Singleton.instance
    }
    func  setuplocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    func stopUpdatingLocation(){
         locationManager.stopUpdatingLocation()
    }
    func updateLocations(didUpdateLocarionCompletion:@escaping JKUpdateLocationsHanlder,didFailWithErrorCompletion:@escaping JKFailureLocationsHanlder){
        
        updateLocationBlock = didUpdateLocarionCompletion
        failureLocationBlovk = didFailWithErrorCompletion
    }
}
extension JKLocationManager:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (self.updateLocationBlock != nil)
        {
            self.updateLocationBlock(locations,manager)
        }
    }
    
    var checkEnableLocation:Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                // restricted by e.g. parental controls. User can't enable Location Services
                self.loacationAlert(message: "enablelocation".localized)
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                if #available(iOS 9.0, *) {
                    locationManager.requestLocation()
                } else {
                    // Fallback on earlier versions
                    locationManager.startUpdatingLocation()
                }
                return true
            }
        } else {
            
            // restricted by e.g. parental controls. User can't enable Location Services
            self.loacationAlert(message:"enablelocation".localized)
            return false
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
                locationManager.startUpdatingLocation()
            }
            break
        case .authorizedAlways:
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
                locationManager.startUpdatingLocation()
            }
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            self.loacationAlert(message: "restrictLocation".localized)
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            
            self.loacationAlert(message: "deniedLocation".localized)
            break
            
        }
    }
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if (self.failureLocationBlovk != nil)
        {
            self.failureLocationBlovk(error,manager)
        }
    }
    
    func loacationAlert(message:String){
        if let controller  =  currentController {
            controller.showAlertAction( message: message, cancelTitle: "OK", otherTitle: "Settings") { (index) in
                if index == 2{
                    guard let url = URL(string: UIApplicationOpenSettingsURLString) else{return}
                    if #available(iOS 10, *) {
                        let open = UIApplication.shared.canOpenURL(url)
                        if  open {
                            
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                        
                        
                    }else{
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
        
    }
    
    
    
}
