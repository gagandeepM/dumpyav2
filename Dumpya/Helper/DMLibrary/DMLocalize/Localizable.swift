//
//  String+Localizable.swift
//  Localizable
//
//  Created by Roman Sorochak <roman.sorochak@gmail.com> on 6/23/17.
//  Copyright © 2017 MagicLab. All rights reserved.
//

import UIKit
import UserNotifications

 let appleLanguagesKey = "AppleLanguages"


enum Language: String {
    
    case english = "en"
    case french = "fr"
    var name:String{
        switch self {
        case .french:
            return "french".localized
        default:
            return "english".localized
        }
    }
    var semantic: UISemanticContentAttribute {
        switch self {
        case .english, .french:
            return .forceLeftToRight
        }
    }
    
    static var language: Language {
        get {
            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
                let language = Language(rawValue: languageCode) {
                return language
            } else {
                let preferredLanguage = NSLocale.preferredLanguages[0] as String
                let index = preferredLanguage.index(
                    preferredLanguage.startIndex,
                    offsetBy: 2
                )
                guard let localization = Language(
                    rawValue: preferredLanguage.substring(to: index)
                    ) else {
                        return Language.english
                }
                
                return localization
            }
        }
        set {
            guard language != newValue else {
                return
            }
            
            //change language in the app
            //the language will be changed after restart
            UserDefaults.standard.set([newValue.rawValue], forKey: appleLanguagesKey)
            UserDefaults.standard.synchronize()
            
            //Changes semantic to all views
            //this hack needs in case of languages with different semantics: leftToRight(en/uk) & rightToLeft(ar)
            UIView.appearance().semanticContentAttribute = newValue.semantic
        
        }
    }
}


extension String {
    
    var localized: String {
        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
   
//    var localized: String {
//        return NSLocalizedString(self, comment: "")
//    }
//
}

extension Bundle {
    //Here magic happens
    //when you localize resources: for instance Localizable.strings, images
    //it creates different bundles
    //we take appropriate bundle according to language
    static var localizedBundle: Bundle {
        let languageCode = Language.language.rawValue
        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
            return Bundle.main
        }
        return Bundle(path: path)!
    }
}

struct LocalNotification {
    
   static func sent(title:String = "",subtitle:String = "", message:String,badge: NSNumber = 0,identifier:String = kAppLanguageNotificationIdentifier)  {
       let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.body = message
        content.badge = badge
        //getting the notification trigger
        //it will be called after 5 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval:1, repeats: false)
        //getting the notification request
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error)in
            
        })
    }
}

struct ChangeLanguageAlert {
    
    
    
    static  func showAction(language:Language,onCompletion:@escaping ()->()){
        let title = "changeLanguage".localized + " \(language.name)?"
        let message = "\(kAppTitle) " + "restartApp".localized + "your".localized + " \(kAppTitle) " + "independentLanguage".localized
       
        let changeButton = "changeTo".localized + " \(language.name)"
        let cancelButton = "cancel".localized
        currentController?.showAlertAction(title: title, message: message, cancelTitle: cancelButton, otherTitle: changeButton, onCompletion: ({ (index) in
            if index == 2{
                Language.language = language
                
                onCompletion()
            }
        }))
    }
}
