//
//  DMPlayerCard.swift
//  Dumpya
//
//  Created by Chandan Taneja on 11/04/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit
import AVKit
class DMPlayerCard: DMCardView {
    
    fileprivate var videoUrl:URL?
    fileprivate lazy var avPlayer: AVPlayer = {
        return AVPlayer()
    }()
    fileprivate lazy var avPlayerLayer: AVPlayerLayer = {
        return AVPlayerLayer(player: avPlayer)
    }()
    fileprivate lazy var avPlayerViewConroller: AVPlayerViewController = {
        return AVPlayerViewController()
    }()
    var isPlay:Bool = false{
        didSet{
            if (videoUrl != nil) {
                if isPlay {
                    self.play()
                }else{
                    self.pause()
                }
               
            }
            
        }
    }
    
    var isEnablePlaybackControls:Bool = true{
        didSet{
            
            avPlayerViewConroller.showsPlaybackControls = isEnablePlaybackControls
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        // self.setupPlayer()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       DispatchQueue.main.async {
            self.setupPlayer()
        }
    }
    func setupPlayer(){
        
        
        avPlayerLayer.videoGravity = .resizeAspect
        avPlayerViewConroller.player = avPlayer
        guard let avPlayerView =  avPlayerViewConroller.view else { return }
        //avPlayerView.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(avPlayerView, at: 0)
        //        avPlayerView.topAnchor.constraint(equalTo:    self.topAnchor).isActive = true
        //        avPlayerView.leadingAnchor.constraint(equalTo:   self.leadingAnchor).isActive = true
        //        avPlayerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        //        avPlayerView.trailingAnchor.constraint(equalTo:  self.trailingAnchor).isActive = true
        
        self.setNeedsLayout()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let avPlayerView =  avPlayerViewConroller.view else { return }
        avPlayerView.frame = self.frame
        self.setNeedsDisplay()
    }
    func initNewPlayerItem(videoItemUrl:URL?) {
        
        // Pause the existing video (if there is one)
        videoUrl = videoItemUrl
        if avPlayer.isPlaying {
            self.isPlay = false
        }
        
        // First we need to make sure we have a valid URL
        guard let videoPlayerItemUrl = videoItemUrl else {
            return
        }
        // Create a new AVAsset from the URL
        let videoAsset = AVAsset(url: videoPlayerItemUrl)
        
        videoAsset.loadValuesAsynchronously(forKeys: ["duration"]) {
            guard videoAsset.statusOfValue(forKey: "duration", error: nil) == .loaded else {
                return
            }
            
            let videoPlayerItem = AVPlayerItem(asset: videoAsset)
            DispatchQueue.main.async {
                self.avPlayer.replaceCurrentItem(with: videoPlayerItem)
                
            }
        }
    }
    func play(){
        if !self.avPlayer.isPlaying {
            self.avPlayer.play()
            
        }
        
    }
    func pause(){
        if self.avPlayer.isPlaying {
            self.avPlayer.pause()
            self.avPlayerLayer.player = nil
            
        }
        
    }
    
    deinit {
        if avPlayerViewConroller.isBeingDismissed  {
            avPlayerViewConroller.player = nil
            
        }
    }
}
extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
