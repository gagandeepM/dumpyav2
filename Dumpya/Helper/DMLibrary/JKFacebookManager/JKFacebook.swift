//
//  JKFacebook.swift
//  Caveman
//
//  Created by Jitendra Kumar on 03/11/16.
//  Copyright © 2016 AsthaSachdeva. All rights reserved.
//

import UIKit
import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import Social
enum FBPostType : Int {
    
    case FBPostTypeStatus = 0
    case FBPostTypePhoto
    case FBPostTypeLink
    case FBPostTypeVideo
    
}
enum FBAlbumPrivacyType : Int
{
    case FBAlbumPrivacyEveryone
    case FBAlbumPrivacyAllFriends
    case FBAlbumPrivacyFriendsOfFriends
    case FBAlbumPrivacySelf
    
    
}

enum FBReadPermission:String {
    case email = "email"
    case profile = "public_profile"
    case friends = "user_friends"
    
}
enum FBPublicPermission:String {
    case publishActions = "publish_actions"
}
typealias JKFacebookCallback = (_ success:Bool,_ result:Any)->Void

class JKFacebook: NSObject  {
    
    var inviteCallcack:JKFacebookCallback?
    var sharedCallcack :JKFacebookCallback?
    var postType : FBPostType?
    var isPublicPermission:Bool = false
    var readPermissions:[String]! = [FBReadPermission.email.rawValue,FBReadPermission.profile.rawValue,FBReadPermission.friends.rawValue]
    var publishPermissions:[String]!  = [String]()
    
    //MARK:- Singleton
    class var shared : JKFacebook {
        struct Static {
            static let instance : JKFacebook = JKFacebook()
        }
        
        return Static.instance
    }
    lazy var loginManager : FBSDKLoginManager = {
        let loginmanager = FBSDKLoginManager()
        return loginmanager
    }()
   
    lazy var isFacebookInstalled :Bool = {
        return UIApplication.shared.canOpenURL(NSURL(string: "fb://")! as URL)
    }()
    func isSessionValid()->Bool {
        
        return (FBSDKAccessToken.current() != nil) ? true:false
    }
    //MARK: Public Method-
    
    //MARK:-viewFacebookInAppStore
    class func viewFacebookInAppStore()
    {
        if #available(iOS 10, *) {
           UIApplication.shared.open(URL(string: "itms-apps://itunes.apple.com/in/app/facebook/id284882215?mt=8")!, options: [:], completionHandler: nil)
        }
        else{
            UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/in/app/facebook/id284882215?mt=8")!)
        }
     
    }
    
    /**
     *  When a person logs into your app via Facebook Login you can access a subset of that person's data stored on Facebook. Permissions are how you ask someone if you can access that data. A person's privacy settings combined with what you ask for will determine what you can access.
     *
     *  Permissions are strings that are passed along with a login request or an API call. Here are two examples of permissions:
     *
     *  email - Access to a person's primary email address.
     *  user_likes - Access to the list of things a person likes.
     *
     *  https://developers.facebook.com/docs/facebook-login/permissions/v2.8
     *
     *  @param permissions
     */
    override init() {
        super.init()

    }
    class func initWithReadPermissions(readPermissions:[String], publishPermission publishPermissions:[String],isPublicPermission:Bool = false)
    {
        JKFacebook.shared.initWithReadPermissions(readPermissions: readPermissions, publishPermission: publishPermissions, isPublicPermission: isPublicPermission)
      // _ = JKFacebook(readPermissions: readPermissions, publishPermission: publishPermissions)
    }
    /**
     *  Checks if there is an open session, if it is not checked if a token is created and returned there to validate session.
     *
     *  @return BOOL
     */
    class func checkFBLoginSession()->Bool
    {
        return JKFacebook.shared.isSessionValid()
    }
    
    /**
     *  Facebook login
     *
     * https://developers.facebook.com/docs/ios/graph/v2.8
     *
     *  @param callBack (BOOL success, id result)
     */
    
    
    class func login(fromController:UIViewController ,completionHandler  CallBack:JKFacebookCallback?){
        JKFacebook.shared.login(fromController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Facebook login
     *
     * https://developers.facebook.com/docs/ios/graph/v2.8
     *  @param FBSDKLoginBehavior behavior
     *  @param callBack (BOOL success, id result)
     */
    class func loginWithBehavior(behavior:FBSDKLoginBehavior, fromViewController fromController: UIViewController ,completionHandler  CallBack:JKFacebookCallback?){
        JKFacebook.shared.loginWithBehavior(behavior: behavior, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Facebook logout
     *
     * https://developers.facebook.com/docs/ios/graph/v2.8
     *
     *  @param callBack (BOOL success, id result)
     */
    class func logout(completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.logout(completionHandler: CallBack)
    }
    
    /**
     *  Get the data from the logged in user by passing the fields.
     *
     *  https://developers.facebook.com/docs/facebook-login/permissions/v2.8#reference-public_profile
     *
     *  Permissions required: public_profile...
     *
     *  @param fields   fields example: id, name, email, birthday, about, picture
     *  @param callBack (BOOL success, id result)
     */
    class func getUserFields(fields:String!,completionHandler callBack :JKFacebookCallback?){
        
        JKFacebook.shared.getUserFields(fields: fields, completionHandler: callBack)
    }
    
    /**
     *  This will only return any friends who have used (via Facebook Login) the app making the request.
     *  If a friend of the person declines the user_friends permission, that friend will not show up in the friend list for this person.
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/user/friends/
     *
     *  Permissions required: user_friends
     *
     *  @param callBack (BOOL success, id result)
     */
    class func getUserFriends(fromController:UIViewController ,completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.getUserFriends(fromController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post in the user profile with link and caption
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/feed
     *
     *  Permissions required: publish_actions
     *
     *  @param url      NSString
     *  @param caption  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostWithLinkPath(url:String? , withCaption caption:String?, fromViewController fromController:UIViewController,completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.feedPostWithLinkPath(url: url, withCaption: caption, withMessage: nil, withPhoto: nil, withVideo: nil,postType:FBPostType.FBPostTypeLink, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post in the user profile with message
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/feed
     *
     *  Permissions required: publish_actions
     *
     *  @param message  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostWithMessage(message:String, fromViewController fromController:UIViewController,completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.feedPostWithLinkPath(url: nil, withCaption: nil, withMessage: message, withPhoto: nil, withVideo: nil,postType:FBPostType.FBPostTypeStatus, fromViewController: fromController, completionHandler: CallBack)

    }
    /**
     *  Post in the user profile with photo and caption
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/feed
     *
     *  Permissions required: publish_actions
     *
     *  @param photo    UIImage
     *  @param caption  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostWithPhoto(photo:UIImage! , withCaption caption:String?, fromViewController fromController:UIViewController! ,completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.feedPostWithLinkPath(url: nil, withCaption: caption, withMessage: nil, withPhoto: photo, withVideo: nil,postType:FBPostType.FBPostTypePhoto, fromViewController: fromController, completionHandler: CallBack)
    }
    /**
     *  Post in the user profile with video, title and description
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/feed
     *
     *  Permissions required: publish_actions
     *
     *  @param videoData   NSData
     *  @param title       NSString
     *  @param description NSString
     *  @param callBack    (BOOL success, id result)
     */
    
    class func feedPostWithVideo(videoData:NSData! , withTitle title:String?,withDescription description:String?, fromViewController fromController:UIViewController! ,completionHandler CallBack:JKFacebookCallback?){
        JKFacebook.shared.feedPostWithLinkPath(url: nil, withCaption: title, withMessage: description, withPhoto: nil, withVideo: videoData,postType:FBPostType.FBPostTypeVideo, fromViewController: fromController, completionHandler: CallBack)
    }
 
    class func presentFacebookSLComposer(controller:UIViewController! , withAddImage image:UIImage?, withInitialText statusStr:String?, withaddURL shareUrl:NSURL?, completionHandler CallBack:JKFacebookCallback?){
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook)
        {
            let composeController : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            if (statusStr != nil) && statusStr?.isEmpty == false
            {
                composeController.setInitialText(statusStr)
            }
            if (image != nil)
            {
                composeController.add(image)
            }
            if (shareUrl != nil)
            {
                composeController.add(shareUrl! as URL!)
            }
            controller.present(composeController, animated: true, completion: nil)
            composeController.completionHandler = {
                (result:SLComposeViewControllerResult) in
                // Your code
                switch result {
                case .done:
                    CallBack!(false,"post Cancelled")
                    break
                default:
                    CallBack!(true,"Image successfully uploaded on Facebook.")
                    break
                }
                composeController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    class func openShareDialogInViewController(currentController:UIViewController!, withPostType posttype :FBPostType!, withTitle title:String? , description caption:String?, withMedia mediaObject:AnyObject,completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.openShareDialogInViewController(currentController: currentController, withPostType: posttype, withTitle: title, description: caption, withMedia: mediaObject, completionHandler: CallBack)
    }
    
   
  
    /**
     *  The feed of posts (including status updates) and links published by this person, or by others on this person's profile.
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/feed
     *
     *  Permissions required: read_stream
     *
     *  @param callBack (BOOL success, id result)
     */
    class func myFeedCallBack(completionHandler CallBack :JKFacebookCallback?){
        
       JKFacebook.shared.myFeedCallBack(completionHandler: CallBack)
    }
    
    /**
     *  Invite friends with message via dialog
     *
     * https://developers.facebook.com/docs/graph-api/reference/v2.8/user/
     *
     *  @param appLink URL
     *  @param preview URL optional
     *  @param callBack (BOOL success, id result)
     */
    class func inviteFriendsWithAppLinkURL(url:NSURL? , previewImageURL preview:NSURL?, fromViewController fromController:UIViewController, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.inviteFriendsWithAppLinkURL(url: url, previewImageURL: preview, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Get pages in user
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: manage_pages
     *
     *  @param callBack (BOOL success, id result)
     */
    class func getPagesFromViewController(fromController:UIViewController,  completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.getPagesFromViewController(fromController: fromController, completionHandler: CallBack)
        
    }
    
    /**
     *
     *  Get page with id
     *
     *  Facebook Web address ou pageId
     *  Example http://www.lucascorrea.com/PageId.png
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: manage_pages
     *
     *  @param pageId   Facebook Web address ou pageId
     *  @param callBack (BOOL success, id result)
     */
    class func getPageById(pageId:String!,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.getPageById(pageId: pageId, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post in the page profile with message
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostForPage(page:String?, feedMessage message:String?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostForPage(page: page, feedMessage: message, fromViewController: fromController, completionHandler: CallBack)
    }
    
    
    /**
     *  Post in the page profile with message and photo
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param photo    UIImage
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostForPage(page:String?, feedMessage message:String?,feedPhoto photo:UIImage?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
          JKFacebook.shared.feedPostForPage(page: page, feedMessage: message, feedPhoto: photo, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post in the page profile with message and link
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param url      NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostForPage(page:String?, feedMessage message:String?,feedLink url:String?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
          JKFacebook.shared.feedPostForPage(page: page, feedMessage: message, feedLink: url, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post in the page profile with video, title and description
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page        NSString
     *  @param videoData   NSData
     *  @param title       NSString
     *  @param description NSString
     *  @param callBack    (BOOL success, id result)
     */
    class func feedPostForPage(page:String?, feedVideo videoData:NSData!,feedTitle title:String?,feedDescription description:String?,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?)
    {
        JKFacebook.shared.feedPostForPage(page: page, feedVideo: videoData, feedTitle: title, feedDescription: description, fromViewController: fromController, completionHandler: CallBack)
    }
    
    
    /**
     *  Post on page with administrator profile with a message
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostAdminForPageName(page:String?, feedMessage message:String?,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostAdminForPageName(page: page, feedMessage: message, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post on page with administrator profile with a message and link
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param url      NSString
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostAdminForPageName(page:String?, feedMessage message:String, feedLink url:String? ,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostAdminForPageName(page: page, feedMessage: message, feedLink: url, fromViewController: fromController, completionHandler: CallBack)
    }
    
    
    
    /**
     *  Post on page with administrator profile with a message and photo
     *
     *  Permissions required: publish_actions
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *
     *  @param page     NSString
     *  @param message  NSString
     *  @param photo    UIImage
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostAdminForPageName(page:String?, feedMessage message:String, feedPhoto photo:UIImage? ,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostAdminForPageName(page: page, feedMessage: message, feedPhoto: photo, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Post on page with administrator profile with a video, title and description
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/page
     *
     *  Permissions required: publish_actions
     *
     *  @param page        NSString
     *  @param videoData   NSData
     *  @param title       NSString
     *  @param description NSString
     *  @param callBack    (BOOL success, id result)
     */
    class func feedPostAdminForPageName(page:String!, video videoData:NSData!, withTitle title:String?, withDescription description:String?, fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostAdminForPageName(page: page, video: videoData, withTitle: title, withDescription: description, fromViewController: fromController, completionHandler: CallBack)
    }
    
    /**
     *  Get albums in user
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/user/albums
     *
     *  Permissions required: user_photos
     *
     *  @param callBack (BOOL success, id result)
     */
    class func getAlbums(completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.getAlbums(completionHandler: CallBack)
    }
    
    /**
     *  Get album with id
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/user/albums
     *
     *  Permissions required: user_photos
     *
     *  @param albumId  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func getAlbumById(albumId:String! ,completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.getAlbumById(albumId: albumId, completionHandler: CallBack)
    }
    
    /**
     *  Get photos the album with id
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/album/photos
     *
     *  Permissions required: user_photos
     *
     *  @param albumId  NSString
     *  @param callBack (BOOL success, id result)
     */
    class func getPhotosAlbumById(albumId:String! ,completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.getPhotosAlbumById(albumId: albumId, completionHandler: CallBack)
    }
    
    /**
     *  Create album the user
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/user/albums
     *
     *  Permissions required: publish_actions and user_photos
     *
     *  @param name     NSString
     *  @param message  NSString
     *  @param privacy  ENUM
     *  @param callBack (BOOL success, id result)
     */
    class func createAlbumName(name:String! , withMessage message:String?, withPrivacy privacy:FBAlbumPrivacyType? , completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.createAlbumName(name: name, withMessage: message, withPrivacy: privacy, completionHandler: CallBack)
    }
    
    /**
     *  Post the photo album in your user profile
     *
     *  https://developers.facebook.com/docs/graph-api/reference/v2.8/album/photos
     *
     *  Permissions required: publish_actions
     *
     *  @param albumId  NSString
     *  @param photo    UIImage
     *  @param callBack (BOOL success, id result)
     */
    class func feedPostForAlbumId(albumId:String!, withPhoto photo:UIImage! ,completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.feedPostForAlbumId(albumId: albumId, withPhoto: photo, completionHandler: CallBack)
    }
    
    /**
     *  Post open graph
     *
     *  Open Graph lets apps tell stories on Facebook through a structured, strongly typed API. When people engage with these stories they are directed to your app or, if they don't have your app installed, to your app's App Store page, driving engagement and distribution for your app.
     *
     *  Stories have the following core components:
     *
     *   An actor: the person who publishes the story, the user.
     *   An action the actor performs, for example: cook, run or read.
     *   An object on which the action is performed: cook a meal, run a race, read a book.
     *   An app: the app from which the story is posted, which is featured alongside the story.
     *  We provide some built in objects and actions for frequent use cases, and you can also create custom actions and objects to fit your app.
     *
     *  https://developers.facebook.com/docs/ios/open-graph
     *
     *  Permissions required: publish_actions
     *
     *  @param actionType       NSString
     *  @param graphObject      NSString
     *  @param objectName       NSString
     *  @param viewController   UIViewController
     *  @param callBack        (BOOL success, id result)
     */
    class func sendForPostOpenGraphWithActionType(actionType:String? , graphObject openGraphObject:FBSDKShareOpenGraphObject?, withObjectName objectName:String? , fromviewController viewController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.sendPostOpenGraph(actionType: actionType, graphObject: openGraphObject, withObjectName: objectName, fromviewController: viewController, completionHandler: CallBack)
    }
    
    /**
     *  If not on the list in SCFacebook method, this method can be used to make calls via the graph API GET
     *
     *  Calling the Graph API GET
     *
     *  https://developers.facebook.com/docs/ios/graph
     *
     *  @param method   NSString
     *  @param params   NSDictionary
     *  @param callBack (BOOL success, id result)
     */
    class func graphFacebookForMethodGET(method:String!, withParams params:AnyObject?, completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.graphFacebookGET(method: method, withParams: params, completionHandler: CallBack)
    }
    
    /**
     *  If not on the list in SCFacebook method, this method can be used to make calls via the graph API POST
     *
     *  Calling the Graph API POST
     *
     *  https://developers.facebook.com/docs/ios/graph
     *
     *  @param method   NSString
     *  @param params   NSDictionary
     *  @param callBack (BOOL success, id result)
     */
    class func graphFacebookForMethodPOST(method:String!, withParams params:Any? , completionHandler CallBack:JKFacebookCallback?){
        
        JKFacebook.shared.graphFacebookPOST(method: method, withParams: params, completionHandler: CallBack)
    }
    
    
    //MARK:- Private Methods
    
    //MARK:-initWithReadPermissions
//    init(readPermissions:[String], publishPermission publishPermissions:[String]) {
//        
//        self.readPermissions = readPermissions
//        self.publishPermissions = publishPermissions
//    }
    func initWithReadPermissions(readPermissions:[String], publishPermission publishPermissions:[String],isPublicPermission:Bool)
    {
        self.isPublicPermission = isPublicPermission
        self.readPermissions = [String]()
        self.readPermissions = readPermissions
        self.publishPermissions = [String]()
        self.publishPermissions = publishPermissions
    }
    func login(withReadPermissions readPermissions:[Any]!, from viewController:UIViewController,handler:
        FBSDKLoginManagerRequestTokenHandler!){
        self.loginManager.logIn(withReadPermissions: readPermissions, from: viewController, handler: handler)
    }
    func login(withPublishPermissions publicPermissions:[Any]!, from viewController:UIViewController,handler:
        FBSDKLoginManagerRequestTokenHandler!){
        self.loginManager.logIn(withPublishPermissions: publicPermissions, from: viewController, handler: handler)
        
    }

     //MARK:-login
    func login(fromController:UIViewController ,completionHandler  CallBack:JKFacebookCallback?){
        self.loginWithBehavior(behavior: .systemAccount, fromViewController: fromController, completionHandler: CallBack)
    }
     //MARK:-loginWithBehavior
    func loginIn(WithBehavior behavior:FBSDKLoginBehavior = .systemAccount, from viewController:UIViewController,handler:
        FBSDKLoginManagerRequestTokenHandler!){
            self.loginManager.loginBehavior = behavior
        if self.isPublicPermission == true {
             self.login(withPublishPermissions: self.publishPermissions, from: viewController, handler: handler!)
        }else{
             self.login(withReadPermissions: self.readPermissions, from: viewController, handler: handler!)
        }
        /*
        self.login(withPublishPermissions: self.publishPermissions, from: viewController, handler: { (result:FBSDKLoginManagerLoginResult?, error:Error?) in
            if (error != nil){
                
               handler(result,error)
            } else if (result?.isCancelled)! == false {
                
                if (result?.declinedPermissions.contains("publish_actions"))! == false {
                    
                     self.login(withReadPermissions: self.readPermissions, from: viewController, handler: handler!)
                }else{
                  handler(result,error)
                }
            }
        })
       */
    }
  
    func loginWithBehavior(behavior:FBSDKLoginBehavior = .systemAccount, fromViewController fromController: UIViewController ,completionHandler  CallBack:JKFacebookCallback?){
        
        self.loginIn(WithBehavior: behavior, from: fromController) { (result:FBSDKLoginManagerLoginResult?, error:Error?) in
            if (error != nil){
                
                CallBack!(false, error!.localizedDescription)
            } else if (result?.isCancelled)! {
                
                CallBack!(false, "User Cancelled")
                
            }else{
                if ((CallBack) != nil)
                {
                    CallBack!(true, result!)
                }
                }
        }
        
       
       
        
    }

    //MARK:-logout
    func logout(completionHandler CallBack:JKFacebookCallback?){
        self.loginManager.logOut()
        
        let cookies : HTTPCookieStorage = HTTPCookieStorage.shared
        let facebookCookies:[HTTPCookie] = cookies.cookies(for: NSURL(string: "https://facebook.com/")! as URL)!
        
        for cookie : HTTPCookie in facebookCookies
        {
            cookies.deleteCookie(cookie)
            
        }
        CallBack!(true, "Logout successfully")
        
    }
        //MARK:-getUserFields
    func getUserFields(fields:String!,completionHandler CallBack :JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        let param :[String:Any] = ["fields" : fields!]
        self.graphFacebookGET(method: "me", withParams: param , completionHandler: CallBack)
    }
    
    //MARK:-getUserFriends
    func getUserFriends(fromController:UIViewController ,completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        
        if FBSDKAccessToken.current().hasGranted("user_friends")
        {
            self.graphFacebookGET(method: "me/friends", withParams: nil, completionHandler: CallBack)
        }
        else {
            

            self.loginIn(WithBehavior: .systemAccount, from: fromController, handler: { (result:FBSDKLoginManagerLoginResult?, error:Error?) in
                if (error != nil){
                    
                    CallBack!(false, error!.localizedDescription)
                    
                } else if (result?.isCancelled)! {
                    
                    CallBack!(false, "User Cancelled")
                    
                } else
                {
                    self.graphFacebookGET(method: "me/friends", withParams: nil, completionHandler: CallBack)
                }
            })
           
            
            
        }
    }
    //MARK:-feedPostWithLinkPath(url:String? , withCaption caption:String?,withMessage message:String?, withPhoto photo :UIImage?, withVideo videoData:NSData?,postType:FBPostType)
    func feedPostWithLinkPath(url:String? , withCaption caption:String?,withMessage message:String?, withPhoto photo :UIImage?, withVideo videoData:NSData?,postType:FBPostType , fromViewController fromController:UIViewController,completionHandler CallBack:JKFacebookCallback?){
        
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in")
            return
        }
        self.postType = postType
        var params = [String:Any]()
        //Need to provide POST parameters to the Facebook SDK for the specific post type
        var graphPath = "me/feed"
        let type = self.postType!
        
        switch type
        {
        case .FBPostTypeLink:
            
            let link : String! = ((url?.isEmpty) != nil) ? url!  : ""
            let caption : String! = ((caption?.isEmpty) != nil) ? caption:  ""
            params = ["link" : link ,"description":caption ]
            
            break
           
        case .FBPostTypePhoto:
            graphPath = "me/photos"
            let photoData : NSData! = UIImagePNGRepresentation(photo!) as NSData!
            let message : String! = ((message?.isEmpty) != nil) ? message!  : ""
            params = ["source":photoData,"message" : message ]
            
            break
            
        case .FBPostTypeVideo:
            
            graphPath = "me/videos"
            
            if (videoData == nil) {
                CallBack!(false, "Not logged in" )
                return
            }
            
            let message : String! = ((message?.isEmpty) != nil) ? message!  : ""
            let caption : String! = ((caption?.isEmpty) != nil) ? caption:  ""
            params = ["video.mp4":videoData!,"title" :caption ,"description": message ]
            
            break
            
        default:
            
            //.FBPostTypeStatus:
            let message : String! = ((message?.isEmpty) != nil) ? message!  : ""
            params = ["message" : message ]
            break
        }
        if FBSDKAccessToken.current().hasGranted("user_friends")
        {
            self.graphFacebookPOST(method: graphPath, withParams: params, completionHandler: CallBack)
        }
        else
        {
            
            self.loginIn(WithBehavior: .systemAccount, from: fromController, handler: { (result:FBSDKLoginManagerLoginResult?, error:Error?) in
                if (error != nil){
                    
                    CallBack!(false, error!.localizedDescription)
                    
                } else if (result?.isCancelled)! {
                    
                    CallBack!(false, "User Cancelled")
                    
                } else
                {
                    self.graphFacebookPOST(method: graphPath, withParams: params, completionHandler: CallBack)
                }
            })
            
        }
        
        
    }
    //MARK:-feedPostWithLinkPath
    func feedPostWithLinkPath(url:String? , withCaption caption:String?, fromViewController fromController:UIViewController,completionHandler CallBack:JKFacebookCallback?)
    {
        self.feedPostWithLinkPath(url: url, withCaption: caption, withMessage: nil, withPhoto: nil, withVideo: nil, postType: .FBPostTypeLink, fromViewController: fromController, completionHandler: CallBack)
    }
      //MARK:-feedPostWithMessage
    func feedPostWithMessage(message:String, fromViewController fromController:UIViewController,completionHandler CallBack:JKFacebookCallback?){
         self.feedPostWithLinkPath(url: nil, withCaption: nil, withMessage: message, withPhoto: nil, withVideo: nil, postType: .FBPostTypeStatus, fromViewController: fromController, completionHandler: CallBack)
    }
    

 
    //MARK:-feedPostWithPhoto
    func feedPostWithPhoto(photo:UIImage! , withCaption caption:String?, fromViewController fromController:UIViewController! ,completionHandler CallBack:JKFacebookCallback?){
        self.postType = FBPostType.FBPostTypePhoto
        
         self.feedPostWithLinkPath(url: nil, withCaption: caption, withMessage: nil, withPhoto: photo, withVideo: nil, postType: .FBPostTypePhoto, fromViewController: fromController, completionHandler: CallBack)
    }
    

    //MARK:-feedPostWithVideo
    func feedPostWithVideo(videoData :NSData! , feedTitle title:String?, feedDescription description:String , fromViewController fromController:UIViewController, completionHandler CallBack:JKFacebookCallback?){
         self.feedPostWithLinkPath(url: nil, withCaption: title, withMessage: description, withPhoto: nil, withVideo: videoData, postType: .FBPostTypeVideo, fromViewController: fromController, completionHandler: CallBack)
    }
    
     //MARK:-myFeedCallBack
    func myFeedCallBack(completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        
        self.graphFacebookPOST(method: "me/feed", withParams: nil, completionHandler: CallBack)
        
    }
    
      //MARK:-inviteFriendsWithAppLinkURL
    func inviteFriendsWithAppLinkURL(url:NSURL? , previewImageURL preview:NSURL?, fromViewController fromController:UIViewController, completionHandler CallBack:JKFacebookCallback?){
        
//        if isSessionValid() == false
//        {
//            CallBack!(false, "Not logged in" )
//            return
//        }
//        
        let content : FBSDKAppInviteContent = FBSDKAppInviteContent()
        content.appLinkURL = url as URL!
        
        if ((preview) != nil) {
            //optionally set previewImageURL
            content.appInvitePreviewImageURL = preview as URL!
        }
        FBSDKAppInviteDialog.show(from: fromController, with: content, delegate: self)
        
        
        if (CallBack != nil)
        {
            self.inviteCallcack = CallBack!
        }
        
        
        
    }
    
    //MARK:-getPagesFromViewController
    func getPagesFromViewController(fromController:UIViewController, completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if FBSDKAccessToken.current().hasGranted("manage_pages")
        {
            self.graphFacebookGET(method: "me/accounts", withParams: nil, completionHandler: CallBack)
        }
        else {
            
            
            self.loginIn(WithBehavior: .systemAccount, from: fromController, handler: { (result:FBSDKLoginManagerLoginResult?, error:Error?) in
                if (error != nil){
                    
                    CallBack!(false, error!.localizedDescription)
                    
                } else if (result?.isCancelled)! {
                    
                    CallBack!(false, "User Cancelled")
                    
                } else
                {
                   self.graphFacebookGET(method: "me/accounts", withParams: nil, completionHandler: CallBack)
                }
            })

            
            
            
        }
        
    }
    
    //MARK:-getPageById
    func getPageById(pageId:String!,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        
        self.graphFacebookGET(method: pageId, withParams: nil, completionHandler: CallBack)
        
        
    }
    
    //MARK:-feedPostForPage(page:String?, feedMessage message:String?)
    func feedPostForPage(page:String?, feedMessage message:String?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (page == nil) {
            CallBack!(false, "Page id or name required" )
            return
        }
        if (message == nil) {
            CallBack!(false, "message required" )
            return
        }
        
        self.graphFacebookPOST(method: "\(page!)/feed", withParams: ["message": message! ] as [String:Any], completionHandler: CallBack)
        
    }
    //MARK:-feedPostForPage(page:String?, feedMessage message:String?,feedPhoto photo:UIImage?)
    func feedPostForPage(page:String?, feedMessage message:String?,feedPhoto photo:UIImage?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (page == nil) {
            CallBack!(false, "Page id or name required" )
            return
        }
        if (message == nil) {
            CallBack!(false, "message required" )
            return
        }
        if (photo == nil) {
            CallBack!(false, "photo required" )
            return
        }
        let param :[String:Any] = ["message": message! ,"source" :UIImagePNGRepresentation(photo!)! ]
        self.graphFacebookPOST(method: "\(page!)/photos", withParams:param , completionHandler: CallBack)
        
    }
    
    //MARK:-feedPostForPage(page:String?, feedMessage message:String?,feedLink url:String?)
    func feedPostForPage(page:String?, feedMessage message:String?,feedLink url:String?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (page == nil) {
            CallBack!(false, "Page id or name required")
            return
        }
        if (message == nil) {
            CallBack!(false, "message required")
            return
        }
        if (url == nil) {
            CallBack!(false, "post Link required")
            return
        }
        let param :[String:Any] = ["message": message! ,"link" :url!]
        self.graphFacebookPOST(method: "\(page!)/feed", withParams:param, completionHandler: CallBack)
        
    }
    //MARK:-feedPostAdminForPageName(page:String?, feedVideo videoData:NSData!,feedTitle title:String?,feedDescription description:String?)
    func feedPostForPage(page:String?, feedVideo videoData:NSData!,feedTitle title:String?,feedDescription description:String?,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?)
    {
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (page == nil) {
            CallBack!(false, "Page id or name required" )
            return
        }
        
        if (videoData == nil) {
            CallBack!(false, "Post video required" )
            return
        }
        
        let title : String! = ((title?.isEmpty) != nil) ? title!  : ""
        let description : String! = ((description?.isEmpty) != nil) ? description:  ""
        
        let param = ["video.mp4":videoData!,"title" :title,"description": description] as [String : Any]
        self.graphFacebookPOST(method: "\(page!)/videos", withParams:param, completionHandler: CallBack)
        
        
    }
     //MARK:-feedPostAdminForPageName(page:String?, feedMessage message:String?)
    func feedPostAdminForPageName(page:String?, feedMessage message:String?,fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        self.getPagesFromViewController(fromController: fromController) { (success, result) in
            
            if success
            {
                var dicPageAdmin:[String:Any]?
                if let resonseDict: [String:AnyObject] = result as? [String:AnyObject]{
                    if let listData = resonseDict["data"] as? [AnyObject]{
                        for dic in listData
                        {
                            if let pageStr = dic["name"] as? String
                            {
                                if pageStr == page{
                                    dicPageAdmin = (dic as? [String:Any])!
                                    break
                                }
                                
                            }
                        }
                    }
                }
                if dicPageAdmin == nil{
                    CallBack!(false, "Page not found!" )
                    return
                }
                
                let request:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(String(describing: dicPageAdmin!["id"]))/feed", parameters: ["message" : message!], httpMethod: "POST")
                request.start(completionHandler: { (connection:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
                    if (error != nil){
                        CallBack!(false, error!.localizedDescription)
                    }
                    else{
                        CallBack!(true, result!)
                    }
                })
                
            }
        }
        
        
        
    }
   //MARK:-feedPostAdminForPageName(page:String!, video videoData:NSData!, withTitle title:String?, withDescription description:String?)
    func feedPostAdminForPageName(page:String!, video videoData:NSData!, withTitle title:String?, withDescription description:String?, fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        
        self.getPagesFromViewController(fromController: fromController) { (success:Bool, result:Any) in
            if success{
                var dicPageAdmin:[String:Any]?
                if let resonseDict: [String:AnyObject] = result as? [String:AnyObject]{
                    if let listData = resonseDict["data"] as? [AnyObject]{
                        for dic in listData
                        {
                            if let pageStr = dic["name"] as? String
                            {
                                if pageStr == page{
                                    dicPageAdmin = (dic as? [String:Any])!
                                    break
                                }
                                
                            }
                        }
                    }
                }
                if dicPageAdmin == nil{
                    CallBack!(false, "Page not found!" )
                    return
                }
                let request:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(String(describing: dicPageAdmin!["id"]))/feed", parameters: ["title" : title!,"description" : description!,"video.mp4" : videoData,"access_token" : dicPageAdmin!["access_token"]!], httpMethod: "POST")
                request.start(completionHandler: { (connection:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
                    if (error != nil){
                        CallBack!(false, error!.localizedDescription)
                    }
                    else{
                        CallBack!(true, result!)
                    }
                })
            }
        }
    }
    //MARK:-feedPostAdminForPageName(page:String?, feedMessage message:String, feedLink url:String?
    func feedPostAdminForPageName(page:String?, feedMessage message:String, feedLink url:String? ,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        self.getPagesFromViewController(fromController: fromController) { (success:Bool, result:Any) in
            if success{
                var dicPageAdmin:[String:Any]?
                if let resonseDict: [String:AnyObject] = result as? [String:AnyObject]{
                    if let listData = resonseDict["data"] as? [AnyObject]{
                        for dic in listData
                        {
                            if let pageStr = dic["name"] as? String
                            {
                                if pageStr == page{
                                    dicPageAdmin = (dic as? [String:Any])!
                                    break
                                }
                                
                            }
                        }
                    }
                }
                if dicPageAdmin == nil{
                    CallBack!(false, "Page not found!" )
                    return
                }
                let request:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(String(describing: dicPageAdmin!["id"]))/feed", parameters: ["message" : message,"link" : url!,"access_token" : dicPageAdmin!["access_token"]!], httpMethod: "POST")
                request.start(completionHandler: { (connection:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
                    if (error != nil){
                        CallBack!(false, error!.localizedDescription)
                    }
                    else{
                        CallBack!(true, result!)
                    }
                })
            }
        }
    }
    //MARK:-feedPostAdminForPageName(page:String?, feedMessage message:String, feedPhoto photo:UIImage?)
    func feedPostAdminForPageName(page:String?, feedMessage message:String, feedPhoto photo:UIImage? ,  fromViewController fromController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        self.getPagesFromViewController(fromController: fromController) { (success:Bool, result:Any) in
            if success{
                var dicPageAdmin:[String:Any]?
                if let resonseDict: [String:AnyObject] = result as? [String:AnyObject]{
                    if let listData = resonseDict["data"] as? [AnyObject]{
                        for dic in listData
                        {
                            if let pageStr = dic["name"] as? String
                            {
                                if pageStr == page{
                                    dicPageAdmin = (dic as? [String:Any])!
                                    break
                                }
                                
                            }
                        }
                    }
                }
                if dicPageAdmin == nil{
                    CallBack!(false, "Page not found!" )
                    return
                }
                let request:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(String(describing: dicPageAdmin!["id"]))/feed", parameters: ["message" : message,"source" : UIImagePNGRepresentation(photo!)!,"access_token" : dicPageAdmin!["access_token"]!], httpMethod: "POST")
                request.start(completionHandler: { (connection:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
                    if (error != nil){
                        CallBack!(false, error!.localizedDescription)
                    }
                    else{
                        CallBack!(true, result!)
                    }
                })
            }
        }
    }
    
    
    
       //MARK:-getAlbums
    func getAlbums(completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        self.graphFacebookGET(method: "/me", withParams: ["fields":"albums"], completionHandler: CallBack)
        
    }
    
       //MARK:-getAlbumById
    func getAlbumById(albumId:String! ,completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (albumId == nil || albumId.isEmpty == true)
        {
            CallBack!(false, "Album id required" )
            return
        }
        self.graphFacebookGET(method: albumId, withParams: nil, completionHandler: CallBack)
    }
    
    //MARK:-getPhotosAlbumById
    func getPhotosAlbumById(albumId:String! ,completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (albumId == nil || albumId.isEmpty == true)
        {
            CallBack!(false, "Album id required" )
            return
        }
        self.graphFacebookGET(method: "\(albumId!)/photos?fields=id,height,width,created_time,images", withParams: nil, completionHandler: CallBack)
    }
    
    //MARK:-createAlbumName
    func createAlbumName(name:String? , withMessage message:String?, withPrivacy privacy:FBAlbumPrivacyType? , completionHandler CallBack:JKFacebookCallback?){
        
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if ((name == nil || name?.isEmpty == true) && (message == nil || message?.isEmpty == true))
        {
            CallBack!(false, "Name and message required" )
            return
        }
        var privacyString:String = ""
        switch (privacy!) {
        case .FBAlbumPrivacyEveryone:
            privacyString = "EVERYONE"
            break
        case .FBAlbumPrivacyAllFriends:
            privacyString = "ALL_FRIENDS"
            break
        case .FBAlbumPrivacyFriendsOfFriends:
            privacyString = "FRIENDS_OF_FRIENDS"
            break
        
        default:
            //FBAlbumPrivacySelf
             privacyString = "SELF"
            break
        }
        self.graphFacebookPOST(method: "me/albums", withParams: ["name" :  name ,"message" : message,"value" : privacyString], completionHandler: CallBack)
    }
    //MARK:-feedPostForAlbumId
    func feedPostForAlbumId(albumId:String!, withPhoto photo:UIImage! ,completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        if (albumId == nil || albumId.isEmpty == true)
        {
            CallBack!(false, "Album id required" )
            return
        }
        self.graphFacebookPOST(method: "\(albumId!)/photos", withParams: ["source": UIImagePNGRepresentation(photo)], completionHandler: CallBack)
    }
    
     //MARK:-sendForPostOpenGraphWithActionType
    func sendPostOpenGraph( actionType:String? , graphObject openGraphObject:FBSDKShareOpenGraphObject?, withObjectName objectName:String? , fromviewController viewController:UIViewController!, completionHandler CallBack:JKFacebookCallback?){
        if isSessionValid() == false
        {
            CallBack!(false, "Not logged in" )
            return
        }
        let action :FBSDKShareOpenGraphAction = FBSDKShareOpenGraphAction()
        action.actionType = actionType
        action.setObject(openGraphObject, forKey: objectName)
        let content :FBSDKShareOpenGraphContent = FBSDKShareOpenGraphContent()
        content.action = action
        content.previewPropertyName = objectName
        FBSDKShareDialog.show(from: viewController, with: content, delegate: self)
        
        self.sharedCallcack = CallBack
        
    }
     //MARK:-graphFacebookForMethodGET
    func graphFacebookGET(method:String!, withParams params:Any?, completionHandler CallBack:JKFacebookCallback?){
        self.graphFacebookForMethod(method: method, httpMethod: "GET", params: params, completionHandler: CallBack)
    }
    //MARK:-graphFacebookForMethodPOST
    func graphFacebookPOST(method:String!, withParams params:Any? , completionHandler CallBack:JKFacebookCallback?){
        self.graphFacebookForMethod(method: method, httpMethod: "POST", params: params, completionHandler: CallBack)

    }
    //MARK:-graphFacebookForMethod
    func graphFacebookForMethod(method:String!,httpMethod:String!, params:Any?,completionHandler CallBack:JKFacebookCallback?){
        
       let request:FBSDKGraphRequest =  FBSDKGraphRequest(graphPath: method, parameters: params as! [AnyHashable : Any]!, httpMethod: httpMethod)
        request.start(completionHandler: { (connection:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
            
            
            if (error != nil ){
                
               if let err = error as NSError?{
                
                if String(describing: err.userInfo[FBSDKGraphRequestErrorGraphErrorCode]) == "200"{
                      CallBack!(false, err.localizedDescription)
                }else{
                    CallBack!(false, err.localizedDescription)
  
                }
                }
                else{
                    CallBack!(false, error!.localizedDescription)
                }
              
            }
            else{
                CallBack!(true, result!)
            }
        })
    }
    
  
    
    
    //MARK:-openShareDialogInViewController-
    func openShareDialogInViewController(currentController:UIViewController!, withPostType posttype :FBPostType!, withTitle title:String? , description caption:String?, withMedia mediaObject:Any!,completionHandler CallBack:JKFacebookCallback?){
        sharedCallcack = CallBack
        self.postType = posttype
        switch self.postType! {
        case .FBPostTypePhoto:
            let photo : FBSDKSharePhoto = FBSDKSharePhoto()
            if let image  = mediaObject as? UIImage{
                photo.image = image
            }
            photo.isUserGenerated = true
            // photo.caption = "\(title!)\n\(caption)"
            let photoContent :FBSDKSharePhotoContent = FBSDKSharePhotoContent()
            photoContent.photos = [photo]
            let shareDialog :FBSDKShareDialog = FBSDKShareDialog.show(from: currentController, with: photoContent, delegate: self)
            shareDialog.mode = .shareSheet
            
            
            break
        case .FBPostTypeVideo:
            
            let video : FBSDKShareVideo = FBSDKShareVideo()
            
            if let videoURl  = mediaObject as? URL{
                video.videoURL = videoURl
                
            }
            let videoContent :FBSDKShareVideoContent = FBSDKShareVideoContent()
            videoContent.video = video
            let shareDialog :FBSDKShareDialog = FBSDKShareDialog.show(from: currentController, with: videoContent, delegate: self)
            shareDialog.mode = .shareSheet
            
            
            
            break
        case .FBPostTypeLink:
            let linkContent :FBSDKShareLinkContent = FBSDKShareLinkContent()
            
            if let contentURL  = mediaObject as? URL{
                linkContent.contentURL = contentURL
                
            }
//            linkContent.contentTitle = title
//            linkContent.contentDescription = caption
            linkContent.quote = caption
            let shareDialog :FBSDKShareDialog = FBSDKShareDialog.show(from: currentController, with: linkContent, delegate: self)
            shareDialog.mode = .shareSheet
            break
            
        default:
            
            break
        }
        
    }
    
}
//MARK:-
//MARK:-FBSDKSharingDelegate methods
extension JKFacebook:FBSDKSharingDelegate{
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
        if (self.sharedCallcack != nil)
        {
            self.sharedCallcack!(false,"Cancelled")
        }
    }
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        if (self.sharedCallcack != nil)
        {
            self.sharedCallcack!(true,results)
        }
    }
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
        var message:String! = "There was a problem sharing, please try again later."
        if let err = error as NSError? {
            message = err.userInfo[FBSDKErrorLocalizedDescriptionKey] as! String!
            
        }
         var title:String! = "Oops!"
        if let err  = error as NSError?   {
            title = err.userInfo[FBSDKErrorLocalizedTitleKey] as! String!
            
        }
       
        print("sharing error:\(error) title = \(title) message= \(message)")
        if (self.sharedCallcack != nil)
        {
            self.sharedCallcack!(false,error.localizedDescription)
        }
    }
    
}
//MARK:-
//MARK:-FBSDKAppInviteDialogDelegate methods
extension JKFacebook:FBSDKAppInviteDialogDelegate{
   
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        if (inviteCallcack != nil)
        {
            self.inviteCallcack!(true, results)
            self.inviteCallcack = nil
        }
        
    }
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        if (inviteCallcack != nil)
        {
            self.inviteCallcack!(true, error.localizedDescription)
            self.inviteCallcack = nil
        }
    }
}
