//
//  AWSS3Manager.swift
//  Dumpya
//
//  Created by Jitendra Kumar on 09/04/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
import AWSMobileClient
//import AWSAuthCore
import AWSS3
enum AWSS3UploadFolder:CustomStringConvertible {
    case images
    case profile
    case videos
    var description: String{
        switch self {
        case .images: return "images"
        case .profile: return "profile"
        case .videos: return "videos"
            
        }
    }
}
internal struct AWSS3UploadUtility {
    static let kBucket = "dumpya"
    static let kUploadKey = ""
    static func folderName(folderType type:AWSS3UploadFolder)->String{
        return "\(type.description)/"
    }
}
class AWSS3Manager: NSObject {
    
    // Get the AWSCredentialsProvider from the AWSMobileClient
    fileprivate lazy var credentialsProvider:AWSCognitoCredentialsProvider = {
        return AWSMobileClient.sharedInstance().getCredentialsProvider()
    }()
    fileprivate lazy var serviceConfiguration:AWSServiceConfiguration = {
        return AWSServiceConfiguration(region: .USEast2, credentialsProvider: credentialsProvider)
    }()
    // Get the identity Id from the AWSIdentityManager
    fileprivate var identityId :String? {return AWSIdentityManager.default().identityId}
    class var shared:AWSS3Manager{
        struct  Singlton{
            static let instance = AWSS3Manager()
        }
        return Singlton.instance
    }
    override init() {
        super.init()
        
        
    }
    func setConfiguration(){
        AWSServiceManager.default().defaultServiceConfiguration = serviceConfiguration
        
    }
    
    func interceptApplication(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)->Bool{
        setConfiguration()
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
        AWSDDLog.sharedInstance.logLevel = .info
        
        return AWSMobileClient.sharedInstance().interceptApplication(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func interceptApplication(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String,
                              completionHandler: @escaping () -> Void){
        //provide the completionHandler to the TransferUtility to support background transfers.
        AWSS3TransferUtility.interceptApplication(application,
                                                  handleEventsForBackgroundURLSession: identifier,
                                                  completionHandler: completionHandler)
    }
    func initialize(){
        
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            guard error == nil else {
                print("Error initializing AWSMobileClient. Error: \(error!.localizedDescription)")
                return
            }
            print("AWSMobileClient initialized.")
        }
    }
    //MARK:- GET MULTIPARTDATA
    fileprivate func get(data:Any)->MultipartData?{
        let dataFormate:DataFormate = .multipart
        let uploadKey = AWSS3UploadUtility.kUploadKey
        if let image = data as? UIImage {
            let dataType = DataType.image(image: image, fileName: nil, uploadKey: uploadKey, formate: .jpeg(quality: .medium))
            return dataFormate.result(dataType: dataType) as? MultipartData
        }else if let url = data as? URL{
            let dataType =  DataType.file(file: url, uploadKey: uploadKey)
            return dataFormate.result(dataType: dataType) as? MultipartData
        }else if let filePath = data as? String{
            let dataType =  DataType.file(file: filePath, uploadKey: uploadKey)
            return dataFormate.result(dataType: dataType) as? MultipartData
        }else{
            return nil
        }
        
        
    }
    
    //MARK: - upload
    func upload(file :Any,folderType:AWSS3UploadFolder,onProgress:@escaping (_ progress:Progress)->Void,onSuccess:@escaping(_ uploadUrl:String)->Void,onError:@escaping(_ error:Error)->Void){
        guard let data = get(data: file) else { return}
        let foldername  = AWSS3UploadUtility.folderName(folderType: folderType)
        let expression = AWSS3TransferUtilityUploadExpression()
        
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Update a progress bar.
                onProgress(progress)
            })
        }
        var completionBlockUpload: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionBlockUpload = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    print("Failed with error: \(error)")
                    onError(error)
                    return
                    
                }else if(task.progress.fractionCompleted != 1.0) {
                    print("Error: Failed - Likely due to invalid region / filename")
                    let error = SMError.init(localizedTitle: "3BucketUploadError", localizedDescription: "Error: Failed - Likely due to invalid region / filename", code: 500)
                    onError(error)
                }
                else{
                    
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(task.bucket).appendingPathComponent(task.key)
                    if let stringURL = publicURL?.absoluteString
                    {
                        onSuccess(stringURL)
                        
                    }
                    // "Success"
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data.media, bucket: AWSS3UploadUtility.kBucket, key: "\(foldername)\(data.fileName!)", contentType: data.mimType, expression: expression, completionHandler: completionBlockUpload).continueWith { (task) -> Any? in
            DispatchQueue.main.async(execute: {
                if let error = task.error {
                    print("error in creating aws upload task",error)
                    onError(error)
                    
                }
                if let uploadTask = task.result {
                    // Do something with uploadTask.
                    print("aws upload task created successfully: \(String(describing: uploadTask.sessionTask.currentRequest))")
                    
                }
            })
            
            return nil
        }
        
    }
    
    
    func uploadMultiPart(file :Any,folderType:AWSS3UploadFolder,onProgress:@escaping (_ progress:Progress)->Void,onSuccess:@escaping(_ uploadUrl:String)->Void,onError:@escaping(_ error:Error)->Void){
        guard let data = get(data: file) else { return}
        let foldername  = AWSS3UploadUtility.folderName(folderType: folderType)
        let expression = AWSS3TransferUtilityMultiPartUploadExpression()
        
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Update a progress bar.
                onProgress(progress)
            })
        }
        var completionBlockUpload: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
        completionBlockUpload = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    print("Failed with error: \(error)")
                    onError(error)
                    return
                    
                }else if(task.progress.fractionCompleted != 1.0) {
                    print("Error: Failed - Likely due to invalid region / filename")
                    let error = SMError.init(localizedTitle: "3BucketUploadError", localizedDescription: "Error: Failed - Likely due to invalid region / filename", code: 500)
                    onError(error)
                }
                else{
                    
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(task.bucket).appendingPathComponent(task.key)
                    if let stringURL = publicURL?.absoluteString
                    {
                        onSuccess(stringURL)
                        
                    }
                    // "Success"
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadUsingMultiPart(data: data.media, bucket: AWSS3UploadUtility.kBucket, key: "\(foldername)\(data.fileName!)", contentType: data.mimType, expression: expression, completionHandler: completionBlockUpload).continueWith {(task) -> Any? in
            DispatchQueue.main.async(execute: {
                if let error = task.error {
                    print("error in creating aws upload task",error)
                    onError(error)
                    
                }
                if let uploadTask = task.result {
                    // Do something with uploadTask.
                    
                    print("aws upload task created successfully: \(String(describing: uploadTask.transferID))")
                    
                }
            })
            
            return nil
        }
        
    }
    
    func uploadFiles(array: [String:Any],folderType:AWSS3UploadFolder,onProgress:@escaping (_ progress:Progress)->Void,onSuccess:@escaping(_ uploadUrls:[String:String])->Void,onError:@escaping(_ error:Error)->Void){
        var retryCount = 0
        var retryArray =  array
        let imageGroup = DispatchGroup()
        imageGroup.enter()
        var uploadedURls = [String:String]()
        
        if(array.count > 0) {
            while(retryCount < retryArray.count) {
                //Iterate through retry array
                let uploadGroup = DispatchGroup()
                for (key,item)  in retryArray{
                    uploadGroup.enter()
                    self.upload(file: item, folderType: folderType, onProgress: onProgress, onSuccess: { (uploadedFile) in
                        //If no error, remove key/value pair
                        retryArray.removeValue(forKey: key)
                        uploadedURls.updateValue(uploadedFile, forKey: key)
                        
                        uploadGroup.leave()
                    }, onError: { (err) in
                        onError(err)
                    })
                }
                
                uploadGroup.wait()
                if(retryArray.keys.count == 0) {
                    break
                } else {
                    retryCount = retryCount + 1
                }
                
            }
            imageGroup.leave()
        } else {
            imageGroup.leave()
        }
        
        imageGroup.notify(queue: DispatchQueue.main) {
            
            if(retryArray.count > 0) {
                print("the images could not be uploaded")
                onSuccess(uploadedURls)
                //execute code to delete folder in aws s3
            } else {
                print("all images uploaded successfully")
                onSuccess(uploadedURls)
            }
        }
        
    }
    
}
