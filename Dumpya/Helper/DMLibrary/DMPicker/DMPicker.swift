//
//  DMPicker.swift
//  Dumpya
//
//  Created by Chandan Taneja on 07/03/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import YPImagePicker
import AVKit
import Photos
import Kingfisher
public typealias DMMediaItem = YPMediaItem
public typealias DMMediaPhoto = YPMediaPhoto
public typealias DMMediaVideo = YPMediaVideo



typealias DMPickerPickedHandler = (_ item:DMMediaItem)->Void
let kDMPickerAlbumName:String = kAppTitle

enum DMPicker {
   
    case camera(onPicked: DMPickerPickedHandler)
    case library(onPicked:DMPickerPickedHandler)
    func picker(from controller:UIViewController){
        
        switch self {
        case .camera(let onPicked):
            let config  = self.config(isLibrary: false)
            let picker = YPImagePicker(configuration: config)
            picker.didFinishPicking(completion: { (items, cancelled) in
                picker.dismiss(animated: true, completion: {
                    if !cancelled {
                        if let item  = items.first{
                            switch item{
                            case .video(let v):
                                DMPicker.trySaveVideo(item: v)
                            default:break
                            }
                            onPicked(item)
                        }
                        
                        
                    }
                })
                
                
            })
            controller.present(picker, animated: true, completion: nil)
            
        case .library(let onPicked):
            let config  = self.config(isLibrary: true)
            let picker = YPImagePicker(configuration: config)
            picker.didFinishPicking(completion: { (items, cancelled) in
                picker.dismiss(animated: true, completion: {
                    if !cancelled {
                        if let item  = items.first{
                            onPicked(item)
                        }
                        
                        
                    }
                })
                
            })
            controller.present(picker, animated: true, completion: nil)
        }
    }
    
    
    fileprivate  func config(isLibrary:Bool = true)->YPImagePickerConfiguration{
        
        var config = YPImagePickerConfiguration()
        config.bottomMenuItemSelectedColour = DMColor.deepRed
        config.colors.trimmerMainColor = DMColor.deepRed
        config.colors.trimmerHandleColor = .white
        config.colors.tintColor = .white
        config.colors.navigationBarActivityIndicatorColor = .white
        
        config.albumName = kDMPickerAlbumName
        /* Customize wordings */
        config.wordings.libraryTitle = "Library"
        /* Defines if the status bar should be hidden when showing the picker. Default is true */
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.startOnScreen = .library
        
        config.video.compression = AVAssetExportPresetHighestQuality
        config.video.recordingTimeLimit = 300.0
        config.video.trimmerMaxDuration = 30.0
        config.video.libraryTimeLimit = 300.0
        config.video.minimumTimeLimit = 3.0
        config.video.trimmerMinDuration  = 3.0
        config.showsPhotoFilters = false
        config.showsVideoFilters = true
        config.shouldSaveNewPicturesToAlbum = true
        config.library.maxNumberOfItems = 0
        config.icons.capturePhotoImage = config.icons.capturePhotoImage.tint(with:DMColor.deepRed)!
        if isLibrary {
            config.library.mediaType = .photoAndVideo
            config.screens =  [.library]
        }else{
            
            config.screens = [.photo,.video]
            config.startOnScreen = .photo
        }
        return config
        
    }
    
}
extension DMMediaVideo{
    var resolutionVideo: CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
    
}
extension DMMediaPhoto{
    var imageSize:CGSize{
        return self.image.size
    }
}
extension DMMediaItem{
    var itemSize:CGSize?{
        switch self {
        case .photo(let p):
            return p.imageSize
        case .video(let v):
            return v.resolutionVideo
        }
    }
}
extension DMPicker{
    static func trySaveVideo(item:DMMediaVideo,completionHandler:((Bool, Error?) -> Void)? = nil){
        if let asset = item.asset {
            DMVideoSaver.trySaveVideo(DMSaveVideoFormate.videoAsset(asset: asset), inAlbumNamed: kDMPickerAlbumName, completionHandler: completionHandler)
        }else{
            DMVideoSaver.trySaveVideo(DMSaveVideoFormate.videoURL(url: item.url), inAlbumNamed: kDMPickerAlbumName, completionHandler: completionHandler)
        }
        
    }
    static func trySaveVideoURl(fileUrl:URL,completionHandler:((Bool, Error?) -> Void)? = nil){
        DMVideoSaver.trySaveVideo(DMSaveVideoFormate.videoURL(url:fileUrl), inAlbumNamed: kDMPickerAlbumName, completionHandler: completionHandler)
    }

    
    
}

extension DMPicker{
    static func streamVideo(file:String)->AVPlayerViewController?{
        guard let controller = currentController, let url  = URL(string: file) else { return nil}
        let avPlayerController = AVPlayerViewController()
        avPlayerController.showsPlaybackControls = true
        avPlayerController.player = AVPlayer.init(url: url)
        avPlayerController.modalTransitionStyle = .crossDissolve
        controller.present(avPlayerController, animated: true) {
            avPlayerController.player?.play()
        }
        return avPlayerController
    }
}
extension URL{
   
    var thumbnailFromVideoURL: UIImage {
        let asset = AVURLAsset(url: self, options: nil)
        let gen = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(0.0, 600)
        var actualTime = CMTimeMake(0, 0)
        let image: CGImage
        do {
            image = try gen.copyCGImage(at: time, actualTime: &actualTime)
            let thumbnail = UIImage(cgImage: image)
            return thumbnail
        } catch { }
        return UIImage()
    }
}
func videoThumnail(videoUrl url:URL,onCompletion:@escaping (_ thumbnail:UIImage)->Void){
  
    
}
class VideoThumnail{
  private  let imageCache = ImageCache.default
  static let shared = VideoThumnail()
   private func set(image:UIImage,forKey key:String){
        imageCache.store(image, forKey: key)
    }
    private func getImage(forKey key:String,completionHanlder:((Result<Image?, Error>) -> Void)?){
        imageCache.retrieveImage(forKey: key) { (result) in
            switch result{
            case .success(let value):
                completionHanlder?(.success(value.image))
            case .failure(let error):
                completionHanlder?(.failure(error))
            }
        }
  
    }
    
    func downloadImage(url: URL,completionHanlder:((Result<Image, Error>) -> Void)?) {
        getImage(forKey: url.absoluteString) { (result) in
            switch result{
            case .success(let value):
                if let image = value{
                    completionHanlder?(.success(image))
                }else{
                    DispatchQueue.userInitiated.async {
                        let asset = AVURLAsset(url: url, options: nil)
                        let gen = AVAssetImageGenerator(asset: asset)
                        gen.appliesPreferredTrackTransform = true
                        let time = CMTimeMakeWithSeconds(0.0, 600)
                        var actualTime = CMTimeMake(0, 0)
                        let image: CGImage
                        do {
                            image = try gen.copyCGImage(at: time, actualTime: &actualTime)
                            let thumbnail = UIImage(cgImage: image)
                            self.set(image: thumbnail, forKey: url.absoluteString)
                            completionHanlder?(.success(thumbnail))
                        } catch  {
                            completionHanlder?(.failure(error))
                        }
                    }
                }
            case .failure(let err):
                completionHanlder?(.failure(err))
            }
        }
    }
    
    
}
