//
//  YPVideoSaver.swift
//  Alamofire
//
//  Created by Chandan Taneja on 08/04/19.
//

import Foundation
import Photos

internal enum DMSaveVideoFormate {
    case videoURL(url:URL)
    case videoAsset(asset:PHAsset)
    
}
 class DMVideoSaver {
    class func trySaveVideo(_ dataType: DMSaveVideoFormate, inAlbumNamed: String, completionHandler: ((Bool, Error?) -> Void)? = nil) {
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            if let album = album(named: inAlbumNamed) {
                saveVideo(dataType, toAlbum: album)
                completionHandler?(true,nil)
            } else {
                createAlbum(withName: inAlbumNamed, completionHandler: {(success, error) in
                    if success{
                        if let album = album(named: inAlbumNamed) {
                            saveVideo(dataType, toAlbum: album)
                            completionHandler?(true,nil)
                        }
                    }else{
                        completionHandler?(false,error)
                    }
                })
               
            }
        }
    }
 
    fileprivate class func saveVideo(_ dataType: DMSaveVideoFormate, toAlbum album: PHAssetCollection) {
        PHPhotoLibrary.shared().performChanges({
            var changeRequest:PHAssetChangeRequest!
            switch dataType{
            case .videoAsset(let asset):
                changeRequest = PHAssetChangeRequest.init(for: asset)
            case .videoURL(let url):
              guard let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)else{return}
                changeRequest = request
            }
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: album)
            let enumeration: NSArray = [changeRequest.placeholderForCreatedAsset!]
            albumChangeRequest?.addAssets(enumeration)
        })
    }
  
    fileprivate class func createAlbum(withName name: String, completionHandler: ((Bool, Error?) -> Void)? = nil) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
        }, completionHandler: completionHandler)
    }
    
    fileprivate class func album(named: String) -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", named)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album,subtype: .any,options: fetchOptions)
        return collection.firstObject
    }
}
