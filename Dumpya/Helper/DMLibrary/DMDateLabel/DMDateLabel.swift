//
//  DMDateLabel.swift
//  Dumpya
//
//  Created by Chandan Taneja on 16/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import UIKit
class DMDateLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.masksToBounds = true
        self.clipsToBounds = true
        layer.shadowOffset = CGSize(width: 0.0, height: 2.6)
        layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.8
    }
    override var intrinsicContentSize: CGSize{
        let originalContentSize = super.intrinsicContentSize
        let height = originalContentSize.height + 12
        layer.cornerRadius = height / 2
        
        return CGSize(width: originalContentSize.width + 20, height: height)
    }
    
}
