//
//  FacebookLogin.swift
//  SwiftDemo
//
//  Created by Ravi Phulara on 29/08/17.
//  Copyright © 2017 Mobilyte. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class FacebookLogin: NSObject {
    
    //MARK:- Facebook Login Methods
    func facebookLogin(withController:UIViewController,success:@escaping (_ finish: Bool,_ user:DMFacebookUser) -> ()) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: [FacebookPermissions.publicProfile.rawValue,FacebookPermissions.email.rawValue,FacebookPermissions.birthday.rawValue], from: withController) { (result, error) in
            
            if error != nil{
                print(error.debugDescription)
                // Calling back to previous class if error occured
                success(false,error as! DMFacebookUser)
                return
            }
            
            let FBLoginResult: FBSDKLoginManagerLoginResult = result!
            
            if FBLoginResult.isCancelled{
                print("User cancelled the login process")
            }else if FBLoginResult.grantedPermissions.contains(FacebookPermissions.email.rawValue){
                self.getFBUserData(success: {(finish,user) in
                    if finish {
                        success(true,user)
                        return
                    }
                    success(false,user)
                })
            }
        }
        //success(true)
    }
    
    private func getFBUserData(success: @escaping(_ finished: Bool,_ user:DMFacebookUser)-> ()){
        if (FBSDKAccessToken.current() != nil) {
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, first_name, last_name, email, birthday, gender,friends"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) -> Void in
                let data = result as! [String : AnyObject]
                let accessToken = FBSDKAccessToken.current()?.tokenString ?? ""
                
                print("THE FB ACCESS TOKEN IS------>",accessToken)
                
                let email = (data["email"] as? String) ?? ""
              
                var snsType = 2
                let snsId = (data["id"] as? String) ?? ""
                let firstName = (data["first_name"] as? String) ?? ""
                let lastName = (data["last_name"] as? String) ?? ""
                let url = "https://graph.facebook.com/\(snsId)/picture?type=large&return_ssl_resources=1"
                let profilePicure = url
                let user = DMFacebookUser(id: snsId, email: email, firstName: firstName, lastName: lastName, profilePic: profilePicure,fbAccessToken:accessToken)
                success(true,user)
            })
            connection.start()
        }
    }
    
    func logoutFB() {
       // let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
       
        URLCache.shared.removeAllCachedResponses()
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
}
struct DMFacebookUser {
    var id :String! = ""
    var email:String! = ""
    var firstName:String! = ""
    var lastName:String! = ""
    var profilePic:String! = ""
    var fbAccessToken:String! =  ""
}
