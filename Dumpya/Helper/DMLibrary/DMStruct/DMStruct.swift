//
//  DMStruct.swift
//  Dumpya
//
//  Created by Chander on 14/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import UIKit
struct EmojiFontIcon {
    static let rightArraow = "→"
    static let leftArrow = "←"
    static  let bulletIcon = "•"
    static  let radioselect = "◉"
    static  let rediounSelect = "◎"
    static let downTrangle = "▾"
    static let rightTrangle = "‣"
    static let leftTrangle = "◀︎"
    static let sadEmoji = "☹"
}
struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
    static var isPhone:Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true :false
    }
}
struct AppInfo {
    static let bundleDisplayName =  Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    static let bundleName = Bundle.main.object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String
    static let kAppTitle = bundleDisplayName ?? bundleName ?? ""
    static let kAppVersionString: String = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    static let kBuildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
}


struct SegueIdentity {
    
    static let kSignupSegue                  = "SignupSegue"
    static let kProfileSegue                 = "ProfileSegue"
    static let kCommentSegue                 = "CommentSegue"
    static let kCommentViewSegue             = "CommentViewSegue"
    static let kProfileDetailSegue           = "ProfileDetailSegue"
    static let kRequestedProfileDetailSegue  = "RequestedProfileDetailSegue"
    static let kProfileDetailSearchSegue     = "ProfileDetailSearchSegue"
    static let kNewsDetailSegue              = "NewsDetailSegue"
    static let kLoginTermsSegue              = "LoginTermsSegue"
    static let kCommentedUserListSegue       = "CommentedUserListSegue"
    static let kCommentedUserViewListSegue   = "CommentedUserListSegue"
    static let kFollowingListSegue           = "FollowingListSegue"
    static let kDumpeeDetailSegue            = "DumpeeDetailSegue"
    static let kDumpeeDetailSearchSegue      = "DumpeeDetailSearchSegue"
    static let kHashTagSegue                 = "HashTagSegue"
    static let kEditProfileSegue             = "EditProfileSegue"
    static let kSearchSegue                  = "SearchSegue"
    static let kLoginSeque                   = "LoginSeque"
    static let kProfileSeque                 = "ProfileSeque"
    static let kResetPasswordSegue           = "ResetPasswordSegue"
    static let kEmailVarificationSegue       = "EmailVarificationSegue"
    static let KResetPasswordSegue           = "ResetPasswordSegue"
    static let kDumpCreateSegue              = "DumpCreateSegue"
    static let kFollowersSegue               = "FollowersSegue"
    static let kFollowingSegue               = "FollowingSegue"
    static let kDumpDetailSegue              = "DumpDetailSegue"
    static let kChatSegue                    = "ChatSegue"
    static let kChatUserSearchSegue          = "UserSearchSegue"
    
    static let kNotificationsSegue           = "NotificationsSegue"
    static let kMessagesSegue                = "MessagesSegue"
    static let kTabBarSegue                  = "TabBarSegue"
    static let kFriendsSegue                 = "FriendsSegue"
    static let kSettingsSegue                = "SettingsSegue"
    static let kChangePasswordSegue          = "ChangePasswordSegue"
    static let kDMDatePickerSegue            = "DMDatePickerSegue"
    static let kDMCountryCodePickerSegue     = "DMCountryCodePickerSegue"
    static let kDMGenderPickerSegue          = "DMGenderPickerSegue"
    static let kOTPSegue                     = "OTPSegue"
    static let kDumpCreateSeque              = "DumpCreateSeque"
    static let kDumpTopicSeque               = "DumpTopicSeque"
    static let kCreateDumpeeSegue            = "CreateDumpeeSegue"
    static let kDashBoardSeque               = "DashBoardSeque"
    static let kDumpFeedSeque                = "DumpFeedSeque"
    static let kPushNotification             = "PushNotification"
    static let kChangePassword               = "ChangePassword"
    static let kCategoryPickerSegue          = "CategoryPickerSegue"
    static let KHistoryTopics                = "HistoryTopics"
    
}
struct StoryBoardIdentity {
    static let kLoginOptionNavigation       = "MainNavigationController"
    static let kLoginNavigationController   = "LoginNavigationController"
    static let kDashBoardNavigation         = "DashBoardNavigation"
    static let kProfileNavigation           = "ProfileNavigation"
    static let kSearchNavigation            = "SearchNavigation"
    static let kDumpFeedNavigation          = "DumpFeedNavigation"
    static let kNewsNavigation              = "NewsNavigation"
    static let kDumpDetailVC                = "DumpDetailVC"
    static let kLeaderBoardNavigation       = "LeaderBoardNavigation"
    static let kNotificationNavigation      = "NotificationNavigation"
    static let kMessagesNavigation          = "MessagesNavigation"
    static let kFriendsNavigation           = "FriendsNavigation"
    static let kGroupsNavigation            = "GroupsNavigation"
    static let kSettingsNavigation          = "SettingsNavigation"
    static let kDMLeftMenuNavigation        = "DMLeftMenuNavigation"
    static let kTHLeftMenuVC                = "THLeftMenuVC"
    static let kDirectMessageNavigation     = "DirectMessageNavigation"
    static let kProfileDetailNavigation     = "ProfileDetailNavigation"
    static let kProfileDetailVC             = "ProfileDetailVC"
    
    static let kConnectionNavigation        = "ConnectionNavigation"
    static let kChatNavigation              = "ChatNavigation"
    static let kMessageVC                   = "MessageVC"
    static let kMessageRequestVC            = "MessageRequestVC"
    static let LEditNavigation              = "DMLeftMenuVC"
    
}






struct FieldValidation {
    static let kFirstNameEmpty         = "firstNameEmpty".localized
    static let kDescription            = "description".localized
    static let kNameEmpty              = "nameEmpty".localized
    static let kFirstNameAlphabatics   = "firstNameAlphabatics".localized
    static let kNameAlphabatics        = "nameAlphabatics".localized
    static let klastNameAlphabatics    = "lastNameAlphabatics".localized
    static let kFirstNameLength        = "firstNameLength".localized
    static let kNameLength             = "nameLength".localized
    static let kUserNameLength         = "userNameLength".localized
    static let kMobileNumberLength     = "mobileNumberLength".localized
    static let kLastNameEmpty          = "lastNameEmpty".localized
    static let kFaceBookIdEmpty        = "faceBookIdEmpty".localized
    static let kGmailIdEmpty           = "gmailIdEmpty".localized
    static let kLastNameLength         = "lastNameLength".localized
    static let kEmailEmpty             = "emailEmpty".localized
    static let kDumpeeNameEmpty        = "dumpeeNameEmpty".localized
    static let KDumpeeImageEmpty       = "dumpeeImageEmpty".localized
    static let kDumpeeNameValidation   = "dumpeeNameValidation".localized
    static let kEmailValid             = "emailValid".localized
    static let kBirthdate              = "birthdate".localized
    static let kBio                    = "bio".localized
    static let kLocationNotFound       = "locationNotFound".localized
    static let kDescriptionLength      = "descriptionLength".localized
    static let kMobileNumberEmpty      = "mobileNumberEmpty".localized
    static let kGender                 = "gender".localized
    static let kPasswordEmpty          = "passwordEmpty".localized
    static let kCommentEmpty           = "commentEmpty".localized
    static let kCategory               = "category".localized
    static let kOldPasswordEmpty       = "oldPasswordEmpty".localized
    static let kHelpDescription        = "helpDescription".localized
    static let kPasswordLength         = "passwordLength".localized
    static let kPasswordValidation     = "passwordValidation".localized
    static let kConfirmPasswordEmpty   = "confirmPasswordEmpty".localized
    static let kMisMatchPasswordEmpty  = "misMatchPasswordEmpty".localized
    static let kUserName               = "userName".localized
    static let kVerificationCodeEmpty  = "verificationCodeEmpty".localized
    static let kLocationName           = "locationName".localized
    static let kPasswordCompare        = "passwordCompare".localized
    static let kEmptyWebsite           = "emptyWebsite".localized
    
}
struct TVCellIdentifier {
    static let kMenuCell               = "MenuCell"
    static let kDumpeeProfileCell      = "DumpeeProfileCell"
    static let kCommentCell            = "CommentCell"
    static let kCategoryCell           = "CategoryCell"
    static let kNotificationCell       = "NotificationCell"
    static let kFriendRequestCell      = "FriendRequestCell"
    static let kFriendsCell            = "FriendsCell"
    static let kAllGroupsCell          = "AllGroupsCell"
    static let kMessageRequestCell          = "MessageRequestCell"
    static let kLanguageCell           = "LanguageCell"
    static let kMemberShipsCell        = "MemberShipsCell"
    static let kInvitationCell         = "InvitationCell"
    static let kDashboardImageCell     = "DashboardImageCell"
    static let kDashboardTextCell      = "DashboardTextCell"
    static let kDashboardLinkCell      = "DashboardLinkCell"
    static let kDumpListCell           = "DumpListCell"
    static let kSearchCell             = "SearchCell"
    static let kFeedMediaCell          = "FeedMediaCell"
    static let kFeedTextCell           = "FeedTextCell"
    static let kUserSearchListCell     = "UserSearchListCell"
    static let kRedumpUserCell         = "RedumpUserCell"
    static let kCommentedUserListCell  = "CommentedUserListCell"
    static let kDumpeeSearchListCell   = "DumpeeSearchListCell"
    static let kTagSearchListCell      = "TagSearchListCell"
    static let kFollowersCell          = "FollowersCell"
    static let kNewsCell               = "NewsCell"
    static let kFollowingCell          = "FollowingCell"
    static let kFaceBookConnectionCell  = "FaceBookConnectionCell"
     static let kRequestCell            = "RequestCell"
    static let kDirectMessageCell      = "DirectMessageCell"
    static let kNotificationListCell   = "NotificationListCell"
    static let kFeedGradientCell       = "FeedGradientCell"
    static let kFeedMediaImageCell     = "FeedMediaImageCell"
    static let kDumpListEmptyCell      = "DumpListEmptyCell"
    static let kLeaderBoardCell        = "LeaderBoardCell"
    static let kLoadMoreCell           = "LoadMoreCell"
    static let KLoadCollectionMoreCell = "LoadMoreCell"
}

struct CVCellIdentifier {
    static let kWalkThroughCell         = "WalkThroughCell"
    static let kDumpCategoryCell        = "DumpCategoryCell"
    static let kSearchCell              = "SearchCell"
    static let KGifCell                 = "GifCell"
    static let kLeaderBoardCategoryCell = "LeaderBoardCategoryCell"
    static let kSearchCategoryCell      = "SearchCategoryCell"
    static let kLeaderBoardTimeCell     = "LeaderBoardTimeCell"
}

