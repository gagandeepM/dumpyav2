//
//  FollowersVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 16/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class FollowersVC: UIViewController {

    @IBOutlet weak fileprivate var searchBar: UISearchBar!
    @IBOutlet weak var headingLBL: UILabel!
    @IBOutlet fileprivate var objFollowers: FollowersViewModel!
    @IBOutlet weak fileprivate var tableView:UITableView!
    var userId:String = ""
    var ownerType:Bool = false
    var followerType:String = ""
    var followHeading:String = ""
    var itemCount:Int {
        return objFollowers.numberOfRows()
    }
   fileprivate var loadmoreCell:LoadMoreCell!
    override func viewDidLoad() {
        super.viewDidLoad()
           tableView.register(UINib(nibName: TVCellIdentifier.kLoadMoreCell, bundle: nil), forCellReuseIdentifier: TVCellIdentifier.kLoadMoreCell)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.itemCount>0 {
            
            self.tableView.reloadData()
        }else{
            pageNumber = 0
            totalRecords = 0
            objFollowers.serviceType = .none
        }
        self.loadFollowers()
    }
    
   fileprivate func loadFollowers()  {
    objFollowers.getFollowFollwers(search:"", userId: userId, page: 0, type:followerType, onSuccess: {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
            
        }}, onFailure: {
            if self.objFollowers.serviceType == .pagging {
                if pageNumber>0{
                    pageNumber -= 1
                }
                if let cell = self.loadmoreCell{
                    cell.isShowLoader = false
                }
                
            }
    }, onUserSuspend: { (message) in
        
        self.showAlert(message: message, completion: { (index) in
            AppDelegate.sharedDelegate.logoutUser()
        })
    })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kProfileDetailSegue{
            let objProfileDetail = segue.destination as! ProfileDetailVC
            guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else{return}
            let userInfo = objFollowers.followFollowingInfo(at: selectedIndexPath.row)
            
            objProfileDetail.objUserDetailViewModel.set(currentUserId: userInfo.userId)
//            controller.objDumpFeed = self.objDumpFeed
//            controller.objDumpFeed?.set(at: selectedIndexPath, searchString: self.searchBar.text ?? "")
            
        }
    }
    
    
    
    
    @IBAction func onClickFollower(_ sender: DMButton) {
        let index = sender.tag
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        
        objFollowers.setFollowerId(at:index)
        if followerStatus == .follower{
            objFollowers.sendFollowerRequest(onSuceess: {
                self.loadFollowers()
            }, onFailure: {
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }
        else if followerStatus == .following{
              let alert = UIAlertController(title: nil, message: "unFollowUserMeesage".localized, preferredStyle: .actionSheet)
            
                    alert.addAction(UIAlertAction(title: "unfollow".localized, style: .default , handler:{ (UIAlertAction)in
                        self.objFollowers.sendUnfollowRequest( onSuceess: {
                            self.loadFollowers()
                            
                        }, onFailure: {
                            
                        }, onUserSuspend: { (message) in
                            
                            self.showAlert(message: message, completion: { (index) in
                                AppDelegate.sharedDelegate.logoutUser()
                            })
                        })
            
            }))
             alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            }))
            
            self.present(alert, animated: true, completion: {
            print("completion block")
            })
          
        }
        
   
    }
    
    @IBAction func onClickRemove(_ sender: UIButton) {
        
        let index = sender.tag
        
        var followerStatus:FollowState {
            return objFollowers.setFollowedState(at: index) ?? .none
        }
        objFollowers.setFollowerId(at:index)
        let alert = UIAlertController(title: nil, message: "removeFollower".localized, preferredStyle: .actionSheet)
      
        alert.addAction(UIAlertAction(title: "remove".localized, style: .default , handler:{ (UIAlertAction)in
            self.objFollowers.sendUnfollowRequest(type:"follower", onSuceess: {
                self.loadFollowers()
                
            }, onFailure: {
                
            }, onUserSuspend: { (message) in
                
                self.showAlert(message: message, completion: { (index) in
                    AppDelegate.sharedDelegate.logoutUser()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension FollowersVC : UITableViewDataSource,UITableViewDelegate{
    
    
     func numberOfSections(in tableView: UITableView) -> Int {
    var numOfSections: Int = 0
    if objFollowers.numberOfRows() > 0{
    tableView.separatorStyle = .singleLine
    numOfSections            = 1
    tableView.backgroundView = nil
    tableView.separatorStyle  = .none
    }
    else{
    let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
    noDataLabel.text          = "startFollower".localized
    noDataLabel.textColor     = UIColor.black
    noDataLabel.textAlignment = .center
    noDataLabel.numberOfLines = 0
    tableView.backgroundView  = noDataLabel
    tableView.separatorStyle  = .none
    }
    return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objFollowers.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let followersCell = tableView.dequeueReusableCell(withIdentifier: TVCellIdentifier.kFollowersCell, for: indexPath) as! FollowersCell
    followersCell.removeBtn.tag =  indexPath.row
       followersCell.followersBtn.tag = indexPath.row
        if ownerType{
            followersCell.removeFollowwidthConstraint.constant = 30
        }
        else{
            followersCell.removeFollowwidthConstraint.constant = 0
        }
        followersCell.objFollowerModel = objFollowers.cellForRowAt(at:indexPath)
 
        return followersCell
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension FollowersVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchText = searchText.removeWhiteSpace
        
      
            if searchText.count>0{
                objFollowers.getFollowFollwers(search:searchText, userId: userId, page: 0, type:followerType, onSuccess: {
                   
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                       
                    
                    }}, onFailure: {
                    
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
            }else{
                objFollowers.getFollowFollwers(search:"", userId: userId, page: 0, type:followerType, onSuccess: {
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        
                        
                    }}, onFailure: {
                        
                }, onUserSuspend: { (message) in
                    
                    self.showAlert(message: message, completion: { (index) in
                        AppDelegate.sharedDelegate.logoutUser()
                    })
                })
            }
            
       
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide Keyboard
        searchBar.resignFirstResponder()
       
        objFollowers.getFollowFollwers(search:"", userId: userId, page: 0, type:followerType, onSuccess: {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
                
            }}, onFailure: {
                
        }, onUserSuspend: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                AppDelegate.sharedDelegate.logoutUser()
            })
        })
    
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
}
