//
//  DMEnum.swift
//  Dumpya
//
//  Created by Chander on 03/10/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
enum DMSocialType:String {
    case `default`
    case facebook
    case google
    
    var loginTypeSelect:String{
        switch self {
        case .facebook:
            return "facebook"
        case .google:
            return "google"
        default:
            return "normal"
        }
    }
    
}


enum DumpFeedType:Int,CustomStringConvertible {
    case image
    case textImage
    case gradient
    case text
    case video
    case textVideo
    var description: String{
        switch self {
        case .image: return "image"
        case .textImage: return "textImage"
        case .text: return "text"
        case .video:return "video"
        case .textVideo: return "textVideo"
        default: return "gradient"
            
        }
    }
   static func getFeedType(value:String)->DumpFeedType{
    switch value {
        case DumpFeedType.image.description:return .image
        case DumpFeedType.textImage.description:return .textImage
        case DumpFeedType.video.description:return .video
        case DumpFeedType.text.description:return .text
        case DumpFeedType.textVideo.description:return .textVideo
        default:
        return .gradient
    }
    
    }
}

enum DumpCreateFeedType:CustomStringConvertible {
    case image(item:String,size:CGSize)
    case textImage(item:String,text:String,size:CGSize)
    case gradient(color:GradientColor,text:String?)
    case text(text:String)
    case video(item:String,size:CGSize)
    case textVideo(item:String,text:String,size:CGSize)
    case gif(item:String,size:CGSize)
    case textGif(item:String,text:String,size:CGSize)
    var description: String{
        switch self {
        case .image,.gif: return "image"
        case .textImage,.textGif: return "textImage"
        case .text: return "text"
        case .video:return "video"
        case .textVideo: return "textVideo"
        default: return "gradient"
            
        }
    }
    
    
}


enum DMGender:String {
    case male
    case female
    case `default`
    var genderType:String{
        switch self {
        case .male:
            return "male"
        case .female:
            return "female"
        default:
            return "other"
        }
    }
}

enum DMGenderSelected:String {
    case male
    case female
    case other
    var genderType:String{
        switch self {
        case .male:
            return "male".localized
        case .female:
            return "female".localized
        default:
            return "other".localized
        }
    }
}
enum FacebookPermissions:String {
    case email = "email"
    case publicProfile = "public_profile"
    case birthday = "user_birthday"
    
}

enum PickerOption:String{
    case dateofbirth 
    case gender
    case countrySelect
}

enum FeedType:String{
    case image     = "image"
    case text      =  "text"
    case textImage =  "textImage"
    case gradient  = "gradient"
}


//MARK:- CHAT SCREEN ENUM



enum MessageType :String,CustomStringConvertible{
  
    case photo = "image"
    case text = "text"
    var description: String{
        switch self {
        case .photo:
            return "image"
        case .text:
            return "text"
            
        }
    }
}
enum  DMServiceType : Int{
    case none = 0
    case pagging = 1

   
}
enum DumpScreenFuncType:Int  {
    case none = 0
    case history 
    case category
    case search
}

//MAR::- END CHAT ENUM

enum HTTPStatusCodes: Int {
    
    // 100 Informational
    
    case Continue = 100
    
    case SwitchingProtocols
    
    case Processing
    
    // 200 Success
    
    case OK = 200
    
    case Created
    
    case Accepted
    
    case NonAuthoritativeInformation
    
    case NoContent
    
    case ResetContent
    
    case PartialContent
    
    case MultiStatus
    
    case AlreadyReported
    
    case IMUsed = 226
    
    
    
    // 300 Redirection
    
    case MultipleChoices = 300
    
    case MovedPermanently
    
    case Found
    
    case SeeOther
    
    case NotModified
    
    case UseProxy
    
    case SwitchProxy
    
    case TemporaryRedirect
    
    case PermanentRedirect
    
    
    
    // 400 Client Error
    
    case BadRequest = 400
    
    case Unauthorized = 401
    
    case PaymentRequired = 402
    
    case Forbidden  = 403
    
    case NotFound = 404
    
    case MethodNotAllowed = 405
    
    case NotAcceptable
    
    case ProxyAuthenticationRequired
    
    case RequestTimeout
    
    case Conflict
    
    case Gone
    
    case LengthRequired
    
    case PreconditionFailed
    
    case PayloadTooLarge
    
    case URITooLong
    
    case UnsupportedMediaType
    
    case RangeNotSatisfiable
    
    case ExpectationFailed
    
    case ImATeapot
    
    case MisdirectedRequest = 421
    
    case UnprocessableEntity
    
    case Locked
    
    case FailedDependency
    
    case UpgradeRequired = 426
    
    case PreconditionRequired = 428
    
    case TooManyRequests
    
    case RequestHeaderFieldsTooLarge = 431
    
    case UnavailableForLegalReasons = 451
    
    // 500 Server Error
    
    case InternalServerError = 500
    
    case NotImplemented
    
    case BadGateway
    
    case ServiceUnavailable
    
    case GatewayTimeout
    
    case HTTPVersionNotSupported
    
    case VariantAlsoNegotiates
    
    case InsufficientStorage
    
    case LoopDetected
    
    case NotExtended = 510
    
    case NetworkAuthenticationRequired
    
    
    
    var description:String{
        
        switch self {
            
        case .BadRequest:
            
            return "BadRequest"
            
        case .Unauthorized:
            
            return "Unauthorized"
            
        case .PaymentRequired:
            
            return "Payment Required"
            
        case  .Forbidden:
            
            return "Forbidden Request"
            
        case .NotFound:
            
            return "Request NotFound"
            
        case .MethodNotAllowed:
            
            return "HTTP Method Not Allowed"
            
        case .NotAcceptable:
            
            return "Request Not Acceptable"
            
        case .ProxyAuthenticationRequired:
            
            return "Proxy Authentication Required"
            
        case .RequestTimeout:
            
            return "Request Timeout"
            
        case .Conflict:
            
            return "Conflict"
            
            
            
        default:
            
            return kAppTitle
            
        }
        
    }
    
}



public enum RectCornerRadiusType:Int {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case all
    case incoming
    case outgoing
    var rectCorner:UIRectCorner{
        switch self {
        case .topLeft:
            return [.topLeft]
        case .topRight:
            return [.topRight]
        case .bottomLeft:
            return [.bottomLeft]
        case .bottomRight:
            return [.bottomRight]
        case .incoming:
            return [.topRight,.bottomLeft,.bottomRight]
        case .outgoing:
            return [.topLeft,.bottomLeft,.bottomRight]
        default:
            return [.allCorners]
        }
    }
}
