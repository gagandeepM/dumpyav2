//
//  DMFont.swift
//  Dumpya
//
//  Created by Chander on 12/09/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import UIKit
enum Roboto:Int {
    case Regular  = 0
    case Italic
    case LightItalic
    case Thin
    case ThinItalic
    case Medium
    case MediumItalic
    case Bold
    case BoldItalic
    case Black
    case BlackItalic
    
    var fontname:String{
        switch self {
        case .Italic:
            return "roboto.italic"
        case .LightItalic:
            return "roboto.light-italic"
        case .Thin:
            return "roboto.thin"
        case .ThinItalic:
            return "roboto.thin-italic"
        case .Medium:
            return "roboto.medium"
        case .MediumItalic:
            return "roboto.medium-italic"
        case .Bold:
            return "roboto.bold"
        case .BoldItalic:
            return "roboto.bold-italic"
        case .Black:
            return "roboto.black"
        case .BlackItalic:
            return "roboto.black-italic"
        default:
            return "roboto"
        }
        
    }
    func font(size:CGFloat)->UIFont?{
        let name  = self.fontname
        return UIFont(name: name, size: size)
}
    
    func attributed(string:String,size:CGFloat = 14.0,textColor:UIColor = .black)->NSAttributedString{
        var font:UIFont =  UIFont.systemFont(ofSize: size)
        if let sfont = self.font(size: size) {
            font = sfont
        }else{
            if self == .Regular{
                font = UIFont.systemFont(ofSize: size)
            }else{
                font = UIFont.boldSystemFont(ofSize: size)
            }
        }
        let attributes:[NSAttributedStringKey : Any] = [.font:font,.foregroundColor:textColor]
        return NSAttributedString(string: string, attributes: attributes)
        
    }
}
