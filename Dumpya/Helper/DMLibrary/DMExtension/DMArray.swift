//
//  DMArray.swift
//  Dumpya
//
//  Created by Chandan Taneja on 18/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    @discardableResult
    public mutating func append(_ newArray: [Element]) -> CountableRange<Int> {
        let range = count..<(count + newArray.count)
        self += newArray
        return range
        
    }
    @discardableResult
    public mutating func insert(_ newArray: [Element], at index: Int) -> CountableRange<Int> {
        
        let mIndex = Swift.max(0, index)
        let start = Swift.min(count, mIndex)
        let end = start + newArray.count
        let left = self[0..<start]
        let right = self[start..<count]
        self = left + newArray + right
        return start..<end
        
    }

    public mutating func remove(_ element: Element) {
        
        var anotherSelf = self
        removeAll(keepingCapacity: true)
        anotherSelf.each { (index: Int, current: Element) in
            if current != element {
                self.append(current)
            }
        }
    }
    public mutating func each(_ exe: (Int, Element) -> ()) {
        for (index, item) in enumerated() {
            exe(index, item)
        }
 
    }
    
    @discardableResult
    public mutating func filter(_ predicate: NSPredicate) -> [Element] {
        return self.filter({predicate.evaluate(with: $0)})
    }

    @discardableResult
    public mutating func shuffled(fromIndex: Int, toIndex: Int) -> [Element]{
        var arr = self
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        return arr
        
    }
    var unique: [Element] {
        return self.reduce([]){ $0.contains($1) ? $0 : $0 + [$1] }
    }
    public func contains(_ elements: [Element]) -> Bool {
        guard !elements.isEmpty else {return false}
        var found = true
        for element in elements {
            if !contains(element) {
                found = false
            }
        }
        return found
    }

    /// All indexes of specified item.
    /// - Parameter item: item to check.
    
    /// - Returns: an array with all indexes of the given item.
    
    public func indexes(of item: Element) -> [Int] {
        var indexes: [Int] = []
        for index in 0..<self.count {
            if self[index] == item {
                indexes.append(index)
            }
        }
        return indexes
    }
    public func index(of item: Element) -> Int? {
        var index:Int? = nil
        for ind in 0..<self.count {
            if self[ind] == item {
                index = ind
            }
        }
        return index
        
    }

    /// Creates an array of elements split into groups the length of size.
    
    /// If array can’t be split evenly, the final chunk will be the remaining elements.
    /// - parameter array: to chunk
    
    /// - parameter size: size of each chunk
    
    /// - returns: array elements chunked
    
    public func chunk(size: Int = 1) -> [[Element]] {
        var result = [[Element]]()
        var chunk = -1
        for (index, elem) in self.enumerated() {
            if index % size == 0 {
                result.append([Element]())
                chunk += 1
                
            }
            result[chunk].append(elem)
        }
        
        return result
        
    }
    
    
    /// Element at the given index if it exists.

    /// - Parameter index: index of element.
    
    /// - Returns: optional element (if exists).
    
    public func item(at index: Int) -> Element? {
        
        guard index >= 0 && index < count else { return nil }
        
        return self[index]
        
    }
    
}
