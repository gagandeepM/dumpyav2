//
//  FileManager+Extra.swift
//  Dumpya
//
//  Created by Chandan Taneja on 22/01/19.
//  Copyright © 2019 Chander. All rights reserved.
//

import Foundation
extension FileManager {
   
    static var paths:[String]{
        
       
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        return paths
    }
    typealias FileManagerResult = ( destinationURL:URL? ,  isfileExists:Bool ,filemanager:FileManager)
    //MARK: - getfileFromDirectory-
    static func getlocalfile(filename:String)->FileManagerResult{
        
        let filemanager =  FileManager.default
        if let dirPath = paths.first {
            let fileurl =  URL(fileURLWithPath: dirPath).appendingPathComponent("\(filename)")
            if filemanager.fileExists(atPath: fileurl.path) {
                return(fileurl,true,filemanager)
                
            }else{
                return(nil,false,filemanager)
            }
        }else{
            return(nil,false,filemanager)
        }
        
    }
    //MARK: - deletefileFromDirectory-
    static func deletelocalfile( filename:String, OnCompletion:(_ success:Bool)->Void){
       let result = self.getlocalfile(filename: filename)
        let isfileExists = result.isfileExists
        let filemanager = result.filemanager
        let destinationURL = result.destinationURL
        if isfileExists, let url = destinationURL  {
            do{
                try filemanager.removeItem(at: url)
                OnCompletion(true)
            }catch {
                OnCompletion(false)
            }
        }else{
            OnCompletion(false)
        }
  
    }

}
