//
//  DMExtension.swift
//  Dumpya
//
//  Created by Chander on 14/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AudioToolbox
import CoreLocation
import Kingfisher

//MARK:- EXTENSION FOR NSAttributedString
extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.height
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.width
    }
}
extension Data {
    var intValue:     Int         { return withUnsafeBytes { $0.pointee }                   }
    var int32Value:   Int32       { return withUnsafeBytes { $0.pointee }                   }
    var int64Value:   Int64       { return withUnsafeBytes { $0.pointee }                   }
    var floatValue:   Float       { return withUnsafeBytes { $0.pointee }                   }
    var doubleValue:  Double      { return withUnsafeBytes { $0.pointee }                   }
    var stringValue:  String?     { return String(data: self, encoding: .utf8)              }
}
extension Int {
    var stringValue:  String     {  return NSNumber(value: self).stringValue  }
    var int8Value:    Int8       {  return NSNumber(value: self).int8Value    }
    var int16Value:   Int16      {  return NSNumber(value: self).int16Value   }
    var int32Value:   Int32      {  return NSNumber(value: self).int32Value   }
    var int64Value:   Int64      {  return NSNumber(value: self).int64Value   }
    var floatValue:   Float      {  return NSNumber(value: self).floatValue   }
    var doubleValue:  Double     {  return NSNumber(value: self).doubleValue  }
    var boolValue:    Bool       {  return NSNumber(value: self).boolValue    }
    var decimalValue: Decimal    {  return NSNumber(value: self).decimalValue }
    
}
extension Float {
    var stringValue:  String     {  return NSNumber(value: self).stringValue  }
    var intValue:     Int        {  return NSNumber(value: self).intValue     }
    var int8Value:    Int8       {  return NSNumber(value: self).int8Value    }
    var int16Value:   Int16      {  return NSNumber(value: self).int16Value   }
    var int32Value:   Int32      {  return NSNumber(value: self).int32Value   }
    var int64Value:   Int64      {  return NSNumber(value: self).int64Value   }
    var doubleValue:  Double     {  return NSNumber(value: self).doubleValue  }
    var boolValue:    Bool       {  return NSNumber(value: self).boolValue    }
    var decimalValue: Decimal    {  return NSNumber(value: self).decimalValue }
}
extension Double {
    var stringValue:  String     {  return NSNumber(value: self).stringValue  }
    var intValue:     Int        {  return NSNumber(value: self).intValue     }
    var int8Value:    Int8       {  return NSNumber(value: self).int8Value    }
    var int16Value:   Int16      {  return NSNumber(value: self).int16Value   }
    var int32Value:   Int32      {  return NSNumber(value: self).int32Value   }
    var int64Value:   Int64      {  return NSNumber(value: self).int64Value   }
    var floatValue:   Float      {  return NSNumber(value: self).floatValue   }
    var boolValue:    Bool       {  return NSNumber(value: self).boolValue    }
    var decimalValue: Decimal    {  return NSNumber(value: self).decimalValue }
}
extension Bool {
    var stringValue:  String     {  return NSNumber(value: self).stringValue  }
    var intValue:     Int        {  return NSNumber(value: self).intValue     }
    
}
//MARK:- EXTENSION FOR String

extension String {
   
    
    var intValue:     Int?        { return NumberFormatter().number(from: self)?.intValue    }
    var int8Value:    Int8?       { return NumberFormatter().number(from: self)?.int8Value   }
    var int16Value:   Int16?      { return NumberFormatter().number(from: self)?.int16Value  }
    var int32Value:   Int32?      { return NumberFormatter().number(from: self)?.int32Value  }
    var int64Value:   Int64?      { return NumberFormatter().number(from: self)?.int64Value  }
    var floatValue:   Float?      { return NumberFormatter().number(from: self)?.floatValue  }
    var doubleValue:  Double?     { return NumberFormatter().number(from: self)?.doubleValue }
    var boolValue:    Bool?       { return NumberFormatter().number(from: self)?.boolValue   }
    var decimalValue: Decimal?    { return NumberFormatter().number(from: self)?.decimalValue}
    var binaryValue:  Data?       { return self.data(using: .utf8)                           }
    
   
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.width
    }
    
   
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    var isAlphanumericWithWhiteSpace: Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^A-Z0-9a-z ].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
    }
    
    var isOnlyAlphanumeric: Bool {
        return !isEmpty && self.onlyNumbers && self.onlyAlphabet
    }
    
    var isAlphanumeric: Bool {
        
        let regex = try! NSRegularExpression(pattern: "[^a-zA-Z0-9]", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
        // return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    //MARK:-isValidPassword-
    var isValidPassword: Bool
    {
        if (self.isEmpty){return false}
        let passRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,20}"
        let passwordTest=NSPredicate(format: "SELF MATCHES %@", passRegEx);
        return passwordTest.evaluate(with: self)
    }
    var removeWhiteSpace:String{
        return self.trimmingCharacters(in: .whitespaces)
    }
    var trimWhiteSpace: String{
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    var isEmail: Bool {
        
        let regex = try? NSRegularExpression(pattern: "^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$", options: .caseInsensitive)
        
        return regex?.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    var checkSpecial: Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9 ].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
            
        }else{
            return true
        }
    }
    var checkAddress: Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9._@#/()-+*., ].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
            
        }else{
            return true
        }
    }

    var onlyNumbers: Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^0-9].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
    }
    var isPhoneNumber: Bool
    {
        let phone_regex = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phone_regex)
        return !self.isEmpty && phoneTest.evaluate(with: self)
    }
    var onlyNumbersExpressionPlus: Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^0-9+].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
    }
    var onlyAlphabet: Bool{
        
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
    }
    var isAlphabetWithSpace: Bool{
        
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
        if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
            return false
        }else{
            return true
        }
    }
    
    func safelyLimitedTo(length n: Int)->String {
        let c = String(self)
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
    
    // convert string date to Date
    func
        
        
        
        
        timeZoneDateFormatter(timeZone :TimeZoneType = .utc, localFormat:String = "MM-dd-yyyy EEE,HH:mm:ss" , serverFormat:String = "yyyy-MM-dd,HH:mm:ss") -> (date:Date?,dateTimeStr:String?,timeStr:String?){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = serverFormat
        dateFormatter.timeZone = timeZone.timeZone
        let date = dateFormatter.date(from: self)// create date from string
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = localFormat
        dateFormatter.timeZone =  TimeZone.current
        let dateStamp = dateFormatter.string(from: date!)
        dateFormatter.dateFormat = "h:mm a"
        let time = dateFormatter.string(from: date!)
        return (date,dateStamp,time)
        
    }
    
    
    var urlQueryAllowed:String?{
       return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    func attributedString(fontType:Roboto = .Regular,size:CGFloat = 14.0,textColor color:UIColor = .black)->NSAttributedString{
        return fontType.attributed(string: self,size:size,textColor: color)
    }
    
 
        //Convert HTML String to simple string
        var html2Attributed: NSAttributedString? {
            var html:NSAttributedString?
            do{
//                html =  try NSAttributedString.init(htmlString: self, font: Roboto.Regular.font(size: 16.0),useDocumentFontSize:false)
                var font  = Roboto.Regular.font(size: 14)
                html =  try NSAttributedString.init(htmlString: self, font:&font, useDocumentFontSize: false)
            }catch{
                
            }
            print("THE HTML STRING IS--------->",html ?? "")
            return html
            
        }
 }
//MARK:-EXTENTION FOR CGRECT USING FOR CUSTOME LOADER(TRANSITIONSUBMIT BUTTON)
extension CGRect {
    var x: CGFloat {
        get {
            return self.origin.x
        }
        set {
            self = CGRect(x: newValue, y: self.y, width: self.width, height: self.height)
        }
    }
    
    var y: CGFloat {
        get {
            return self.origin.y
        }
        set {
            self = CGRect(x: self.x, y: newValue, width: self.width, height: self.height)
        }
    }
    
    var width: CGFloat {
        get {
            return self.size.width
        }
        set {
            self = CGRect(x: self.x, y: self.y, width: newValue, height: self.height)
        }
    }
    
    var height: CGFloat {
        get {
            return self.size.height
        }
        set {
            self = CGRect(x: self.x, y: self.y, width: self.width, height: newValue)
        }
    }
    
    
    var top: CGFloat {
        get {
            return self.origin.y
        }
        set {
            y = newValue
        }
    }
    
    var bottom: CGFloat {
        get {
            return self.origin.y + self.size.height
        }
        set {
            self = CGRect(x: x, y: newValue - height, width: width, height: height)
        }
    }
    
    var left: CGFloat {
        get {
            return self.origin.x
        }
        set {
            self.x = newValue
        }
    }
    
    var right: CGFloat {
        get {
            return x + width
        }
        set {
            self = CGRect(x: newValue - width, y: y, width: width, height: height)
        }
    }
    
    
    var midX: CGFloat {
        get {
            return self.x + self.width / 2
        }
        set {
            self = CGRect(x: newValue - width / 2, y: y, width: width, height: height)
        }
    }
    
    var midY: CGFloat {
        get {
            return self.y + self.height / 2
        }
        set {
            self = CGRect(x: x, y: newValue - height / 2, width: width, height: height)
        }
    }
    
    
    var center: CGPoint {
        get {
            return CGPoint(x: self.midX, y: self.midY)
        }
        set {
            self = CGRect(x: newValue.x - width / 2, y: newValue.y - height / 2, width: width, height: height)
        }
    }
}

//MARK:- EXTENSION FOR TIMER
extension Timer {
    class func schedule(delay: TimeInterval, handler: ((Timer?) -> Void)!) -> Timer! {
        let fireDate = delay + CFAbsoluteTimeGetCurrent()
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, 0, 0, 0, handler)
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, .commonModes)
        return timer
    }
    class func schedule(repeatInterval interval: TimeInterval, handler: ((Timer?) -> Void)!) -> Timer! {
        let fireDate = interval + CFAbsoluteTimeGetCurrent()
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, interval, 0, 0, handler)
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, .commonModes)
        return timer
    }
}




//MARK: - Double Extension -
extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
//MARK: - DispatchQueue Extension -
extension DispatchQueue {
    
    static var userInteractive: DispatchQueue { return DispatchQueue.global(qos: .userInteractive) }
    static var userInitiated: DispatchQueue { return DispatchQueue.global(qos: .userInitiated) }
    static var utility: DispatchQueue { return DispatchQueue.global(qos: .utility) }
    static var background: DispatchQueue { return DispatchQueue.global(qos: .background) }
    
    func after(_ delay: TimeInterval, execute closure: @escaping () -> Void) {
        asyncAfter(deadline: .now() + delay, execute: closure)
    }
    func syncResult<T>(_ closure: () -> T) -> T {
        var result: T!
        sync { result = closure() }
        return result
    }
}

//MARK:- EXTENSION FOR TEXTFIELD
private var __maxLengths = [UITextField: Int]()
extension UITextField {
    
    
    
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
    
}
//MARK:- EXTENSION FOR UIIMAGE
extension UIImage {
    var png: Data                 { return UIImagePNGRepresentation(self)!       }
    var highestJPEG: Data        { return UIImageJPEGRepresentation(self, 1.0)!  }
    var highJPEG: Data           { return UIImageJPEGRepresentation(self, 0.75)! }
    var mediumJPEG: Data         { return UIImageJPEGRepresentation(self, 0.5)!  }
    var lowQualityJPEG: Data     { return UIImageJPEGRepresentation(self, 0.25)! }
    var lowestQualityJPEG:Data   { return UIImageJPEGRepresentation(self, 0.0)!  }
    
    func tint(with fillColor: UIColor) -> UIImage? {
        let image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        fillColor.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        
        guard let imageColored = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        
        UIGraphicsEndImageContext()
        return imageColored
    }
    
    
    func imageWithImage(scaledToSize newSize:CGSize) -> UIImage?
    {
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage? =  UIGraphicsGetImageFromCurrentImageContext()  //UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        let cgImage :CGImage = self.cgImage!
        let widthRatio  = newSize.width  / CGFloat(cgImage.width)
        let heightRatio = newSize.height / CGFloat(cgImage.height)
        let width:CGFloat =  newSize.width//CGFloat(cgImage.width / 2)
        var height:CGFloat = CGFloat(newSize.width)
        if widthRatio < heightRatio {
            height =  CGFloat(cgImage.height) * CGFloat(widthRatio)
            
        }
        
        let bitsPerComponent = cgImage.bitsPerComponent
        let bytesPerRow = cgImage.bytesPerRow
        let colorSpace = cgImage.colorSpace
        let bitmapInfo = cgImage.bitmapInfo
        
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: bitmapInfo.rawValue)
        context?.interpolationQuality = .high
        context?.draw(cgImage, in: CGRect(origin: .zero, size: CGSize(width:width, height: height)))
        
        
        let scaledImage =  UIImage(cgImage: (context?.makeImage()!)!)
        
        return scaledImage
        
    }
}

extension UserDefaults {
    class func DMDefault(setIntegerValue integer: Int , forKey key : String){
        UserDefaults.standard.set(integer, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func DMDefault(setObject object: Any , forKey key : String){
        UserDefaults.standard.set(object, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func DMDefault(setValue object: Any , forKey key : String){
        UserDefaults.standard.setValue(object, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func DMDefault(setBool boolObject:Bool  , forKey key : String){
        UserDefaults.standard.set(boolObject, forKey : key)
        UserDefaults.standard.synchronize()
    }
    class func DMDefault(integerForKey  key: String) -> Int{
        let integerValue : Int = UserDefaults.standard.integer(forKey: key) as Int
        UserDefaults.standard.synchronize()
        return integerValue
    }
    class func DMDefault(objectForKey key: String) -> Any {
        let object  = UserDefaults.standard.object(forKey: key)
        if (object != nil) {
            UserDefaults.standard.synchronize()
            return object!
        }else{
            UserDefaults.standard.synchronize()
            return ""
        }
        
    }
    class func DMDefault(valueForKey  key: String) -> Any {
        let value  = UserDefaults.standard.value(forKey: key)
        if (value != nil) {
            UserDefaults.standard.synchronize()
            return value!
        }else{
            return ""
        }
        
    }
    class func DMDefault(boolForKey  key : String) -> Bool {
        let booleanValue : Bool = UserDefaults.standard.bool(forKey: key) as Bool
        UserDefaults.standard.synchronize()
        return booleanValue
    }
    
    class func DMDefault(removeObjectForKey key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    //Save no-premitive data
    static func DMDefault( set codble:Mappable,forKey key:String){
        
        if let encoded = codble.JKEncoder() {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key)
            defaults.synchronize()
        }
    }
    static func DMDefault<T>(_ type:T.Type ,forKey key:String)->T? where T:Mappable{
        let defaults = UserDefaults.standard
        if let storedData = defaults.object(forKey: key) as? Data {
            let objectValue = storedData.JKDecoder(T.self)
            defaults.synchronize()
            return objectValue
        }
        return nil
    }
    
    class func DMDefault(setArchivedDataObject object: Any! , forKey key : String) {
        if (object != nil) {
            let data : NSData? = NSKeyedArchiver.archivedData(withRootObject: object) as NSData?
            UserDefaults.standard.set(data, forKey: key)
            UserDefaults.standard.synchronize()
        }
        
    }
    class func DMDefault(getUnArchiveObjectforKey key: String) -> Any {
        //var objectValue : Any?
        if  let storedData  = UserDefaults.standard.object(forKey: key) as? Data{
            
            let objectValue   =  NSKeyedUnarchiver.unarchiveObject(with: storedData)
            if (objectValue != nil)  {
                UserDefaults.standard.synchronize()
                return objectValue!
                
            }else{
                UserDefaults.standard.synchronize()
                return ""
                
            }
        }else{
            //objectValue = ""
            return ""
        }
    }
}
extension UIViewController{
    
//    @IBAction func onMenuClick(_ sender: Any) {
//        self.toggleLeft()
//    }

    func modalFromSheet(){
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .formSheet
        
    }
    
    func modalPresentation(){
        //self.providesPresentationContextTransitionStyle = true
        // self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    
    func zoomBounceAnimation(containtView popUp:UIView){
        popUp.transform = CGAffineTransform(scaleX: 0.001, y: 0.001) // CGAffineTransformIdentity.scaledBy(x: 0.001, y: 0.001);
        
        UIView.animate(withDuration: 0.3/1.5, animations: {
            popUp.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (finished) in
            UIView.animate(withDuration: 0.3/2, animations: {
                popUp.transform = CGAffineTransform(scaleX:  0.9, y:  0.9)
            }) { (finished) in
                UIView.animate(withDuration: 0.3/2, animations: {
                    popUp.transform = .identity
                })
                
            }
        }
        
        
    }
    func showLogoutAlert(title:String = kAppTitle,message:String = "logout".localized){
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: message, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.DeepOrange)
        let cancel = AlertActionModel(image: nil, title:"NO".localized, color: DMColor.ACRed, style: .cancel,alignment:.none)
        let destructive = AlertActionModel(image: nil, title:"YES".localized, color: DMColor.Red, style: .destructive,alignment:.none)
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: [cancel,destructive]) { (alert:UIAlertController, action:UIAlertAction, buttonIndex:Int) in
            
            switch buttonIndex {
            case 1:
                AppDelegate.sharedDelegate.logoutUser()
               // self.unRegisterInstanceToken()
                break
                
            default:
                break
            }
        }
    }
    
    fileprivate func unRegisterInstanceToken(){
        print(instanceToken)
        
        let params:[String:Any] =
            [
                "deviceToken" : instanceToken,
                "isLogout" : true
        ]
        ServerManager.shared.httpPut(request:kUpdateToken, params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
            
            guard let response = responseData.JKDecoder(DMLoginReponseModel.self) else{return}
            guard let status = response.status else{return}
            guard let message = response.message else{return}
            switch status{
                
            case 200:
                 UserDefaults.DMDefault(removeObjectForKey: instanceToken)
                break
                
            case 503:
                suspendMessage = message
                break
                
            default:
                alertMessage = message
                break
            }
        }, failureHandler: { (error) in
            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                
            }
        })
        
        
    }
    //MARK:- showAlert-
    func showAlertAction(title:String = kAppTitle,message:String?,cancelTitle:String = "Cancel",otherTitle:String = "OK",onCompletion:@escaping (_ didSelectIndex:Int)->Void){
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: message, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.Blue)
        
        let cancel = AlertActionModel(image: nil, title:cancelTitle, color: DMColor.deepRed, style: .cancel,alignment:.none)
        let other = AlertActionModel(image: nil, title:otherTitle, color: DMColor.blueColor, style: .default,alignment:.none)
        
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: [cancel,other]) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            onCompletion(index)
        }
        // alertController.zoomBounceAnimation(containtView: alertController.view)
    }
    
    //MARK:- showAlert-
    func showAlert(title:String = kAppTitle,message:String?,completion:((_ didSelectIndex:Int)->Swift.Void)? = nil){
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: message, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.Blue)
        let cancel = AlertActionModel(image: nil, title:"Ok", color: DMColor.deepRed, style: .cancel,alignment:.none)
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: cancel) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            if (completion != nil) {
                completion!(index)
            }
        }
     }
    
   
    
    func fetchCurrentLoction(){
        JKLocationManager.shared.setuplocationManager()
        JKLocationManager.shared.updateLocations(didUpdateLocarionCompletion: { (locations:[CLLocation], manager:CLLocationManager) in
             print("locations.last = \(String(describing: locations.last))")
            currentLocation = locations.last
        }) { (error:Error, manager:CLLocationManager) in
            
            
            
        }
        
    }

    func shareActionSheet(cancelTitle:String = "cancel".localized,otherAction:inout [AlertActionModel] ,onCompletion:@escaping (_ didSelectIndex:Int)->Void){
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: nil, message: nil, titleFont: nil, messageFont: nil, titleColor: nil, messageColor: nil, tintColor: DMColor.Blue)
        let cancel = AlertActionModel(image: nil, title:cancelTitle, color: .black, style: .cancel,alignment:.none)
       otherAction.append(cancel)
        _ = UIAlertController.showActionSheet(from: self, controlModel: alertModel, actions: otherAction) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            onCompletion(index)
        }
    }
}
extension UIView{
    
    func setGradeinetBackground(colorOne:UIColor,colorTwo:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [colorOne.cgColor,colorTwo.cgColor]
        gradientLayer.locations = [0.0,1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.addSublayer(gradientLayer)
    }
    
    
    func setGradeinetBackgroundTimeLine(colorOne:UIColor,colorTwo:UIColor) {
        
         clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        print(self.bounds)
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [colorOne.cgColor,colorTwo.cgColor]
        gradientLayer.locations = [0.0,1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
       
        print(gradientLayer.frame)
       // self.layer.insertSublayer(gradientLayer, at: 0)
        self.layer.addSublayer(gradientLayer)
    }
}


var loader:THIndicatorView = {
    let loader  = THIndicatorView();
    loader.lineColor = DMColor.deepRed;
    loader.lineWidth = 3
    loader.translatesAutoresizingMaskIntoConstraints = false
    return loader
}()
extension UIImageView{
    
    func isShowLoader(isShow:Bool = false){
        if isShow {
            if self.subviews.contains(loader){
                loader.removeFromSuperview()
            }
            self.addSubview(loader)
            loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            loader.widthAnchor.constraint(equalToConstant:25).isActive = true
            loader.heightAnchor.constraint(equalToConstant:25).isActive = true
            loader.startAnimation();
        }else{
            loader.stopAnimation()
            loader.removeFromSuperview()
            
        }
    }
    
    func cancelledTask(){
        self.kf.cancelDownloadTask()
    }
    
    
    
    func loadImage(filePath:String, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: Error?)->Swift.Void)? = nil){
        guard let url = URL(string: filePath) else {return}
        self.isShowLoader(isShow: true)
        //  let resource = ImageResource(downloadURL: url)
        self.kf.setImage(with: url, options: [.transition(ImageTransition.fade(1))], progressBlock: progressBlock, completionHandler: {(result) in
             self.isShowLoader(isShow: false)
            switch result{
            case .failure(let err):
                 onCompletion?(nil,err)
            case .success(let value):
                 onCompletion?(value.image,nil)
            }
            })
      
    }
    
    
//    func loadImage(filePath:String, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
//        guard let url = URL(string: filePath) else {return}
//        self.isShowLoader(isShow: true)
//      //  let resource = ImageResource(downloadURL: url)
//        let processor = DownsamplingImageProcessor(size: self.bounds.size)
//        self.kf.setImage(with: url,options: [
//            .processor(processor),
//            .scaleFactor(UIScreen.main.scale),
//            .transition(.fade(1)),
//            .cacheOriginalImage
//        ], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
//
//            self.isShowLoader(isShow: false)
//            if (onCompletion != nil) {
//                if (error != nil){
//
//                    onCompletion!(nil,error)
//                }else{
//
//                    onCompletion!(image,nil)
//                }
//            }
//
//        }
//    }

    
    func loadGifImage(filePath:String, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: Error?)->Swift.Void)? = nil ){
        guard let url = URL(string: filePath) else {return}
        self.isShowLoader(isShow: true)
        //  let resource = ImageResource(downloadURL: url)
        self.kf.setImage(with: url, options: [.transition(ImageTransition.fade(1)),.cacheSerializer(FormatIndicatedCacheSerializer.gif)], progressBlock: progressBlock, completionHandler: {(result) in
            self.isShowLoader(isShow: false)
            switch result{
            case .failure(let err):
                onCompletion!(nil,err)
            case .success(let value):
                onCompletion!(value.image,nil)
            }
        })
    }
    
    
//
//    func loadGifImage(filePath:String, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
//        guard let url = URL(string: filePath) else {return}
//        self.isShowLoader(isShow: true)
//        //  let resource = ImageResource(downloadURL: url)
//        let processor = DownsamplingImageProcessor(size: self.bounds.size)
//        self.kf.setImage(with: url,options: [
//            .processor(processor),
//            .scaleFactor(UIScreen.main.scale),
//            .transition(.fade(1)),
//            .cacheOriginalImage
//        ], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
//
//            self.isShowLoader(isShow: false)
//            if (onCompletion != nil) {
//                if (error != nil){
//
//                    onCompletion!(nil,error)
//                }else{
//
//                    onCompletion!(image,nil)
//                }
//            }
//
//
//        }
//    }
    func loadVideoImage(filePath:String,completionHanlder:((Result<Image, Error>) -> Void)? = nil){
        guard let url = URL(string: filePath) else {return}
        self.isShowLoader(isShow: true)
        VideoThumnail.shared.downloadImage(url: url) { (result) in
            DispatchQueue.main.async {
                self.isShowLoader(isShow: false)
                switch result{
                case .success(let image):
                    self.image = image
                    completionHanlder?(.success(image))
                case .failure(let err):
                    completionHanlder?(.failure(err))
                }
            }
        }
    }
}

extension UIButton{
    
    var nornamlImage:UIImage?{
        get{
            return self.image(for: .normal)
        }
        set{
            self.setImage(newValue, for: .normal)
        }
    }
    
    func isShowLoader(isShow:Bool = false){
        if isShow {
            if self.subviews.contains(loader){
                  loader.removeFromSuperview()
            }
            self.addSubview(loader)
            loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            loader.widthAnchor.constraint(equalToConstant: 25).isActive = true
            loader.heightAnchor.constraint(equalToConstant: 25).isActive = true
            loader.startAnimation();
        }else{
            loader.stopAnimation()
            loader.removeFromSuperview()
            
        }
    }
    
    
    func loadImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
        
        self.isShowLoader(isShow: true)
        self.kf.setImage(with: URL(string: filePath), for: state, placeholder:  #imageLiteral(resourceName: "profile_placeholder"), options: [.transition(ImageTransition.fade(1))], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
            self.isShowLoader(isShow: false)
            if (error != nil){
                if let handler = onCompletion{
                    handler(nil,error)
                }
                
            }else{
                
                if let handler = onCompletion{
                    handler(image,nil)
                }
                
            }
            
            
        }
    }
    
    
//    //MARK: - loadImage
//    func loadImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
//         let processor = DownsamplingImageProcessor(size: self.bounds.size)
//        self.isShowLoader(isShow: true)
//        self.kf.setImage(with: URL(string: filePath), for: state, placeholder:  #imageLiteral(resourceName: "profile_placeholder"), options: [
//            .processor(processor),
//            .scaleFactor(UIScreen.main.scale),
//            .transition(.fade(1)),
//            .cacheOriginalImage
//        ], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
//            self.isShowLoader(isShow: false)
//            if (error != nil){
//                if let handler = onCompletion{
//                     handler(nil,error)
//                }
//
//            }else{
//
//                if let handler = onCompletion{
//                    handler(image,nil)
//                }
//
//            }
//
//
//        }
//   }
    

    
    
    func loadDumpeeImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
        
        self.isShowLoader(isShow: true)
        self.kf.setImage(with: URL(string: filePath), for: state, placeholder:  #imageLiteral(resourceName: "icon_DumpeeDumy"), options: [.transition(ImageTransition.fade(1))], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
            self.isShowLoader(isShow: false)
            if (error != nil){
                if let handler = onCompletion{
                    handler(nil,error)
                }
                
            }else{
                
                if let handler = onCompletion{
                    handler(image,nil)
                }
                
            }
            
            
        }
    }
    
    
//    func loadDumpeeImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil ){
//        let processor = DownsamplingImageProcessor(size: self.bounds.size)
//        self.isShowLoader(isShow: true)
//        self.kf.setImage(with: URL(string: filePath), for: state, placeholder:  #imageLiteral(resourceName: "icon_DumpeeDumy"), options: [
//            .processor(processor),
//            .scaleFactor(UIScreen.main.scale),
//            .transition(.fade(1)),
//            .cacheOriginalImage
//        ], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
//            self.isShowLoader(isShow: false)
//            if (error != nil){
//                if let handler = onCompletion{
//                    handler(nil,error)
//                }
//
//            }else{
//
//                if let handler = onCompletion{
//                    handler(image,nil)
//                }
//
//            }
//
//
//        }
//    }
    
    func cancelledTask(){
        self.kf.cancelImageDownloadTask()
    }
    //MARK: - loadBackgroundImage
    func loadBackgroundImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: NSError?)->Swift.Void)? = nil){
        
       
        self.isShowLoader(isShow: true)
    let processor = DownsamplingImageProcessor(size: self.bounds.size)
        self.kf.setBackgroundImage(with:URL(string: filePath), for: state, placeholder:nil ,options: [
            .processor(processor),
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage
        ], progressBlock: progressBlock) { (image, error, cacheType, imageURL) in
            self.isShowLoader(isShow: false)
            
           
            if (onCompletion != nil) {
                if (error != nil){
                    
                    onCompletion!(nil,error)
                }else{
                    onCompletion!(image,nil)
                }
            }
            
        }
    }
    
    func loadChatImage(filePath:String,for state: UIControlState, progressBlock: DownloadProgressBlock? = nil,onCompletion: ((_ image: Image?, _ error: Error?)->Swift.Void)? = nil){
        self.isShowLoader(isShow: true)
        self.kf.setImage(with: URL(string: filePath), for: state, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: progressBlock) { (result) in
            self.isShowLoader(isShow: false)
            if let err = result.error{
                onCompletion?(nil,err)
            }else if let rs = result.value{
                onCompletion?(rs.image,nil)
            }
        }
        
    }
     func loadVideoImage(filePath:String,for state: UIControlState,completionHanlder:((Result<Image, Error>) -> Void)? = nil){
        guard let url = URL(string: filePath) else {return}
        self.isShowLoader(isShow: true)
        VideoThumnail.shared.downloadImage(url: url) { (result) in
            DispatchQueue.main.async {
                self.isShowLoader(isShow: false)
                switch result{
                case .success(let image):
                    self.setImage(image, for: state)
                    completionHanlder?(.success(image))
                case .failure(let err):
                    completionHanlder?(.failure(err))
                }
            }
        }
    }
    func loadVideoBackgroundImage(filePath:String,for state: UIControlState,completionHanlder:((Result<Image, Error>) -> Void)? = nil){
        guard let url = URL(string: filePath) else {return}
        self.isShowLoader(isShow: true)
        VideoThumnail.shared.downloadImage(url: url) { (result) in
            DispatchQueue.main.async {
                self.isShowLoader(isShow: false)
                switch result{
                case .success(let image):
                    self.setBackgroundImage(image, for: state)
                    completionHanlder?(.success(image))
                case .failure(let err):
                    completionHanlder?(.failure(err))
                }
            }
        }
    }
}

extension UITableView {
    
    func didUpdates(completion:@escaping()->()){
        self.beginUpdates()
        completion()
        self.endUpdates()
    }
    
    func register<T: UITableViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        
        self.register(T.self, forCellReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
        
    }
    
    
    
    func dequeue<T: UITableViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        
        guard
            
            let cell = dequeueCell(reuseIdentifier: String(describing: T.self),
                                   
                                   for: indexPath) as? T
            
            else { fatalError("Could not deque cell with type \(T.self)") }
        
        
        
        return cell
        
    }
    
    
    
    func dequeueCell(reuseIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
        
        return dequeueReusableCell(
            
            withIdentifier: identifier,
            
            for: indexPath
            
        )
        
    }
    
    
    
}


extension NSAttributedString {
    
    convenience init(htmlString html: String, font:inout UIFont?, useDocumentFontSize: Bool = true) throws {
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        let data = html.data(using: .utf8, allowLossyConversion: true)
        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            return
        }
        
        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        let range = NSRange(location: 0, length: attr.length)
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            
            if let htmlFont = attrib as? UIFont {
                var userFont  = font ?? Roboto.Regular.font(size: fontSize ?? htmlFont.pointSize)!
                 let fontDescriptor = htmlFont.fontDescriptor
               //  let traits = fontDescriptor.symbolicTraits
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
                
                if fontDescriptor.isBold{
                    descrip = descrip.withSymbolicTraits(.traitBold)!
                    userFont = Roboto.Medium.font(size: fontSize ?? htmlFont.pointSize) ?? UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
                }
                if fontDescriptor.isItalic {
                     descrip = descrip.withSymbolicTraits(.traitItalic)!
                      userFont = Roboto.Italic.font(size: fontSize ?? htmlFont.pointSize) ?? UIFont.italicSystemFont(ofSize: 14)
                }
                
             
                
                userFont  = useDocumentFontSize ?  UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize) : userFont
                
                attr.addAttribute(.font, value:userFont , range: range)
            }
        }
        
        self.init(attributedString: attr)
    }
    
}
extension UIFontDescriptor {
    var isBold: Bool {
        if (symbolicTraits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) == 0 {
            return false
        } else {
            return true
        }
    }
    var isItalic: Bool {
        if (symbolicTraits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) == 0 {
            return false
        } else {
            return true
        }
    }
}
