//
//  UIImageCropPickerKit.swift
//  B2BApp
//
//  Created by Jitendra Kumar on 29/03/18.
//  Copyright © 2018 Mobilyte. All rights reserved.
//

import UIKit

class UIImageCropPickerKit :NSObject{
    
    
    class var shared:UIImageCropPickerKit{
        
        struct Singlton{
            
            static let instance = UIImageCropPickerKit()
        }
        return Singlton.instance
    }
    
    typealias UIImageCropPickerFinalizationBlock = (_ cropedimage: UIImage?,_ Cancelled:Bool)->Void
    
    fileprivate var CropPickerHanlder:UIImageCropPickerFinalizationBlock!
    fileprivate var source:UIView!
    fileprivate var controller :UIViewController!
    func imageCropPicker(from ViewController :UIViewController,source:UIView,croppingStyle:TOCropViewCroppingStyle,image:UIImage,OnCompletion:@escaping UIImageCropPickerFinalizationBlock){
        
        self.controller = ViewController
        self.source = source
        self.CropPickerHanlder = OnCompletion
        let cropController = TOCropViewController(croppingStyle: croppingStyle, image:  image)
        cropController.delegate = self
        cropController.aspectRatioPreset = .presetSquare
        cropController.aspectRatioLockEnabled = true
        cropController.resetAspectRatioEnabled = false
        cropController.aspectRatioPickerButtonHidden = true
        cropController.toolbarPosition = .top
        
        ViewController.present(cropController, animated: true, completion: nil)
    }
}
extension UIImageCropPickerKit:TOCropViewControllerDelegate{
    
    
    
    //MARK:-TOCropViewController Delegate-
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        let cropedimage:UIImage = image
        // let viewFrame: CGRect = controller.view.convert(source.frame, to: source) //CGRectZero
        
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(cropedimage,false)
            }
            
        }) {
            
        }
        
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircleImage image: UIImage, rect cropRect: CGRect, angle: Int)
    {
        let cropedimage:UIImage = image
        //  let viewFrame: CGRect = controller.view.convert(source.frame, to: source) //CGRectZero
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(cropedimage,false)
            }
        }) {
            
        }
        
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(nil,true)
            }
            
        }) {
            
        }
        //        cropViewController .dismiss(animated: true, completion: { () -> Void in
        //
        //        })
    }
    
}

extension UIViewController{
    //MARK: - showActionSheet
    fileprivate func showActionSheet(isPhotoType:Bool = true,message:String,onCompletion:@escaping (Int)->Void){
        
        let titleFont  = Roboto.Bold.font(size:18) ?? UIFont.boldSystemFont(ofSize: 18)
        let messageFont = Roboto.Regular.font(size: 16) ?? UIFont.systemFont(ofSize: 16)
        let alertModel = AlertControllerModel(contentViewController: nil, title: kAppTitle, message: message, titleFont:titleFont , messageFont: messageFont, titleColor: .black, messageColor:.darkGray, tintColor:  DMColor.deepRed)
        
        let cancel = AlertActionModel(image:nil, title:"cancel".localized, color: DMColor.deepRed, style: .cancel)
        let album  =  isPhotoType == true ? AlertActionModel(image:#imageLiteral(resourceName: "photoAlbum"), title:"photoGallery".localized, color: DMColor.deepRed, style: .default) : AlertActionModel(image:#imageLiteral(resourceName: "video_icon"), title:"Video Gallery", color: DMColor.deepRed, style: .default)
        let camera = isPhotoType == true ? AlertActionModel(image:  #imageLiteral(resourceName: "camera"), title:"Photo Camera", color: DMColor.deepRed, style: .default) : AlertActionModel(image:  #imageLiteral(resourceName: "camera"), title:"Video Camera", color: DMColor.deepRed, style: .default)
      _ =  UIAlertController.showActionSheet(from: self, controlModel: alertModel, actions: [cancel,album,camera]) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            onCompletion(index)
        }
      
        
    }

    func showPicker(message:String,OnPickerHandler:@escaping(_ isPicked:Bool,_ image:UIImage? , _ filename:String?)->Void){
        
        self.showActionSheet(message: message) { (index) in
            
            if index == 2{
                
                self.presentImagePicker(isCamera: false, OnPickerHandler: OnPickerHandler)
            }else if index == 3{
                self.presentImagePicker(isCamera: true, OnPickerHandler: OnPickerHandler)
            }
        }
    }
    
    fileprivate func presentImagePicker(isCamera:Bool = false,OnPickerHandler:@escaping(_ isPicked:Bool,_ image:UIImage? , _ filename:String?)->Void){
        UIImagePickerKit.shared.showPhotoPickedController(from: self, showCamera: isCamera, showFrontCamera: false, allowsEditing: true, OnFinalizationBlock: { (picker, image,fileName) in
            DispatchQueue.main.async {
                picker?.dismiss(animated: true, completion: {
                    OnPickerHandler(true , image,fileName)
                })
            }
            
        }) { (picker) in
            picker?.dismiss(animated: true, completion: {
                OnPickerHandler(false , nil,nil)
            })
        }
    }
    
    func showVideoPicker(message:String,OnPickerHandler:@escaping(_ isPicked:Bool,_ videoUrl:URL?)->Void){
        
        self.showActionSheet(isPhotoType:false,message: message) { (index) in
            
            if index == 2{
                self.presentVidoePicker(isCamera: false, OnPickerHandler: OnPickerHandler)
            }else if index == 3{
                self.presentVidoePicker(isCamera: true, OnPickerHandler: OnPickerHandler)
            }
        }
    }
    
    fileprivate func presentVidoePicker(isCamera:Bool = false,OnPickerHandler:@escaping(_ isPicked:Bool,_ videoUrl:URL?)->Void){
        
        
         UIImagePickerKit.shared.showFilePickedController(from: self, showCamera: isCamera, showFrontCamera: false, allowsEditing: false, OnFinalizationBlock: { (picker, videoUrl) in
            DispatchQueue.main.async {
                picker?.dismiss(animated: true, completion: {
                    
                    OnPickerHandler(true , videoUrl)
                })
                
            }
            
        }) { (picker) in
            picker?.dismiss(animated: true, completion: {
                
                OnPickerHandler(false , nil)
            })
        }
    }
    
    fileprivate func showActionSheetForGallery(isPhotoType:Bool = true,message:String,onCompletion:@escaping (Int)->Void){
        
        let titleFont  = Roboto.Bold.font(size:18) ?? UIFont.boldSystemFont(ofSize: 18)
        let messageFont = Roboto.Regular.font(size: 16) ?? UIFont.systemFont(ofSize: 16)
        let alertModel = AlertControllerModel(contentViewController: nil, title: kAppTitle, message: message, titleFont:titleFont , messageFont: messageFont, titleColor: .black, messageColor:.darkGray, tintColor:  DMColor.deepRed)
        
        let cancel = AlertActionModel(image:nil, title:"cancel".localized, color: DMColor.deepRed, style: .cancel)
        if isPhotoType{
          let album =  AlertActionModel(image:#imageLiteral(resourceName: "photoAlbum"), title:"Photo Gallery", color: DMColor.deepRed, style: .default)

            _ =  UIAlertController.showActionSheet(from: self, controlModel: alertModel, actions: [cancel,album]) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
                onCompletion(index)
 } }
        
}
    func showDirectGallery(message:String,OnPickerHandler:@escaping(_ isPicked:Bool,_ image:UIImage? , _ filename:String?)->Void){
         self.presentImagePicker(isCamera: false, OnPickerHandler: OnPickerHandler)
    }
    
    func showDirectCamera(message:String,OnPickerHandler:@escaping(_ isPicked:Bool,_ image:UIImage? , _ filename:String?)->Void){
        self.presentImagePicker(isCamera: true, OnPickerHandler: OnPickerHandler)
    }
}
