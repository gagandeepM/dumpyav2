//
//  UIImagePickerKit.swift
//  UtilityDemoSwift3
//
//  Created by Jitendra Kumar on 23/12/16.
//  Copyright © 2016 Jitendra. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import CoreImage
enum UIImagePickerMedia:Int {
    case Photo = 0
    case Video
}
//MARK:-UIImagePickerController-
typealias UIImagePickerControllerCancellationBlock = (_ controller : UIImagePickerController?)->Void
typealias UIImagePickerControllerFinalizationBlock = (_ controller : UIImagePickerController?,_ info: [String : Any])->Void
typealias UIImagePickerControllerPopoverPresentationControllerBlock = (_ popover:UIPopoverPresentationController)->Void
typealias UIImagePickerPhotoBlock = (_ controller : UIImagePickerController?,_ image: UIImage?,_ fileName:String?)->Void
typealias UIImagePickerFileBlock = (_ controller : UIImagePickerController?,_ mediafile: URL?)->Void

class UIImagePickerKit: NSObject {
    
    
    var pickerCancellationBlock :  UIImagePickerControllerCancellationBlock?
    var pickerFinalizationBlock : UIImagePickerControllerFinalizationBlock?
    var pickerPopoverPresentationControllerBlock : UIImagePickerControllerPopoverPresentationControllerBlock?

    
    class var shared:UIImagePickerKit{
        
        struct Singlton{
            
            static let instance = UIImagePickerKit()
        }
        return Singlton.instance
    }
    
    fileprivate var sourceTypeAvailable:Bool{
      let sourceType:Bool = UIImagePickerController.isSourceTypeAvailable(.camera) == true ? true : false
        return sourceType
    }
   fileprivate var frontCameraDeviceAvailable: Bool{
        return  UIImagePickerController.isCameraDeviceAvailable(.front)
    }
    
    fileprivate func authorisationStatus(showCamera:Bool = false,onCompletion:@escaping (_ authorrized:Bool)->Void){
      
        if showCamera {
            let cameraMediaType = AVMediaType.video
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
            switch cameraAuthorizationStatus {
            case .denied:
                
                onCompletion(false)
            case .authorized:
                onCompletion(true)
            case .restricted:
                onCompletion(false)
                
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                    if granted {
                        onCompletion(true)
                        print("Granted access to \(cameraMediaType)")
                    } else {
                        onCompletion(false)
                        print("Denied access to \(cameraMediaType)")
                    }
                }
            }
        }else{
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                print("permission authorized")
                onCompletion(true)
            case .denied:
                print("permission denied")
                onCompletion(false)
            case .notDetermined:
                print("Permission Not Determined")
                
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == PHAuthorizationStatus.authorized{
                        // photo library access given
                        onCompletion(true)
                    }else{
                        print("restriced manually")
                        onCompletion(false)
                    }
                })
                
            case .restricted:
                print("permission restricted")
                onCompletion(false)
                
            }
        }
        
      
        
    }
    
    
   fileprivate func set(picker:UIImagePickerController,mediaType:UIImagePickerMedia = .Photo ,showCamera:Bool ,showFrontCamera:Bool){
       
    
            if  showCamera == true , self.sourceTypeAvailable == true {
                
                if showFrontCamera == true , self.frontCameraDeviceAvailable == true {
                    picker.cameraDevice = .front
                }
                if mediaType == .Video{
                    picker.sourceType = .camera
                    picker.mediaTypes = [kUTTypeMovie] as [String]
                    picker.videoMaximumDuration = 45
                }else{
                    picker.sourceType = .camera
                }
            }else {
                if mediaType == .Video{
                    picker.sourceType = .photoLibrary
                    picker.mediaTypes = [kUTTypeMovie as String]
                }else{
                    picker.sourceType = .photoLibrary
                    
                }
            }
        
        
    }
    func showImagePickerController(from viewController:UIViewController!,mediaType:UIImagePickerMedia = .Photo,showCamera:Bool ,showFrontCamera:Bool = false,allowsEditing:Bool = false, source:UIView?,OnFinalizationBlock finishBlock:UIImagePickerControllerFinalizationBlock?,OnCancellationBlock CancelledBlock:UIImagePickerControllerCancellationBlock?){
    
        self.pickerCancellationBlock = CancelledBlock
        self.pickerFinalizationBlock = finishBlock
        authorisationStatus(showCamera: showCamera, onCompletion: { (isAuthorized) in
          
            if !isAuthorized{
                 let message =  showCamera == false ? "App does not have access to your \(mediaType == .Video ? "videos" : "photos"). To enable access, tap settings and turn on  \(mediaType == .Video ? "Video" : "Photo") Library Access." :"App does not have access to your camera. To enable access, tap settings and turn on Camera."
                viewController.showAlertAction( message: message, cancelTitle: "OK", otherTitle: "Setting", onCompletion: { (index) in
                    if index == 2{
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                                UIApplication.shared.openURL(settingsUrl)
                            }
                        }
                    }
                })
            }else{
               
                let picker : UIImagePickerController = UIImagePickerController()
                picker.allowsEditing = allowsEditing
                picker.delegate = self
                self.set(picker: picker, mediaType: mediaType, showCamera: showCamera, showFrontCamera: showFrontCamera)
                
                DispatchQueue.main.async {
                    if UIDevice.current.userInterfaceIdiom  == .pad {
                        if  let source = source {
                            picker.modalPresentationStyle = .popover
                            if let barButtonItem = source as? UIBarButtonItem {
                                
                                if let popoverController = picker.popoverPresentationController {
                                    popoverController.barButtonItem = barButtonItem
                                    popoverController.sourceRect = source.bounds
                                    let size  = viewController.view.bounds.size
                                    picker.preferredContentSize = CGSize(width: size.width - 150, height: size.height - 190)
                                }
                                
                            }else{
                                if let popoverController = picker.popoverPresentationController {
                                    
                                    popoverController.sourceView = source
                                    popoverController.sourceRect = source.bounds
                                    let size  = viewController.view.bounds.size
                                    picker.preferredContentSize = CGSize(width: size.width - 150, height: size.height - 190)
                                    
                                }
                                
                            }
                        }else{
                            picker.modalFromSheet()
                        }
                        
                    }
                    
                    
                    viewController.present(picker, animated: true, completion: nil)
                    
                    
                }
                
            }
        })
        
       

        
        
    }
    

    
    
    func showPhotoPickedController(from viewController:UIViewController!,showCamera:Bool,showFrontCamera:Bool,allowsEditing:Bool,source:UIView? = nil,OnFinalizationBlock finishBlock:UIImagePickerPhotoBlock?,OnCancellationBlock CancelledBlock:UIImagePickerControllerCancellationBlock?){
        
         self.showImagePickerController(from: viewController, mediaType:.Photo, showCamera: showCamera, showFrontCamera: showFrontCamera, allowsEditing: allowsEditing, source: source, OnFinalizationBlock: { (picker:UIImagePickerController?, info:[String : Any]?) in
            guard let info = info else{ return }
            let image : UIImage = ((allowsEditing == true) ? (info[UIImagePickerControllerEditedImage] as? UIImage) :(info[UIImagePickerControllerOriginalImage ] as? UIImage))!
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
         
                guard let asset = result.firstObject else{ return }
                guard let name = asset.value(forKey: "filename") else{return}
              finishBlock!(picker,image,"\(name)")
            }else{
                 finishBlock!(picker,image,"")
            }
           
        }, OnCancellationBlock: CancelledBlock)
        
    }
    
    
    func showFilePickedController(from viewController:UIViewController!,showCamera:Bool,showFrontCamera:Bool,allowsEditing:Bool,source:UIView? = nil,OnFinalizationBlock finishBlock:UIImagePickerFileBlock?,OnCancellationBlock CancelledBlock:UIImagePickerControllerCancellationBlock?){
         self.showImagePickerController(from: viewController, mediaType:.Video, showCamera: showCamera, showFrontCamera: showFrontCamera, allowsEditing: allowsEditing, source:  source, OnFinalizationBlock: { (picker:UIImagePickerController?, info:[String : Any]?) in
            
            let type = info![UIImagePickerControllerMediaType] as? String
            if type == kUTTypeVideo as String || type! == kUTTypeMovie as String{
                let videoURL = info![UIImagePickerControllerMediaURL]
                finishBlock!(picker,videoURL as! URL?)
            }
            
        }, OnCancellationBlock: CancelledBlock)
    }
    
    
    //MARK:-didFaceDetect-
    class func didFaceDetect(userImage:UIImage ,currentController:UIViewController,faceDetectComplition:(( _ image:UIImage ,  _ isdetected:Bool)->Void)?)
    {
        var exifOrientation : Int
        switch userImage.imageOrientation {
        case .up:
            exifOrientation = 1
            break
        case .down:
            exifOrientation = 3
            break
        case.left:
            exifOrientation = 8
            break
        case .right:
            exifOrientation = 6
            break
        case .upMirrored:
            exifOrientation = 2
            break
        case.downMirrored:
            exifOrientation = 4
            break
        case .leftMirrored:
            exifOrientation = 5
            break
        case .rightMirrored:
            exifOrientation = 7
            break
            
        }
        let imageOptions =  NSDictionary(object: NSNumber(value: exifOrientation) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage(cgImage: userImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector!.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        if(faces.count == 0)
        {
            currentController.showAlert(message: "No face detected.")
        }
        else if(faces.count > 1)
        {
            currentController.showAlert( message: "More then one face found.Please try again.")
        }
        else
        {
            
            if let face = faces.first as? CIFaceFeature
            {
                print("found bounds are \(face.bounds)")
                
                if face.hasSmile {
                    print("face is smiling");
                }
                if face.hasLeftEyePosition {
                    print("Left eye bounds are \(face.leftEyePosition)")
                }
                
                if face.hasRightEyePosition {
                    print("Right eye bounds are \(face.rightEyePosition)")
                }
                faceDetectComplition!(userImage,true)
            }
        }
        
    }
    
   
}

extension UIImagePickerKit:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        if (pickerCancellationBlock != nil)
        {
            pickerCancellationBlock!(picker)
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (pickerFinalizationBlock != nil)
        {
            pickerFinalizationBlock!(picker,info)
        }
    }
    
    
}


