//
//  JKMediaBrowser.swift
//  FoodApp
//
//  Created by Jitendra Kumar on 26/12/18.
//  Copyright © 2018 Jitendra Kumar. All rights reserved.
//

import UIKit
enum JKBrowseType {
    case photo
    case audio
    case video
    case docment
}
class JKMediaBrowser: NSObject {
    
    fileprivate var orginalFrame:CGRect = .zero
    fileprivate var orginalView:UIView?
    fileprivate var blackBackgroundView:UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    fileprivate var navigationBarView:UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate lazy var previewBtn:DMButton = {
        let button = DMButton(type: .custom)
        button.backgroundColor = .clear
        return  button
    }()
    fileprivate lazy var crossBtn:DMButton = {
        let button = DMButton(type: .custom)
        button.setTitle("×", for: .normal) 
        button.titleLabel?.font = Roboto.Bold.font(size: 30)
        button.backgroundColor = .clear
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(zoomOut), for: .touchUpInside)
        return  button
    }()
    fileprivate lazy var loader:DMProgressHUD = {
        let loader = DMProgressHUD()
        loader.hudMode = .AnnularDeterminate
        
        return loader
    }()
    
    fileprivate lazy var scrollView:UIScrollView = {
        let scroll = UIScrollView()
        scroll.alwaysBounceVertical = false
        scroll.alwaysBounceHorizontal = false
        scroll.showsVerticalScrollIndicator = true
        scroll.isScrollEnabled = false
        
        return scroll
    }()
    override init() {
        super.init()
    }
    
    class var shared:JKMediaBrowser{
        struct Singlton{
            static let instance = JKMediaBrowser()
        }
        return Singlton.instance
    }
    
    //MARK:- Private Function
    fileprivate func setloader(in view:UIView){
        loader.frame = CGRect(origin: view.center, size: CGSize(width: 40, height: 40))
        view.addSubview(loader)
    }
    fileprivate func setFrame()->(isAdded:Bool,keyFrame:CGRect){
        self.scrollView.removeFromSuperview()
        self.blackBackgroundView.removeFromSuperview()
        guard let window = UIApplication.shared.keyWindow, let startingView = orginalView, let startingFrame = startingView.superview?.convert(startingView.frame, to: window) else{return (false,.zero)}
        
        orginalFrame = startingFrame
        let keyFrame = window.frame
        blackBackgroundView.frame = keyFrame
        blackBackgroundView.alpha = 0
        window.addSubview(blackBackgroundView)
        
        scrollView.frame =  keyFrame
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        blackBackgroundView.addSubview(scrollView)
        blackBackgroundView.addSubview(navigationBarView)
        
        navigationBarView.leftAnchor.constraint(equalTo: blackBackgroundView.leftAnchor).isActive = true
        navigationBarView.rightAnchor.constraint(equalTo: blackBackgroundView.rightAnchor).isActive = true
        navigationBarView.topAnchor.constraint(equalTo: blackBackgroundView.topAnchor).isActive = true
        navigationBarView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        navigationBarView.addSubview(crossBtn)
        crossBtn.leftAnchor.constraint(equalTo: navigationBarView.leftAnchor, constant: 8).isActive = true
        crossBtn.bottomAnchor.constraint(equalTo: navigationBarView.bottomAnchor, constant: -4).isActive = true
        crossBtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        crossBtn.heightAnchor.constraint(equalToConstant: 34).isActive = true
        previewBtn.frame = orginalFrame
        
        previewBtn.clipsToBound = true
        scrollView.addSubview(previewBtn)
        
        scrollView.delegate = self
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleSingleTap(_:)))
        singleTap.require(toFail: singleTap)
        previewBtn.addGestureRecognizer(singleTap)
        return (true,keyFrame)
    }
    fileprivate func hideNavigationBar(){
        let  isHide = self.navigationBarView.alpha == 0 ? true :false
        UIView.animate(withDuration: 0.35, animations: {
            self.navigationBarView.alpha = !isHide ? 0 :1
            self.blackBackgroundView.setNeedsLayout()
        })
        
        
    }
    
    @objc fileprivate func zoomOut(){
        let startingFrame   = self.orginalFrame
        UIView.animate(withDuration: 0.5, animations: {
            self.previewBtn.frame = startingFrame
            self.blackBackgroundView.alpha = 0
        },completion:{(didComplete) in
             self.previewBtn.removeFromSuperview()
            self.scrollView.removeFromSuperview()
            self.blackBackgroundView.removeFromSuperview()
            self.orginalView?.alpha = 1.0
        })
        
    }
    @objc fileprivate func handleSingleTap(_ recognizer: UITapGestureRecognizer) {
        self.hideNavigationBar()
    }
    
    //MARK:- Public Function
    func browseMedia(object:Any,orginalView sender:UIView, type:JKBrowseType = .photo){
        if let filePath = object as? String {
            if type == .photo{
                self.photoBrowse(filePath: filePath, orginalView: sender)
            }
        }else if let image = object as? UIImage {
            if type == .photo{
                self.photoBrowse(image: image, orginalView: sender)
            }
        }
    }
    func photoBrowse(image :UIImage ,orginalView sender:UIView){
        orginalView = sender
        let isAdded = self.setFrame().isAdded
        if isAdded {
            let keyFrame = self.setFrame().keyFrame
            previewBtn.setImage(image, for: .normal)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                let height = (keyFrame.width/self.orginalFrame.width)*self.orginalFrame.height
                let y = keyFrame.height/2 - height/2
                self.previewBtn.frame = CGRect(x: 0, y: y, width: keyFrame.width, height: height)
                self.blackBackgroundView.alpha = 1
                self.orginalView?.alpha = 0
                self.hideNavigationBar()
            }, completion: nil)
        }

    }
    
    func photoBrowse(filePath :String , orginalView sender:UIView){
        if !ServerManager.shared.CheckNetwork() {
            return
        }
        orginalView = sender
        let isAdded = self.setFrame().isAdded
        if isAdded {
            let keyFrame = self.setFrame().keyFrame
            self.setloader(in: previewBtn)
            previewBtn.loadImage(filePath: filePath, for: .normal, progressBlock: { (receivedSize, totalSize) in
                let progress = Float(totalSize/receivedSize)
                DispatchQueue.main.async {
                    self.loader.progress = progress
                }
            }, onCompletion: { (image , error) in
                self.loader.removeFromSuperview()
            })
            
            UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                let height = (keyFrame.width/self.orginalFrame.width)*self.orginalFrame.height
                let y = keyFrame.height/2 - height/2
                self.previewBtn.frame = CGRect(x: 0, y: y, width: keyFrame.width, height: height)
                self.blackBackgroundView.alpha = 1
                self.hideNavigationBar()
            }, completion: nil)
        }
        
    }
    
    
    
}
extension JKMediaBrowser:UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return previewBtn
    }
    
}
